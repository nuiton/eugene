.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

============
Introduction
============

:Authors: Julien Ruchaud, Florian Desbois, Jean Couteau
:Contact: eugene-devel@list.nuiton.org or eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$

.. contents::

What is MDA (Model Driven Architecture)
---------------------------------------

EUGene is based on MDA technology. MDA's main principle is the software
development based on models. It consists in abstracting technical choices by
modeling the needs, and then transform this model according to the target
platform. One of many examples is java code generation from an UML model.

To make models (M1), it is necessary to define a modeling language which is
called meta-model (M2). To make transformations between languages, it is
necessary to have in common a meta-language, which is called meta-meta-model
(M3). To do not have another language in top of the meta-meta-model, this one
needs to be self-descriptive, i.e. it is possible to write a meta-model
describing the meta-meta-model. The last level is the model application (M0) in
the real world.

Here are some examples :

M3   MOF              Grammar            UML
M2   UML              Language           Flying specific language
M1   Java             Sentence           Airport definition
M0   Objet            Speech             The airport


It exists  two strategies in manipulating models : either defining
transformation rules from a meta-model to another, either by a template
representing the target model and manipulating the source model.


What is EUGene (Efficient Universal Generator) ?
------------------------------------------------

EUGene allows manipulating and generating models. It features :

  * reading UML class model in XMI independent from modeling.
  * generation templates
  * models transformation
  * integration in project build
  * independence in developer code and generated code
  * independence from development tools

Numerous solutions are available on the market, EUGene position itself, compared
to its competitors, as easy to use and to put into place, independent from
development tools and full-JAVA with no other language to learn.

Comparison
----------

Comparison to other solutions existing on the market :

+---------+----------------------+----------------------------+--------------+---------+--------+
|         |                      |                            |              |         | Output |
|         | Generator            | Independence               | Build        | Plugin  | format |
+=========+======================+============================+==============+=========+========+
| EUGene  | java template        | Development tools          | Maven        | Eclipse | All    |
+---------+----------------------+----------------------------+--------------+---------+--------+
| Acceleo | specific template    | Linked to Eclipse platform | Not possible | Eclipse | All    |
+---------+----------------------+----------------------------+--------------+---------+--------+
|         | Model transformation |                            |              |         |        |
| ATL     | in QVT               | Linked to Eclipse platform | Not possible | Eclipse | Model  |
+---------+----------------------+----------------------------+--------------+---------+--------+
