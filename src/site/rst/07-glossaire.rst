.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

========
Glossary
========

MDA
---

Model Driven Architecture : programming technique based on models.

Refs : `MDA`_ , `Introduction EUGene`_

.. _MDA: http://fr.wikipedia.org/wiki/Model_driven_architecture
.. _Introduction EUGene: 01-introduction.html

UML
---

Unified Modeling Language : modeling language.

Refs : `UML official website`_ , `ObjectModel EUGene`_

.. _UML official website: http://www.uml.org/
.. _ObjectModel EUGene: 02-objectmodel.html

XMI
---

XML Metadata Interchange : XML norm for UML language.

Refs : `XMI official website`_ , `ObjectModel EUGene`_

.. _XMI official website: http://www.omg.org/spec/XMI/

Template
--------

Class that allows file generation from a data model.

Ref : `EUgene templates`_ , `Nuiton-processor library`_

.. _EUGene templates: 04-templates.html
.. _Nuiton-processor library: http://doc.nuiton.org/processor

Transformer
-----------

Model transformer : conversion from a model A to a model B.

Ref : `EUGene templates`_

Generator
---------

Output files generator from an input data model.

Ref : `EUGene templates`_

ObjectModel
-----------

EUGene object meta-model with UML specificities (multiplicities, inheritance, ...)

Refs : `ObjectModel EUGene`_

Builder
-------

Model builder, allows to create and fill a model directly in Java.

Refs : `EUGene advanced functionalities`_

.. _EUGene advanced functionalities: 06-fonctionnalites-avancees.html

Extension
---------

Model extensions, allows to add specific functionalities to a model. Mainly used
in ObjectModel to add Java language specificities to it.

Refs : `EUGene advanced functionalities`_

Nuiton-processor
----------------

Library allowing the template syntax interpretation.

Ref : `Nuiton-processor library`_
