.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======
EUGene
======

Efficient Universal GENErator

You will find in this documentation :

- `Introduction`_ : introduction to EUGene's principles
- `ObjectModel`_ : EUGene main meta-model used for generation
- `Generation process`_ : Generation steps
- `Templates`_ : Templates writing (`Transformer`_ et `Generator`_ )
- `Execution`_ : Generation's execution (via Maven ou Ant)
- `Advanced functionalities`_ : How to extend EUGene's functionalities
- `Glossary`_ : Glossary of the terms used in EUGene
- `FAQ`_ : Frequently Asked Question

.. _Introduction: 01-introduction.html
.. _ObjectModel: 02-objectmodel.html
.. _Generation process: 03-generation.html
.. _Templates: 04-templates.html
.. _Execution: 05-execution.html
.. _Advanced functionalities: 06-fonctionnalites-avancees.html
.. _Glossary: 07-glossaire.html
.. _Transformer: 07-glossaire.html
.. _Generator: 07-glossaire.html
.. _FAQ: 08-FAQ.html

EUGene is divided into 5 modules :

  * `eugene-api`_ (EUGene Api)
  * `eugene-java-templates`_ (Templates to generate java stuff)
  * `eugene-plantuml-templates`_ (since version 2.7) (Templates to generate plantuml from an object model)
  * `eugene-yaml-templates`_ (since version 2.7) (Templates to model object model from yaml files)
  * `eugene-maven-plugin`_ (Maven Plugin to generate stuff using EUGene)

.. _eugene-api: eugene/index.html
.. _eugene-java-templates: ../eugene-java-templates/fr/index.html
.. _eugene-maven-plugin: eugene-maven-plugin/index.html
.. _eugene-plantuml-templates: ../eugene-plantuml-templates/index.html
.. _eugene-yaml-templates: ../eugene-yaml-templates/index.html

More info on the project on `nuiton's forge`_ .

.. _nuiton's forge: https://forge.nuiton.org/projects/eugene

