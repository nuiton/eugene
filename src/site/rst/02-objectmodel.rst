.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===========
ObjectModel
===========

:Authors: Arnaud Thimel, Florian Desbois, Jean Couteau
:Contact: eugene-devel@list.nuiton.org or eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$

Note
  This document does not take into account modifications made on EUGene 2.0
  version

.. contents::

TODO : revoir cette documentation

Introduction
============

ObjectModelGenerator generator is aimed to read and analyse object models and
generate code from them. In UML, an object model is represented by a class
diagram. This object models vision being wildly spread, it is
ObjectModelGenerator's basis (please note that this is not mandatory).

Let start from the principle that we got a model (class diagram) created from
an XMI (XML Metadata Interchange format modeling tool.

Code generation is made into three steps :

- XMI purification into an XML code that keeps only useful information ;
- Simplified model put into memory ;
- Templates used / code generation.

XMI model purification
======================

Most of the modelign tools describe their models using XMI, but XMI is too
verbose to be easily understood.

Eugene uses XSLT transformation allowing to get a purified XML describing the
model and keeping the main information.

This intermediate model insure EUGene's stability and durability as it is always
based on its own models and not external models (XMI) that changes too much from
a version to another.

If we want to support a new XMI version, for example, we just need to create the
appropriate transformation stylesheet.

Amongst the extracted information, we can name :

- Objects (classes, abstract classes, interfaces)
- Attributes (names, type, visibility, ...)
- Relations between classes (all multiplicities, navigability, association classes, ...)
- Operations (name, return type, arguments name and type, thrown exceptions, ...)
- Stereotypes

From the following diagram :

.. image:: objectmodel/Hotel.png

We got the following ObjectModel :

.. image:: objectmodel/Hotel.objectmodel.png


Memory model
============

Once XMI has been changed into an understandable XML, the model is loaded into
memory so that it is generated.

The instanciated model is based on the following class diagram (meta-model) :

.. image:: objectmodel/ObjectModel.png

Available interfaces for the generators are the following :

.. image:: objectmodel/ObjectModel_Interfaces.png

ObjectModelGenerator :

.. image:: objectmodel/ObjectModel_Generator.png

Consolidate model with a properties file
========================================

You can use another properties file with same name as your model to consolidate
your model by adding sometagvalues and stereotypes, their will be merge into the memory model.

For example, with a model file *model.zargo*, use then a file named
*model.properties*.

This file is useful if you don't like to put all your tagValues and
stereotypes into a zargo model when synthetic view of them is not possible.

Howto add tagValues
-------------------

They can be added at different levels:

- On a complete model

::

  model.tagValue.version=1.0

- on a classifier

::

  org.nuiton.eugene.tutorial.Hotel.tagValue.version=1.0

- on a classifier's attribute

::

  org.nuiton.eugene.tutorial.Hotel.attribute.room.tagValue.version=1.0

- on a classifier's operation

::

  org.nuiton.eugene.tutorial.Hotel.operation.enter.tagValue.version=1.0


Howto add stereotypes
---------------------

Stereotypes can not be placed on a model, but still

- on a classifier

::

  org.nuiton.eugene.tutorial.Hotel.stereotype=unique

- on a classifier's attribute

::

  org.nuiton.eugene.tutorial.Hotel.attribute.room.stereotype=unique

- on a classifier's operation

::

  org.nuiton.eugene.tutorial.Hotel.operation.enter.stereotype=unique

*TODO See how to add more than one stereotype oat a level*

Howto discover which tagValues and stereotypes to use
-----------------------------------------------------

Usage of *ModelPropertiesProvider* permits to define, list and validate usage
of tagValues and stereotypes.

For more informations about this concept see `ModelPropertiesProvider`_

You only need to invoke the *available-data* goal of the maven plugin

Example :

::

  $ mvn eugene:available-data

  [INFO] --- eugene-maven-plugin:xxx:available-data (default-cli) @ tutti-persistence ---
  [INFO] Get datas for data types : [modeltype, writer, modelreader, modeltemplate, tagvalue, stereotype]

  Found 2 modeltypes :
   [objectmodel] with implementation 'org.nuiton.eugene.models.object.xml.ObjectModelImpl'
   [statemodel] with implementation 'org.nuiton.eugene.models.state.xml.StateModelImpl'

  Found 3 writers :
   [xmi] with implementation 'org.nuiton.eugene.plugin.writer.XmiChainedFileWriter
    inputProtocol             : xmi
    outputProtocol            : model
    defaultIncludes           : **/*.xmi
    defaultInputDirectory     : src/main/xmi
    defaultTestInputDirectory : src/test/xmi'
   [model2Java] with implementation 'org.nuiton.eugene.plugin.writer.ModelChainedFileWriter
    inputProtocol             : model
    outputProtocol            : null
    defaultIncludes           : **/*.*model
    defaultInputDirectory     : src/main/models
    defaultTestInputDirectory : src/test/models'
   [zargo2xmi] with implementation 'org.nuiton.eugene.plugin.writer.ZargoChainedFileWriter
    inputProtocol             : zargo
    outputProtocol            : xmi
    defaultIncludes           : **/*.zargo
    defaultInputDirectory     : src/main/xmi
    defaultTestInputDirectory : src/test/xmi'

  Found 2 modelreaders :
   [objectmodel] with implementation 'org.nuiton.eugene.models.object.ObjectModelReader'
   [statemodel] with implementation 'org.nuiton.eugene.models.state.StateModelReader'

  Found 5 modeltemplates :
   [org.nuiton.eugene.java.JavaInterfaceTransformer] with implementation 'org.nuiton.eugene.java.JavaInterfaceTransformer'
   [org.nuiton.eugene.java.JavaGenerator] with implementation 'org.nuiton.eugene.java.JavaGenerator'
   [org.nuiton.eugene.java.JavaEnumerationTransformer] with implementation 'org.nuiton.eugene.java.JavaEnumerationTransformer'
   [org.nuiton.eugene.java.SimpleJavaBeanTransformer] with implementation 'org.nuiton.eugene.java.SimpleJavaBeanTransformer'
   [org.nuiton.eugene.java.JavaBeanTransformer] with implementation 'org.nuiton.eugene.java.JavaBeanTransformer'

  Found 7 tagvalues in 2 provider(s) : [eugene, java]

  Provider [eugene] - Found 5 tagvalues :
   [constantPrefix] targets : 'ObjectModel, ObjectModelClassifier' : Sets the prefix of any constant to be generated for the given class
   [doNotGenerateBooleanGetMethods] targets : 'ObjectModel, ObjectModelClassifier' : To specify to NOT generate getXXX methods for boolean properties
   [documentation] targets : 'ObjectModel, ObjectModelElement' : Sets the documentation of a model or any of his elements
   [i18n] targets : 'ObjectModel, ObjectModelClassifier' : Sets the i18n prefix to use on I18n keys generated
   [version] targets : 'ObjectModel' : To set the version of the model.
  Provider [java] - Found 2 tagvalues :
   [beanSuperClass] targets : 'ObjectModel, ObjectModelClassifier' : To specify a super-class to used on generated bean for a class or any class of a model
   [noPCS] targets : 'ObjectModel, ObjectModelClassifier' : To specify to not generate any propertyChange code for a class or any class of a model

  Found 4 stereotypes in 2 provider(s) : [eugene, java]

  Provider [eugene] - Found 3 stereotypes :
   [indexed] targets : 'ObjectModelAttribute' : To specify that a attribute is indexed
   [ordered] targets : 'ObjectModelAttribute' : To specify that a attribute is ordered
   [skip] targets : 'ObjectModelClassifier' : To specify that a classifier should not been generated.
  Provider [java] - Found one stereotype :
   [bean] targets : 'ObjectModelClassifier' : To specify that a class is a JavaBean

`More info about this goal`_

Templates application
=====================

Each template is itself a generator inheriting from ObjectModelGenerator. All
the parts of this generator can be overloaded allowing a strong personalisation
of the generators. ObjectModelGenerator's aim is to navigate through the model
and on each key element of the model (model, classes, interfaces, classifier,
...) call the appropriate methods. By default, the methods described in the
generator are empty, so no code is generated. The templates are aimed to
overload those methods and describe the code that will be generated.

The templates can be of all kind because they can generate a different file by
model, by interface, by class or even by classifier (common stub to classes and
interfaces). Moreover, they can generate every kind of code what so ever : Java,
XML, text, C++, ...



.. _More info about this goal: ./eugene-maven-plugin/available-data-mojo.html
.. _ModelPropertiesProvider: ./06-fonctionnalites-avancees.html#ModelPropertiesProvider
