.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

========================
Advanced functionalities
========================

:Author: Florian Desbois, Jean Couteau
:Contact: eugene-devel@list.nuiton.org or eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$

This chapter concerns EUGene advanced functionalities, i.e. how to extend EUGene
to adapt it to another model than the ObjectModel or how to extend the
ObjectModel using extensions.

EUGene can be extended on the four basic notions :

- `Writer`_ : file to file conversion
- `ModelReader`_ : file to model conversion
- Transformer : model to model conversion (see `Templates`_ )
- Generator : model to file conversion (see `Templates`_ )

The `Extensions`_ also allow to enrich EUGene basic model : ObjectModel.

Writer
------

EUGene manage by default the following input formats :

- xmi : standard file for UML modeling language. Versions 1.2 and 2.1 are
  supported by EUGene
- zargo : `ArgoUML`_ file (xmi 1.2)
- zuml : `TopCased`_ file (xmi 2.1 voir 3.0)
- objectmodel : xmi simplified version to generate ObjectModel
- statemodel : xmi simplified version to generate StateModel

A writer is defined by :

- a model type (objectmodel, statemodel)
- an input protocol
- an output protocol
- resources to process
- the task execution logic

The writers can be chained (the output protocol becomes the input protocol of
the following Writer) and this in a hierarchical way (we have to keep an order
on the tasks to execute...).

For example, if we have a zargo file input and we want java files, the following
steps are necessary :

- extract xmi file from zargo archive
- xmi transformation to an objectmodel file
- templates application and java file generation from objectmodel

Each of those steps uses a writer :

- zargo2xmi (input: zargo, output: xmi)
- xmi2objectmodel (input: xmi, output: objectmodel)
- objectmodel2java (input: objectmodel, output: file (java or other))

To use Writers' execution engine, we need to save its resources to process. It
will find the right Writers to call amongst those available and will launch
the tasks (Writer) in the correct order.

If, for example, we give him an objectmodel file, it will only call the last
writer (objectmodel2java).

ModelReader
-----------

`Ref ModelReader`_

The ModeLReader allows to load a Model from files. It's used by EUGene's
execution to read some input files and fill the corresponding model. The two
ModelReader available are :

- `ObjectModelReader`_ : use digesters to read **objectmodel** files.
- `StateModelReader`_ : use digesters to read **statemodel** files.

It is possible to manage other input file formats and manipulate them using a
new ModelReader that you will need to create inheriting from the basis
ModelReader. It is also possible to manage another type of Model and to create
a ModelReader that will fill it in.

The maven plugin allows to specify which reader to use. For more informations
consult the maven goals configuration.

Extensions
----------

EUGene mainly manipulates ObjectModel. This model gives 90% of the necessary
functionalities to numerous output files generation whatever the programming
language might be. Meanwhile some specificities of those languages might block
the usage of ObjectModel and lead the developer to create a new model finally
really close to the existing ObjectModel.

The idea is so to extend the ObjectModel by extensions not to interfere with its
main goal : provide classes representing UML norm.

The root ObjectModel allows the recovery of an extension with a unique
reference. If the extension does not exists, it will be automatically created.
You have to use the `ObjectModel interface`_ to do so::

    <O> O getExtension(String reference, Class<O> extensionClass)
                    throws ClassCastException, IllegalArgumentException;

You just have to attach any class to the model to ba able to manipulate it as
an extension. The aim being to recover it at any step of the generation process
(reader, transformer, generator).

For Java, three extensions have been created :

- `ImportsManagerExtension`_ : associate an `ImportsManager`_ with an
  `ObjectModelClassifier`_ . The ImportsManager will record all the imports
  necessary for this classifier (class, interface, enum).

- `AnnotationsManagerExtension`_ : associate an `AnnotationsManager`_ with an
  `ObjectModelClassifier`_ . The AnnotationManager will record the annotations
  for a classifier on an `ObjectModelElement`_ (operation, parameter, attribute,
  classifier, ...)

- `ConstantsManagerExtension`_ : allows to manage a cache on the manipulated
  constants' names during a generation. The recent generation constants
  integration (for example to specify a bean property name) now authorize us to
  use those constants in the generated code. So the constant's name calculation
  from a property come quite often and is cached by this extension.

TagValueDefinitionProvider
--------------------------

It is possible to describe tag values you can use on your models. See `TagValueDefinitionProvider`_

StereotypeDefinitionProvider
----------------------------

It is possible to describe stereotypes you can use on your models. See `StereotypeDefinitionProvider`_

.. _argouml: http://argouml.tigris.org/
.. _topcased: http://www.topcased.org/
.. _Templates: ./04-templates.html

.. _Ref ModelReader: eugene/apidocs/org/nuiton/eugene/ModelReader.html
.. _ObjectModelReader: eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelReader.html
.. _StateModelReader: eugene/apidocs/org/nuiton/eugene/models/state/StateModelReader.html

.. _ObjectModel interface: eugene/apidocs/org/nuiton/eugene/models/object/ObjectModel.html

.. _ImportsManagerExtension: eugene/apidocs/org/nuiton/eugene/java/ImportsManagerExtension.html
.. _ImportsManager: eugene/apidocs/org/nuiton/eugene/java/ImportsManager.html
.. _AnnotationsManagerExtension: eugene/apidocs/org/nuiton/eugene/java/AnnotationsManagerExtension.html
.. _AnnotationsManager: eugene/apidocs/org/nuiton/eugene/java/AnnotationsManager.html
.. _ConstantsManagerExtension: eugene/apidocs/org/nuiton/eugene/java/ConstantsManagerExtension.html

.. _ObjectModelElement: eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelElement.html
.. _ObjectModelClassifier: eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelClassifier.html

.. _TagValueDefinitionProvider: eugene/apidocs/org/nuiton/eugene/models/tagvalue/TagValueDefinitionProvider.html
.. _StereotypeDefinitionProvider: eugene/apidocs/org/nuiton/eugene/models/stereotype/StereotypeDefinitionProvider.html

