.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

==================
Generation process
==================

:Author: Florian Desbois, Jean Couteau
:Contact: eugene-devel@list.nuiton.org or eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$

Generation principle is split into three points :

- Input files, representing a data model
- Generation process (templates, model transformers)
- Output files for a final application in the programming language wanted


EUGene uses UML modeling language (in XMI format) to read a data model, load it
into memory to manipulate it using Java and finally generate output files
adapted to a technology or a programming language.

Two generation option :

- `Simple generation`_ : generation template
- `Complete generation`_ : model reading + model transformer + simple generation
  template (usually unique for the output language)

We will also see the case of `Java generation`_ .

Simple generation
-----------------

EUGene needs one or more XMI files as input of the generation process. Those
files corresponds to the model to manipulate during generation. Those XMI files
will be converted in **.objectmodel** files easier to read to fill the
`ObjectModel`_ memory model. Since EUGene first versions, it is possible to
write generation templates to interpret the obtained ObjectModel's specificity
according to the desired output (whatever the language might be : java, xml,
php, C++, ...)

In this generation case, the developer only needs to write one or more
generators (Generator) using a `simple syntax`_ interpreted by
`Nuiton-processor`_ .

.. image:: schemas/03-generation-simple.png

By default EUgene manipulate ObjectModel resulting from known file formats such
as zargo, zuml or xmi files (see `Writer`_ ).

Complete generation
-------------------

It is possible to use another file format than xmi or objectmodel as EUGene
input. In this case, it is necessary to convert this format to fill the memory
Objectmodel. It is then possible to use an appropiate `ModelReader`_. It is also
possible to use another model than the ObjectModel, StateModel for exemple (the
ModelReader used will then be StateModelReader). As seen previously, it is
possible to write genertors to interpret ObjectModel and convert it into files.
But it is also possible to use a model transformer that is behaving the same way
than a generator ; except that it does not end on files writing but on a new
model creation ready to be interpreted again by a generator. The transformer's
content is not a template anymore but model transformation : the conversion from
the input model interpreted by the ModelReader to an output model easier to
manipulate in another generator. this allows essentially to concentrate model's
interpretation by the transformers and use a basic generator specific to a
language for example.

.. image:: schemas/03-generation-complete.png

The ObjectModel can also be used as result model from the transformation without
necessarily contains model's specificity (UML norm) but more language's one
(Java for example).

Java generation
---------------

We come to the practical case of Java code generation. Here the input model used
is ObjectModel with the default ObjectModelReader that will interpret the
.objectmodel files to fill in the memory ObjectModel. The developer will have to
create transformers inheriting from ObjectModelTransformerToJava to interpret
the input model (UML) to a new model close to Java language (still an
ObjectModel). The generator used is still the same, it is the JavaGenerator,
that will simply write the content of the Java specific model.

.. image:: schemas/03-generation-java.png

Tools can help adapt easily ObjectModel to the language wanted, like
`extensions`_ (for Java : ImportsManager, AnnotationManager, ...) and the
builder (for Java : JavaBuilder).

Is is mainly advised to use Maven to `execute`_ generation using
`eugene-maven-plugin`_ . Plugin configuration allows to precise the different
generation elements (writer, reader, model, template, ...).

.. _ObjectModel: 02-objectmodel.html
.. _simple syntax: 04-templates.html#Generator_writing_syntax
.. _Nuiton-processor: http://doc.nuiton.org/processor/nuiton-processor/index.html
.. _Writer: 06-fonctionnalites-avancees.html#Writer

.. _ModelReader: 06-fonctionnalites-avancees.html#ModelReader

.. _extensions: 06-fonctionnalites-avancees.html#Extensions
.. _execute: 05-execution.html
.. _eugene-maven-plugin: eugene-maven-plugin/index.html
