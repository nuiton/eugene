.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===
FAQ
===

How to convert UML models from Poseidon to ArgoUML ?
====================================================

You have to save your model using Poseidon in XMI, or extract it from the .zuml
file which is in fact a zip file::

    xlstproc -o <argo-file>.xmi poseidon2argouml.xsl <poseidon-file>.xmi

Then in ArgoUML open the generated file.

The diagrams are not converted, you have to create them agein from the classes
doing drag&drop.

Howto update object model ?
===========================

When updating obecject model, think to update also the associated xsd to
generate always a valid xml object model files.

How to write Transformer's methods ?
====================================

The interest of the Transformers is to use less template syntax as possible to
generate the final code. Nevertheless it is necessary to use this syntax  for
the methods body. Nuiton-processor interprets differently Transformers and
Generators, it is necessary to add as Transformer's heading the following
configuration ::

    /*{generator option: parentheses = false}*/
    /*{generator option: writeString = +}*/

You then just need to use the balises ``/*{ }*/`` to add the body of a method::

    setOperationBody(setValue, ""
    /*{
        this.value = value;
    }*/
    );

Note
  It is advised to make tests for the output file page setting as well as for
  Generators results.

Where can I find examples of Transformer or Generator ?
=======================================================

Waiting for tutorials and a demo version, the project you can find with the most
of concrete examples on EUGene use is `ToPIA`_ . You can find in the source code
(ToPIA-persistence module) lots of examples of Transformer and/or Generator for
Java code generation (uses ObjectModelTransformerToJava or ObjectModelGenerator
as superclass). You can also find some examples of integration tests of
`eugene-maven-plugin`_ : directory **src/it/smart-generate**.

.. _ToPIA: http://nuiton.org/projects/show/topia
.. _eugene-maven-plugin: eugene-maven-plugin/index.html

Why properties are not taken during generation after renomming .zargo file ?
============================================================================

The .zargo file is an archive that contains some files, including .xmi file that
is used in the xmi phase after zargo phase. But when we change .zargo file name,
those included files are not renamed. The .zargo file must be edited to
synchronized all its included files.
