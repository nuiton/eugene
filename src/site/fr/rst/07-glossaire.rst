.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========
Glossaire
=========

MDA
---

Model Driven Architecture : technique de programmation basé sur
les modèles.

Refs : `MDA`_ , `Introduction EUGene`_

.. _MDA: http://fr.wikipedia.org/wiki/Model_driven_architecture
.. _Introduction EUGene: 01-introduction.html

UML
---

Unified Modeling Language : langage de modélisation.

Refs : `Site officiel UML`_ , `ObjectModel EUGene`_

.. _Site officiel UML: http://www.uml.org/
.. _ObjectModel EUGene: 02-objectmodel.html

XMI
---

XML Metadata Interchange : norme XML pour le langage UML.

Refs : `Site officiel XMI`_ , `ObjectModel EUGene`_

.. _Site officiel XMI: http://www.omg.org/spec/XMI/

Template
--------

Classe permettant la génération de fichiers à partir d'un modèle de données.

Ref : `Templates EUgene`_ , `librairie Nuiton-processor`_

.. _Templates EUGene: 04-templates.html
.. _librairie Nuiton-processor: http://doc.nuiton.org/processor

Transformer
-----------

Transformateur de modèle : conversion d'un modèle A en modèle B.

Ref : `Templates EUGene`_

Generator
---------

Générateur de fichiers de sorties à partir d'un modèle d'entrée.

Ref : `Templates EUGene`_

ObjectModel
-----------

Méta-modèle objet d'EUGene avec spécificités UML (multiplicités, héritage, ...)

Refs : `ObjectModel EUGene`_

Builder
-------

Constructeur de modèle, permet de créer et remplir un modèle directement en
Java.

Refs : `Fonctionnalites avancees EUGene`_

.. _Fonctionnalites avancees EUGene: 06-fonctionnalites-avancees.html

Extension
---------

Extensions de modèle, permet de rajouter des fonctionnalités spécifiques à
un modèle. Utilisé principalement dans l'ObjectModel pour y rattacher les
spécificités au langage Java non présente par défaut.

Refs : `Fonctionnalites avancees EUGene`_

Nuiton-processor
----------------

Librairie permettant d'interprété la syntaxe des templates.

Ref : `librairie Nuiton-processor`_
