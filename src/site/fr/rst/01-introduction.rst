.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

============
Introduction
============

:Authors: Julien Ruchaud, Florian Desbois
:Contact: eugene-devel@list.nuiton.org ou eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$


.. contents::

Qu'est ce le MDA (Model Driven Architecture)
--------------------------------------------

EUGene s'inscrit dans les technologies de pointe, et tout particulièrement dans
l'approche MDA. L'approche MDA a comme principe la conception par modèle sur le
développement des logiciels. Ce qui consiste à s'abstraire de la technique en
modèlisant le besoin, puis transformer ce modèle selon la plate-forme cible.
Un exemple classique d'utilisation est la génération de code java grâce à un
modèle UML.


Pour pouvoir réaliser des modèles (M1), il est nécessaire de définir un langage
de modèlisation qui est appellé méta-modèle (M2). Pour pouvoir réaliser des
transformations entre les différents languages, il est nécessaire d'avoir un 
méta-language commun, qui est appellé méta-méta-modèle (M3). Pour ne pas avoir
un autre language au dessus du méta-méta-modèle, celui-ci doit être 
auto-descriptif c'est à dire qu'il est possible de réaliser un méta-modèle
représentant le méta-méta-modèle. Le dernier niveau est l'application (M0) du
modèle dans le monde réel.


Voici quelques exemples :

M3   MOF              Grammaire            UML
M2   UML              Langage              Langage spécifique d'aviation
M1   Java             Phrase               Définition de l'aéroport
M0   Objet            Parler               L'aéroport


Il existe deux stratégies pour la manipulation des modèles soit en définissant
les règles de transformation d'un méta-modèle à un autre, soit par un template
représentant le modèle cible et manipulant le modèle source.


Qu'est ce EUGene (Efficient Universal Generator)
------------------------------------------------

EUGene permet la manipulation et la génération des modèles. Il fournit les
fonctionnalités de bases suivantes :

  * lecture d'un modèle de classe UML en XMI indépendant de la modélisation
  * template de génération
  * transformation de modèles
  * intégration dans le build du projet
  * indépendance entre le code developpeur et le code généré
  * indépendance des outils de développement


De nombreuses solutions sont disponibles sur le marché, EUGene se positionne par
rapport à ses concurrents, comme simple d'utilisation et de mise en oeuvre,
indépendant des outils de développement et full JAVA sans autre language à
apprendre.


Comparaison
-----------

Comparaison par rapport aux solutions existantes du marché :

+---------+---------------------+----------------------------+--------------+---------+-----------+
|         |                     |                            |              |         | Format    |
|         | Générateur          | Indépendance               | Build        | Plugin  | de sortie |
+=========+=====================+============================+==============+=========+===========+
| EUGene  | Template en java    | Outils de développement    | Maven        | Eclipse | Tout      |
+---------+---------------------+----------------------------+--------------+---------+-----------+
| Acceleo | Template spécifique | Lié à la pateforme Eclipse | Pas possible | Eclipse | Tout      |
+---------+---------------------+----------------------------+--------------+---------+-----------+
|         | Tranformation de    |                            |              |         |           |
| ATL     | modèle en QVT       | Lié à la pateforme Eclipse | Pas possible | Eclipse | Modèle    |
+---------+---------------------+----------------------------+--------------+---------+-----------+
