.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========
Templates
=========

:Author: Florian Desbois
:Contact: eugene-devel@list.nuiton.org ou eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$

Comme expliqué dans le chapitre précédent (`Processus de generation`_ ), il
y a deux possibilités pour générer des fichiers :

- Utilisation d'un Generator (template écrite en Java avec utilisation de
  Nuiton-processor)
- Utilisation d'un Transformer (transformation de modèle écrite en Java)

EUGene considère les Generator et les Transformer comme tout deux des templates
de génération avec comme point commun un modèle en entrée. La différence réside
à la sortie, le Generator aura la charge d'écrire des fichiers tandis que le
Transformer chargera un nouveau modèle de même type ou non.

EUGene manipule principalement l' `ObjectModel`_ , il y a donc un existant
abstrait pour la manipulation de ce type de modèle : ObjectModelGenerator et
ObjectModelTransformer.

EUGene implémente également une solution simple pour la génération de code Java.
Ainsi un générateur et un transformeur java sont disponibles : JavaGenerator et
ObjectModelTransformerToJava.

Voici la hiérarchie qui en résulte :

.. image:: ../schemas/04-templates-hierarchy.png


Nous allons commencer par décrire la syntaxe d'écriture d'un Generator avec
l'utilisation de Nuiton-processor. Ensuite nous décrirons l'utilisation
des Transformer pour finir sur la génération de code Java.

.. contents::

Syntaxe d'écriture d'un Generator
---------------------------------

Pour créer un Generator il faut tout d'abord créer une nouvelle classe qui
hérite de l'ObjectModelGenerator pour le cas d'une génération depuis un
ObjectModel.

TODO

Generator disponibles
~~~~~~~~~~~~~~~~~~~~~

- `AbstractGenerator`_ : entrée à définir
- `ObjectModelGenerator`_ : ObjectModel en entrée
- `StateModelGenerator`_ : StateModel en entrée
- `JavaGenerator`_ : ObjectModel orienté Java (avec extensions) en entrée

Note
  Le plugin maven permet de connaître tous les transformers disponibles.

Implantation d'une transformation de modèle
-------------------------------------------

Comme pour les Generator, les Transformer fonctionnent par héritage. Tout dépend
des types de modèle en entrée et en sortie souhaités. Pour un ObjectModel en
entrée, il faut hériter de l'ObjectModelTransformer.

Les étapes du transformer sont les suivantes :

- initialisation des sorties
- appel d'un transformer avant la présente transformation (utile pour certains
  chaînages)
- transformation du modèle d'entrée
- appel de la template de sortie avec pour entrée le modèle de sortie
  précédemment transformé.

Entrée du transformer
~~~~~~~~~~~~~~~~~~~~~

L'entrée du Transformer est un Model. Généralement il s'agit d'un ObjectModel
mais il est possible de gérer d'autres types de modèle (comme StateModel par
exemple).

Sortie du transformer
~~~~~~~~~~~~~~~~~~~~~

Il est nécessaire d'initialiser les sorties du Transformer :

- Modèle de sortie : quelle est le type de modèle en sortie ?
- Template de sortie : quelle est la template qui se chargera de manipuler
  le modèle de sortie ? Cela peut être soit un Generator, soit un autre
  Transformer.

ObjectModelTransformer
~~~~~~~~~~~~~~~~~~~~~~

L'ObjectModelTransformer propose un pattern simple pour transformer les
composantes du modèle. L'ensemble de l'ObjectModel va être parcouru et
pour chaque type d'élément (Classifier, Class, Interface, Enumeration)
une méthode associé permettra sa transformationn, le développeur aura la charge
d'écraser ces méthodes pour faciliter la transformation ::

    public void transformFromModel(ObjectModel model) {
    }

    public void transformFromInterface(ObjectModelInterface interfacez) {
    }

    public void transformFromClass(ObjectModelClass clazz) {
    }

    public void transformFromClassifier(ObjectModelClassifier clazz) {
    }

    public void transformFromEnumeration(ObjectModelEnumeration enumeration) {
    }

L'ObjectModelTransformer est abstrait et nécessite un héritage pour spécifier
le type de modèle en sortie (Pour le cas de la génération Java il s'agit
également de l'ObjectModel).

ObjectModelBuilder
~~~~~~~~~~~~~~~~~~

`Ref ObjectModelBuilder`_

Cet outil permet de remplir un ObjectModel directement depuis un Transformer.
Il s'agit bien évidemment du cas où le modèle de sortie du Transformer est
un ObjectModel. Cette classe permet de faciliter l'écriture de l'ObjectModel,
les interfaces de ce modèle étant dépourvus de setters.

Exemples de méthodes disponibles :

- createClass : création d'un ObjectModelClass
- createInterface : création d'un ObjectModelInterface
- addOperation : ajout d'une méthode à un ObjectModelClassifier
  (ObjectModelClass ou ObjectModelInterface)
- addAttribute : ajout d'un attribute à un ObjectModelClassifier
- addParamater : ajout d'un paramètre à un ObjectModelOperation
- ...

Transformer disponibles
~~~~~~~~~~~~~~~~~~~~~~~

- `Transformer`_ : entrée et sortie à définir
- `ObjectModelTransformer`_ : ObjectModel en entrée, sortie à définir
- `ObjectModelTransformerToJava`_ : ObjectModel en entrée, ObjectModel en sortie,
  `JavaGenerator`_ en template de sortie.

Note
  Le plugin maven permet de connaître tous les transformers disponibles.

Générateur de java
------------------

Tous les éléments de la chaîne de génération sont disponibles pour une
génération de code Java à partir d'un modèle de données XMI (package
org.nuiton.eugene.java) :

- Fichiers en entrée : xmi, objectmodel, zargo, zuml
- `ModelReader`_ : `ObjectModelReader`_
- input `Model`_ : ObjectModel
- output `Model`_ : ObjectModel orienté Java (avec extensions)
- `Transformer`_ abstrait : `ObjectModelTransformerToJava`_
- `Generator`_ de sortie : `JavaGenerator`_

Pour chaque type de génération souhaité, il suffit donc d'hérité de
ObjectModelTransformerToJava et d'utiliser les méthodes transformFrom
pour enregistrer l'ObjectModel de sortie orienté Java qui sera automatiquement
généré via le JavaGenerator. Le principe reste d'interpréter les spécificités
UML du modèle d'entrée pour les transformer en Java selon les besoins techniques
(technologie, framework, environnement, ...).

JavaBuilder
~~~~~~~~~~~

`Ref JavaBuilder`_

Pour faciliter l'écriture de l'ObjectModel orienté Java, le JavaBuilder est
utilisé de façon transparente car l'ObjectModelTransformerToJava propose
l'ensemble de ses méthodes directement (délégation). Ainsi, la transformation
se fait via le parcours du modèle d'entrée et l'utilisation de ces méthodes
pour charger le modèle de sortie.

Exemples de méthodes :

- createClass : création d'un ObjectModelClass
- createInterface : création d'un ObjectModelInterface
- addImport : ajout d'un import à un ObjectModelClassifier
- addAnnotation : ajout d'une annotation à un ObjectModelElement
- setSuperClass : ajoute une superclass à un ObjectModelClassifier (extends)
  avec ajout de l'import automatique sur le type de la superclass.
- addConstant : ajoute une constante (static final) à un ObjectModelClassifier
- addConstructor : ajoute un constructeur à une classe.
- ...

Note
  La gestion des imports est faite automatiquement sur les types manipulés
  (type d'attribut, de paramètre, retour de méthode, interface, ...). Cette
  gestion utilise l'extension ImportsManagerExtension qui gère un ImportsManager
  par classifier.

.. _Processus de generation: 03-generation.html
.. _ObjectModel: 02-objectmodel.html

.. _AbstractGenerator: ../eugene/apidocs/org/nuiton/eugene/AbstractGenerator.html
.. _ObjectModelGenerator: ../eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelGenerator.html
.. _StateModelGenerator: ../eugene/apidocs/org/nuiton/eugene/models/state/StateModelGenerator.html
.. _JavaGenerator: ../eugene/apidocs/org/nuiton/eugene/java/JavaGenerator.html

.. _Ref ObjectModelBuilder: ../eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelBuilder.html

.. _Transformer: ../eugene/apidocs/org/nuiton/eugene/Transformer.html
.. _ObjectModelTransformer: ../eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelTransformer.html
.. _ObjectModelTransformerToJava: ../eugene/apidocs/org/nuiton/eugene/java/ObjectModelTransformerToJava.html

.. _ModelReader: ../eugene/apidocs/org/nuiton/eugene/ModelReader.html
.. _ObjectModelReader: ../eugene/apidocs/org/nuiton/eugene/models/object/ObjectModeReader.html
.. _Model: ../eugene/apidocs/org/nuiton/eugene/models/Model.html
.. _Generator: ../eugene/apidocs/org/nuiton/eugene/AbstractGenerator.html

.. _Ref JavaBuilder: ../eugene/apidocs/org/nuiton/eugene/java/JavaBuilder.html
