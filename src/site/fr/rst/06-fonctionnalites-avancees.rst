.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

========================
Fonctionnalités avancées
========================

:Author: Florian Desbois
:Contact: eugene-devel@list.nuiton.org ou eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$

Ce chapitre concerne les fonctionnalités avancées d'EUGene, c'est à dire,
comment étendre EUGene pour l'adapter à un autre modèle que l'ObjectModel
ou comment étendre ce dernier par le biais des extensions.

EUGene peut être étendu sur les quatres notions de base :

- `Writer`_ : conversion fichiers vers fichiers
- `ModelReader`_ : conversion fichiers vers modèle
- Transformer : conversion modèle vers modèle (voir `Templates`_ )
- Generator : conversion modèle vers fichiers (voir `Templates`_ )

Les `Extension`_ permettent également d'enrichir le modèle de base d'EUGene :
l'ObjectModel.

Writer
------

EUGene gère par défaut les formats de fichiers d'entrée suivants :

- xmi : fichier standard pour le langage de modélisation UML. Les versions
  1.2 et 2.1 du xmi sont supportés par EUGene
- zargo : fichier `ArgoUML`_ (xmi 1.2)
- zuml : fichier `TopCased`_ (xmi 2.1 voir 3.0)
- objectmodel : version simplifié du xmi pour générer de l'ObjectModel
- statemodel : version simplifié du xmi pour générer du StateModel

Un writer est défini par :

- un type de modèle (objectmodel, statemodel)
- un protocole d'entrée
- un protocole de sortie
- des ressources à traiter
- la logique d'exécution de la tâche.

Les Writers peuvent être chaînés (le protocole de sortie devient le protocole
d'entrée du Writer suivant) et ceci de manière hiérarchisé (on doit évidemment
conserver un ordre sur les tâches à exécuter...).

Par exemple, si on a en point d'entrée un fichier zargo et en sortie on veut
des fichiers java les étapes suivantes sont nécessaires :

- extraction du fichier xmi de l'archive zargo
- transformation xmi en un fichier objectmodel
- application des templates et génération de fichiers java à partir de l'objetmodel

Chacune de ces étapes utilise un writer :

- zargo2xmi (entrée: zargo, sortie: xmi)
- xmi2objectmodel (entrée: xmi, sortie: objectmodel)
- objectmodel2java (entrée: objectmodel, sortie: fichier (java ou autre))

Pour utiliser le moteur d'exécution des Writer, on doit lui enregistrer des
ressources à traiter. Il trouvera les bons Writers à appeller parmis ceux
disponibles et lancera les tâches (Writer) dans l'ordre nécessaire.

Si par exemple on lui donne en entrée un fichier objectModel, il appelera
uniquement le dernier writer (objectmodel2java).

ModelReader
-----------

`Ref ModelReader`_

Le ModelReader permet de charger un Model à partir de fichiers. Il sert à
l'execution d'EUGene pour lire certains fichiers d'entrée et remplir le modèle
correspondant. Les deux ModelReader disponibles sont :

- `ObjectModelReader`_ : utilisation de digester pour lire les fichiers
  **objectmodel**.
- `StateModelReader`_ : utilisation de digester pour lire les fichiers
  **statemodel**.

Il est possible de gérer un autre format de fichier en entrée et de le manipuler
via un nouveau ModelReader que vous devrez créer en héritant du ModelReader de
base. Il est également possible de gérer un autre type de Model et de créer un
ModelReader qui se chargera de le remplir.

Le plugin maven permet de spécifier quel reader utiliser. Pour plus
d'informations consulter la configuration des goals maven.

Extension
---------

EUGene manipule majoritairement l'ObjectModel. Ce modèle fourni à 90% l'ensemble
des fonctionnalités nécessaires à la génération de nombreux fichiers de sorties
quelque soit le langage de programmation. Cependant certaines spécifités propres
à ces langages bloquent l'utilisation de l'ObjectModel et mènerait le développeur
à créer un nouveau modèle finalement très proche de l'ObjectModel existant.

L'idée est donc d'étendre l'ObjectModel via des extensions pour ne pas perturber
son but premier : fournir un ensemble de classes représentant la norme UML.

L'ObjectModel racine permet la récupération d'une extension avec une référence
unique. Si l'extension n'existe pas, elle sera automatiquement créer.
Il faut utiliser pour se faire la méthode de l' `interface ObjectModel`_ ::

    <O> O getExtension(String reference, Class<O> extensionClass)
                    throws ClassCastException, IllegalArgumentException;

Il vous suffit donc d'attacher n'importe quelle classe au modèle pour pouvoir
la manipuler en tant qu'extension. Le but étant de pouvoir la récupérer
à n'importe quel moment du processus de génération (reader, transformer,
generator).

Pour Java, trois extensions ont été créés :

- `ImportsManagerExtension`_ : associe un `ImportsManager`_ avec un
  `ObjectModelClassifier`_ . L'ImportsManager se charge d'enregistrer tous les
  imports nécessaires à ce classifier (class, interface, enum).

- `AnnotationsManagerExtension`_ : associe un `AnnotationsManager`_ à un
  `ObjectModelClassifier`_ . L'AnnotationManager se charge d'enregistrer les
  annotations pour un classifier sur un `ObjectModelElement`_ (operation,
  parameter, attribute, classifier, ...)

- `ConstantsManagerExtension`_ : permet de gérer un cache sur les noms des constantes
  manipulés pendant une génération. L'introduction récente dans constantes de
  génération  (par exemple pour spécifier le nom d'une propriété d'un bean) nous
  autorise désormais à utiliser aussi ces constantes dans le code qu'on génère.
  Le calcul du nom d'une constante à partir de la proprité revient donc
  régulièrement et est caché grâçe à cette extension.

TagValueDefinitionProvider
--------------------------

Il est possible de définir les tagValues applicables sur un modèle.

Voir `TagValueDefinitionProvider`_

StereotypeDefinitionProvider
----------------------------

Il est possible de définir les stéréotypes applicables sur un modèle.

Voir `StereotypeDefinitionProvider`_

ModelPropertiesProvider
-----------------------

Il est possible de définir les tagValues et stéréotypes applicables sur un
modèle ou sur des templates en utilisant la classe *ModelPropertiesProvider*.

Docuementationà finir avant la version 3.0...

.. _Templates: 04-templates.html

.. _Ref ModelReader: ../eugene/apidocs/org/nuiton/eugene/ModelReader.html
.. _ObjectModelReader: ../eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelReader.html
.. _StateModelReader: ../eugene/apidocs/org/nuiton/eugene/models/state/StateModelReader.html

.. _interface ObjectModel: ../eugene/apidocs/org/nuiton/eugene/models/object/ObjectModel.html

.. _ImportsManagerExtension: ../eugene/apidocs/org/nuiton/eugene/java/ImportsManagerExtension.html
.. _ImportsManager: ../eugene/apidocs/org/nuiton/eugene/java/ImportsManager.html
.. _AnnotationsManagerExtension: ../eugene/apidocs/org/nuiton/eugene/java/AnnotationsManagerExtension.html
.. _AnnotationsManager: ../eugene/apidocs/org/nuiton/eugene/java/AnnotationsManager.html
.. _ConstantsManagerExtension: ../eugene/apidocs/org/nuiton/eugene/java/ConstantsManagerExtension.html

.. _ObjectModelElement: ../eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelElement.html
.. _ObjectModelClassifier: ../eugene/apidocs/org/nuiton/eugene/models/object/ObjectModelClassifier.html

.. _TagValueDefinitionProvider: eugene/apidocs/org/nuiton/eugene/models/tagvalue/TagValueDefinitionProvider.html
.. _StereotypeDefinitionProvider: eugene/apidocs/org/nuiton/eugene/models/stereotype/StereotypeDefinitionProvider.html




