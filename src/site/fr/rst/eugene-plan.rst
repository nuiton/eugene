.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=============================
Plan documentation EUGene 2.0
=============================

:Authors: jruchaud, fdesbois
:Date: 18 fév. 2010
:Version: 1.1

Plan documentation EUGene 2.0
=============================

- Introduction

  * C'est quoi le MDA
  * Philosophie d'Eugene
  * Comparaison avec les solutions existantes (acceleo)

- MétaModel: ObjectModel

  * Présentation
  * Comparaison EMF/MOF

- Processus de génération

  * Générateur
  * Transformation de modèle

- Template

  * Syntaxe des générateurs
  * Implantation d'une transformation de modèle
  * Générateur en Java

- Build

  * Ant (désactivé depuis la version 2.5)
  * Maven

- Fonctionnalités avancées

  * Writer
  * ModelReader
  * Extension

- Glossaire

- Tutoriels
  
  * Tutoriel 1 : Création d'un modèle UML avec résultat ObjectModel
  * Tutoriel 2 : Création d'une template de génération (Generator)
  * Tutoriel 3 : Création d'un transformer de modèle pour Java (ObjectModel to ObjectModel)
  * Tutoriel 4 : Création d'un reader pour charger l'ObjectModel
  * Tutoriel 5 : Création d'un transformer pour un autre modèle que l'ObjectModel (ObjectModel to ?)
  * Tutoriel 6 : ...

- FAQ

1- Introduction
---------------

Ressource : jruchaud?

- Revoir DiscussionSurTypeDeGeneration.rst

2- MétaModel : ObjectModel
--------------------------

Présentation
~~~~~~~~~~~~

Ressources : jruchaud?, fdesbois

- Revoir ObjectModel.rst
- Diagramme de classes vu MetaModel (ObjectModel.png)
- Diagramme de classes réel (implantation) (ObjectModel_interfaces.png)

Comparaison EMF/MOF
~~~~~~~~~~~~~~~~~~~

Ressource : jruchaud?

3- Processus de génération
--------------------------

Ressource : fdesbois

- Diagrammes d'activités : processus standard, processus avec transformer, processus java

4- Templates
------------

- Diagramme de classes sur l'héritage entre Template, Generator, Transformer, ...

Syntaxe des générateurs
~~~~~~~~~~~~~~~~~~~~~~~

Ressource : jruchaud?

- Utilisation de nuiton-processor
- Pattern generateFrom ...


Implantation d'une transformation de modèle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ressource : fdesbois

- Entrée du transformer (model)
- Sortie du transformer (template, model)
- Pattern transformFrom
- ObjectModelTransformer
- ObjectModelBuilder

Générateur en java
~~~~~~~~~~~~~~~~~~

Ressource : fdesbois

- JavaGenerator
- ObjectModelTransformerToJava
- JavaBuilder
- ImportsManager

5- Build (Execution)
--------------------

Ant
~~~

Ressource : echatellier?

- plugin eclipse ???

Maven
~~~~~

Ressource : tchemit?

- Documentation existante : eugene-maven-plugin

6- Fonctionnalités avancées
---------------------------

Writer
~~~~~~

Ressource : tchemit?

- Principe de chaînage

ModelReader
~~~~~~~~~~~

Ressources : fdesbois, echatellier?

- Utilisation de digester
- ObjectModelReader / StateModelReader

Extension
~~~~~~~~~

Ressources : tchemit?, fdesbois

- extension de l'ObjectModel
- extensions pour Java : imports, annotations, constantes, ...

Divers
------

7- Glossaire
~~~~~~~~~~~~

Ressources : tout le monde

- A compléter au fur et à mesure

8- FAQ
~~~~~~

- Reprendre existant + compléments si besoin

9- Tutoriels
~~~~~~~~~~~~

- Cas concrets, en attente car très long à produire

Traduction anglais
~~~~~~~~~~~~~~~~~~

Ressource : jcouteau?














