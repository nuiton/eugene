.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=======================
Processus de génération
=======================

:Author: Florian Desbois
:Contact: eugene-devel@list.nuiton.org ou eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$

Le principe de génération réside en trois points :

- Fichiers d'entrée, représentant un modèle de données concret
- Moulinette de génération (templates, transformeurs de modèle)
- Fichiers de sorties pour une application finale dans le langage de
  programmation souhaité.

EUGene utilise le langage de modélisation UML (au format XMI) pour interpréter
un modèle de données, le charger en mémoire pour pouvoir le manipuler en Java
et pour finalement générer des fichiers de sorties adaptés à une technologie
ou un langage de programmation.

Deux possibilités de génération :

- `Generation simple`_ : template de génération
- `Generation complete`_ : lecture de modèle + transformeur de modèle +
  template de génération simple (généralement unique au langage de sortie)

Nous verrons également le cas concret de la `Generation Java`_ .

Generation simple
-----------------

EUGene nécessite en entrée du processus de génération un ou plusieurs fichiers
xmi correspondants au modèle à manipuler pendant la génération. Ces fichiers
xmi seront convertis en fichiers **.objectmodel** plus simple à interpréter pour
ensuite remplir le modèle mémoire `ObjectModel`_ . Depuis les premières versions
d'EUGene il est possible d'écrire des templates de génération pour interpréter
les spécifités de l'ObjectModel obtenu suivant le résultat final escompté
(quelque soit le langage des fichiers résultants : java, xml, php, c++, ...).

Dans ce cas de génération, le développeur a seulement besoin d'écrire un ou
plusieurs générateurs (Generator) en utilisant une `syntaxe simple`_
interprété par `Nuiton-processor`_ .

.. image:: ../schemas/03-generation-simple.png

Par défaut EUGene manipule de l'ObjectModel résultants de formats de fichiers
connus comme les fichiers zargo, zuml ou xmi (voir `Writer`_ ) .

Generation complete
-------------------

Il est possible d'utiliser un autre format de fichier que le xmi ou
l'objectmodel en entrée d'EUGene, dans ce cas il est nécessaire de convertir
ce format pour remplir l'ObjectModel mémoire. Il est ainsi possible d'utiliser
un `ModelReader`_ approprié. Il est également possible d'utiliser un autre modèle
que l'ObjectModel comme le StateModel par exemple (le ModelReader utilisé sera
le StateModelReader). Comme précédemment, il est possible d'écrire des
générateurs pour interprété l'ObjectModel et le convertir en fichiers. Mais il
est également possible d'utiliser un transformeur de modèle qui se comporte
de la même manière qu'un générateur ; excepté qu'il ne termine pas sur l'écriture
de fichiers mais sur la création d'un nouveau modèle prêt à être de nouveau
interprété par un générateur. Le contenu du transformeur n'est plus une template
de génération mais de la transformation de modèle : la conversion du modèle
d'entrée interprété par le ModelReader vers un modèle de sortie plus simple à
manipuler dans un autre générateur. Cela permet principalement de concentrer
l'interprétation du modèle dans les transformeurs et d'utiliser un générateur
 basique spécifique à un langage par exemple.

.. image:: ../schemas/03-generation-complete.png

L'ObjectModel peut être également utilisé comme modèle résultat de la
transformation sans contenir nécessairement les spécificités du modèle
(norme UML) mais plus celles du langage (Java par exemple).

Generation Java
---------------

Nous arrivons vers le cas pratique de la génération de code Java. Ici le modèle
d'entrée utilisé est l'ObjectModel avec l'ObjectModelReader par défaut qui
va interpréter les fichiers .objectmodel pour remplir l'ObjectModel mémoire.
Le développeur aura à sa charge de créer des transformeurs qui héritent
de l'ObjectModelTransformerToJava pour ainsi interpréter le modèle d'entrée (UML)
vers un nouveau modèle proche du langage Java (également un ObjectModel).
Le générateur utilisé est toujours le même, il s'agit du JavaGenerator, qui
se chargera d'écrire simplement le contenu du modèle spécifique à Java.

.. image:: ../schemas/03-generation-java.png

Des outils permettent de facilement adapté l'ObjectModel au langage
souhaité, comme notamment les `extensions`_ (pour Java :
ImportsManager, AnnotationManager, ...) et le builder (pour Java : JavaBuilder).

Il est conseillé d'utiliser maven pour `executer`_ la génération en utilisant le
`eugene-maven-plugin`_ . La configuration du plugin permet de préciser les
différents éléments de la génération (writer, reader, model, template, ...).

.. _ObjectModel: 02-objectmodel.html
.. _syntaxe simple: 04-templates.html#Syntaxe_dcriture_dun_Generator
.. _Nuiton-processor: http://doc.nuiton.org/processor/nuiton-processor/index.html
.. _Writer: 06-fonctionnalites-avancees.html#Writer

.. _ModelReader: 06-fonctionnalites-avancees.html#ModelReader

.. _extensions: 06-fonctionnalites-avancees.html#Extensions
.. _executer: 05-execution.html
.. _eugene-maven-plugin: ../eugene-maven-plugin/fr/index.html
