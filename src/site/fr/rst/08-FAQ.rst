.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===
FAQ
===

Comment convertir les modèles UML de Poseidon vers ArgoUML ?
============================================================

Il faut sauver le modèle sous Poseidon en XMI, on bien l'extraire du
fichier .zmul qui est en fait un fichier zip::

    xlstproc -o <argo-file>.xmi poseidon2argouml.xsl <poseidon-file>.xmi

Ensuite dans ArgoUML ouvrez le fichier généré.

Les diagrammes ne sont pas convertie, il faut donc les recréer a partir des
classes en faisant du drag&drop.

Comment écrire le corps des méthodes dans un Transformer ?
==========================================================

L'intérêt des Transformer est d'utiliser au minimum la syntaxe template pour
générer le code final. Cependant il est nécessaire d'utiliser cette syntaxe
pour le corps des méthodes. Nuiton-processor interprète différemment les
Transformer des Generator, il est nécessaire de rajouter en en-tête d'un
transformer la configuration suivante ::

    /*{generator option: parentheses = false}*/
    /*{generator option: writeString = +}*/

Il suffit ensuite d'utiliser les balises ``/*{ }*/`` pour ajouter le corps d'une
méthode ::

    setOperationBody(setValue, ""
    /*{
        this.value = value;
    }*/
    );

Note
  Il est conseillé de faire des essais pour la mise en page du fichier résultant
  au même titre que pour les résultats d'un Generator.

Où puis-je trouver des exemples de Transformer ou Generator ?
=============================================================

En attendant les tutoriels et une version demo, le projet ayant le plus
d'exemples concrets sur l'utilisation d'EUGene est le projet
`ToPIA`_ . Vous y trouverez dans le code source (module ToPIA-persistence)
de nombreux exemples de Transformer et/ou Generator pour la génération de code
Java (utilisation en tant que superclasse du Transformer
ObjectModelTransformerToJava ou du Generator ObjectModelGenerator).
Vous trouverez également quelques exemples dans les tests d'intégration du
`eugene-maven-plugin`_ : répertoire **src/it/smart-generate**.

.. _ToPIA: http://nuiton.org/projects/show/topia
.. _eugene-maven-plugin: ../eugene-maven-plugin/fr/index.html

Pourquoi la génération ne marche plus après renommage des fichiers .zargo et properties
=======================================================================================

Lorsque l'on renomme les fichiers .zargo et .properties correspondant, les
propriétés peuvent ne plus être prises en compte lors de la génération. En fait,
le fichier .zargo est une archive qui contient plusieurs fichiers dont le
fichier .xmi utilisé par la génération. Ce fichier n'est pas renommé avec le
fichier .zargo et il est donc nécessaire d'éditer le fichier .zargo afin de
synchroniser les noms de fichier qu'il contient.
