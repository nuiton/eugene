.. -
.. * #%L
.. * EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======
EUGene
======

Efficient Universal GENErator

Vous trouverez dans cette documentation :

- `Introduction`_ : introduction sur les principes d'EUGene
- `ObjectModel`_ : Méta-modèle principal d'EUGene utiliser pour la génération
- `Processus de generation`_ : Etapes de génération
- `Templates`_ : Ecriture des templates (`Transformer`_ et `Generator`_ )
- `Execution`_ : Execution de la génaration (via Maven ou Ant)
- `Fonctionnalites avancees`_ : Comment étendre les fonctionnalités d'EUGene
- `Glossaire`_ : Glossaire des termes utiliser dans EUGene
- `FAQ`_ : Foire aux questions

.. _Introduction: 01-introduction.html
.. _ObjectModel: 02-objectmodel.html
.. _Processus de generation: 03-generation.html
.. _Templates: 04-templates.html
.. _Execution: 05-execution.html
.. _Fonctionnalites avancees: 06-fonctionnalites-avancees.html
.. _Glossaire: 07-glossaire.html
.. _Transformer: 07-glossaire.html
.. _Generator: 07-glossaire.html
.. _FAQ: 08-FAQ.html

Le projet EUGene est composé de 5 modules :

  * `eugene-api`_ (Api de base d'EUGene)
  * `eugene-java-templates`_ (Templates pour générer des classes java)
  * `eugene-plantuml-templates`_ (depuis la version 2.7) (Templates pour générer des diagrammes plantumlclasses java)
  * `eugene-yaml-templates`_ (depuis la version 2.7) (Pour lire un object model depuis des fichiers yaml)
  * `eugene-maven-plugin`_ (Plugin maven pour lancer des générations)

.. _eugene-api: ../eugene/fr/index.html
.. _eugene-java-templates: ../eugene-java-templates/fr/index.html
.. _eugene-maven-plugin: eugene-maven-plugin/fr/index.html
.. _eugene-plantuml-templates: ../eugene-plantuml-templates/fr/index.html
.. _eugene-yaml-templates: ../eugene-yaml-templates/fr/index.html

Plus d'infos sur le projet sur la `forge nuiton`_ .

.. _forge nuiton: http://nuiton.org/projects/eugene

A venir des tutoriels ainsi qu'une application demo.
