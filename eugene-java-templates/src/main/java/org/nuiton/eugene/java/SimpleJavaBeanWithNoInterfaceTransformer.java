package org.nuiton.eugene.java;

/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;

/**
 * Generates a java bean and a utility class around it. This templates acts like {@link SimpleJavaBeanTransformer}
 * but with no interface generation (plus there is not factory generation at all).
 *
 * For example:
 * <pre>
 *     AbstractBoat
 *     Boat (extends AbstractBoat)
 *     AbstractBoats
 *     Boats (extends AbstractBoats)
 * </pre>
 *
 * <b>This transformer is deprecated and you should use now {@link BeanTransformer} instead.</b>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.java.SimpleJavaBeanWithNoInterfaceTransformer"
 * @deprecated since 3.0, no more usable, replaced by {@link BeanTransformer}.
 */
@Deprecated
public class SimpleJavaBeanWithNoInterfaceTransformer extends ObjectModelTransformerToJava {

    @Override
    public void transformFromModel(ObjectModel model) {
        throw new IllegalStateException("You can't use any longer this transformer, please use now " + BeanTransformer.class.getName() + ".");
    }

    @Override
    public void transformFromClass(ObjectModelClass input) {
        throw new IllegalStateException("You can't use any longer this transformer, please use now " + BeanTransformer.class.getName() + ".");
    }

}

