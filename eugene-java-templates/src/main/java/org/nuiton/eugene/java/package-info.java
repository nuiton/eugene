/**
 * Eugene java package : Java generators.
 * <pre>
 * - Generator : {@link org.nuiton.eugene.java.JavaBeanTransformer } to generate a bean (with abstract and impl class).
 * - Generator : {@link org.nuiton.eugene.java.JavaEnumerationTransformer } to generate a enumeration.
 * - Generator : {@link org.nuiton.eugene.java.JavaInterfaceTransformer } to generate a interface.
 * - Generator : {@link org.nuiton.eugene.java.SimpleJavaBeanTransformer} to generate a bean (one class and interface if required).
 * </pre>
 */
package org.nuiton.eugene.java;

/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
