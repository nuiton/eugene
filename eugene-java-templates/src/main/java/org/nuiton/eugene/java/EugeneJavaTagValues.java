package org.nuiton.eugene.java;

/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Defines all tag values managed by Java templates.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider" role-hint="eugene-java-templates"
 * @since 2.5.6
 */
public class EugeneJavaTagValues extends DefaultTagValueMetadatasProvider {

    @Override
    public String getDescription() {
        return t("eugene.java.tagvalues");
    }

    public enum Store implements TagValueMetadata {

        /**
         * Boolean tag value for JavaBean objects to place on a classifier or package.
         *
         * @see #isBean(ObjectModelClassifier, ObjectModelPackage)
         * @since 2.5.6
         */
        bean(n("eugene.java.stereotype.bean"), boolean.class, null, ObjectModelClassifier.class, ObjectModelPackage.class),

        /**
         * To use java 8 new syntax and api in generation.
         *
         * You can globally use it on the complete model.
         *
         * @see #isUseJava8(ObjectModel)
         * @since 2.15
         */
        java8(n("eugene.java.tagvalue.java8"), boolean.class, "false", ObjectModel.class);

        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String i18nDescriptionKey;
        private final String defaultValue;

        Store(String i18nDescriptionKey, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = ImmutableSet.copyOf(targets);
            this.type = type;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return t(i18nDescriptionKey);
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }

    }

    public EugeneJavaTagValues() {
        super((TagValueMetadata[]) Store.values());
    }

    /**
     * Obtain the value of the {@link Store#java8} tag value on the given model.
     *
     * @param model model to seek
     * @return {@code true} the none empty value of the found tag value or {@code false} if not found nor empty.
     * @see Store#java8
     * @since 2.15
     */
    public boolean isUseJava8(ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.java8, model);
    }

    /**
     * Check if the given classifier has the {@link Store#bean} stereotype.
     *
     * @param classifier classifier to test
     * @return {@code true} if tag value was found, {@code false otherwise}
     * @see Store#bean
     */
    public boolean isBean(ObjectModelClassifier classifier, ObjectModelPackage aPackage) {
        return TagValueUtil.findBooleanTagValue(Store.bean, classifier, aPackage);
    }

}
