/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;

import java.util.Set;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * JavaInterfaceTransformer generates simple interfaces for Java language.
 *
 * Created: 7 nov. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 * @since 2.0.2
 */
@Component(role = Template.class, hint = "org.nuiton.eugene.java.JavaInterfaceTransformer")
public class JavaInterfaceTransformer extends ObjectModelTransformerToJava {

    private static final Log log =
            LogFactory.getLog(JavaInterfaceTransformer.class);

    @Override
    public void transformFromInterface(ObjectModelInterface input) {

        boolean canGenerate = canGenerate(input);
        if (!canGenerate) {
            
            return;
        }

        ObjectModelInterface output = createInterface(input.getName(),
                                                      input.getPackageName());

        if (log.isDebugEnabled()) {
            log.debug("generate interface " +
                      output.getQualifiedName());
        }

        // extend interface

        for (ObjectModelInterface extend : input.getInterfaces()) {
            addInterface(output, extend.getQualifiedName());
        }

        String prefix = getConstantPrefix(input);

        if (StringUtils.isEmpty(prefix)) {

            // no specific prefix, so no prefix
            if (log.isWarnEnabled()) {
                log.warn("[" + input.getName() + "] Will generate constants with NO prefix, not a good idea...");
            }
        }
        setConstantPrefix(prefix);

        Set<String> constants = addConstantsFromDependency(input, output);

        // constant attributes
        for (ObjectModelAttribute attr : input.getAttributes()) {

            if (attr.isStatic() ||
                !StringUtils.isNotEmpty(attr.getDefaultValue())) {

                // only static attribut with value
                continue;
            }

            String constantName = attr.getName();

            if (constants.contains(constantName)) {

                // already generated
                continue;
            }

            // add constant
            addConstant(output,
                        constantName,
                        attr.getType(),
                        attr.getDefaultValue(),
                        ObjectModelJavaModifier.PUBLIC
            );
        }


        // interface operations
        JavaGeneratorUtil.cloneOperations(this,
                                          input.getOperations(),
                                          output,
                                          false
        );
    }

    protected boolean canGenerate(ObjectModelInterface input) {
        
        // check if not found in class-path
        boolean b = !getResourcesHelper().isJavaFileInClassPath(input.getQualifiedName());

        if (b) {

            // only generate when no stereotype are on interface ? Strange !?
            b = input.getStereotypes().isEmpty();
        }
        return b;
    }

}
