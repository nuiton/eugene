package org.nuiton.eugene.java;

/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Defines all tag values managed by Java templates.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider" role-hint="bean"
 * @since 2.5.6
 */
public class BeanTransformerTagValues extends DefaultTagValueMetadatasProvider {

    @Override
    public String getDescription() {
        return t("eugene.bean.tagvalues");
    }

    public enum Store implements TagValueMetadata {

        /**
         * To generate a utility class around the bean. this classe offers functions, predicates, copy methods, ...
         *
         * You must use it on the complete model.
         *
         * @see #isGenerateHelper(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        generateHelper(n("eugene.bean.tagvalue.generateHelper"), boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),


        /**
         * To generate or not guava predicates on each property of the bean.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #isGenerateHelperPredicates(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        generateHelperPredicates(n("eugene.bean.tagvalue.generateHelperPredicates"), boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To generate or not guava functions on each property of the bean.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #isGenerateHelperFunctions(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        generateHelperFunctions(n("eugene.bean.tagvalue.generateHelperFunctions"), boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To generate or not constructors methods on Default classes.
         *
         * You can globally use it on the complete model, package or on a specific classifier.
         *
         * @see #isGenerateHelperConstructors(ObjectModelClassifier, ObjectModelPackage, ObjectModel)}
         * @since 3.0
         */
        generateHelperConstructors(n("eugene.bean.tagvalue.generateHelperConstructors"), boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to generate property change support on generated beans.
         *
         * You can globally use it on the complete model, on packages, or to a specific classifier.
         *
         * @see #isGeneratePropertyChangeSupport(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        generatePropertyChangeSupport(n("eugene.bean.tagvalue.generatePropertyChangeSupport"), boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to generate lazy instantiation of any collection to avoid NPEs.
         *
         * You can globally use it on the complete model or a package, or to a specific classifier.
         *
         * @see #isGenerateNotEmptyCollections(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        generateNotEmptyCollections(n("eugene.bean.tagvalue.generateNotEmptyCollections"), boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to use a super class for generated bean.
         *
         * If the bean needs Property change support (says you use the tag-value {@link Store#generatePropertyChangeSupport},
         * then your class must provide everything for it.
         *
         * More over, if you use some collections in your bean you must also define
         * two method named {@code getChild(Collection list, int index)} and
         * {@code getChild(List list, int index)}
         *
         * See new code to know minimum stuff to add in your class for this purpose.
         * <pre>
         * public abstract class BeanSupport implements Serializable {
         *
         *     private static final long serialVersionUID = 1L;
         *
         *     protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
         *
         *     public void addPropertyChangeListener(PropertyChangeListener listener) {
         *         pcs.addPropertyChangeListener(listener);
         *     }
         *
         *     public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
         *         pcs.addPropertyChangeListener(propertyName, listener);
         *     }
         *
         *     public void removePropertyChangeListener(PropertyChangeListener listener) {
         *         pcs.removePropertyChangeListener(listener);
         *     }
         *
         *     public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
         *         pcs.removePropertyChangeListener(propertyName, listener);
         *     }
         *
         *     protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
         *         pcs.firePropertyChange(propertyName, oldValue, newValue);
         *     }
         *
         *     protected void firePropertyChange(String propertyName, Object newValue) {
         *         firePropertyChange(propertyName, null, newValue);
         *     }
         *
         *     protected &lt;T&gt; T getChild(Collection&lt;T&gt; list, int index) {
         *         return CollectionUtil.getOrNull(list, index);
         *     }
         *
         *     protected &lt;T&gt; T getChild(List&lt;T&gt; list, int index) {
         *         return CollectionUtil.getOrNull(list, index);
         *     }
         * }
         * </pre>
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getSuperClassTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        superClass(n("eugene.bean.tagvalue.superClass"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to use a super super-class for generated defaults class of a simple bean.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getHelperSuperClassTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        helperSuperClass(n("eugene.bean.tagvalue.helperSuperClass"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To add a prefix on the name of each generated bean class.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getClassNamePrefixTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        classNamePrefix(n("eugene.bean.tagvalue.classNamePrefix"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To add a prefix on the name of each generated bean class.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getClassNameSuffixTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        classNameSuffix(n("eugene.bean.tagvalue.classNameSuffix"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To add a prefix on the name of each generated bean class.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getHelperClassNamePrefixTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        helperClassNamePrefix(n("eugene.bean.tagvalue.helperClassNamePrefix"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To add a suffix on the name of each generated bean class.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getHelperClassNameSuffixTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        helperClassNameSuffix(n("eugene.bean.tagvalue.helperClassNameSuffix"), String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class);

        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String i18nDescriptionKey;
        private final String defaultValue;

        Store(String i18nDescriptionKey, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = ImmutableSet.copyOf(targets);
            this.type = type;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return t(i18nDescriptionKey);
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }

    }

    public BeanTransformerTagValues() {
        super((TagValueMetadata[]) Store.values());
    }

    /**
     * Obtain the value of the {@link Store#superClass} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#superClass
     * @since 3.0
     */
    public String getSuperClassTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.superClass, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#helperSuperClass} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#helperSuperClass
     * @since 3.0
     */
    public String getHelperSuperClassTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.helperSuperClass, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#classNamePrefix} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#classNamePrefix
     * @since 3.0
     */
    public String getClassNamePrefixTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.classNamePrefix, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#classNameSuffix} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#classNameSuffix
     * @since 3.0
     */
    public String getClassNameSuffixTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.classNameSuffix, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#helperClassNamePrefix} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#helperClassNamePrefix
     * @since 3.0
     */
    public String getHelperClassNamePrefixTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.helperClassNamePrefix, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#helperClassNameSuffix} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * <strong>If not filled, then use default {@code s} value.</strong>
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#helperClassNameSuffix
     * @since 3.0
     */
    public String getHelperClassNameSuffixTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        String value = TagValueUtil.findTagValue(Store.helperClassNameSuffix, classifier, aPackage, model);
        if (StringUtils.isBlank(value)) {
            value = "Helper";
        }
        return value;
    }

    /**
     * Obtain the value of the {@link Store#generateHelper} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generateHelper
     * @since 3.0
     */
    public boolean isGenerateHelper(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generateHelper, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#generateHelperPredicates} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * <strong>If not filled, then use default {@code s} value.</strong>
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generateHelperPredicates
     * @since 3.0
     */
    public boolean isGenerateHelperPredicates(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generateHelperPredicates, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#generateHelperFunctions} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * <strong>If not filled, then use default {@code s} value.</strong>
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generateHelperFunctions
     * @since 3.0
     */
    public boolean isGenerateHelperFunctions(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generateHelperFunctions, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#generateHelperConstructors} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * <strong>If not filled, then use default {@code s} value.</strong>
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generateHelperConstructors
     * @since 3.0
     */
    public boolean isGenerateHelperConstructors(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generateHelperConstructors, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#generatePropertyChangeSupport} tag value on the given model, package or classifier.
     *
     * It will first look on the model, then and package and then in the given classifier.
     *
     * If no value found, then will use the default value of the tag value.
     *
     * @param classifier classifier to seek
     * @param aPackage   package to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generatePropertyChangeSupport
     * @since 3.0
     */
    public boolean isGeneratePropertyChangeSupport(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generatePropertyChangeSupport, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#generateNotEmptyCollections} tag value on the given model, package or classifier.
     *
     * It will first look on the model, then and package and then in the given classifier.
     *
     * If no value found, then will use the default value of the tag value.
     *
     * @param classifier classifier to seek
     * @param aPackage   package to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generateNotEmptyCollections
     * @since 3.0
     */
    public boolean isGenerateNotEmptyCollections(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generateNotEmptyCollections, classifier, aPackage, model);
    }
}
