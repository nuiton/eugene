/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.java;

import org.nuiton.eugene.models.object.ObjectModelClass;

/**
 * JavaBeanTransformer generates simple bean with pcs support
 * (and nothing else) according to the JavaBeans 1.1 norm.
 *
 * <b>This transformer is deprecated and you should use now {@link BeanTransformer} instead.</b>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.java.JavaBeanTransformer"
 * @since 2.0.2
 * @deprecated since 3.0, no more usable, replaced by {@link BeanTransformer}.
 */
public class JavaBeanTransformer extends ObjectModelTransformerToJava {

    @Override
    public void transformFromClass(ObjectModelClass input) {
        throw new IllegalStateException("You can't use any longer this transformer, please use now " + BeanTransformer.class.getName() + ".");
    }

}
