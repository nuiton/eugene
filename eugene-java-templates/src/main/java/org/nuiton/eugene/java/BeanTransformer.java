package org.nuiton.eugene.java;

/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Generates a bean and a helper class around it.
 *
 * Generates also a model initializer contract which permits you to interact with all classes of your model.
 *
 * For example:
 * <pre>
 *     GeneratedBoat
 *     Boat (extends GeneratedBoat)
 *     GeneratedBoatHelper
 *     BoatHelper (extends AbstractBoats)
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.java.BeanTransformer"
 * @since 3.0
 */
public class BeanTransformer extends ObjectModelTransformerToJava {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BeanTransformer.class);

    ImmutableMap<ObjectModelClass, String> classesNameTranslation;

    ImmutableMap<ObjectModelClass, String> helpersNameTranslation;
    ImmutableSet<ObjectModelClass> classes;

    ImmutableSet<ObjectModelClass> helpers;
    protected boolean useJava8;

    protected final EugeneJavaTagValues javaTemplatesTagValues;
    protected final BeanTransformerTagValues beanTagValues;

    public BeanTransformer() {
        javaTemplatesTagValues = new EugeneJavaTagValues();
        beanTagValues = new BeanTransformerTagValues();
    }

    @Override
    public void transformFromModel(ObjectModel model) {
        super.transformFromModel(model);

        useJava8 = javaTemplatesTagValues.isUseJava8(model);
        ImmutableMap.Builder<ObjectModelClass, String> classesNameTranslationBuilder = new ImmutableMap.Builder<>();
        ImmutableMap.Builder<ObjectModelClass, String> helpersNameTranslationBuilder = new ImmutableMap.Builder<>();
        ImmutableSet.Builder<ObjectModelClass> classesBuilder = new ImmutableSet.Builder<>();
        ImmutableSet.Builder<ObjectModelClass> helpersBuilder = new ImmutableSet.Builder<>();

        for (ObjectModelClass aClass : model.getClasses()) {

            ObjectModelPackage aPackage = model.getPackage(aClass.getPackageName());
            if (javaTemplatesTagValues.isBean(aClass, aPackage)) {

                classesBuilder.add(aClass);

                String classNamePrefix = beanTagValues.getClassNamePrefixTagValue(aClass, aPackage, model);
                String classNameSuffix = beanTagValues.getClassNameSuffixTagValue(aClass, aPackage, model);

                String generateName = generateName(classNamePrefix, aClass.getName(), classNameSuffix);
                classesNameTranslationBuilder.put(aClass, generateName);

                boolean canGenerateHelper = beanTagValues.isGenerateHelper(aClass, aPackage, model);

                if (canGenerateHelper) {

                    helpersBuilder.add(aClass);

                    String helperNamePrefix = beanTagValues.getHelperClassNamePrefixTagValue(aClass, aPackage, model);
                    String helperNameSuffix = beanTagValues.getHelperClassNameSuffixTagValue(aClass, aPackage, model);

                    String generateHelperName = generateName(helperNamePrefix, aClass.getName(), helperNameSuffix);
                    helpersNameTranslationBuilder.put(aClass, generateHelperName);

                }

            }
        }

        classes = classesBuilder.build();
        helpers = helpersBuilder.build();
        classesNameTranslation = classesNameTranslationBuilder.build();
        helpersNameTranslation = helpersNameTranslationBuilder.build();

        ImmutableMap<String, ObjectModelClass> beanClassesByFqn = Maps.uniqueIndex(classes, new Function<ObjectModelClass, String>() {

            @Override
            public String apply(ObjectModelClass input) {
                return input.getQualifiedName();
            }
        });
        List<String> beanClassesFqn = new ArrayList<>(beanClassesByFqn.keySet());
        Collections.sort(beanClassesFqn);

        String defaultPackageName = getDefaultPackageName();

        String modelBeanInitializeClassName = model.getName() + "ModelInitializer";
        boolean generateModelInitializer = !getResourcesHelper().isJavaFileInClassPath(defaultPackageName + "." + modelBeanInitializeClassName);
        if (generateModelInitializer) {

            ObjectModelInterface anInterface = createInterface(modelBeanInitializeClassName, defaultPackageName);

            addOperation(anInterface, "start", "void");
            addOperation(anInterface, "end", "void");

            for (String fqn : beanClassesFqn) {
                ObjectModelClass beanClass = beanClassesByFqn.get(fqn);
                String beanName = classesNameTranslation.get(beanClass);
                addImport(anInterface, beanName);
                addOperation(anInterface, "init" + beanName, "void");

            }
        }

        String modelInitializerRunnerClassName = model.getName() + "ModelInitializerRunner";
        boolean generateInitializerRunnerClassName = !getResourcesHelper().isJavaFileInClassPath(defaultPackageName + "." + modelInitializerRunnerClassName);
        if (generateInitializerRunnerClassName) {

            ObjectModelClass aClass = createClass(modelInitializerRunnerClassName, defaultPackageName);

            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.append(""
/*{
        initializer.start();}*/
            );
            for (String fqn : beanClassesFqn) {
                ObjectModelClass beanClass = beanClassesByFqn.get(fqn);
                String beanName = classesNameTranslation.get(beanClass);
                addImport(aClass, beanName);
                bodyBuilder.append(""
/*{
        initializer.init<%=beanName%>();}*/
                );

            }

            bodyBuilder.append(""
/*{
        initializer.end();}*/
            );
            ObjectModelOperation operation = addOperation(aClass, "init", "void", ObjectModelJavaModifier.STATIC);
            addParameter(operation, modelBeanInitializeClassName, "initializer");
            setOperationBody(operation, bodyBuilder.toString());
        }

    }

    @Override
    public void transformFromClass(ObjectModelClass input) {

        ObjectModelPackage aPackage = getPackage(input);

        if (classes.contains(input)) {

            String prefix = getConstantPrefix(input);
            setConstantPrefix(prefix);

            String className = classesNameTranslation.get(input);
            String generatedClassName = "Generated" + className;

            boolean generateClass = notFoundInClassPath(input, className);
            if (generateClass) {
                generateClass(input, className, generatedClassName);
            }

            boolean generateGeneratedClass = canGenerateAbstractClass(input, generatedClassName);
            if (generateGeneratedClass) {
                generateGeneratedClass(aPackage, input, generatedClassName);
            }

            boolean generateHelper = helpers.contains(input);
            if (generateHelper) {

                String helperClassName = helpersNameTranslation.get(input);
                String generatedHelperClassName = "Generated" + helperClassName;

                if (notFoundInClassPath(input, helperClassName)) {

                    generateHelper(input, generatedHelperClassName, helperClassName);
                }

                if (canGenerateAbstractClass(input, generatedHelperClassName)) {

                    generateGeneratedHelper(aPackage, input, className, generatedHelperClassName);
                }

            }

        }

    }

    protected ObjectModelClass generateClass(ObjectModelClass input,
                                             String className,
                                             String abstractClassName) {

        ObjectModelClass output;

        if (input.isAbstract()) {
            output = createAbstractClass(className, input.getPackageName());
        } else {
            output = createClass(className, input.getPackageName());
        }

        setSuperClass(output, abstractClassName);

        if (log.isDebugEnabled()) {
            log.debug("will generate " + output.getQualifiedName());
        }

        addSerializable(input, output, true);

        return output;
    }

    protected ObjectModelClass generateGeneratedClass(ObjectModelPackage aPackage,
                                                      ObjectModelClass input,
                                                      String className) {

        String superClass = null;

        // test if a super class has bean stereotype
        boolean superClassIsBean = false;
        Collection<ObjectModelClass> superclasses = input.getSuperclasses();
        if (CollectionUtils.isNotEmpty(superclasses)) {
            for (ObjectModelClass superclass : superclasses) {
                superClassIsBean = classes.contains(superclass);
                if (superClassIsBean) {
                    superClass = superclass.getPackageName() + "." + classesNameTranslation.get(superclass);
                    break;
                }
                superClass = superclass.getQualifiedName();
            }
        }

        if (!superClassIsBean) {

            // try to find a super class by tag-value
            superClass = beanTagValues.getSuperClassTagValue(input, aPackage, model);
            if (superClass != null) {

                // will act as if super class is a bean
                superClassIsBean = true;
            }
        }

        ObjectModelClass output;

        output = createAbstractClass(className, input.getPackageName());

        if (superClass != null) {
            setSuperClass(output, superClass);
        }
        if (log.isDebugEnabled()) {
            log.debug("will generate " + output.getQualifiedName());
        }

        boolean serializableFound;

        serializableFound = addInterfaces(input, output, null);

        generateI18nBlockAndConstants(aPackage, input, output);

        addSerializable(input, output, serializableFound || superClassIsBean);

        // Get available properties
        List<ObjectModelAttribute> properties = getProperties(input);

        boolean usePCS = beanTagValues.isGeneratePropertyChangeSupport(input, aPackage, model);
        boolean generateBooleanGetMethods = eugeneTagValues.isGenerateBooleanGetMethods(input, aPackage, model);
        boolean generateNotEmptyCollections = beanTagValues.isGenerateNotEmptyCollections(input, aPackage, model);

        // Add properties field + javabean methods
        for (ObjectModelAttribute attr : properties) {

            createProperty(output,
                           attr,
                           usePCS,
                           generateBooleanGetMethods,
                           generateNotEmptyCollections);
        }

        if (!superClassIsBean) {
            addDefaultMethodForNoneBeanSuperClass(output, usePCS, properties);
        }
        return output;
    }

    protected void generateHelper(ObjectModelClass aClass, String abstractClassName, String defaultClassName) {

        String packageName = aClass.getPackageName();

        ObjectModelClass output = createClass(defaultClassName, packageName);
        setSuperClass(output, packageName + "." + abstractClassName);
        if (log.isDebugEnabled()) {
            log.debug("will generate " + output.getQualifiedName());
        }

    }

    protected void generateGeneratedHelper(ObjectModelPackage aPackage,
                                           ObjectModelClass aClass,
                                           String typeName,
                                           String abstractClassName) {

        ObjectModelClass output = createAbstractClass(abstractClassName, aPackage.getName());
        String superClassName = getGeneratedHelperSuperClassName(aPackage, aClass);

        if (StringUtils.isNotBlank(superClassName)) {
            setSuperClass(output, superClassName);
        }

        if (log.isDebugEnabled()) {
            log.debug("will generate " + output.getQualifiedName());
        }

        addImport(output, Binder.class);
        addImport(output, BinderFactory.class);

        ObjectModelOperation operation = addOperation(
                output,
                "typeOf" + typeName,
                "<BeanType extends " + typeName + "> Class<BeanType>",
                ObjectModelJavaModifier.STATIC,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return (Class<BeanType>) <%=typeName%>.class;
    }*/
        );

        boolean generateConstructors = beanTagValues.isGenerateHelperConstructors(aClass, aPackage, model) && !aClass.isAbstract();
        if (generateConstructors) {
            generateGeneratedHelperConstructors(output, typeName);
        }

        generateGeneratedHelperCopyMethods(output, typeName);

        boolean generatePredicates = beanTagValues.isGenerateHelperPredicates(aClass, aPackage, model);
        if (generatePredicates) {
            generateGeneratedHelperPredicates(aClass, output, typeName);
        }

        boolean generateFunctions = beanTagValues.isGenerateHelperFunctions(aClass, aPackage, model);
        if (generateFunctions) {
            generateGeneratedHelperFunctions(aClass, output, typeName);
        }

    }

    protected void generateGeneratedHelperConstructors(ObjectModelClass output, String typeName) {

        ObjectModelOperation operation = addOperation(
                output,
                "new" + typeName,
                typeName,
                ObjectModelJavaModifier.STATIC,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return new <%=typeName%>();
    }*/
        );

        operation = addOperation(
                output,
                "new" + typeName,
                "<BeanType extends " + typeName + "> BeanType",
                ObjectModelJavaModifier.STATIC,
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "BeanType", "source");
        setOperationBody(operation, ""
    /*{
        Class<BeanType> sourceType = typeOf<%=typeName%>();
        Binder<BeanType, BeanType> binder = BinderFactory.newBinder(sourceType);
        BeanType result = new<%=typeName%>(source, binder);
        return result;
    }*/
        );

        operation = addOperation(
                output,
                "new" + typeName,
                "<BeanType extends " + typeName + "> BeanType",
                ObjectModelJavaModifier.STATIC,
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "BeanType", "source");
        addParameter(operation, "Binder<BeanType, BeanType>", "binder");
        setOperationBody(operation, ""
    /*{
        BeanType result = (BeanType) new<%=typeName%>();
        binder.copy(source, result);
        return result;
    }*/
        );

    }

    protected void generateGeneratedHelperCopyMethods(ObjectModelClass output, String typeName) {

        ObjectModelOperation operation = addOperation(
                output,
                "copy" + typeName,
                "<BeanType extends " + typeName + "> void",
                ObjectModelJavaModifier.STATIC,
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "BeanType", "source");
        addParameter(operation, "BeanType", "target");
        setOperationBody(operation, ""
    /*{
        Class<BeanType> sourceType = typeOf<%=typeName%>();
        Binder<BeanType, BeanType> binder = BinderFactory.newBinder(sourceType);
        binder.copy(source, target);
    }*/
        );

        operation = addOperation(
                output,
                "copy" + typeName,
                "<BeanType extends " + typeName + "> void",
                ObjectModelJavaModifier.STATIC,
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "BeanType", "source");
        addParameter(operation, "BeanType", "target");
        addParameter(operation, "Binder<BeanType, BeanType>", "binder");
        setOperationBody(operation, ""
    /*{
        binder.copy(source, target);
    }*/
        );

    }

    protected void generateGeneratedHelperPredicates(ObjectModelClass input, ObjectModelClass output, String typeName) {

        boolean atLeastOnePropertyFound = false;
        for (ObjectModelAttribute attribute : getProperties(input)) {

            boolean multiple = JavaGeneratorUtil.isNMultiplicity(attribute);

            if (multiple) {
                continue;
            }

            atLeastOnePropertyFound = true;
            String attrName = getAttributeName(attribute);
            String attrType = getAttributeTypeWithGeneric(attribute);
            addImport(output, attrType);

            String simpleType = JavaGeneratorUtil.getSimpleName(attrType);

            String capitalizeAttrName = JavaGeneratorUtil.capitalizeJavaBeanPropertyName(attrName);
            String newPreficateMethodName = "new" + capitalizeAttrName + "Predicate";

            ObjectModelOperation operation;


            operation = addOperation(
                    output,
                    newPreficateMethodName,
                    "<BeanType extends " + typeName + "> Predicate<BeanType>",
                    ObjectModelJavaModifier.STATIC,
                    ObjectModelJavaModifier.PUBLIC
            );
            addParameter(operation, simpleType, attrName);
            String getterName = getGetterName(attribute, attrName);

            if (useJava8) {
                setOperationBody(operation, ""
    /*{
        return o -> Objects.equals(<%=attrName%>, o.<%=getterName%>());

    }*/
                );
            } else {
                setOperationBody(operation, ""
    /*{
        final <%=simpleType%> $tmp = <%=attrName%>;
        return new Predicate<BeanType>() {

            @Override
            public boolean apply(BeanType input) {
                return Objects.equal($tmp, input.<%=getterName%>());
            }
        };

    }*/
                );
            }

            operation = addOperation(
                    output,
                    "filterBy" + capitalizeAttrName,
                    "<BeanType extends " + typeName + "> List<BeanType>",
                    ObjectModelJavaModifier.STATIC,
                    ObjectModelJavaModifier.PUBLIC
            );
            addParameter(operation, "Collection<BeanType>", "$source");
            addParameter(operation, simpleType, attrName);

            if (useJava8) {
                setOperationBody(operation, ""
    /*{
        return $source.stream().filter(<%=newPreficateMethodName%>(<%=attrName%>)).collect(Collectors.toList());
    }*/
                );
            } else {
                addImport(output, Collection.class);
                addImport(output, List.class);
                addImport(output, Lists.class);
                setOperationBody(operation, ""
    /*{
        return Lists.newArrayList(Iterables.filter($source, <%=newPreficateMethodName%>(<%=attrName%>)));
    }*/
                );
            }
        }

        if (atLeastOnePropertyFound) {
            if (useJava8) {

                addImport(output, Collection.class);
                addImport(output, List.class);
                addImport(output, "java.util.Objects");
                addImport(output, "java.util.function.Predicate");
                addImport(output, "java.util.stream.Collectors");
            } else {
                addImport(output, com.google.common.base.Objects.class);
                addImport(output, com.google.common.base.Predicate.class);
                addImport(output, com.google.common.collect.Iterables.class);
            }
            addImport(output, Iterable.class);
        }

    }

    protected void generateGeneratedHelperFunctions(ObjectModelClass input, ObjectModelClass output, String typeName) {

        boolean atLeastOnePropertyFound = false;
        for (ObjectModelAttribute attribute : getProperties(input)) {

            boolean multiple = JavaGeneratorUtil.isNMultiplicity(attribute);

            if (multiple) {
                continue;
            }

            atLeastOnePropertyFound = true;

            String attrName = getAttributeName(attribute);
            String attrType = getAttributeTypeWithGeneric(attribute);
            addImport(output, attrType);

            String simpleType = JavaGeneratorUtil.getSimpleName(attrType);
            simpleType = wrapPrimitiveType(simpleType);
            String capitalizeAttrName = JavaGeneratorUtil.capitalizeJavaBeanPropertyName(attrName);
            String getterName = getGetterName(attribute, attrName);

            String newFunctionMethodName = "new" + capitalizeAttrName + "Function";
            String getFunctionMethodName = "get" + capitalizeAttrName + "Function";
            String functionTypeName = "Function<BeanType, " + simpleType + ">";

            String functionFieldName = JavaGeneratorUtil.convertVariableNameToConstantName(capitalizeAttrName + "Function");
            addAttribute(
                    output,
                    functionFieldName,
                    "Function<" + typeName + ", " + simpleType + ">",
                    useJava8 ? typeName + "::" + getterName : newFunctionMethodName + "()",
                    ObjectModelJavaModifier.FINAL,
                    ObjectModelJavaModifier.STATIC,
                    useJava8 ? ObjectModelJavaModifier.PUBLIC : ObjectModelJavaModifier.PROTECTED
            );

            if (!useJava8) {
                ObjectModelOperation operation = addOperation(
                        output,
                        getFunctionMethodName,
                        "<BeanType extends " + typeName + "> " + functionTypeName,
                        ObjectModelJavaModifier.STATIC,
                        ObjectModelJavaModifier.PUBLIC
                );


                setOperationBody(operation, ""
    /*{
        return (<%=functionTypeName%>) <%=functionFieldName%>;

    }*/
                );

                operation = addOperation(
                        output,
                        newFunctionMethodName,
                        "<BeanType extends " + typeName + "> " + functionTypeName,
                        ObjectModelJavaModifier.STATIC,
                        ObjectModelJavaModifier.PUBLIC
                );

                setOperationBody(operation, ""
/*{
    return new <%=functionTypeName%>() {

        @Override
        public <%=simpleType%> apply(BeanType input) {
            return input.<%=getterName%>();
        }
    };

}*/
                );
            }

            ObjectModelOperation operation = addOperation(
                    output,
                    "uniqueIndexBy" + capitalizeAttrName,
                    "<BeanType extends " + typeName + "> ImmutableMap<" + simpleType + ", BeanType>",
                    ObjectModelJavaModifier.STATIC,
                    ObjectModelJavaModifier.PUBLIC
            );
            addParameter(operation, "Iterable<BeanType>", "$source");
            if (useJava8) {
                setOperationBody(operation, ""
    /*{
        return Maps.uniqueIndex($source, <%=functionFieldName%>::apply);
    }*/
                );
            } else {
                setOperationBody(operation, ""
    /*{
        return Maps.uniqueIndex($source, <%=functionFieldName%>);
    }*/
                );
            }
        }

        if (atLeastOnePropertyFound) {
            if (useJava8) {
                addImport(output, "java.util.function.Function");
                addImport(output, "java.util.Objects");
            } else {
                addImport(output, Function.class);
                addImport(output, com.google.common.collect.Iterables.class);
                addImport(output, com.google.common.base.Objects.class);
            }

            addImport(output, ImmutableMap.class);
            addImport(output, Iterable.class);
            addImport(output, Maps.class);
        }

    }

    protected String getGeneratedHelperSuperClassName(ObjectModelPackage aPackage, ObjectModelClass aClass) {
        String superClassName = null;

        // test if a super class has bean stereotype
        boolean superClassIsBean = false;
        Collection<ObjectModelClass> superclasses = aClass.getSuperclasses();
        if (CollectionUtils.isNotEmpty(superclasses)) {
            for (ObjectModelClass superclass : superclasses) {
                superClassIsBean = helpers.contains(superclass);
                if (superClassIsBean) {
                    superClassName = superclass.getPackageName() + "." + helpersNameTranslation.get(superclass);
                    break;
                }
                superClassName = superclass.getQualifiedName();
            }
        }

        if (!superClassIsBean) {

            // try to find a super class by tag-value
            superClassName = beanTagValues.getHelperSuperClassTagValue(aClass, aPackage, model);

        }
        return superClassName;
    }

    protected String getAttributeType(ObjectModelAttribute attr) {
        String attrType = attr.getType();
        if (attr.hasAssociationClass()) {
            attrType = attr.getAssociationClass().getName();
        }
        return getAttributeType(attrType);
    }

    protected String getAttributeType(String attrType) {
        if (!JavaGeneratorUtil.isPrimitiveType(attrType)) {
            boolean hasClass = model.hasClass(attrType);
            if (hasClass) {
                ObjectModelClass attributeClass = model.getClass(attrType);
                String attributeType = classesNameTranslation.get(attributeClass);
                if (attributeType != null) {
                    attrType = attributeClass.getPackageName() + "." + attributeType;
                }
            }
        }
        return attrType;
    }

    protected boolean notFoundInClassPath(ObjectModelClass input, String className) {
        String fqn = input.getPackageName() + "." + className;
        boolean inClassPath = getResourcesHelper().isJavaFileInClassPath(fqn);
        return !inClassPath;
    }

    protected void createProperty(ObjectModelClass output,
                                  ObjectModelAttribute attr,
                                  boolean usePCS,
                                  boolean generateBooleanGetMethods,
                                  boolean generateNotEmptyCollections) {

        String attrName = getAttributeName(attr);
        String attrType = getAttributeTypeWithGeneric(attr);

        boolean multiple = JavaGeneratorUtil.isNMultiplicity(attr);

        String constantName = getConstantName(attrName);
        String simpleType = JavaGeneratorUtil.getSimpleName(attrType);

        if (multiple) {

            createGetChildMethod(output,
                                 attrName,
                                 attrType,
                                 simpleType
            );

            createIsEmptyMethod(output, attrName);

            createSizeMethod(output, attrName);

            createAddChildMethod(output,
                                 attrName,
                                 attrType,
                                 constantName,
                                 usePCS
            );

            createAddAllChildrenMethod(output,
                                       attrName,
                                       attrType,
                                       constantName,
                                       usePCS
            );

            createRemoveChildMethod(output,
                                    attrName,
                                    attrType,
                                    constantName,
                                    usePCS
            );

            createRemoveAllChildrenMethod(output,
                                          attrName,
                                          attrType,
                                          constantName,
                                          usePCS
            );

            createContainsChildMethod(output,
                                      attrName,
                                      attrType,
                                      constantName,
                                      usePCS
            );

            createContainsAllChildrenMethod(output,
                                            attrName,
                                            attrType,
                                            constantName
            );

            // Change type for Multiple attribute
            attrType = JavaGeneratorUtil.getAttributeInterfaceType(attr, getAttributeTypeWithGeneric(attr), true);
            simpleType = JavaGeneratorUtil.getSimpleName(attrType);
        }

        boolean booleanProperty = JavaGeneratorUtil.isBooleanPrimitive(attr);

        if (multiple) {

            String collectionImplementationType = JavaGeneratorUtil.getAttributeImplementationType(attr, getAttributeTypeWithGeneric(attr), true);

            // creates a getXXX (multiple) method
            createGetMethod(output,
                            attrName,
                            attrType,
                            JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX,
                            generateNotEmptyCollections,
                            collectionImplementationType
            );

        } else {

            if (booleanProperty) {

                // creates a isXXX method
                createGetMethod(output,
                                attrName,
                                attrType,
                                JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX
                );
            }

            if (!booleanProperty || generateBooleanGetMethods) {

                // creates a getXXX method
                createGetMethod(output,
                                attrName,
                                attrType,
                                JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX
                );

            }


        }

        createSetMethod(output,
                        attrName,
                        attrType,
                        simpleType,
                        constantName,
                        usePCS
        );

        // Add attribute to the class
        addAttribute(output,
                     attrName,
                     attrType,
                     "",
                     ObjectModelJavaModifier.PROTECTED
        );

    }

    protected List<ObjectModelAttribute> getProperties(ObjectModelClass input) {
        List<ObjectModelAttribute> attributes =
                (List<ObjectModelAttribute>) input.getAttributes();

        List<ObjectModelAttribute> attrs =
                new ArrayList<>();
        for (ObjectModelAttribute attr : attributes) {
            if (attr.isNavigable()) {

                // only keep navigable attributes
                attrs.add(attr);
            }
        }
        return attrs;
    }

    protected void createGetMethod(ObjectModelClass output,
                                   String attrName,
                                   String attrType,
                                   String methodPrefix,
                                   boolean generateLayzCode,
                                   String collectionImplementationType) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName(methodPrefix, attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        if (generateLayzCode) {
            addImport(output, collectionImplementationType);
            String implementationSimpleType = JavaGeneratorUtil.getSimpleName(collectionImplementationType);
            setOperationBody(operation, ""
/*{
    if (<%=attrName%> == null) {
        <%=attrName%> = new <%=implementationSimpleType%>();
    }
    return <%=attrName%>;
}*/
            );
        } else {
            setOperationBody(operation, ""
/*{
    return <%=attrName%>;
}*/
            );
        }

    }

    protected void createGetMethod(ObjectModelClass output,
                                   String attrName,
                                   String attrType,
                                   String methodPrefix) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName(methodPrefix, attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%>;
    }*/
        );
    }

    protected void createGetChildMethod(ObjectModelClass output,
                                        String attrName,
                                        String attrType,
                                        String simpleType) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("get", attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "int", "index");
        setOperationBody(operation, ""
    /*{
        <%=simpleType%> o = getChild(<%=attrName%>, index);
        return o;
    }*/
        );
    }

    protected void createIsEmptyMethod(ObjectModelClass output,
                                       String attrName) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("is", attrName) + "Empty",
                boolean.class,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%> == null || <%=attrName%>.isEmpty();
    }*/
        );
    }

    protected void createSizeMethod(ObjectModelClass output,
                                    String attrName) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("size", attrName),
                int.class,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%> == null ? 0 : <%=attrName%>.size();
    }*/
        );
    }

    protected void createAddChildMethod(ObjectModelClass output,
                                        String attrName,
                                        String attrType,
                                        String constantName,
                                        boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("add", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);

        String methodName = getJavaBeanMethodName("get", attrName);
        StringBuilder buffer = new StringBuilder(""
    /*{
        <%=methodName%>().add(<%=attrName%>);
    }*/
        );
        if (usePCS) {
            buffer.append(""
    /*{    firePropertyChange(<%=constantName%>, null, <%=attrName%>);
    }*/
            );
        }
        setOperationBody(operation, buffer.toString());
    }

    protected void createAddAllChildrenMethod(ObjectModelClass output,
                                              String attrName,
                                              String attrType,
                                              String constantName,
                                              boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("addAll", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "java.util.Collection<" + attrType + ">", attrName);

        String methodName = getJavaBeanMethodName("get", attrName);
        StringBuilder buffer = new StringBuilder(""
    /*{
        <%=methodName%>().addAll(<%=attrName%>);
    }*/
        );
        if (usePCS) {
            buffer.append(""
    /*{    firePropertyChange(<%=constantName%>, null, <%=attrName%>);
    }*/
            );
        }
        setOperationBody(operation, buffer.toString());
    }

    protected void createRemoveChildMethod(ObjectModelClass output,
                                           String attrName,
                                           String attrType,
                                           String constantName,
                                           boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("remove", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);
        String methodName = getJavaBeanMethodName("get", attrName);
        StringBuilder buffer = new StringBuilder();
        buffer.append(""
    /*{
        boolean removed = <%=methodName%>().remove(<%=attrName%>);}*/
        );

        if (usePCS) {
            buffer.append(""
    /*{
        if (removed) {
            firePropertyChange(<%=constantName%>, <%=attrName%>, null);
        }}*/
            );
        }
        buffer.append(""
    /*{
        return removed;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createRemoveAllChildrenMethod(ObjectModelClass output,
                                                 String attrName,
                                                 String attrType,
                                                 String constantName,
                                                 boolean usePCS) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("removeAll", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "java.util.Collection<" + attrType + ">", attrName);
        StringBuilder buffer = new StringBuilder();
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean  removed = <%=methodName%>().removeAll(<%=attrName%>);}*/
        );

        if (usePCS) {
            buffer.append(""
    /*{
        if (removed) {
            firePropertyChange(<%=constantName%>, <%=attrName%>, null);
        }}*/
            );
        }
        buffer.append(""
    /*{
        return removed;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createContainsChildMethod(ObjectModelClass output,
                                             String attrName,
                                             String attrType,
                                             String constantName,
                                             boolean usePCS) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("contains", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);
        StringBuilder buffer = new StringBuilder();
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean contains = <%=methodName%>().contains(<%=attrName%>);
        return contains;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createContainsAllChildrenMethod(ObjectModelClass output,
                                                   String attrName,
                                                   String attrType,
                                                   String constantName) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("containsAll", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "java.util.Collection<" + attrType + ">", attrName);
        StringBuilder buffer = new StringBuilder();
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean  contains = <%=methodName%>().containsAll(<%=attrName%>);
        return contains;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createSetMethod(ObjectModelClass output,
                                   String attrName,
                                   String attrType,
                                   String simpleType,
                                   String constantName,
                                   boolean usePCS) {
        boolean booleanProperty = GeneratorUtil.isBooleanPrimitive(simpleType);
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("set", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);

        if (usePCS) {
            String methodPrefix = JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;
            if (booleanProperty) {
                methodPrefix = JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX;
            }
            String methodName = getJavaBeanMethodName(methodPrefix, attrName);
            setOperationBody(operation, ""
    /*{
        <%=simpleType%> oldValue = <%=methodName%>();
        this.<%=attrName%> = <%=attrName%>;
        firePropertyChange(<%=constantName%>, oldValue, <%=attrName%>);
    }*/
            );
        } else {
            setOperationBody(operation, ""
    /*{
        this.<%=attrName%> = <%=attrName%>;
    }*/
            );
        }
    }

    protected void addSerializable(ObjectModelClass input,
                                   ObjectModelClass output,
                                   boolean interfaceFound) {
        if (!interfaceFound) {
            addInterface(output, Serializable.class);
        }

        // Generate the serialVersionUID
        long serialVersionUID = JavaGeneratorUtil.generateSerialVersionUID(input);

        addConstant(output,
                    JavaGeneratorUtil.SERIAL_VERSION_UID,
                    "long",
                    serialVersionUID + "L",
                    ObjectModelJavaModifier.PRIVATE
        );
    }

    /**
     * Add all interfaces defines in input class and returns if
     * {@link Serializable} interface was found.
     *
     * @param input  the input model class to process
     * @param output the output generated class
     * @return {@code true} if {@link Serializable} was found from input,
     * {@code false} otherwise
     */
    protected boolean addInterfaces(ObjectModelClass input,
                                    ObjectModelClassifier output,
                                    String extraInterfaceName) {
        boolean foundSerializable = false;
        Set<String> added = new HashSet<>();
        for (ObjectModelInterface parentInterface : input.getInterfaces()) {
            String fqn = parentInterface.getQualifiedName();
            added.add(fqn);
            addInterface(output, fqn);
            if (Serializable.class.getName().equals(fqn)) {
                foundSerializable = true;
            }
        }
        if (extraInterfaceName != null && !added.contains(extraInterfaceName)) {
            addInterface(output, extraInterfaceName);
        }
        return foundSerializable;
    }

    protected void createPropertyChangeSupport(ObjectModelClass output) {

        addAttribute(output,
                     "pcs",
                     PropertyChangeSupport.class,
                     "new PropertyChangeSupport(this)",
                     ObjectModelJavaModifier.PROTECTED,
                     ObjectModelJavaModifier.FINAL,
                     ObjectModelJavaModifier.TRANSIENT
        );

        // Add PropertyListener

        ObjectModelOperation operation;

        operation = addOperation(output,
                                 "addPropertyChangeListener",
                                 "void",
                                 ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.addPropertyChangeListener(listener);
    }*/
        );

        operation = addOperation(output,
                                 "addPropertyChangeListener",
                                 "void",
                                 ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.addPropertyChangeListener(propertyName, listener);
    }*/
        );

        operation = addOperation(output,
                                 "removePropertyChangeListener",
                                 "void",
                                 ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.removePropertyChangeListener(listener);
    }*/
        );

        operation = addOperation(output,
                                 "removePropertyChangeListener",
                                 "void",
                                 ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.removePropertyChangeListener(propertyName, listener);
    }*/
        );

        operation = addOperation(output,
                                 "firePropertyChange",
                                 "void",
                                 ObjectModelJavaModifier.PROTECTED
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, Object.class, "oldValue");
        addParameter(operation, Object.class, "newValue");
        setOperationBody(operation, ""
    /*{
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }*/
        );

        operation = addOperation(output,
                                 "firePropertyChange",
                                 "void",
                                 ObjectModelJavaModifier.PROTECTED
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, Object.class, "newValue");
        setOperationBody(operation, ""
    /*{
        firePropertyChange(propertyName, null, newValue);
    }*/
        );
    }

    protected void createGetChildMethod(ObjectModelClass output) {
        ObjectModelOperation getChild = addOperation(
                output,
                "getChild", "<T> T",
                ObjectModelJavaModifier.PROTECTED
        );
        addImport(output, List.class);

        addParameter(getChild, "java.util.Collection<T>", "childs");
        addParameter(getChild, "int", "index");
        setOperationBody(getChild, ""
/*{
        T result = null;
        if (childs != null) {
            if (childs instanceof List) {
                if (index < childs.size()) {
                    result = ((List<T>) childs).get(index);
                }
            } else {
                int i = 0;
                for (T o : childs) {
                    if (index == i) {
                        result = o;
                        break;
                    }
                    i++;
                }
            }
        }
        return result;
}*/
        );
    }

    protected void generateI18nBlockAndConstants(ObjectModelPackage aPackage,
                                                 ObjectModelClass input,
                                                 ObjectModelClassifier output) {

        String i18nPrefix = eugeneTagValues.getI18nPrefixTagValue(input,
                                                                  aPackage,
                                                                  model);
        if (!StringUtils.isEmpty(i18nPrefix)) {
            generateI18nBlock(input, output, i18nPrefix);
        }

        String prefix = getConstantPrefix(input);

        setConstantPrefix(prefix);

        Set<String> constantNames = addConstantsFromDependency(input, output);

        // Add properties constant
        for (ObjectModelAttribute attr : getProperties(input)) {

            createPropertyConstant(output, attr, prefix, constantNames);
        }
    }

    protected void addDefaultMethodForNoneBeanSuperClass(ObjectModelClass output,
                                                         boolean usePCS,
                                                         List<ObjectModelAttribute> properties) {


        if (usePCS) {

            // Add property change support
            createPropertyChangeSupport(output);
        }

        boolean hasAMultipleProperty = containsMultiple(properties);

        // Add helper operations
        if (hasAMultipleProperty) {

            // add getChild methods
            createGetChildMethod(output);
        }
    }

    protected String wrapPrimitiveType(String attrType) {
        if (JavaGeneratorUtil.isPrimitiveType(attrType)) {
            attrType = JavaGeneratorUtil.getPrimitiveWrapType(attrType);
        }
        return attrType;
    }

    protected String getGetterName(ObjectModelAttribute attribute, String attrName) {
        boolean booleanProperty = JavaGeneratorUtil.isBooleanPrimitive(attribute);
        String methodPrefix = JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;
        if (booleanProperty) {
            methodPrefix = JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX;
        }
        return getJavaBeanMethodName(methodPrefix, attrName);
    }

    protected String generateName(String prefix, String name, String suffix) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(prefix)) {
            sb.append(prefix);
        }
        sb.append(name);
        if (StringUtils.isNotEmpty(suffix)) {
            sb.append(suffix);
        }
        return sb.toString();
    }

    protected boolean canGenerateAbstractClass(ObjectModelClass aClass, String abstractClassName) {

        boolean inClassPath = !notFoundInClassPath(aClass, abstractClassName);

        if (inClassPath) {
            throw new IllegalStateException(String.format("Can't generate %s, already found in class-path, this is a generated class, you should not ovveride it.\n\nPlease remove it from class path and use the %s class instead.", aClass.getPackageName() + "." + abstractClassName, aClass));
        }

        return true;

    }

    protected void createPropertyConstant(ObjectModelClassifier output,
                                          ObjectModelAttribute attr,
                                          String prefix,
                                          Set<String> constantNames) {

        String attrName = getAttributeName(attr);

        String constantName = prefix + builder.getConstantName(attrName);

        if (!constantNames.contains(constantName)) {

            addConstant(output,
                        constantName,
                        String.class,
                        "\"" + attrName + "\"",
                        ObjectModelJavaModifier.PUBLIC
            );
        }
    }

    protected String getAttributeName(ObjectModelAttribute attr) {
        String attrName = attr.getName();
        if (attr.hasAssociationClass()) {
            String assocAttrName = JavaGeneratorUtil.getAssocAttrName(attr);
            attrName = JavaGeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
        }
        return attrName;
    }

    protected String getAttributeTypeWithGeneric(ObjectModelAttribute attr) {
        String attrType = getAttributeType(attr);
        String generic = eugeneTagValues.getAttributeGenericTagValue(attr);
        if (generic != null) {
            attrType += "<" + getAttributeType(generic) + ">";
        }
        return attrType;
    }

    protected boolean containsMultiple(List<ObjectModelAttribute> attributes) {

        boolean result = false;

        for (ObjectModelAttribute attr : attributes) {

            if (JavaGeneratorUtil.isNMultiplicity(attr)) {
                result = true;

                break;
            }

        }
        return result;
    }


}
