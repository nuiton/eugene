/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.java;


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Collection;

/**
 * JavaEnumerationTransformer generates a enumeration for enuration with
 * stereotype enumeration.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.java.JavaEnumerationTransformer"
 * @since 2.5
 */
public class JavaEnumerationTransformer extends ObjectModelTransformerToJava {

    private static final Log log =
            LogFactory.getLog(JavaEnumerationTransformer.class);

    @Override
    public void transformFromEnumeration(ObjectModelEnumeration input) {
        if (!canGenerate(input)) {

            if (log.isDebugEnabled()) {
                log.debug("Skip generation for " + input.getQualifiedName());
            }
            return;
        }

        ObjectModelEnumeration output =
                createEnumeration(input.getName(), input.getPackageName());

        if (log.isDebugEnabled()) {
            log.debug("will generate " + output.getQualifiedName());
        }

        Collection<String> literals = input.getLiterals();

        for (String literal : literals) {
            addLiteral(output, literal);
        }
    }

    protected boolean canGenerate(ObjectModelEnumeration input) {
        ObjectModelPackage aPackage = getPackage(input);
        boolean b = !EugeneCoreTagValues.isSkip(input, aPackage);
        if (b) {

            // check if not found in class-path
            b = !getResourcesHelper().isJavaFileInClassPath(input.getQualifiedName());
        }
        return b;
    }
}
