package org.nuiton.eugene.java;

/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.eugene.models.extension.tagvalue.MismatchTagValueTargetException;
import org.nuiton.eugene.models.extension.tagvalue.TagValueNotFoundException;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;

public class BeanTransformerTagValuesTest {

    protected TagValueMetadatasProvider provider;

    @Before
    public void setUp() throws Exception {
        provider = new BeanTransformerTagValues();
    }

    @Test
    public void validate() throws Exception {

        validate(BeanTransformerTagValues.Store.generateNotEmptyCollections.getName(), true, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelClass.class, ObjectModelEnumeration.class);
        validate(BeanTransformerTagValues.Store.generatePropertyChangeSupport.getName(), true, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelClass.class, ObjectModelEnumeration.class);
        validate(BeanTransformerTagValues.Store.generateNotEmptyCollections.getName(), false, ObjectModelOperation.class, ObjectModelAttribute.class);
        validate(BeanTransformerTagValues.Store.generatePropertyChangeSupport.getName(), false, ObjectModelOperation.class, ObjectModelAttribute.class);

        long l = System.nanoTime();

        validate(BeanTransformerTagValues.Store.generateNotEmptyCollections.getName() + l, false, ObjectModelPackage.class, ObjectModelOperation.class, ObjectModelAttribute.class);
        validate(BeanTransformerTagValues.Store.generatePropertyChangeSupport.getName() + l, false, ObjectModelPackage.class, ObjectModelOperation.class, ObjectModelAttribute.class);
    }


    protected void validate(String name, boolean expected, Class<?>... types) {
        for (Class<?> type : types) {
            try {
                provider.validate(name, type);
                Assert.assertTrue(expected);
            } catch (TagValueNotFoundException | MismatchTagValueTargetException e) {
                Assert.assertFalse(expected);
            }
        }
    }

}
