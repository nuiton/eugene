.. -
.. * #%L
.. * EUGene :: Java templates
.. * %%
.. * Copyright (C) 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======
Eugene
======

:Authors: tony CHEMIT
:Contact: chemit@codelutin.com
:Revision: $Revision$
:Date: $Date$

.. contents::

Templates java
==============

Ce module offre des templates pour générer des objets java, à savoir :

- des interfaces (*org.nuiton.eugene.java.JavaInterfaceTransformer*)
- des énumérations (*org.nuiton.eugene.java.JavaEnumerationTransformer*)
- des beans simples (sans surcharge possible) (*org.nuiton.eugene.java.SimpleJavaBeanTransformer*)
- des beans simples (avec surcharge possible : génération d'un abstract et d'un Impl) (*org.nuiton.eugene.java.JavaBeanTransformer*)

*TODO Finir cette documentation avant la version 3.0 en donnant un exemple complet d'utilisation.*
