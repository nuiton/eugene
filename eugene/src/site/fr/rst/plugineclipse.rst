.. -
.. * #%L
.. * EUGene :: EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=====================================
Installation du plugin LutinGenerator
=====================================

Installation du plugin LutinGenerator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce guide détail comment installer le plugin eclipse LutinGenerator via
l'update site (assitant d'instalation de plugins).

Installation
------------

Ouvrez l'assitant d'installation de plugin via le menu
(Help/Software Updates/Find and install...).

.. image:: ../images/plugin_menu.png

Sur la fenêtre suivante sélectionnez "Search for new features to install".

.. image:: ../images/plugin_typeinstall.png

Une nouvelle fenêtre s'affiche, sélectionnez "New remote site" pour ajouter le
dépot "LutinGenerator".

L'adresse du dépot à utiliser est :

::

 http://lutingenerator.labs.libre-entreprise.org/eclipse/

.. image:: ../images/plugin_newdepot.png


Sélectionnez ensuite ce dépot et cliquez sur "Finish".

Installez ensuite la dernière version disponible et redémarrez eclipse:

.. image:: ../images/plugin_choix.png


Utilisation
-----------

Pour disposer de la coloration des templates, cliquez droit sur un fichier
source java et sélectionnez "Open with > / LutinGenerator Template Editor"

.. image:: ../images/plugin_utilisation_editeur.png
