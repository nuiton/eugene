
==========
Tag values
==========

:Authors: Tony CHEMIT
:Contact: chemit@codelutin.com

.. contents::

Abstract
========

You can add some **tag values** on each element of an model.

This can be done in two ways:

- inside the zargo model
- in the optinal *model properties file* related to your model

Scope
=====

There is two main scopes for a tag value:

- on the the model, the tag value can be used for any element of your model (if the tag value accept it)
- on a specific element of the model (classifier, class, interface, enumeration, attribute, paramter, operation, ...)
  to restrict the usage of the tag value.

Note that a tag value can be override (for example put in on the global model, and for a specific class override it,
and so even on a attribute, ...).

Format
======

We describe here the format of a tag value when written in the *model properties file*.

- On a model

::

  model.tagvalue.myTagValueKey=myTagValueValue

- On a specific element of the model

::

  org.nuiton.eugene.model.Myclass.class.tagValue.myTagValueKey=myTagValueValue
  org.nuiton.eugene.model.Myclass.attribute.myAttribute.tagValue.myTagValueKey=myTagValueValue

Validation
==========

Tag values availables from modules can be validated to make their experience safer and nearly *compile safe*.

Each module which offers some tagValues can describe a report of them which describe.

Have a look for example to the report for the `EUGene core api`_ or `java templates`_.

How to write tag values
=======================

This section describe how to write safe tag values using the EUGene tag values API.

See `JavaDocs API`_ or `API Sources`_ for more details.

Once this is done, use the `EUGene tag-values-report`_ to expose them in your site::

   <build>

      <plugins>

        <plugin>
          <groupId>${project.groupId}</groupId>
          <artifactId>eugene-maven-plugin</artifactId>
          <version>${project.version}</version>
          <dependencies>
            <!-- To make your stereotypes and tag values available in the report plugin -->
            <dependency>
              <groupId>\${project.groupId}</groupId>
              <artifactId>\${project.artifactId}</artifactId>
              <version>\${project.version}</version>
            </dependency>
          </dependencies>
        </plugin>

      </plugins>
    </build>

    ...

  <reporting>
    <plugins>

      <plugin>
        <groupId>org.nuiton.eugene</groupId>
        <artifactId>eugene-maven-plugin</artifactId>
        <reportSets>
          <reportSet>
            <reports>
              <report>tag-values-report</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>

    </plugins>
  </reporting>

.. _eugene core api: ./tag-values-report.html
.. _java templates: ../eugene-java-templates/tag-values-report.html
.. _JavaDocs API: ./apidocs/index.html?org/nuiton/eugene/models/textension/agvalue/package-summary.html
.. _API Sources: ./xref/org/nuiton/eugene/models/textension/tagvalue/package-summary.html
.. _EUGene tag-values-report: ../eugene-maven-plugin/tag-values-report-mojo.html