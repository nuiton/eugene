.. -
.. * #%L
.. * EUGene :: EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======
Eugene
======

.. contents::

Origine du projet
-----------------

Eugene est né à la suite d'une recherche de générateur de code basé sur
un modèle mémoire simple qui s'est terminée par un échec.

Les projets alors étudiés étaient alors entre autres : 

- Jostraca ;
- EMF ;
- ...

Le choix de la génération de code par rapport à l'introspection a été fait car
la génération code permet de passer par l'étape compilation et donc de
validation du code généré. En effet, si le besoin était initialement porté sur
de la génération de code Java, Eugene a été pensé pour générer tout type
de code.


Côté technique
--------------

Eugene permet l'utilisation d'un ensemble de générateurs. Ces
générateurs sont abstraits de toute spécificité permettant ainsi de les adapter
en fonction des besoins.

Par défaut, Eugene propose trois implantations de ces générateurs :

- ObjectModelGenerator (génération à partir d'un modèle objet) ;
- StateModelGenerator (génération à partir d'un modèle d'états) ;
- UIModelGenerator (génération à partir d'un modèle graphique).

Chacun de ces modèles a ses propres spécificités liés à sa structure et son mode
de fonctionnement...

Cependant, ces générateurs sont inutiles sans des templates de génération. Les
templates sont les fichiers qui vont permettre de déterminer quel sera le
contenu généré en fonction du modèle initial. Grâce à NuitonProcessor_, ces
templates sont écrit avec une sytaxe proche de la syntaxe JSP en imbriquant les
portions de code Java avec les portions de code généré. Le rôle de 
NuitonProcessor_ est de transformer ces templates en remplaçant la syntaxe JSP par
la syntaxe Java correpondante. Les classes Java obtenues peuvent donc être
compilées et deviennent autonomes.

.. _NuitonProcessor: http://doc.nuiton.org/processor/

Règle ant
---------

Une tâche ant est disponible à l'adresse suivante
http://doc.nuiton.org/eugene/ant-eugene-task. Il permet l'utilisation
depuis Ant de Eugene.


Plugin maven
------------

Un plugin maven est disponnible à l'adresse suivante 
http://doc.nuiton.org/eugene/eugene-maven-plugin . Il permet l'utilisation
depuis maven de Eugene.
