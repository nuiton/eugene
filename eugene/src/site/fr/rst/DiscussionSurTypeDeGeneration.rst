.. -
.. * #%L
.. * EUGene :: EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================
Principe de la génération
=========================

Principe de la génération
~~~~~~~~~~~~~~~~~~~~~~~~~

Il y a deux façon de voir la génération. 

La première que l'on rencontre le plus
souvent génère des fichiers avec des sections à
modifier par le développeur. Ces sections sont marquées par des commentaires
que le générateur interprète pour savoir qu'il n'a pas le droit de modifier
cette partie de code.

La deuxième qui à pour principe:

- le générateur ne doit jamais modifier les fichiers de l'utilisateur.
- l'utilisateur ne doit jamais modifier le code généré

De cette façon les choses sont clairement séparé, et cela est possible sans
utilisation d'artifice perturbant le développeur grâce au nouveau langage à
objet et à l'héritage.

La grosse différence n'est pas dans les générateurs eux même mais dans les
templates de génération. Si tous les générateurs de la première catégorie
peuvent très bien servir pour faire de la génération de la seconde,
l'inverse n'est pas vrai. Car la seconde catégorie demande des générateurs
beaucoup plus simple, ce qui est d'ailleur un avantage pour la maintenance.

Code Lutin utilise un générateur du deuxième type: Eugene

Quelques générateurs de la première catégorie:

- Acceleo (Obeo)
- Pragmatic (Argia)

Les principes de Eugene
=======================

- le générateur ne doit jamais modifier les fichiers de l'utilisateur.
- l'utilisateur ne doit jamais modifier le code généré
- le modèle est toujours la source (pas de reverse, pas de modification du
  modèle par le générateur
- être le plus simple possible (simple à maintenir)
- s'appuyer sur les normes (XMI)
- pouvoir utiliser plusieurs fichiers XMI pour la même génération
- ne pas mettre dans le modèle des choses techniques (ex: type pour la base de
  données) mais dans un fichier de propriété à coté du modèle.
- avoir une couche d'abstraction du XMI pour évité la modification des
  templates si la version de XMI change (XMLObjectModel)
- s'appuyer sur un modèle mémoire simple (ObjectModel écrit spécifiquement
  car aucun modèle simple n'a été trouvé)
- s'appuyer sur un langage puissant et connu des développeurs (Java)
- ne jamais mélanger le code généré et le code utilisateur
- pouvoir générer n'importe quelle type de fichier (XML, Java, texte, ...)
- être facilement intégrable dans une phase de génération/compilation
  (task ant, plugin maven)
- être indépendant d'un outil de développement spécifique (chacun à le droit de
  choisir l'éditeur qu'il souhaite).

Défaut de la première solution
==============================

- il faut prévoir partout ou l'utilisateur pourrait souhaiter ajouter du
  code. Par exemple dans Pragmatic, on ne peut pas utiliser les imports car il
  n'y a pas de section utilisateur à cet endroit, il faut donc à chaque fois
  écrire le package lorsque l'on souhaite utiliser une classe non encore
  importée.

- le développeur voit beaucoup de code qu'il n'a pas le droit de modifier
  ce qui complexifie la vue du développeur pour rien

Défaut des générateurs le plus souvent rencontré
================================================

- Les générateurs au lieu d'utiliser un langage et façon de faire que tous
  les développeurs connaissent réinvente leur propre syntaxe et langage.

Eugene lui utilise seulement du Java et les tags JSP (<%, <%=, %>).
De cette façon le générateur reste simple pas de langage à développer et
parser. L'utilisateur qui connait Java connait le langage de template. Et
surtout l'utilisateur n'est pas limité par le langage développé
spécifiquement pour les templates.

