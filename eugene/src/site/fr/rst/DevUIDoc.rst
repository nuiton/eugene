.. -
.. * #%L
.. * EUGene :: EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=======
UIModel
=======

:Authors: Aurelie MAZELIER
:Revision: $Revision$
:Date: $Date$

.. contents::

Model UIModel
=============

En parcourant un fichier xml, il est possible de construire un objet
UIModel. Cet objet est défini par les interfaces suivantes.


Interfaces
----------

- UIModel :

  - version XML
  - root de type UIModelObject
  - le nom du package
  - liste des objets du model

- UIModelObject :

  - nom
  - type
  - un UIModelObject parent
  - un UIModel
  - une liste d'arguments
  - une liste de propriétés
  - une liste d'évènements
  - une liste d'enfants

- UIModelArgument :

  - une liste d'arguments

- UIModelProperty :

  - un nom
  - une valeur (de différents type : int, float ...)
  - un index

- UIModelEvent :

  - le nom de la addListenerMethod
  - le nom de la listenerInterface 
  - le nom de la listenerMethod
  - le nom du handler
  - le nom de la eventProperty

- UIModelChild :

  - un enfant UIModelObject 
  - la contrainte de l'enfant de type UIModelConstraint

- UIModelConstraint :

 - une valeur de type Object ou String


Implantations
-------------

Il existe deux implantations de ces interfaces.


impl
~~~~

Cette première implantation permet d'obtenir un UIModel lors
du parcours du fichier xml de type uimodel par le parser
XMLParser.


xml
~~~

Cette deuxième implantation permet d'obtenir un UIModel lors
du parcours du fichier xml de type javaxml par le parser
JavaXMLParser.


JavaXMLParser
-------------

Ce parser permet de parcourir des fichiers javaxml afin d'obtenir un
objet UIModel. Ce parser utilise dom4j.


Générateurs
===========

- Génération d'objet UIModel (UIModelGenerator). Ce générateur utilise les classes précédentes afin d'obtenir un objet UIModel en fonction des fichiers xml. A partir de cet objet UIModel, il est possible de lancer la génération. Ces générateurs sont utilisées dans Topia pour générer les fichiers java à partir de l'objet UIModel.

- Génération d'objet UIModel (UIModelGeneratorWithCapitalizeName). Ce générateur est une sous classe du premier. Il est possède une méthode permettant de mettre la première lettre en majuscule du nom d'un objet, d'une propriété...
