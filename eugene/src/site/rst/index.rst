.. -
.. * #%L
.. * EUGene :: EUGene
.. * %%
.. * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======
Eugene
======

:Authors: Arnaud THIMEL
:Contact: thimel@codelutin.com
:Revision: $Revision$
:Date: $Date$

.. contents::

Project origin
==============

Eugene was born after a research of an easy-to-use code generator
oriented on "in memory" models. But the research ended by a failure.

Analyzed projects were :

- Jostraca ;
- EMF ;
- ...

Code Generation were chosen instead of Introspection because it allows to use
the compilation processes and therefore to validate the generated code. Even if
the initial need was to generate Java source code, Eugene is able to
generate any type of code.



Technical part
==============

Eugene allows using a set of generators. These generators are abstract
of any specificity enabling to adapt them to individual needs.


By default, Eugene has two implementations of these generators :

- ObjectModelGenerator (generation from an object model) ;
- UIModelGenerator (generation from a graphic model).

Each of these models has its own specificities due to its structure and working
mode...

Anyway, these generators are useless without generation templates. Templates
are files allowing to determinate what will be the generated content according
to the initial model. Thanks to NuitonProcessor_, these templates are written
with a syntax close to the JSP syntax mixing Java and generated code. The goal
of LutinProcessor is to transform the templates by replacing JSP code by the
matching Java code. Obtained Java classes can be compiled and so become
independents.


.. _NuitonProcessor: http://doc.nuiton.org/processor/
