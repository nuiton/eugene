.. -
.. * #%L
.. * EUGene :: EUGene
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

==============
Generator Help
==============

How to create a UML model for use with Eugene
---------------------------------------------

In Poseidon,
 - Create an empty project
 - Rename root package to your application name (used by ???)
 - create a package hierarchy according to your company name and project organisation (eg : org.codelutin.jdevis.entities)
 - Create a class diagram is this package, so that classes you will define to it will belong to this package at creation.
 - Make a model of your entities :
  - All entities must be stereotyped 'entity'
  - You may use both class and interface
  - You may set several taggedValues on all items :
   - doc : documentation related to this item, used for javadoc comments (not used yet)
  - You may set several taggedValues on classes :
   - asText : OCL expression used as for toString() method generation (eg : this.firstName+" "+this.lastName)
  - You may set several taggedValues on attributes :
   - readonly : boolean that indicates if change must be made impossible on this attribute (used for setter generation)
   - mandatory : boolean that indicates if this attribute is mandatory (used to build a minimal not empty constructor)
   - defines : boolean that indicates if this attribute is needed for defining the class (used for default UIs)
   - primaryKey : boolean that indicates if this attribute is part of the primary key for the class (not used yet)
   - indexed : boolean that indicates if it must be possible to search items by this attribute (not used yet)

   
   
TopiaUI :
---------

<topiaUI xmlns="http://www.codelutin.org/topia/topiaUI" name="XXX">
  <content>
    <item>
      <containmentProperties>
        <property name="_containementPropertyName_" type="_containementPropertyType_">XXX</property>
      </containmentProperties>
      <widget name="XXX" type="_widgetType_">
        <properties>
          <property name="_propertyName_" type="_propertyType_">XXX</property>
        </properties>
        <signals>
          <signal name="_signalName_" handler="XXX"/>
        </signals>
        <content>
          <item>
            ...
          </item>
          ...
        </content>
      </widget>
    </item>
    ...
  </content>
</topiaUI>  

widgetType :
  gridBag
  panel
  scrollPane
  table
  label
  textfield
  button
          
properties :
  minimumWidth of type int
  minimumHeight of type int
  preferedWidth of type int
  preferedHeight of type int
  maximumWidth of type int
  maximumHeight of type int
  text of type String (default to "")
  editable of type boolean (default to true)
  borderType of type String = {"SoftBevelBorder", "EtchedBorder"}
  borderDepth of type String = {"LOWERED", "RAISED"}
  borderText of type String
  
containmentProperties :  
  gridX of type int (default to 0)
  gridY of type int (default to 0)
  gridWidth of type int (default to 1)
  gridHeight of type int (default to 1)
  anchor of type String = {"CENTER", "NORTH", "SOUTH", "EAST", "WEST", "NORTHEAST", "NORTHWEST", "SOUTHEAST", "SOUTHWEST"} (default to CENTER)
  fill of type String = {"HORIZONTAL", "VERTICAL", "BOTH", "NONE"} (default to BOTH)
  weightX of type double (default to 1.0)
  weightY of type double (default to 1.0)
  insetTop of type int (default to 2)
  insetBottom of type int (default to 2)
  insetLeft of type int (default to 2)
  insetRight of type int (default to 2)

