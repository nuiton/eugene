/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene;

/**
 * GeneratorException.
 *
 * Created: 21 juin 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class GeneratorException extends RuntimeException { // GeneratorException

    /** serialVersionUID */
    private static final long serialVersionUID = 5861583066249595300L;

    public GeneratorException(String msg) {
        super(msg);
    }

    public GeneratorException(String msg, Throwable e) {
        super(msg, e);
    }

} // GeneratorException

