/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene;

import org.nuiton.eugene.writer.WriterReport;

import java.util.Properties;

/**
 * Contract of a {@link Template} configuration
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.2
 */
public interface TemplateConfiguration {

    String PROP_OVERWRITE = "overwrite";

    String PROP_VERBOSE = "verbose";

    String PROP_ENCODING = "encoding";

    String PROP_CLASS_LOADER = "classLoader";

    String PROP_DEFAULT_PACKAGE = "defaultPackage";

    String PROP_LAST_MODIFIED_SOURCE = "lastModifiedSource";

    String PROP_GENERATED_PACKAGES = "generatedPackages";

    String PROP_EXCLUDE_TEMPLATES = "excludeTemplates";

    String PROP_WRITER_REPORT = "writerReport";

    /**
     * @return {@code true} if must regenerate files even if they are up to date
     */
    boolean isOverwrite();

    /** @return {@code true} if build is verbose. */
    boolean isVerbose();

    /** @return encoding to use to read and write files */
    String getEncoding();

    /** @return the classloader to use to seek for resources */
    ClassLoader getClassLoader();

    WriterReport getWriterReport();

    long getLastModifiedSource();

    Properties getProperties();

    String getProperty(String key);

    <V> V getProperty(String key, Class<V> type);

    void setProperty(String key, Object value);

}
