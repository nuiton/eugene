package org.nuiton.eugene;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.models.object.reader.XmlObjectModelReader;
import org.nuiton.eugene.models.object.reader.YamlObjectModelReader;
import org.nuiton.eugene.models.state.StateModelReader;

import java.util.Collections;
import java.util.Map;

/**
 * Help methods around model.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.3
 */
public class ModelHelper {

    /**
     * All available models (obtain by plexus, keys are plexus roles,
     * values are a instance of corresponding model).
     */
    protected Map<String, Model> models;

    /** All available writers introspects via plexus. */
    protected Map<String, ModelReader<?>> modelReaders;

    public ModelHelper(Map<String, Model> models,
                       Map<String, ModelReader<?>> modelReaders) {
        this.models = Collections.unmodifiableMap(models);
        this.modelReaders = Collections.unmodifiableMap(modelReaders);
    }

    public Model getModel(String modelType) {
        Model model = models.get(modelType.trim());
        return model;
    }

    public ModelReader getModelReader(String modelType, String type) {
        ModelReader result = null;
        for (ModelReader<?> modelReader : modelReaders.values()) {
            if (modelType.equals(modelReader.getModelType()) &&
                type.equals(modelReader.getInputType())) {
                result = modelReader;
                break;
            }
        }
        return result;
    }

    public Map<String, ModelReader<?>> getModelReaders() {
        return modelReaders;
    }

    /**
     * Define type of model known by eugene
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.6.3
     */
    public enum ModelType {

        /**
         * Object model.
         *
         * @see org.nuiton.eugene.models.object
         */
        OBJECT("objectmodel"),

        /**
         * Object model.
         *
         * @see org.nuiton.eugene.models.state
         */
        STATE("statemodel");

        private final String alias;

        ModelType(String alias) {
            this.alias = alias;
        }

        public String getAlias() {
            return alias;
        }
    }

    /**
     * Define type onf input model known by eugene.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 2.6.3
     */
    public enum ModelInputType {

        /**
         * Read object model from xml files.
         *
         * @see XmlObjectModelReader
         * @see StateModelReader
         */
        XML("xml"),

        /**
         * Read object model from yaml files.
         *
         * @see YamlObjectModelReader
         */
        YAML("yaml");

        private final String alias;

        ModelInputType(String alias) {
            this.alias = alias;
        }

        public String getAlias() {
            return alias;
        }

    }
}
