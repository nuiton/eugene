/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.writer.WriterReport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

/**
 * AbstractGenerator
 *
 * Created: 28 oct. 2009
 *
 * @param <M> type of Model
 * @author Florian Desbois - desbois@codelutin.com
 */
public abstract class AbstractGenerator<M extends Model> extends Template<M> {

    private static final Log log = LogFactory.getLog(AbstractGenerator.class);

    protected AbstractGenerator<M> parent;

    public AbstractGenerator() {
    }

    public AbstractGenerator(AbstractGenerator<M> parent) {
        setParent(parent);
    }

    public void setParent(AbstractGenerator<M> parent) {
        this.parent = parent;
    }

    @Override
    public boolean isOverwrite() {
        if (parent != null) {
            return parent.isOverwrite();
        }
        return super.isOverwrite();
    }

    @Override
    public String getEncoding() {
        if (parent != null) {
            return parent.getEncoding();
        }
        return super.getEncoding();
    }

    @Override
    public String getProperty(String name) {
        if (parent != null) {
            return parent.getProperty(name);
        }
        return super.getProperty(name);
    }

    @Override
    public long getLastModifiedSource() {
        if (parent != null) {
            return parent.getLastModifiedSource();
        }
        return super.getLastModifiedSource();
    }

    @Override
    public <V> V getProperty(String key, Class<V> type) {
        if (parent != null) {
            return parent.getConfiguration().getProperty(key, type);
        }
        return getConfiguration().getProperty(key, type);
    }

    /**
     * @param file fichier a tester
     * @return vrai si le fichier passé en parametre est plus recent que
     * les sources sur generateur.
     */
    protected boolean isNewerThanSource(File file) {
        if (log.isDebugEnabled()) {
            log.debug("source date: " + getLastModifiedSource()
                      + " file date: " + file.lastModified() + " (" + file + ")");
        }
        return file.lastModified() > getLastModifiedSource();
    }

    protected File getDestinationFile(File destDir, String filename) {
        return new File(destDir, filename);
    }

    /**
     * Test if given package is allowed for generation.
     * An element can be generated if his package is in the
     * {@link #generatedPackages} list or if {@link #generatedPackages} is null
     * or empty.
     *
     * @param packageName package name to test
     * @return generation allowed
     */
    protected boolean canGeneratePackage(String packageName) {
        boolean canGenerate = true;
        if (parent != null) {
            return parent.canGeneratePackage(packageName);
        }
        // if not generation restriction, generate everything
        List<String> generatedPackages = getGeneratedPackages();

        if (generatedPackages != null && !generatedPackages.isEmpty()) {
            canGenerate = false;

            // on doit donc tester que le package courant
            // soit est egal a un des element de la liste
            // soit commence par ca
            for (String generatedPackage : generatedPackages) {

                // cas egalité parfaites
                if (packageName != null &&
                    packageName.equals(generatedPackage)) {
                    canGenerate = true;
                }

                // cas commence par (on ajout un point à la fin)
                String localGeneratedPackage = generatedPackage;
                if (!localGeneratedPackage.endsWith(".")) {
                    localGeneratedPackage += ".";
                }
                if (packageName != null &&
                    packageName.startsWith(localGeneratedPackage)) {
                    canGenerate = true;
                }

            }
        }

        return canGenerate;
    }

    protected void write(File outputFile, MonitorWriter out) {

        if (!out.isModified()) {
            if (log.isDebugEnabled()) {
                log.debug("skip generation of " + outputFile + ", No content.");
            }
            return;
        }
        try {
            outputFile = outputFile.getCanonicalFile();

            if (!outputFile.getParentFile().exists()) {
                boolean b = outputFile.getParentFile().mkdirs();
                if (!b) {
                    throw new IllegalStateException(
                            "could not create directory " +
                            outputFile.getParentFile());
                }
            }

            WriterReport writerReport = getWriterReport();

            if (writerReport != null) {
                writerReport.addFile(getClass().getName(), outputFile, false);
            }

            try (Writer output = getWriter(outputFile)) {
                if (log.isDebugEnabled()) {
                    log.debug("Will generate file " + outputFile);
                }
                output.write(out.getBuffer().toString());
            }
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Unable to write file : " +
                         outputFile.getAbsolutePath(), eee);
            }
            throw new RuntimeException(eee);
        }
    }

    protected Writer getWriter(File outputFile) throws IOException {

        FileOutputStream stream = new FileOutputStream(outputFile);
        Writer output;
        String encoding = getEncoding();
        if (encoding != null) {
            if (log.isDebugEnabled()) {
                log.debug("Force encoding to " + encoding + " : " + this);
            }
            output = new OutputStreamWriter(stream, encoding);
        } else {
            output = new OutputStreamWriter(stream);
        }
        return output;

    }
}
