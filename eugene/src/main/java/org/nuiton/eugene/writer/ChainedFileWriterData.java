/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.writer;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Contains data to be reacted by a {@link ChainedFileWriter}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.1.3
 */
public class ChainedFileWriterData {

    File outputDirectory;

    Map<File, List<File>> filesByRoot;

    Map<File, List<File>> resourcesByFile;


    public File getOutputDirectory() {
        return outputDirectory;
    }

    public Map<File, List<File>> getFilesByRoot() {
        return filesByRoot;
    }

    public Map<File, List<File>> getResourcesByFile() {
        return resourcesByFile;
    }

    public void setResourcesByFile(Map<File, List<File>> resourcesByFile) {
        this.resourcesByFile = resourcesByFile;
    }

    public void setOutputDirectory(File outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public void setFilesByRoot(Map<File, List<File>> filesByRoot) {
        this.filesByRoot = filesByRoot;
    }
}
