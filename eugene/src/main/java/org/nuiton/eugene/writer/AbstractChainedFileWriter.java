/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Abstract implementation of the {@link ChainedFileWriter}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.0
 */
public abstract class AbstractChainedFileWriter implements ChainedFileWriter {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(AbstractChainedFileWriter.class);

    /** entries to treate with this writer */
    protected List<ChainedFileWriterEntry> entries;

    /** previous writer (can be null) */
    protected ChainedFileWriter previousWriter;

    /** next writer (can be null) */
    protected ChainedFileWriter nextWriter;

    /**
     * universe of authorized properties (keys are property names, values are
     * property descriptions).
     */
    protected final Map<String, String> authorizedPropertyDescriptions;

    /**
     * real properties obtained fro a configuration in {@link
     * #initWriter(ChainedFileWriterConfiguration)} method
     */
    protected Map<String, Object> properties;

    private WriterReport writerReport;

    /**
     * Generates for all given files ({@code filesByRoot} and then copy
     * resources given for his file reacted in the {@code resourcesByFile}
     * dictionnary.
     *
     * @param configuration   the shared configuration for all writers
     * @param outputDir       where to generate files
     * @param filesByRoot     all files to treate
     * @param resourcesByFile resources associated to files to treate
     * @throws IOException for any IO pb.
     * @since 2.1.3
     */
    protected abstract void generate(
            ChainedFileWriterConfiguration configuration,
            File outputDir,
            Map<File, List<File>> filesByRoot,
            Map<File, List<File>> resourcesByFile) throws IOException;

    protected AbstractChainedFileWriter(String... propertyNameAndDescriptions) {
        if (propertyNameAndDescriptions.length % 2 != 0) {
            throw new IllegalArgumentException(
                    "propertyNameAndDescriptions must be couple of " +
                    "(property key, property description), but was " +
                    Arrays.toString(propertyNameAndDescriptions));
        }
        entries = new ArrayList<>();
        properties = new TreeMap<>();
        Map<String, String> authorizedPropertyDescriptions =
                new TreeMap<>();
        for (int i = 0, max = propertyNameAndDescriptions.length / 2;
             i < max; i++) {
            authorizedPropertyDescriptions.put(
                    propertyNameAndDescriptions[2 * i],
                    propertyNameAndDescriptions[2 * i + 1]);
        }
        this.authorizedPropertyDescriptions =
                Collections.unmodifiableMap(authorizedPropertyDescriptions);
    }

    @Override
    public Map<String, String> getAuthorizedPropertyDescriptions() {
        return authorizedPropertyDescriptions;
    }

    @Override
    public String[] getAuthorizedPropertyNames() {
        Set<String> keys = authorizedPropertyDescriptions.keySet();
        return keys.toArray(new String[keys.size()]);
    }

    @Override
    public void clear() {
        entries.clear();
        properties.clear();
        previousWriter = nextWriter = null;
    }

    @Override
    public void addEntry(ChainedFileWriterEntry entry) {
        entries.add(entry);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public <T> T getProperty(String key, Class<T> type) {
        return (T) properties.get(key);
    }

    @Override
    public void generate(ChainedFileWriterConfiguration configuration,
                         ChainedFileWriterData data)
            throws IOException {

        initWriter(configuration);

        try {
            File outputDir = data.getOutputDirectory();
            Map<File, List<File>> filesByRoot = data.getFilesByRoot();

            Map<File, List<File>> resourcesByFile = data.getResourcesByFile();

            // launch generation
            generate(configuration, outputDir, filesByRoot, resourcesByFile);

        } finally {
            clear();
        }
    }

    @Override
    public String getInputProtocol(String modelType) {
        // input protocol is the same for all model
        return acceptModel(modelType) ? getInputProtocol() : null;
    }

    @Override
    public File getOutputDirectory(File outputBasedir, boolean testPhase) {
        return new File(outputBasedir, testPhase ?
                                       getDefaultTestOutputDirectory() :
                                       getDefaultOutputDirectory()
        );
    }

    @Override
    public File getExtractDirectory(File outputBasedir, boolean testPhase) {
        return new File(outputBasedir, testPhase ?
                                       "test-" + getInputProtocol() :
                                       getInputProtocol()
        );
    }

    @Override
    public List<ChainedFileWriterEntry> getEntries() {
        return entries;
    }

    @Override
    public WriterReport getWriterReport() {
        return writerReport;
    }

    @Override
    public void setWriterReport(WriterReport writerReport) {
        this.writerReport = writerReport;
    }

    protected ChainedFileWriter getNextWriter() {
        return nextWriter;
    }

    protected ChainedFileWriter getPreviousWriter() {
        return previousWriter;
    }

    protected void setNextWriter(ChainedFileWriter nextWriter) {
        this.nextWriter = nextWriter;
    }

    /**
     * Initialize the writer before the generation.
     *
     * @param configuration the configuration to use for int
     */
    protected void initWriter(ChainedFileWriterConfiguration configuration) {
        Map<String, Object> map = configuration.getProperties();

        boolean verbose = configuration.isVerbose();
        for (String key : getAuthorizedPropertyNames()) {
            //TODO-TC-20091217, should prefix keys by the inputProtocol to
            //TODO-TC-20091217  avoid collisions ?

            if (map.containsKey(key)) {
                // keep this property
                Object value = map.get(key);
                if (verbose) {
                    log.info("[" + getClass().getName() + "] add configuration property " + key + " = " + value);
                }
                properties.put(key, value);

            }
        }
    }

}
