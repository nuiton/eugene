/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.writer;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Definition of  of the chained writer entry.
 *
 * Created: 04 mars. 2010
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.0
 */
public class ChainedFileWriterEntry {

    /** input path of entry (can be a directory or a classpath entry) */
    protected String inputPath;

    /** include pattern of entry */
    protected String includePattern;

    /**
     * Flag to knwon if resources should be searched in classpath.
     *
     * If sets to {@code true}, then the {@link #inputPath} is the
     * absolute path where to seek resources in classpath.
     *
     * @since 2.1.3
     */
    protected boolean useClassPath;

    public ChainedFileWriterEntry(String inputPath,
                                  String includePattern,
                                  boolean useClassPath) {
        this(inputPath, includePattern);
        this.useClassPath = useClassPath;
    }

    public ChainedFileWriterEntry(String inputPath, String includePattern) {
        this.inputPath = inputPath;
        this.includePattern = includePattern;
    }

    public String getIncludePattern() {
        return includePattern;
    }

    public String getInputPath() {
        return inputPath;
    }

    public boolean isUseClassPath() {
        return useClassPath;
    }

    @Override
    public String toString() {
        String s = ToStringBuilder.reflectionToString(this);
        return s;
    }
}
