package org.nuiton.eugene.writer;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * To grab files to treate.
 *
 * Created on 5/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public interface FileGrabber {

    void addFilesToTreate(File extractDirectory,
                          String inputDirectory,
                          Set<String> includePatterns,
                          ChainedFileWriterData result) throws IOException;

}
