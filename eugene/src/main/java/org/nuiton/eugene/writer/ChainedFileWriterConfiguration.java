/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.writer;

import org.nuiton.eugene.ModelHelper;

import java.io.File;
import java.util.Map;

/**
 * Shared configuration of a {@link ChainedFileWriter}, should be the same for
 * all writers to use at a time.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.0
 */
public interface ChainedFileWriterConfiguration {

    /** @return the type of model used */
    String getModelType();

    /** @return project base directory */
    File getBasedir();

    /** @return base directory where to generate */
    File getOutputDirectory();

    /**
     * @return base directory where to extract files obtain from class-path
     * @since 2.1.3
     */
    File getExtractDirectory();

    /**
     * @return {@code true} if must regenerate files even if they are up to date
     */
    boolean isOverwrite();

    /**
     * @return {@code true} if build is off-line and should not be able to access outside resources.
     */
    boolean isOffline();

    /** @return {@code true} if build is verbose. */
    boolean isVerbose();

    /** @return {@code true} if build is done on a test phase. */
    boolean isTestPhase();

    /** @return encoding to use to read and write files */
    String getEncoding();

    /** @return the universe of availables writers */
    Map<String, ChainedFileWriter> getWriters();

    /**
     * @return model helper tool.
     * @since 2.6.3
     */
    ModelHelper getModelHelper();

    /** @return properties to pass to writers */
    Map<String, Object> getProperties();

    /** @return the classloader to use to seek for resources */
    ClassLoader getClassLoader();

    File getModelExtensionFile();

}
