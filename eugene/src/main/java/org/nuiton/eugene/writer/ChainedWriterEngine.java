/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.writer;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Contract of the chained writer engine.
 *
 * You can register inputs via the method {@link #registerInclude(String)}.
 *
 * And later obtain of the selected writers for your inputs via the method
 * {@link #getSelectedWriters()}.
 *
 * Created: 17 déc. 2009
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.0
 */
public interface ChainedWriterEngine {

    void init(ChainedFileWriterConfiguration configuration);

    /**
     * Register in engine a new input source.
     *
     * this method will detects writers to register and chain them if necessary.
     *
     * @param include the new include to digest
     */
    void registerInclude(String include);


    /** @return the set of all available writers discovered at runtime */
    Set<ChainedFileWriter> getAvailableWriters();

    /** @return the list of selected writers after having registred some inputs. */
    List<ChainedFileWriter> getSelectedWriters();

    /**
     * Tests if there is a selected writer using the given input protocol.
     *
     * @param inputProtocol the inputProtocol to test
     * @return {@code true} if there is a selected writer using this input protocol
     * @see ChainedFileWriter#getInputProtocol()
     * @see #getSelectedWriters()
     */
    boolean containsWriter(String inputProtocol);

    /**
     * Filter the given {@code universe} of writers which accept the given {@code modelType}.
     *
     * @param universe  the list of writers to filter
     * @param modelType the accepted model type
     * @return the set of filtered writers
     * @see ChainedFileWriter#acceptModel(String)
     */
    Set<ChainedFileWriter> filterWriterForModelType(
            Map<String, ChainedFileWriter> universe,
            String modelType);

    /**
     * Filter the given {@code universe} of writers which accept the given
     * {@code modelType} and {@code inputProtocol}.
     *
     * @param universe      the list of writers to filter
     * @param inputProtocol the accepted input protocol
     * @param modelType     the accepted model type
     * @return the set of filtered writers
     * @see ChainedFileWriter#acceptModel(String)
     * @see ChainedFileWriter#getInputProtocol()
     * @see ChainedFileWriter#getInputProtocol(String)
     */
    ChainedFileWriter getWriterForInputProtocol(
            Set<ChainedFileWriter> universe,
            String inputProtocol,
            String modelType);

    /**
     * Filter the given {@code universe} of writers which accept the given
     * {@code modelType} and {@code include}.
     *
     * The include can have several forms : <ul> <li></li> <li></li> </ul>
     *
     * @param universe  the list of writers to filter
     * @param include   the include configuration
     * @param modelType the accepted model type
     * @return the set of filtered writers
     * @see ChainedFileWriter#acceptModel(String)
     * @see ChainedFileWriter#getInputProtocol()
     * @see ChainedFileWriter#getInputProtocol(String)
     */
    ChainedFileWriter getWriterForInclude(
            Set<ChainedFileWriter> universe,
            String include,
            String modelType);

    /**
     * Obtain the universe of files to react by the given {@code writer}
     * associated with each (distinct).
     *
     * <b>Note:</b> If some files are in class-path (using the classpath:
     * prefix) then they will be extracted to the
     * {@link ChainedFileWriterConfiguration#getExtractDirectory()}.
     *
     * @param writer the writer to inspect
     * @return the universe of files to react for the given writer
     * @throws IOException for any IO pb
     * @since 2.1.3
     */
    ChainedFileWriterData getData(ChainedFileWriter writer) throws IOException;

    /** clean all internal states */
    void clear();

}
