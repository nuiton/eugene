package org.nuiton.eugene.writer;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.util.DirectoryScanner;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 5/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class FileGrabberFromDirectory implements FileGrabber {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FileGrabberFromDirectory.class);

    private final ChainedFileWriterConfiguration configuration;

    public FileGrabberFromDirectory(ChainedFileWriterConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void addFilesToTreate(File extractDirectory,
                                 String inputDirectory,
                                 Set<String> includePatterns,
                                 ChainedFileWriterData result) throws IOException {

        Map<File, List<File>> filesByRoot = result.getFilesByRoot();
        Map<File, List<File>> resourcesByFile = result.getResourcesByFile();

        // final input directory to use

        File realInputDirectory = new File(inputDirectory);

        List<File> files = filesByRoot.get(realInputDirectory);
        if (files == null) {
            files = new ArrayList<>();
            filesByRoot.put(realInputDirectory, files);
        }

        List<File> newFiles = getFiles(inputDirectory, includePatterns);

        for (File file : newFiles) {

            // add the file in reactor
            files.add(file);

            // get resources associated with the file
            File resourceFile = getAssociatedResource(file);

            if (resourceFile == null) {

                // no resource associated with the file
                if (log.isDebugEnabled()) {
                    log.debug("[" + file + "] No resource associated.");
                }

            } else {

                if (configuration.isVerbose() && log.isDebugEnabled()) {
                    log.debug("[" + file + "] Detected resource " + resourceFile);
                }
                List<File> resources = new ArrayList<>(1);
                resources.add(resourceFile);

                resourcesByFile.put(file, resources);

            }

        }

    }

    protected List<File> getFiles(String inputPath, Set<String> includePattern) {

        if (CollectionUtils.isEmpty(includePattern)) {
            throw new IllegalArgumentException("Must have at least one include pattern");
        }

        List<File> result = new ArrayList<>();

        DirectoryScanner ds = new DirectoryScanner();
        File inputDirectory = new File(inputPath);
        ds.setBasedir(inputDirectory);
        ds.setIncludes(includePattern.toArray(new String[includePattern.size()]));
        ds.setExcludes(null);
        ds.addDefaultExcludes();
        ds.scan();

        for (String file : ds.getIncludedFiles()) {
            File in = new File(inputDirectory, file);
            result.add(in);
        }

        return result;

    }

    protected File getAssociatedResource(File file) throws IOException {

        String extension = "." + FileUtil.extension(file.getName());

        String path = file.getAbsolutePath();

        String filename = FileUtil.basename(path, extension).concat(".properties");

        if (log.isDebugEnabled()) {
            log.info("path of file : " + path);
            log.info("path of resource : " + filename);
        }

        File propertiesFile = new File(filename);

        if (!propertiesFile.exists()) {
            propertiesFile = null;
        }

        return propertiesFile;

    }

}
