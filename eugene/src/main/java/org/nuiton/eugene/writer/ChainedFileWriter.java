/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.writer;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Contract to generate files from any incoming sources (zargo, xmi, ...) to any other ones.
 *
 * User: chemit Date: 27 nov. 2009 Time: 11:20:39
 *
 * @since 2.0.0
 */
public interface ChainedFileWriter {

    /** @return the accepted incoming protocol */
    String getInputProtocol();

    /**
     * Obtain the input protocol of this writer given the passed {@code modelType}.
     *
     * @param modelType the type of model used
     * @return the input protocol or {@code null} if this writer does not accept the type of model
     */
    String getInputProtocol(String modelType);

    /**
     * Obtain the output protocol of this writer given the passed {@code modelType}.
     *
     * @param modelType the type of model used
     * @return the output protocol or {@code null} if should not be chained
     */
    String getOutputProtocol(String modelType);

    /**
     * Test if a type of model can be treated by this writer.
     *
     * @param modelType model type to test
     * @return {@code true} if this writer accept the given type of model, {@code false} otherwise.
     */
    boolean acceptModel(String modelType);

    /**
     * Test in a entry can be treated by this writer.
     *
     * @param include the include to test
     * @return {@code true} if the writer accept the entry, {@code false} otherwise.
     */
    boolean acceptInclude(String include);

    /**
     * @return the default includes files to be treated by the writer (can be an ant-like expression)
     */
    String getDefaultIncludes();

    /** @return the defalt relative path where to pick files to treate. */
    String getDefaultInputDirectory();

    /**
     * @return the defalt relative path where to pick files to treate on a test phase.
     */
    String getDefaultTestInputDirectory();

    /** @return the default relative path to add to output basedir */
    String getDefaultOutputDirectory();

    /**
     * @return the default relative path to add to output basedir on a test
     * phase.
     */
    String getDefaultTestOutputDirectory();

    /**
     * Obtain the real directory where to write files.
     *
     * //FIXME-TC20091126 make this configurable (via the properties)
     *
     * @param outputBasedir the output base directory
     * @param testPhase     {@code true} if writer is used in a test phase
     * @return the real output directory where to generate for this particular writer
     */
    File getOutputDirectory(File outputBasedir, boolean testPhase);

    /**
     * Obtain the real directory where to extract files (when using resources
     * from class-path).
     *
     * @param outputBasedir the output base directory
     * @param testPhase     {@code true} if writer is used in a test phase
     * @return the real output directory where to extract for this particular
     * writer
     * @since 2.1.3
     */
    File getExtractDirectory(File outputBasedir, boolean testPhase);

    /**
     * Launch the generation for this writer with all pre-computed data to
     * treate and resources to copy.
     *
     * @param configuration the share configuration of all writers.
     * @param data          data to treate (files to react + resources to copy)
     * @throws IOException if any io pb.
     * @since 2.1.3
     */
    void generate(ChainedFileWriterConfiguration configuration,
                  ChainedFileWriterData data)
            throws IOException;

//    /**
//     * Obtain for a given {@code inputDirectory}, all files to treate.
//     *
//     * @param configuration  the shared configuration
//     * @param inputPath      the input path (can be a directory or a classpath path)
//     * @param includePattern the include pattern separated by comma
//     * @param inClassPath    a flag to say if we should search in classpath
//     * @return the list of resources detected
//     * @throws IOException              if any IO pb while searching resources
//     * @throws IllegalArgumentException if no include pattern given
//     * @since 2.1.3
//     */
//    List<URL> getFiles(ChainedFileWriterConfiguration configuration,
//                       String inputPath,
//                       List<String> includePattern,
//                       boolean inClassPath) throws IOException, IllegalArgumentException;
//
//    /**
//     * Obtain the optional resource files associated to the given file to react.
//     *
//     * @param file the file to react
//     * @return the array of resources associated to the file
//     * @throws IOException if could not get resources
//     * @since 2.1.3
//     */
//    List<URL> getResources(URL file) throws IOException;

    /** Clear all internal states */
    void clear();

    /**
     * Add an entry to treate.
     *
     * @param entry the entry to add to writer
     */
    void addEntry(ChainedFileWriterEntry entry);

    /**
     * @return the array of properties names authorized for the chained writer.
     */
    String[] getAuthorizedPropertyNames();

    /**
     * @return the dictionnary of authorized property descriptions (keys are
     * property names and values are descriptions).
     */
    Map<String, String> getAuthorizedPropertyDescriptions();

    /**
     * Obtain a writer extra property.
     *
     * @param key  the key of required property
     * @param type the type of property
     * @param <T>  the type of property
     * @return the property found or {@code null} if not found.
     */
    <T> T getProperty(String key, Class<T> type);

    /** @return the list of all entries registered */
    List<ChainedFileWriterEntry> getEntries();


    /** @return the writer report (to save generated file to later report) */
    WriterReport getWriterReport();

    void setWriterReport(WriterReport writerReport);

}
