package org.nuiton.eugene.writer;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.Model;

import java.io.File;

/**
 * Contract to define a specialized {@link ChainedFileWriter} which generate no files but a memory model.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.3
 */
public interface ChainedFileWriterToMemoryModel extends ChainedFileWriter {

    /**
     * Reads inputs files and load them in a unique memory model.
     *
     * @return the memory model loaded from input files.
     */
    Model getModel();

    /**
     * Gets the latest last modified time of physicial files used to load the memory model.
     *
     * @return the latest last modified of any files used to load memory model.
     */
    long getLastModifiedSource();

    /**
     * Gets the directory where to generate later files froma pplying template to this memory model.
     *
     * @return where to generate files from this model.
     */
    File getOutputDirectory();
}
