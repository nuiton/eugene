/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.writer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A class to save generated files in {@link ChainedFileWriter}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.2
 */
public class WriterReport {

    protected Map<String, List<File>> datas;

    protected Map<String, List<File>> resources;

    public Map<String, List<File>> getDatas() {
        if (datas == null) {
            datas = new TreeMap<>();
        }
        return datas;
    }

    public Map<String, List<File>> getResources() {
        if (resources == null) {
            resources = new TreeMap<>();
        }
        return resources;
    }

    public void addFile(String entry, File file, boolean verbose) {
        List<File> files = getDatas().get(entry);
        if (files == null) {
            files = new ArrayList<>();
            getDatas().put(entry, files);
        }
        files.add(file);
    }

    public void addResource(String entry, File file, boolean verbose) {
        List<File> files = getResources().get(entry);
        if (files == null) {
            files = new ArrayList<>();
            getResources().put(entry, files);
        }
        files.add(file);
    }

    public int getFilesCount() {
        int tot = 0;
        for (List<File> files : getDatas().values()) {
            tot += files.size();
        }
        return tot;
    }

    public int getResourcesCount() {
        int tot = 0;
        for (List<File> files : getResources().values()) {
            tot += files.size();
        }
        return tot;
    }
}
