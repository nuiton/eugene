/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.models.Model;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * Transformer
 *
 * Created: 28 oct. 2009
 *
 * @param <I> input model to transform
 * @param <O> output model transformed
 * @author Florian Desbois - desbois@codelutin.com
 */
public abstract class Transformer<I extends Model, O extends Model>
        extends Template<I> {

    /** Logger */
    private static final Log log = LogFactory.getLog(Transformer.class);

    /**
     * Output generator, to generate files from Output model.
     *
     * Could be an other Transformer.
     */
    protected Template<O> outputTemplate;

    /**
     * Previous transformer to modify input model
     */
    protected Transformer<I, I> previousTransformer;

    /**
     * Output model.
     */
    protected O outputModel;

    /**
     * Properties for output generator.
     */
    protected Properties outputProperties = new Properties();

    public Transformer() {
    }

    /**
     * Initialization of the Transformer :
     * <pre>
     * - initOutputModel : default, do nothing
     * - initOutputGenerator : must be override to instanciate output Generator
     * - initOutputModel : must be override to instanciate output Model
     * </pre>
     *
     * @param model input model
     * @since 2.0.0
     */
    protected void init(I model) {
        this.model = model;
        // tchemit 2010-11-26 : this is not the good place to init a previous
        // transformer : once coming here, we lost our chance to use the previousTransformer
//        previousTransformer = initPreviousTransformer();
        resourcesHelper = null;
        outputTemplate = initOutputTemplate();

        // build output template configuration
        // merge input configuration + output properties
        Properties properties = new Properties();
        properties.putAll(getConfiguration().getProperties());
        properties.putAll(outputProperties);

        TemplateConfiguration outconfig =
                new DefaultTemplateConfiguration(properties);

        // Note tchemit 2014-04-26, do not touch the outputProperties, init the transformer
        // should not change this state
        // push back to outputPropertie the all configuration
//        outputProperties = properties;

        outputTemplate.setConfiguration(outconfig);
        outputModel = initOutputModel();
    }

    protected Transformer<I, I> initPreviousTransformer() {
        return null;
    }

    /**
     * Initialization of the Output generator. Must be override to instanciate
     * the Generator that will make the generation of the Output model.
     *
     * @return the output generator
     * @since 2.0.0
     */
    protected abstract Template<O> initOutputTemplate();

    /**
     * Initialization of the Output model. Must be override to instanciate
     * and initialize the output model.
     *
     * For ObjectModel you can use, ObjectModelBuilder to build easily an
     * empty ObjectModel.
     *
     * @return the output model
     * @since 2.0.0
     */
    protected abstract O initOutputModel();

    /**
     * Output properties for output generator. Initially set from build configuration.
     *
     * @return output properties
     * @since 2.0.0
     * @deprecated since 2.9, wille be removed in version 3.0, prefer use now {@link #addOutputProperty(String, Object)}
     */
    @Deprecated
    protected Properties getOutputProperties() {
//        if (outputProperties == null) {
//            outputProperties = new Properties();
//        }
        return outputProperties;
    }

    public void addOutputProperty(String key, Object value) {
        outputProperties.put(key, value);
    }

    public Template<O> getOutputTemplate() {
        return outputTemplate;
    }

    public O getOutputModel() {
        return outputModel;
    }

    public String getDefaultPackageName() {
        String packageName = getConfiguration().getProperty(PROP_DEFAULT_PACKAGE);
        return packageName;
    }

    /**
     * This method apply the current transformation. You can use an other
     * transformer for previous transformation by overriding
     * {@link #initPreviousTransformer} method. In this case, the current
     * transformation will be apply on the output model of the previous one.
     * In the simple other case, the current transformation transform the
     * input model into an output model. You must override
     * {@link #initOutputModel} and {@link #initOutputTemplate } methods
     * to initialize output model and output template to apply (generally a
     * generator).
     *
     * @param model   input model to transform and generate
     * @param destDir destination directory to put generated files
     * @throws IOException if any IO problems while applying template
     * @since 2.0.0
     */
    @Override
    public void applyTemplate(I model, File destDir) throws IOException {

        // init previous transformer
        previousTransformer = initPreviousTransformer();

        // previous transformation
        if (previousTransformer != null) {

            if (isVerbose()) {
                log.info("Use previousTransformer " + previousTransformer);
            }
            // transformation only, no application of next template
            previousTransformer.transform(model);

            // current transformation from the ouput model of previous transformer
            transform(previousTransformer.getOutputModel());
        } else {

            // current transformation directly from the incoming model
            transform(model);
        }

        // after template application
        getOutputTemplate().applyTemplate(getOutputModel(), destDir);
    }

    /**
     * Transformation method from an input Model. This method also initialize
     * previous transformer, output model and output generator with
     * ${@link #init(Model) } method.
     *
     * @param model input model
     * @since 2.0.0
     */
    public void transform(I model) {
        init(model);
        transform();
    }

    /**
     * Method to override for the transformation. Initialization
     * (PreviousTransformer, OutputModel, OutputGenerator) is made before
     * transformation call. This method only transform an input model into an
     * output model.
     *
     * No generation is done neither outputGenerator call.
     *
     * @since 2.0.0
     */
    protected abstract void transform();
}
