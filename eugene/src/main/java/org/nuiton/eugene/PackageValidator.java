/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene;

/**
 * The PackageValidator class is used by the xmi1.2ToObjectModel.xsl
 * stylesheet and Xalan-Java to extend the stylesheet functionnalities.
 * Allows to include external classes and interfaces to the generated
 * ObjectModel.
 *
 * Created: 21 avr. 2005
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class PackageValidator {

    /**
     * The <code>toContinue</code> method indicates if the package
     * <code>localPackageNameDot</code> has to be iterated by the stylesheet.
     *
     * @param fullPackagePath     the full package path
     * @param localPackageNameDot the current package to evaluate
     * @param extraPackages       the list of extra packages
     * @return true if there is to iterate on <code>localPackageNameDot</code>
     */
    public static boolean toContinue(String fullPackagePath, String localPackageNameDot, String extraPackages) {
        if (fullPackagePath.startsWith(localPackageNameDot)) {
            return true;
        }
        String[] packages = extraPackages.split(",");
        for (int i = 0; i < packages.length; i++) {
            packages[i] = packages[i].trim();
            if (packages[i].startsWith(localPackageNameDot)) {
                return localPackageNameDot.length() < packages[i]
                                                              .lastIndexOf(".") + 1;
            }
        }
        return false;
    }

    /**
     * <code>isValid</code> indicates if the current package
     * (<code>localPackageNameDot</code>) is valid according to the extra
     * packages list (<code>extraPackages</code>).
     *
     * @param fullPackagePath  the full package path
     * @param localPackageName the current package to evaluate
     * @param extraPackages    the list of extra packages
     * @return true is the current package is valid
     */
    public static boolean isValid(String fullPackagePath, String localPackageName, String extraPackages) {
        if (localPackageName.startsWith(fullPackagePath)) {
            return true;
        }
        if (localPackageName.length() == 0) {
            return false;
        }
        String[] packages = extraPackages.split(",");
        for (int i = 0; i < packages.length; i++) {
            packages[i] = packages[i].trim();
            if (localPackageName.matches(packages[i])
                || packages[i].startsWith(localPackageName)) {
                return true;
            }
        }
        return false;
    }
}
