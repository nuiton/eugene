package org.nuiton.eugene;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.URL;

/**
 * Some useful methods to locate resources.
 *
 * Created on 08/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ResourcesHelper {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ResourcesHelper.class);

    protected final ClassLoader classLoader;
    protected final boolean verbose;

    public ResourcesHelper(ClassLoader classLoader, boolean verbose) {
        this.classLoader = classLoader;
        this.verbose = verbose;
    }

    /**
     * Checks if the given fully qualified name java file is in class-path and log a message that fqn will not generated if found.
     *
     * @param fqn fully qualified name to test
     * @return {@code true} if java file was found in class-path, {@code false} otherwise.
     */
    public boolean isJavaFileInClassPath(String fqn) {
        return isFullyQualifiedNameInClassPath(fqn, ".java");
    }

    /**
     * Checks if the given fully qualified name (path separated by {@code pathSeparator} and optionaly suffixed by
     * {@code extension}) is in class-path and log a message that fqn will not generated if found.
     *
     * @param extension file extension
     * @param fqn       fully qualified name to test
     * @return {@code true} if resource was found in class-path, {@code false} otherwise.
     */
    public boolean isFullyQualifiedNameInClassPath(String fqn, String extension) {
        return isInClassPath(fqn,"\\.", extension);
    }

    /**
     * Checks if the given fully qualified name (path separated by {@code pathSeparator} and optionaly suffixed by
     * {@code extension}) is in class-path and log a message that fqn will not generated if found.
     *
     * @param extension file extension
     * @param fqn       fully qualified name to test
     * @return {@code true} if resource was found in class-path, {@code false} otherwise.
     */
    public boolean isInClassPath(String fqn, String pathSeparator, String extension) {

        URL fileLocation = getFileInClassPath(fqn, pathSeparator, extension);

        if (fileLocation != null) {

            // there is already a existing file in class-path, skip

            if (log.isDebugEnabled()) {
                log.debug("Will not generate [" + fqn + "], already found in class-path at location : " + fileLocation);
            } else if (verbose) {
                log.info("Will not generate [" + fqn + "], already found in class-path.");
            }

            return true;
        }

        // is not found
        return false;
    }

    protected URL getFileInClassPath(String fqn, String pathSeparator, String extension) {
        String resourceName = fqn.replaceAll(pathSeparator, "/") + extension;
        URL fileLocation = classLoader.getResource(resourceName);
        if (log.isDebugEnabled()) {
            log.debug("Look for resource : " + resourceName + " = " + fileLocation);
        }
        return fileLocation;
    }
}
