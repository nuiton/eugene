/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Permet de savoir si on a ecrit dans le flux ou non. L'ecriture d'une chaine
 * vide a le meme comportement que l'ecriture d'une vrai chaine.
 * De cette façon meme les fichiers peuvent être créer.
 *
 * Created: Jun 8, 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class MonitorWriter extends FilterWriter { // MonitorWriter

    boolean modified;

    public MonitorWriter(StringWriter out) {
        super(out);
    }

    public StringBuffer getBuffer() {
        return ((StringWriter) out).getBuffer();
    }

    public boolean isModified() {
        return modified;
    }

    @Override
    public void write(int c) throws IOException {
        super.write(c);
        modified = true;
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        super.write(cbuf, off, len);
        modified = true;
    }

    @Override
    public void write(String str, int off, int len) throws IOException {
        super.write(str, off, len);
        modified = true;
    }

    @Override
    public void write(String str) throws IOException {
        super.write(str);
        modified = true;
    }

    @Override
    public void write(char... cbuf) throws IOException {
        super.write(cbuf);
        modified = true;
    }

    @Override
    public Writer append(CharSequence csq) throws IOException {
        Writer writer = super.append(csq);
        modified = true;
        return writer;
    }

    @Override
    public Writer append(CharSequence csq, int start, int end) throws IOException {
        Writer writer = super.append(csq, start, end);
        modified = true;
        return writer;
    }

    @Override
    public Writer append(char c) throws IOException {
        Writer writer = super.append(c);
        modified = true;
        return writer;
    }
}// MonitorWriter

