/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state;

import org.apache.commons.digester3.Digester;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.ModelHelper;
import org.nuiton.eugene.ModelReader;
import org.nuiton.eugene.models.state.xml.DigesterStateModelRuleSet;
import org.nuiton.eugene.models.state.xml.StateModelImpl;
import org.nuiton.util.FileUtil;
import org.nuiton.util.RecursiveProperties;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * To read state model files into a memory state model.
 *
 * Created: 26 oct. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 * @plexus.component role="org.nuiton.eugene.ModelReader" role-hint="statemodel"
 */
public class StateModelReader extends ModelReader<StateModel> {

    private static final Log log = LogFactory.getLog(StateModelReader.class);

    @Override
    public String getModelType() {
        return ModelHelper.ModelType.STATE.getAlias();
    }

    @Override
    public String getInputType() {
        return ModelHelper.ModelInputType.XML.getAlias();
    }

    @Override
    public StateModel read(File... files) {
        Digester digester = new Digester();
        digester.addRuleSet(new DigesterStateModelRuleSet());

        StateModelImpl stateModel = new StateModelImpl();

        // process each file
        for (File file : files) {

            // fin a deplacer
            try {
                digester.push(stateModel);
                digester.parse(file);

                // try to load property file
                loadPropertyFile(file, stateModel);
            } catch (IOException | SAXException e) {
                log.warn("Can't read model file", e);
            }
        }
        return stateModel;
    }

    /**
     * Try to load property file, associated to current statemodel file
     *
     * @param stateModelFile state model file
     * @param stateModel     state model
     */
    protected void loadPropertyFile(File stateModelFile, StateModelImpl stateModel) {
        // recherche et charge le fichier propriete associe au modele
        File dir = stateModelFile.getParentFile();
        String ext = FileUtil.extension(stateModelFile);
        String name = FileUtil.basename(stateModelFile, "." + ext);
        File propFile = new File(dir, name + ".properties");
        RecursiveProperties prop = new RecursiveProperties();

        if (!propFile.exists()) {
            if (log.isInfoEnabled()) {
                log.info("No property file associated to model : " + propFile);
            }
        } else {
            if (log.isInfoEnabled()) {
                log.info("Reading model property file " + propFile);
            }
            try (FileInputStream inStream = new FileInputStream(propFile)) {
                prop.load(inStream);
            } catch (IOException e) {
                log.warn("Cannot read or close property file " + propFile, e);
            }

            // on ajoute les proprietes du fichier associe au model
            for (Object o : prop.keySet()) {
                String key = (String) o;
                String value = prop.getProperty(key);

                if (!key.startsWith("model.tagvalue.")) {
                    log.warn("only tagvalue is allowed on model in properties");
                } else {
                    String tag = key.substring("model.tagvalue.".length());
                    stateModel.addTagValue(tag, value);
                }
            }
        }
    }
}
