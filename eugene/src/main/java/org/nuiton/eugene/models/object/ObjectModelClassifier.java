/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import java.util.Collection;

/**
 * ObjectModelClassifier.
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public interface ObjectModelClassifier extends ObjectModelElement {

    /**
     * Returns the package name of this classifier.
     *
     * @return the package name of this classifier.
     */
    String getPackageName();

    /**
     * Returns the qualified name of this classifier.
     * Class qualified name is composed of the package name and the classifier name.
     *
     * @return the qualified name of this classifier.
     */
    String getQualifiedName();

    /**
     * Returns all parent interfaces for this classifier.
     *
     * @return a Collection containing all parent ObjectModelInterface for this classifier.
     */
    Collection<ObjectModelInterface> getInterfaces();

    /**
     * Returns all operations defined on this classifier.
     *
     * @param name name of operation should be returned
     * @return a Collection containing all ObjectModelOperation for this classifier.
     * @see ObjectModelOperation
     */
    Collection<ObjectModelOperation> getOperations(String name);

    /**
     * Returns all operations defined on this classifier.
     *
     * @return a Collection containing all ObjectModelOperation for this classifier.
     * @see ObjectModelOperation
     */
    Collection<ObjectModelOperation> getOperations();

    /**
     * Returns all operations defined on all interfaces implemented by this
     * classifier, directly or indirectly.
     *
     * @param distinct if this boolean is true only distinct operation
     *                 are add to list.
     * @return a Collection of ObjectModelOperation
     */
    Collection<ObjectModelOperation> getAllInterfaceOperations(
            boolean distinct);

    /**
     * Returns all operations defined on all implemented by this
     * classifier, directly or indirectly. For interface this methode return
     * the same result as getAllInterfaceOperations, for Class this
     * method add all operation of SuperClass.
     *
     * @param distinct if this boolean is true only distinct operation
     *                 are add to list.
     * @return a Collection of ObjectModelOperation
     */
    Collection<ObjectModelOperation> getAllOtherOperations(
            boolean distinct);

    /**
     * Returns all attributes defined on this class.
     *
     * @return a Collection containing all ObjectModelAttribute for this class.
     * @see ObjectModelAttribute
     */
    Collection<ObjectModelAttribute> getAttributes();

    /**
     * Returns the attribute corresponding to the given name, or null if the class contains no attribute for this name.
     *
     * @param attributeName attribute name
     * @return the ObjectModelAttribute of the found attribute, or null if the class contains no attribute for this name.
     */
    ObjectModelAttribute getAttribute(String attributeName);

    /**
     * Returns all attributes defined on all interfaces implemented by this
     * classifier, directly or indirectly.
     *
     * @return a Collection of ObjectModelAttribute
     */
    Collection<ObjectModelAttribute> getAllInterfaceAttributes();

    /**
     * Returns all attributes defined on all super class extended by this
     * classifier, directly or indirectly.
     *
     * @return a Collection of ObjectModelAttribute
     */
    Collection<ObjectModelAttribute> getAllOtherAttributes();

    /**
     * Returns all dependencies of this client classifier
     *
     * @return a Collection of ObjectModelDependency
     */
    Collection<ObjectModelDependency> getDependencies();

    /**
     * Return a dependency identifier by her name
     *
     * @param name of the dependency
     * @return the dependency
     */
    ObjectModelDependency getDependency(String name);

    /**
     * Returns whether this classifier is a class or not
     *
     * @return a boolean indicating whether this classifier is a class or not.
     * @see ObjectModelClass
     */
    boolean isClass();

    /**
     * Returns whether this classifier is an interface or not
     *
     * @return a boolean indicating whether this classifier is an interface or not.
     * @see ObjectModelInterface
     */
    boolean isInterface();

    /**
     * Returns whether this classifier is an enumeration or not
     *
     * @return a boolean indicating whether this classifier is an enumeration or not.
     * @see ObjectModelEnumeration
     */
    boolean isEnum();

    /**
     * Returns whether this class is inner an other class or not.
     *
     * @return a boolean indicating whether this class is inner an other class or not.
     */
    boolean isInner();

} //ObjectModelClassifier
