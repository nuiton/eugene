package org.nuiton.eugene.models.extension.model;

/*-
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.extension.io.ModelExtensionFileParser;
import org.nuiton.eugene.models.extension.io.ModelExtensionFileParserCallback;
import org.nuiton.eugene.models.extension.tagvalue.InvalidStereotypeSyntaxException;
import org.nuiton.eugene.models.extension.tagvalue.InvalidTagValueSyntaxException;

import java.io.File;
import java.io.IOException;

/**
 * Created on 10/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ModelExtensionBuilder implements ModelExtensionFileParserCallback {

    protected final ModelExtension modelExtension;
    protected int modelStereotypeHits;
    protected int modelTagValueHits;
    protected int packageStereotypeHits;
    protected int packageTagValueHits;
    protected int classStereotypeHits;
    protected int classTagValueHits;
    protected int classAttributeStereotypeHits;
    protected int classAttributeTagValueHits;
    protected boolean strictLoading;

    public ModelExtensionBuilder(boolean strictLoading, String modelName) {
        this.modelExtension = new ModelExtension(modelName);
    }

    public ModelExtension build() {
        return modelExtension;
    }

    public int getStereotypeHits() {
        return modelStereotypeHits + packageStereotypeHits + classStereotypeHits + classAttributeStereotypeHits;
    }

    public int getTagValueHits() {
        return modelTagValueHits + packageTagValueHits + classTagValueHits + classAttributeTagValueHits;
    }

    public int getModelStereotypeHits() {
        return modelStereotypeHits;
    }

    public int getModelTagValueHits() {
        return modelTagValueHits;
    }

    public int getPackageStereotypeHits() {
        return packageStereotypeHits;
    }

    public int getPackageTagValueHits() {
        return packageTagValueHits;
    }

    public int getClassStereotypeHits() {
        return classStereotypeHits;
    }

    public int getClassTagValueHits() {
        return classTagValueHits;
    }

    public int getClassAttributeStereotypeHits() {
        return classAttributeStereotypeHits;
    }

    public int getClassAttributeTagValueHits() {
        return classAttributeTagValueHits;
    }

    @Override
    public boolean onModelTagValueFound(String tag, String value) {
        addTagValue(modelExtension, tag, value);
        modelTagValueHits++;
        return true;
    }

    @Override
    public boolean onModelStereotypeFound(String stereotype) {
        addTagValue(modelExtension, stereotype, "true");
        modelStereotypeHits++;
        return true;
    }

    @Override
    public boolean onPackageTagValueFound(String packageName, String tag, String value) {
        addTagValue(modelExtension.getOrCreatePackage(packageName), tag, value);
        packageTagValueHits++;
        return true;
    }

    @Override
    public boolean onPackageStereotypeFound(String packageName, String stereotype) {
        addTagValue(modelExtension.getOrCreatePackage(packageName), stereotype, "true");
        packageStereotypeHits++;
        return true;
    }

    @Override
    public boolean onClassTagValueFound(String className, String tag, String value) {
        addTagValue(modelExtension.getOrCreateClass(className), tag, value);
        classTagValueHits++;
        return true;
    }

    @Override
    public boolean onClassStereotypeFound(String className, String stereotype) {
        addTagValue(modelExtension.getOrCreateClass(className), stereotype, "true");
        classStereotypeHits++;
        return true;
    }

    @Override
    public boolean onAttributeTagValueFound(String className, String attributeName, String tag, String value) {
        addTagValue(modelExtension.getOrCreateClassAttribute(className, attributeName), tag, value);
        classAttributeTagValueHits++;
        return true;
    }

    @Override
    public boolean onAttributeStereotypeFound(String className, String attributeName, String stereotype) {
        addTagValue(modelExtension.getOrCreateClassAttribute(className, attributeName), stereotype, "true");
        classAttributeStereotypeHits++;
        return true;
    }

    public void addFile(File inputFile) throws InvalidTagValueSyntaxException, InvalidStereotypeSyntaxException, IOException {

        ModelExtensionFileParser parser = ModelExtensionFileParser.newParser(strictLoading, inputFile);
        parser.parse(inputFile, this);

    }

    protected void addTagValue(ModelExtensionElement modelExtensionElement, String tag, String value) {
        modelExtensionElement.getTagValues().put(tag, value);
    }

}
