package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * User: agiraudet
 * Date: 14/05/13
 * Time: 11:53
 */

//TODO: faire classes dédiée au parser ?

public class YamlUtil {

    @Deprecated
    public static void keySetToLowerCase(Map<Object, Object> map) {
        List<Object> keys = new ArrayList<>(map.keySet());
        for (Object obj : keys) {
            String key = String.valueOf(obj).toLowerCase();
            Object value = map.get(obj);
            map.remove(obj);
            map.put(key, value);
        }
    }

    @Deprecated
    public static void browseKeySetToLowerCase(Object obj, List<String> ignore) {
        if (obj instanceof Map) {
            keySetToLowerCase((Map<Object, Object>) obj);

            for (Object key : ((Map) obj).keySet()) {
                //tester si String ou caster avec valueOf
                if (!ignore.contains(key)) {
                    browseKeySetToLowerCase(((Map) obj).get(key), ignore);
                }
            }
        } else if (obj instanceof List) {
            for (Object tmp : (List) obj) {
                browseKeySetToLowerCase(tmp, ignore);
            }
        }
    }

    //retourne la chaîne se trouvant avant le caractère de la chaîne passée en paramètre
    //exemple: beforeChar("1-2-3",'-') -> "1"
    public static String beforeChar(String str, Character target) {
        StringBuilder tmp = new StringBuilder(str.length() + 1);
        for (Character c : str.toCharArray()) {
            if (c.equals(target)) {
                return tmp.toString();
            } else {
                tmp.append(c);
            }
        }
        return tmp.toString();
    }

    //retourne la chaîne se trouvant après le caractère de la chaîne passée en paramètre
    //exemple: afterChar("/boot/vmlinuz",'/') -> "vmlinuz"
    public static String afterChar(String str, Character target) {
        StringBuilder in = new StringBuilder(str);
        StringBuilder out = new StringBuilder(beforeChar(in.reverse().toString(), target));
        return out.reverse().toString();
    }

    public static Object collectElementList(List data, Object key) {
        for (Object obj : data) {
            if (obj instanceof Map) {
                if (((Map) obj).containsKey(key)) {
                    return ((Map) obj).get(key);
                }
            }
        }
        return null;
    }

    public static List<Object> collectAllElementsList(List data, Object key) {
        List<Object> res = new LinkedList<>();
        for (Object obj : data) {
            if (obj instanceof Map) {
                if (((Map) obj).containsKey(key)) {
                    res.add(((Map) obj).get(key));
                }
            }
        }
        return res;
    }

    public static Object collectElementMap(Map data, Object key) {
        if (data.containsKey(key)) {
            return data.get(key);
        } else {
            return null;
        }
    }

    //decoupe une chaine par le caractere passe en parametre
    public static List<String> charParse(String str, Character target) {
        LinkedList<String> res = new LinkedList<>();
        StringBuilder tmp = new StringBuilder(str.length() + 1);//+1 utile en Java ? != C
        boolean pre = false;
        for (Character c : str.toCharArray()) {
            if (c.equals(target)) {
                if (pre) {
                    res.add(tmp.toString());
                }
                tmp.delete(0, tmp.length());
                pre = false;
            } else {
                tmp.append(c);
                pre = true;
            }
        }
        if (pre) {
            res.add(tmp.toString());
        }
        return res;
    }

    public static List<String> multiCharParse(String str, List<Character> targets) {
        LinkedList<String> res = new LinkedList<>();
        StringBuilder tmp = new StringBuilder(str.length() + 1);//+1 utile en Java ? != C
        boolean pre = false;
        for (Character c : str.toCharArray()) {
            if (targets.contains(c)) {
                if (pre) {
                    res.add(tmp.toString());
                }
                tmp.delete(0, tmp.length());
                pre = false;
            } else {
                tmp.append(c);
                pre = true;
            }
        }
        if (pre) {
            res.add(tmp.toString());
        }
        return res;
    }

    //permet de parser une chaine en ignorant les parentheses par exemple
    public static List<String> charParseIgnore(String str, Character target, Character ignoreStart, Character ignoreEnd) {
        LinkedList<String> res = new LinkedList<>();
        StringBuilder tmp = new StringBuilder(str.length() + 1);//+1 utile en Java ? != C
        boolean pre = false;
        int ign = 0;//parentheses ouvertes
        for (Character c : str.toCharArray()) {
            if (c.equals(ignoreStart)) {
                ign++;
                tmp.append(c);
            } else if (c.equals(ignoreEnd)) {
                ign--;
                tmp.append(c);
            } else if (c.equals(target) && ign < 1) {
                if (pre) {
                    res.add(tmp.toString());
                }
                tmp.delete(0, tmp.length());
                pre = false;
            } else {
                tmp.append(c);
                pre = true;
            }
        }
        if (pre) {
            res.add(tmp.toString());
        }
        return res;
    }

    public static List<String> charParseMultiIgnore(String str, Character target, List<Character> ignoreStart, List<Character> ignoreEnd) {
        LinkedList<String> res = new LinkedList<>();
        StringBuilder tmp = new StringBuilder(str.length() + 1);//+1 utile en Java ? != C
        boolean pre = false;
        int ign = 0;//parentheses ouvertes
        for (Character c : str.toCharArray()) {
            if (ignoreStart.contains(c)) {
                ign++;
                tmp.append(c);
            } else if (ignoreEnd.contains(c)) {
                ign--;
                tmp.append(c);
            } else if (c.equals(target) && ign < 1) {
                if (pre) {
                    res.add(tmp.toString());
                }
                tmp.delete(0, tmp.length());
                pre = false;
            } else {
                tmp.append(c);
                pre = true;
            }
        }
        if (pre) {
            res.add(tmp.toString());
        }
        return res;
    }

    public static String removeMultiChar(String str, List<Character> targets) {
        StringBuilder res = new StringBuilder(str.length() + 1);
        for (Character c : str.toCharArray()) {
            if (!targets.contains(c)) {
                res.append(c);
            }
        }
        return res.toString();
    }

    //extrait une chaine delimitee par les caracteres en parametres
    public static String extract(Character start, Character end, String str) {
        StringBuilder tmp = new StringBuilder(str.length() + 1);
        boolean start_end = false;
        int count = 0;
        for (Character c : str.toCharArray()) {
            if (!start_end && c.equals(start)) {
                start_end = true;
            } else if (start_end && c.equals(start)) {
                count++;
                tmp.append(c);
            } else if (start_end && c.equals(end)) {
                if (count > 0) {
                    count--;
                    tmp.append(c);
                } else {
                    return tmp.toString();
                }
            } else if (start_end) {
                tmp.append(c);
            }
        }
        return null;
    }

    public static String extract(Character startend, String str) {
        StringBuilder tmp = new StringBuilder(str.length() + 1);
        boolean start_end = false;
        for (Character c : str.toCharArray()) {
            if (!start_end && c.equals(startend)) {
                start_end = true;
            } else if (start_end && c.equals(startend)) {
                return tmp.toString();
            } else if (start_end) {
                tmp.append(c);
            }
        }
        return null;
    }

    /*public static void main(String[] args) {
        ;
    }*/
}
