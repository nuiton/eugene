/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


package org.nuiton.eugene.models;

import org.nuiton.eugene.models.extension.tagvalue.WithTagValuesOrStereotypes;

/**
 * Model.
 */
public interface Model extends WithTagValuesOrStereotypes {

    /**
     * Plexus role name
     */
    String ROLE_NAME = Model.class.getName();

    /**
     * Returns the name of this model.
     *
     * @return the name of this model.
     */
    String getName();

    /**
     * Returns the version of this model.
     *
     * @return the version of this model.
     */
    String getVersion();

    /**
     * @return the type of model.
     * @since 2.6.3
     */
    String getModelType();

    /**
     * Get the extension associated to the reference (unique)
     *
     * @param <O>            object type returned
     * @param reference      unique corresponding to the extension to get
     * @param extensionClass class of the extension
     * @return the object value for the extension
     * @throws ClassCastException       when extensionClass is not valid
     * @throws IllegalArgumentException for non existing extension with reference
     */
    <O> O getExtension(String reference, Class<O> extensionClass) throws ClassCastException, IllegalArgumentException;

} //Model
