/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import java.util.Collection;
import java.util.Set;

/**
 * Abstraction for the operation node of object model trees.
 * This object presents all information concerning the given operation.
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public interface ObjectModelOperation extends ObjectModelElement {

    /**
     * Returns the return type of this operation.
     *
     * @return the return type of this operation.
     */
    String getReturnType();

    /**
     * Returns the visibility of this operation.
     * Possible values includes {@code public}, {@code protected} and {@code private}.
     *
     * @return the visibility of this operation.
     */
    String getVisibility();

    /**
     * Returns whether this operation is abstract or not.
     *
     * @return a boolean indicating whether this operation is abstract or not.
     */
    boolean isAbstract();

    /**
     * Returns all parameters defined on this operation.
     *
     * @return a Collection containing all parameters defined on this operation.
     * @see ObjectModelParameter
     */
    Collection<ObjectModelParameter> getParameters();

    /**
     * Return the return parameter of the operation
     *
     * @return an ObjectModelParameter representing the return parameter
     */
    ObjectModelParameter getReturnParameter();

//    /**
//    * In implementation you must write a good equals method
//    */
//    boolean equals(Object o);

    /**
     * Returns all exception qualified names thrown by this operation
     * (as strings)
     *
     * @return a Set containing the exceptions strings
     */
    Set<String> getExceptions();

    /**
     * Return body of the operation (source code)
     *
     * @return body of the operation (source code)
     */
    String getBodyCode();

} //ObjectModelOperation
