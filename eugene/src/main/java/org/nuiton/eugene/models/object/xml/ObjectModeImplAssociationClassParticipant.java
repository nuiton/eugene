/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.nuiton.eugene.GeneratorUtil;

/**
 * ObjectModeImplAssociationClassParticipant.
 *
 * @author cedric, chatellier
 */
public class ObjectModeImplAssociationClassParticipant extends
        ObjectModelImplRef {

    // class
    protected ObjectModelAssociationClassImpl associationClass;

    // role
    protected String attributeName;

    public ObjectModeImplAssociationClassParticipant() {
    }

    /**
     * @param associationClass the associationClass to set
     */
    public void setAssociationClass(
            ObjectModelAssociationClassImpl associationClass) {
        this.associationClass = associationClass;
    }

    public void setAttribute(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeName() {
        if (attributeName == null || "".equals(attributeName)) {
            for (ObjectModeImplAssociationClassParticipant participant :
                    associationClass.getParticipantsRefs()) {
                if (!equals(participant)) {
                    attributeName = GeneratorUtil.toLowerCaseFirstLetter(
                            GeneratorUtil.getClassNameFromQualifiedName(participant.getName()));
                    break;
                }
            }
        }
        return attributeName;
    }

}
