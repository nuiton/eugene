/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

/**
 * Abstraction for the root node of object model trees.
 * This an entry point for browsing a model tree. This object offers
 * as well several facilities for a direct access to some of the object model elements.
 *
 * Created: april 23th 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public interface ObjectModelDependency {

    /**
     * Dependency name
     *
     * @return a String
     */
    String getName();

    /**
     * Supplier classifier of the dependency
     *
     * @return an ObjectModelClassifier
     */
    ObjectModelClassifier getSupplier();

    /**
     * Client classifier of the dependency
     *
     * @return an ObjectModelClassifier
     */
    ObjectModelClassifier getClient();
}
