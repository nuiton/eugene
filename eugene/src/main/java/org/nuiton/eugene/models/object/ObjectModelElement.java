/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import org.nuiton.eugene.models.extension.tagvalue.WithTagValuesOrStereotypes;

import java.util.List;

/**
 * ObjectModelElement.
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public interface ObjectModelElement extends WithTagValuesOrStereotypes {

    /**
     * Returns the name of this element.
     *
     * @return the name of this element.
     */
    String getName();

    /**
     * Returns the element in which this element is defined, or null if there's none.
     *
     * @return the ObjectModelElement in which this element is defined, or null if there's none.
     */
    ObjectModelElement getDeclaringElement();

    /**
     * Returns the whole documentation associated with this element (description + source documentation).
     *
     * @return the whole documentation associated with this element.
     */
    String getDocumentation();

    /**
     * The description of this element is the upper part of the element's
     * documentation.
     *
     * The other part of the document can be accessed with
     * {@link #getSourceDocumentation()}
     *
     * @return the description associated with this element.
     */
    String getDescription();

    /**
     * Returns the source documentation part associated with this element.
     * Source documentation is at end of documentation and are separated of
     * over documentation by "--"
     *
     * @return the source documentation part associated with this element.
     */
    String getSourceDocumentation();

    /**
     * Return if this element has static declaration, only valid when
     * getDeclaringElement is classifier. Not possible for the moment
     * to have static innerClass (from XMI 1.2 and 2.1).
     *
     * @return true if element is static
     */
    boolean isStatic();

    /**
     * Returns all comments lied to this particular model element
     *
     * @return a List containing all comments for this element as Strings.
     */
    List<String> getComments();

} //ObjectModelElement
