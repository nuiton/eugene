package org.nuiton.eugene.models.extension.tagvalue;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.extension.tagvalue.matcher.TagValueDefinitionMatcher;

import java.util.Set;

/**
 * Created on 24/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface TagValueMetadata {

    /**
     * Get the stereotype name.
     *
     * @return the stereotype name
     */
    String getName();

    /**
     * Define the types of object model api which can use this tag value.
     *
     * @return the set of target object model element which can accept the tag value.
     */
    Set<Class<?>> getTargets();

    /**
     * Get the i18n documentation key.
     *
     * @return the i18n documentation key
     */
    String getDescription();

    /**
     * @return default value for this tag value
     */
    String getDefaultValue();

    /**
     * @return {@code true} if this stereotype is deprecated
     */
    boolean isDeprecated();

    /**
     * @return the tag value type
     */
    Class<?> getType();

    Class<? extends TagValueDefinitionMatcher> getMatcherClass();
}
