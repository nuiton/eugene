/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state.xml;

import org.nuiton.eugene.models.state.StateModelState;
import org.nuiton.eugene.models.state.StateModelTransition;

/**
 * StateModelTransitionImpl
 *
 * @author chatellier
 */
public class StateModelTransitionImpl implements StateModelTransition {

    /**
     * Destination state name
     */
    protected String stateName;

    /**
     * Event
     */
    protected String event;

    /**
     * Destination state reference
     */
    protected StateModelState state;

    /**
     * Constructor
     */
    public StateModelTransitionImpl() {
    }

    /**
     * ToState name
     *
     * @param stateName name
     */
    public void setToState(String stateName) {
        this.stateName = stateName;
    }

    /**
     * Get toState name
     *
     * @return the stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * Permet de positionner un lien sur l'instance de l'état.
     *
     * On ne peut pas le faire directement car le fichier xml n'est
     * potentielement par ordonne, et par concequent, les etat n'ont pas encore
     * ete traite.
     *
     * @param state state to set
     */
    public void setState(StateModelState state) {
        this.state = state;
    }

    @Override
    public StateModelState getDestinationState() {
        return state;
    }

    @Override
    public String getEvent() {
        return event;
    }

    /**
     * Set transition event
     *
     * @param event event name
     */
    public void setEvent(String event) {
        this.event = event;
    }
}
