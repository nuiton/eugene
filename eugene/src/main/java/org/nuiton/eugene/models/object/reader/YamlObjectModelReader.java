/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.reader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.ModelHelper;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.reader.yaml.LoadYamlFile;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.File;
import java.io.IOException;

/**
 * To read object model from yaml files into an memory object model.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.eugene.ModelReader" role-hint="yamlobjectmodel"
 * @since 2.6.3
 */
public class YamlObjectModelReader extends AbstractObjectModelReader {

    private static final Log log = LogFactory.getLog(YamlObjectModelReader.class);

    protected LoadYamlFile loaderYAML;

    @Override
    public String getInputType() {
        return ModelHelper.ModelInputType.YAML.getAlias();
    }

    @Override
    protected void beforeReadFile(File... files) {
        super.beforeReadFile(files);
        loaderYAML = new LoadYamlFile();
    }

    @Override
    protected void readFileToModel(File file, ObjectModel model) throws IOException {
        try {
            loaderYAML.loadFile(file, model);
        } catch (YAMLException e) {
            throw new IOException("Unable to parse ObjectModel input file : " + file, e);
        }
    }

}
