package org.nuiton.eugene.models.extension.tagvalue.provider;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import org.nuiton.eugene.models.extension.tagvalue.MismatchTagValueTargetException;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueNotFoundException;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.matcher.StartsWithTagNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.matcher.TagValueDefinitionMatcher;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 24/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class DefaultTagValueMetadatasProvider implements TagValueMetadatasProvider {

    protected final Set<TagValueMetadata> tagValues;
    protected final Set<TagValueDefinitionMatcher> matcher;

    public DefaultTagValueMetadatasProvider(TagValueMetadata... tagValues) {
        this.tagValues = ImmutableSet.copyOf(tagValues);
        this.matcher = ImmutableSet.of(
                new EqualsTagValueNameMatcher(getDefinitionForMatcher(EqualsTagValueNameMatcher.class)),
                new StartsWithTagNameMatcher(getDefinitionForMatcher(StartsWithTagNameMatcher.class)));
    }

    @Override
    public Set<TagValueMetadata> getTagValues() {
        return tagValues;
    }

    @Override
    public void validate(String tagValueName, Class<?> type) throws TagValueNotFoundException, MismatchTagValueTargetException {
        Optional<TagValueMetadata> def = getTagValue(tagValueName);

        if (!def.isPresent()) {
            throw new TagValueNotFoundException();
        }

        boolean valid = false;

        for (Class<?> target : def.get().getTargets()) {
            if (target.equals(type) || target.isAssignableFrom(type)) {

                // found one accepting target
                valid = true;
                break;
            }
        }

        if (!valid) {
            throw new MismatchTagValueTargetException();
        }
    }

    @Override
    public Optional<TagValueMetadata> getTagValue(String tagValueName) {
        for (TagValueDefinitionMatcher tagValueDefinitionMatcher : matcher) {
            TagValueMetadata def = tagValueDefinitionMatcher.match(tagValueName);
            if (def != null)
                return Optional.of(def);
        }
        return Optional.absent();
    }

    protected <M extends TagValueDefinitionMatcher> Set<TagValueMetadata> getDefinitionForMatcher(Class<M> matcherType) {
        Set<TagValueMetadata> result = new LinkedHashSet<>();
        for (TagValueMetadata entry : getTagValues()) {
            if (matcherType.equals(entry.getMatcherClass())) {
                result.add(entry);
            }
        }
        return result;
    }

}
