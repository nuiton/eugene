package org.nuiton.eugene.models.extension.io;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.SystemUtils;
import org.nuiton.eugene.models.extension.model.ModelExtension;
import org.nuiton.eugene.models.extension.model.ModelExtensionAttribute;
import org.nuiton.eugene.models.extension.model.ModelExtensionClass;
import org.nuiton.eugene.models.extension.model.ModelExtensionPackage;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.Set;

/**
 * Created on 08/10/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
class ModelExtensionWriterIniImpl extends ModelExtensionWriter {

    private final String eol = SystemUtils.LINE_SEPARATOR;

    @Override
    public void write(ObjectModel model, Writer writer) throws IOException {

        if (model.getStereotypes().size() > 0 || model.getTagValues().size() > 0) {
            writer.append("[model]").append(eol);
            appendMap(model.getTagValues(), "", writer);
            appendSet(model.getStereotypes(), "", writer);
        }

        if (CollectionUtils.isNotEmpty(model.getPackages())) {

            for (ObjectModelPackage packageBean : model.getPackages()) {

                if (packageBean.getStereotypes().size() > 0 || packageBean.getTagValues().size() > 0) {
                    writer.append(eol).append("[package ").append(packageBean.getName()).append("]").append(eol);
                    appendMap(packageBean.getTagValues(), "", writer);
                    appendSet(packageBean.getStereotypes(), "", writer);
                }
            }

        }

        if (CollectionUtils.isNotEmpty(model.getClasses())) {

            for (ObjectModelClass classBean : model.getClasses()) {

                boolean addSection = classBean.getStereotypes().size() > 0 || classBean.getTagValues().size() > 0;

                if (!addSection) {


                    for (ObjectModelAttribute attributeBean : classBean.getAttributes()) {

                        addSection = attributeBean.getStereotypes().size() > 0 || attributeBean.getTagValues().size() > 0;

                        if (addSection) {
                            break;
                        }
                    }

                }

                if (addSection) {

                    writer.append(eol).append("[class ").append(classBean.getName()).append("]").append(eol);
                    appendMap(classBean.getTagValues(), "", writer);
                    appendSet(classBean.getStereotypes(), "", writer);

                    for (ObjectModelAttribute attributeBean : classBean.getAttributes()) {
                        appendMap(attributeBean.getTagValues(), attributeBean.getName() + ".", writer);
                        appendSet(attributeBean.getStereotypes(), attributeBean.getName() + ".", writer);
                    }

                }

            }

        }

    }

    @Override
    public void write(ModelExtension model, Writer writer) throws IOException {

        writer.append("[model]").append(eol);
        appendMap(model.getTagValues(), "", writer);

        if (model.withPackages()) {

            for (ModelExtensionPackage modelExtensionPackage : model.getPackages()) {
                writer.append(eol).append("[package ").append(modelExtensionPackage.getName()).append("]").append(eol);
                appendMap(modelExtensionPackage.getTagValues(), "", writer);
            }

        }

        if (model.withClasses()) {

            for (ModelExtensionClass modelExtensionClass : model.getClasses()) {

                writer.append(eol).append("[class ").append(modelExtensionClass.getName()).append("]").append(eol);
                appendMap(modelExtensionClass.getTagValues(), "", writer);

                for (ModelExtensionAttribute modelExtensionAttribute : modelExtensionClass.getAttributes()) {
                    appendMap(modelExtensionAttribute.getTagValues(), modelExtensionAttribute.getName() + ".", writer);
                }

            }

        }

    }

    private void appendMap(Map<String, String> map, String prefix, Writer writer) throws IOException {
        if (!map.isEmpty()) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                writer.append(prefix).append(entry.getKey()).append(" = ").append(entry.getValue()).append(eol);
            }
        }
    }

    private void appendSet(Set<String> set, String prefix, Writer writer) throws IOException {
        if (!set.isEmpty()) {
            for (String s : set) {
                writer.append(prefix).append(s).append(" = true").append(eol);
            }
        }
    }
}
