/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.models.object;

import org.nuiton.util.StringUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * To build seriablVersionUID for a givne objectModelClassifier.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class SerialVersionUIDBuilder {

    protected ObjectModelClassifier clazz;

    protected List<String> interfaceSignatures;

    protected List<String> fieldSignatures;

    protected List<String> methodSignatures;

    public static long computeDefaultSUID(ObjectModelClassifier psiClass) {

        SerialVersionUIDBuilder builder = new SerialVersionUIDBuilder(psiClass);
        long result = builder.buildUID();
        return result;
    }

    public SerialVersionUIDBuilder(ObjectModelClassifier clazz) {
        this.clazz = clazz;

        interfaceSignatures = new ArrayList<>();
        for (ObjectModelInterface method : clazz.getInterfaces()) {
            interfaceSignatures.add(method.getQualifiedName());
        }
        Collections.sort(interfaceSignatures);
        if (clazz instanceof ObjectModelClass) {
            ObjectModelClass objectModelClass = (ObjectModelClass) clazz;
            for (ObjectModelClass modelClass :
                    objectModelClass.getSuperclasses()) {
                interfaceSignatures.add(modelClass.getQualifiedName());
            }
        }

        methodSignatures = new ArrayList<>();
        for (ObjectModelOperation method :
                clazz.getAllInterfaceOperations(true)) {
            StringBuilder sb = new StringBuilder();
            sb.append(method.getReturnType()).append(method.getName());
            for (ObjectModelParameter parameter : method.getParameters()) {
                sb.append(parameter.getType()).append(parameter.getName());
            }
            for (String exception : method.getExceptions()) {
                sb.append(exception);
            }
            methodSignatures.add(sb.toString());
        }
        Collections.sort(methodSignatures);

        fieldSignatures = new ArrayList<>();
        for (ObjectModelAttribute field : clazz.getAllInterfaceAttributes()) {
            StringBuilder sb = new StringBuilder();
            sb.append(field.getType()).append(field.getName());
            fieldSignatures.add(sb.toString());
        }
        Collections.sort(fieldSignatures);
    }

    public long buildUID() {
        StringBuilder sb = new StringBuilder();

        String className = clazz.getName();
        sb.append(className);

        for (String interfactSignature : getFieldSignatures()) {
            sb.append(interfactSignature);
        }

        for (String fieldSignature : getFieldSignatures()) {
            sb.append(fieldSignature);
        }

        for (String methodSignature : getMethodSignatures()) {
            sb.append(methodSignature);
        }

        byte[] digestBytes = StringUtil.encodeSHA1(sb.toString()).getBytes();
        long serialVersionUID = 0L;
        for (int i = Math.min(digestBytes.length, 8) - 1; i >= 0; i--) {
            serialVersionUID = serialVersionUID << 8 | digestBytes[i] & 0xFF;
        }
        return serialVersionUID;

    }

    public List<String> getInterfaceSignatures() {
        return interfaceSignatures;
    }

    public List<String> getFieldSignatures() {
        return fieldSignatures;
    }

    public List<String> getMethodSignatures() {
        return methodSignatures;
    }
}
