package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * User: agiraudet
 * Date: 10/06/13
 * Time: 16:11
 */
public class LoadYamlFile implements KeyWords {

    Yaml yaml;

    public LoadYamlFile() {
        yaml = new Yaml();
    }

    public void loadFile(File file, ObjectModel model) throws IOException {
        //parse le fichier YAML
        InputStream inputModel = new FileInputStream(file);
        Object modelYAML = yaml.load(inputModel);
        inputModel.close();

        ObjectModelImpl modelOM = (ObjectModelImpl) model;
        YamlObject modelYAMLO = new YamlObject();

        //recherche la version
        Object version = null;
        String syntaxeVersion = "0";
        String defaultVersion = "0";

        if (modelYAML instanceof List) {
            version = YamlUtil.collectElementList((List) modelYAML, SYNTAXE);
            syntaxeVersion = "1";
            defaultVersion = "0";
        } else if (modelYAML instanceof Map) {
            version = YamlUtil.collectElementMap((Map) modelYAML, SYNTAXE);
            syntaxeVersion = "2";
            defaultVersion = "0";
        }

        if (version != null) {
            syntaxeVersion = YamlUtil.beforeChar(String.valueOf(version), '.');
            defaultVersion = YamlUtil.afterChar(String.valueOf(version), '.');
        }

        //charge le modele en fonction de la version
        if (syntaxeVersion.equals("1")) {
            SyntaxePureYaml.loadYamlObject(modelYAML, modelYAMLO);
        } else if (syntaxeVersion.equals("2")) {
            SyntaxeUserFriendly.loadYamlObject(modelYAML, modelYAMLO);
        } else {
            ;//impossible de charger le modele
        }

        LoadObjectModel loader = new LoadObjectModel(modelYAMLO, modelOM, DefaultValues.getDefaultValues(defaultVersion));//sortir getDefaultValues ?
        loader.loadModel();
    }
}
