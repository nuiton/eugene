/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.AbstractGenerator;
import org.nuiton.eugene.MonitorWriter;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * StateModelGenerator.
 *
 * Pour utiliser ce type de générateur, il faut implanter au moins une des
 * méthodes generateFrom... et le getFilenameFor... associé si l'on souhaite un
 * nom de fichier convenable. Si dans une méthode generateFrom... on utilise pas
 * le writer (output) alors aucun fichier n'est généré.
 *
 * Le nom de l'argument writer doit absolument etre output et pas autre chose si
 * vous souhaitez utiliser le processeur
 * {@code org.nuiton.processor.filters.GeneratorTemplatesFilter} pour vous
 * s'implifier l'écriture des templates.
 *
 * @author chatellier
 */
public class StateModelGenerator extends AbstractGenerator<StateModel> {

    /**
     * Logger for this class
     */
    private static Log log = LogFactory.getLog(StateModelGenerator.class);

    /**
     * Empty constructor
     */
    public StateModelGenerator() {
    }

    /**
     * Constructor with parent generator.
     *
     * @param parent parent generator
     */
    public StateModelGenerator(AbstractGenerator<StateModel> parent) {
        super(parent);
    }

    /**
     * Par défaut, appel {@link #generateFromModel(MonitorWriter, StateModel)} pour le
     * model et {@link #generateFromState(Writer, StateModelState)} pour tous les
     * etats du modele.
     *
     * @param stateModel Le modele d'état
     * @param destDir    le dossier de destination
     * @throws IOException if any IO problem while applying templtate
     */
    @Override
    public void applyTemplate(StateModel stateModel, File destDir) throws IOException {

        model = stateModel;

        String filename = getFilenameFromModel(stateModel);
        File outputFile = getDestinationFile(destDir, filename);
        if (isOverwrite() || !isNewerThanSource(outputFile)) {
            try (MonitorWriter monitorOut = new MonitorWriter(new StringWriter())) {
                generateFromModel(monitorOut, stateModel);
                write(outputFile, monitorOut);
            } catch (Exception eee) {
                log.warn("Erreur lors de la génération du fichier "
                         + outputFile);
                throw new RuntimeException(
                        "Erreur lors de la génération du fichier " + outputFile,
                        eee);
            }
        }

        // pour tous les diagrammes du modele
        for (StateModelStateChart chart : stateModel.getStateCharts()) {

            // elements can be restricted in package
            if (canGenerateElement(chart)) {

                // et tous les états de ces diagrammes
                for (Object oState : chart.getStates().toArray()) {
                    StateModelState state = (StateModelState) oState;
                    String filenameState = getFilenameFromState(state, chart
                            .getName());
                    File outputFiletate = getDestinationFile(destDir, filenameState);
                    if (isOverwrite() || !isNewerThanSource(outputFiletate)) {
                        try (MonitorWriter monitorOut = new MonitorWriter(new StringWriter())) {
                            generateFromState(monitorOut, state);
                            write(outputFiletate, monitorOut);
                        } catch (Exception eee) {
                            log.warn("Erreur lors de la génération du fichier "
                                     + outputFiletate);
                            throw new RuntimeException(
                                    "Erreur lors de la génération du fichier "
                                    + outputFiletate, eee);
                        }
                    }
                }
            }
        }
    }

    /**
     * Test if given element can be generated.
     *
     * @param chart chart to test
     * @return generation allowed
     */
    protected boolean canGenerateElement(StateModelStateChart chart) {

        boolean canGenerate = true;

        // disabled until tested
        //String chartPackage = chart.getPackageName();
        //canGenerate = super.canGeneratePackage(chartPackage);

        return canGenerate;
    }

    /**
     * Generate model code
     *
     * @param monitorOut the output writer
     * @param stateModel the model
     */
    protected void generateFromModel(MonitorWriter monitorOut, StateModel stateModel) {

    }

    /**
     * Return filename from model
     *
     * @param stateModel the model
     * @return model file name
     */
    protected String getFilenameFromModel(StateModel stateModel) {
        return stateModel.getName();
    }

    /**
     * Return filename from state
     *
     * @param state       the state
     * @param packageName the package name
     * @return the filename
     */
    public String getFilenameFromState(StateModelState state, String packageName) {
        return (packageName + '.' + state.getName()).replace('.', File.separatorChar);
    }

    /**
     * Generate a state code
     *
     * @param monitorOut the output writer
     * @param state      the state
     * @throws IOException if any IO problem while generating
     */
    public void generateFromState(Writer monitorOut, StateModelState state) throws IOException {

    }
}
