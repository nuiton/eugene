package org.nuiton.eugene.models.object;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.extension.tagvalue.WithTagValuesOrStereotypes;

import java.util.List;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.12
 */
public interface ObjectModelPackage extends WithTagValuesOrStereotypes {

    /**
     * @return the name of this package.
     */
    String getName();

    /**
     * @return the number of sub packages. (0 for a root package).
     */
    int getNumberOfSubPackages();

    /**
     * @return parent package or {@code null} if the package is a root package.
     */
    ObjectModelPackage getParentPackage();

    /**
     * Returns all packages defined in this package.
     *
     * @return a Collection containing all ObjectModelPackages for this package.
     * @see ObjectModelPackage
     */
    Iterable<ObjectModelPackage> getPackages();

    /**
     * Indicates whether the package contains the package associated to the given package name
     *
     * @param packageName - the name of the package to retrieve.
     * @return true if the package has been found.
     */
    boolean hasPackage(String packageName);

    /**
     * Returns all classifiers defined in this package. (Except innerClasses)
     *
     * @return a Collection containing all ObjectModelClassifier for this package.
     * @see ObjectModelClassifier
     */
    Iterable<ObjectModelClassifier> getClassifiers();

    /**
     * Returns the classifier corresponding to the given simple name, or null if the package contains no classifier for this simple name.
     *
     * @param simpleName - the qualified name of the classifier to retrieve.
     * @return the ObjectModelClassifier of the found classifier, or null if the model contains no classifier for this simple name.
     */
    ObjectModelClassifier getClassifier(String simpleName);

    /**
     * Returns all classes defined in this package. (Except innerClasses)
     *
     * @return a Collection containing all ObjectModelClass for this package.
     * @see ObjectModelClass
     */
    Iterable<ObjectModelClass> getClasses();

    /**
     * Returns the class corresponding to the given qualified name, or null if the package contains no class for this simple name.
     *
     * @param simpleName - the qualified name of the class to retrieve.
     * @return the ObjectModelClass of the found class, or null if the package contains no class for this simple name.
     */
    ObjectModelClass getClass(String simpleName);


    /**
     * Indicates whether the package contains the class associated to the given className
     *
     * @param simpleName - the qualified name of the class to retrieve.
     * @return true if the class has been found.
     */
    boolean hasClass(String simpleName);

    /**
     * Returns all interfaces defined in this package.
     *
     * @return a Collection containing all ObjectModelInterface for this package.
     * @see ObjectModelInterface
     */
    Iterable<ObjectModelInterface> getInterfaces();

    /**
     * Returns the interface corresponding to the given qualified name, or null if the package contains no interface for this simple name.
     *
     * @param simpleName the qualified name of the interface to retrieve.
     * @return the ObjectModelInterface of the found interface, or null if the package contains no interface for this simple name.
     */
    ObjectModelInterface getInterface(String simpleName);


    /**
     * Returns all enumerations defined in this package.
     *
     * @return a Collection containing all ObjectModelEnumeration for this package.
     * @see ObjectModelEnumeration
     */
    Iterable<ObjectModelEnumeration> getEnumerations();

    /**
     * Return the enumeration corresponding to the given simple name
     *
     * @param simpleName the fully qualified name of the enumeration to retrieve.
     * @return the ObjectModelEnumeration of the found enumeration or null if the package contains no enumeration for this simple name.
     */
    ObjectModelEnumeration getEnumeration(String simpleName);

    /**
     * Returns all comments not lied to a particular package element
     *
     * @return a List containing all comments for this package as Strings.
     */
    List<String> getComments();

    /**
     * Returns the whole documentation associated with this element (description + source documentation).
     *
     * @return the whole documentation associated with this element.
     */
    String getDocumentation();

    /**
     * The description of this element is the upper part of the element's
     * documentation.
     *
     * The other part of the document can be accessed with
     * {@link #getSourceDocumentation()}
     *
     * @return the description associated with this element.
     */
    String getDescription();

    /**
     * Returns the source documentation part associated with this element.
     * Source documentation is at end of documentation and are separated of
     * over documentation by "--"
     *
     * @return the source documentation part associated with this element.
     */
    String getSourceDocumentation();

} //ObjectModelPackage