/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.validator;

import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;

/**
 * AttributeNamesValidator.
 *
 * Created: 7 mars 2006
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class AttributeNamesValidator extends NameBasedValidator {

    public AttributeNamesValidator(ObjectModel model) {
        super(model, false);
    }

    public AttributeNamesValidator(ObjectModel model, boolean caseSensitive) {
        super(model, caseSensitive);
    }

    @Override
    protected boolean validateAttribute(ObjectModelAttribute attr) {
        String name = attr.getName();
        if (containsName(name)) {
            addError(attr.getDeclaringElement(), getReason(name));
            return false;
        }
        return true;
    }

} //AttributeNamesValidator
