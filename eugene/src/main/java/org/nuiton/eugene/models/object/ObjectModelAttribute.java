/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

/**
 * Abstraction for the attribute node of object model trees.
 * This object presents all information concerning the given attribute.
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public interface ObjectModelAttribute extends ObjectModelParameter {

    /**
     * Returns the type of this attribute.
     *
     * @return the type of this attribute.
     */
    String getType();

    /**
     * Returns whether this attribute is an aggregate or not.
     *
     * @return {@code true} if this attribute is an aggregate or not.
     */
    boolean isAggregate();

    /**
     * Returns whether this attribute is a composite or not.
     *
     * @return {@code true} if this attribute is a composite or not.
     */
    boolean isComposite();

    /**
     * Returns the visibility of this attribute.
     *
     * Possible values includes <tt>public</tt>, <tt>protected</tt>
     * and <tt>private</tt>.
     *
     * @return the visibility of this attribute.
     */
    String getVisibility();

    /**
     * Returns whether this attribute reference a model classifier or not.
     *
     * @return a boolean indicating whether this attribute reference a model
     * classifier or not.
     */
    boolean referenceClassifier();

    /**
     * Returns the classifier referenced by this attribute or null if it does
     * not reference a model classifier.
     *
     * @return the ObjectModelClassfifier referenced by this attribute or null
     * if it does not reference a model classifier.
     */
    ObjectModelClassifier getClassifier();

    /**
     * Returns the attribute used to reference this class at the other end of
     * the association or null if this is not an association, or if it is not
     * bi-directionnal.
     *
     * @return the ObjectModelAttribute used to reference this class at the
     * other end of the association or null if this is not an
     * association, or if it is not bi-directionnal.
     * @see #getClassifier()
     */
    ObjectModelAttribute getReverseAttribute();

    /**
     * Return attribute reserver attribute name.
     *
     * @return reverse attribute name or null
     */
    String getReverseAttributeName();

    /**
     * max multiplicity of the reverse attribute
     *
     * @return max multiplicity of the reverse attribute
     */
    int getReverseMaxMultiplicity();

    /**
     * @return a ObjectModelClass corresponding to the association class
     * associated with this association, or null if there is none.
     */
    ObjectModelClass getAssociationClass();

    /**
     * @return {@code true} if this association has an associated association
     * class, ot not.
     */
    boolean hasAssociationClass();

    /**
     * @return {@code true}  if this attribute is static or not.
     */
    boolean isStatic();

    /**
     * @return {@code true}  if this attribute is final or not.
     */
    boolean isFinal();

    /**
     * @return {@code true}  if this attribute is transient.
     */
    boolean isTransient();

    /**
     * @return {@code true} if this attribute is navigable.
     */
    boolean isNavigable();
} //ObjectModelAttribute
