/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state.xml;

import org.nuiton.eugene.models.state.StateModelComplexState;
import org.nuiton.eugene.models.state.StateModelSimpleState;
import org.nuiton.eugene.models.state.StateModelState;
import org.nuiton.eugene.models.state.StateModelTransition;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * StateModelComplexeStateImpl.java
 *
 * @author chatellier
 */
public class StateModelComplexeStateImpl extends StateModelStateImpl implements
        StateModelComplexState {

    /**
     * States'set of this state
     */
    protected Map<String, StateModelState> mapState;

    /**
     * Constructor
     */
    public StateModelComplexeStateImpl() {
        mapState = new HashMap<>();
    }

    /**
     * Add a state
     *
     * @param state the state
     */
    public void addState(StateModelState state) {
        mapState.put(state.getName(), state);
    }

    /* (non-Javadoc)
     * @see org.nuiton.eugene.models.state.StateModelComplexeState#getStates()
     */
    public Collection<StateModelState> getStates() {
        return mapState.values();
    }

    /**
     * Correct association, because, the xml file migth be non ordonated
     *
     * @param parent
     */
    void correctTransitionNameToInstance(StateModelComplexeStateImpl parent) {

        // iterator

        for (StateModelState stateModelState : mapState.values()) {
            StateModelState state = stateModelState;

            // reboucle si l'etat est complexe
            if (state instanceof StateModelComplexeStateImpl) {
                ((StateModelComplexeStateImpl) state)
                        .correctTransitionNameToInstance(this);
            } else {
                for (StateModelTransition tr : state.getTransitions()) {
                    StateModelTransitionImpl tri =
                            (StateModelTransitionImpl) tr;
                    String name = tri.getStateName();

                    // l'etat apartient au cas complexe courant
                    if (getState(name) != null) {
                        tri.setState(getState(name));
                    } else {
                        // l'etat apartient au cas complexe parent
                        if (parent != null && parent.getState(name) != null) {
                            tri.setState(parent.getState(name));
                        }
                        // sinon il reste a null, tant pis
                    }
                }
            }
        }
    }

    /**
     * @param stateName a state or null if state doesnt exists
     * @return a state ref by his name
     */
    StateModelState getState(String stateName) {
        return mapState.get(stateName);
    }

    @Override
    public boolean isComplex() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public StateModelState getInitialState() {

        StateModelState response = null;

        // iterator

        for (StateModelState stateModelState : mapState.values()) {
            StateModelState state = stateModelState;

            // if state is simple
            if (!state.isComplex()) {
                StateModelSimpleState simpleState =
                        (StateModelSimpleState) state;

                if (simpleState.isInitial()) {
                    // get(0), normalement il n'y a qu'une transition sur
                    // un etat initial
                    response = simpleState.getTransitions().get(0)
                            .getDestinationState();
                }
            }
        }

        return response;
    }

}
