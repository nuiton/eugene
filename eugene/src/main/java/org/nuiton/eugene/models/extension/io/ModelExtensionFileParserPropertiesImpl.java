package org.nuiton.eugene.models.extension.io;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.models.extension.tagvalue.InvalidStereotypeSyntaxException;
import org.nuiton.eugene.models.extension.tagvalue.InvalidTagValueSyntaxException;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.object.reader.InvalidModelPropertiesException;
import org.nuiton.util.RecursiveProperties;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;

/**
 * Created on 09/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
class ModelExtensionFileParserPropertiesImpl extends ModelExtensionFileParser {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ModelExtensionFileParserPropertiesImpl.class);

    protected final boolean strictLoading;

    public ModelExtensionFileParserPropertiesImpl(boolean strictLoading) {
        this.strictLoading = strictLoading;
    }

    @Override
    public void parse(File file, ModelExtensionFileParserCallback callback) throws IOException, InvalidStereotypeSyntaxException, InvalidTagValueSyntaxException {

        Properties prop = new RecursiveProperties();

        try (Reader inStream = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)) {
            prop.load(inStream);
        }

        // get all the tagValues keys and sort them
        List<String> keys = new ArrayList<>();
        CollectionUtils.addAll(keys, prop.stringPropertyNames());
        Collections.sort(keys);

        boolean safe = true;
        for (String key : keys) {
            String value = prop.getProperty(key);
            safe &= consumeKeyValue(key, value, callback);

        }

        if (strictLoading && !safe) {

            String errorMessage = "There is some tag value(s) or stereotype(s) failed to be imported from " + file;
            throw new InvalidModelPropertiesException(errorMessage);

        }

    }

    protected boolean consumeKeyValue(String key, String value, ModelExtensionFileParserCallback callback) throws InvalidStereotypeSyntaxException, InvalidTagValueSyntaxException {

        boolean safe = true;

        boolean stereoTypeKey = isStereotype(key);
        boolean tagValueTypeKey = isTagValue(key);

        if (log.isDebugEnabled()) {
            log.debug("Property: '" + key + "' = " + value);
        }

        if (key.startsWith(MODEL)) {

            // model stereotype
            if (stereoTypeKey) {

                Set<String> stereotypes = TagValueUtil.getStereotypes(value);
                for (String stereotype : stereotypes) {
                    safe &= callback.onModelStereotypeFound(stereotype);
                }
                return safe;

            }

            // model tag value
            if (tagValueTypeKey) {

                Matcher matcher = TagValueUtil.getModelMatcher(key);

                String tag = matcher.group(2);

                return callback.onModelTagValueFound(tag, value);

            }

            throw new InvalidModelPropertiesException(key + " is not a valid model entry.");

        }

        if (key.startsWith(PACKAGE)) {

            // package stereotype or tag value

            if (stereoTypeKey) {

                // package stereotype
                Matcher matcher = TagValueUtil.getPackageStereotypeMatcher(key);

                String fqn = matcher.group(1);
                fqn = StringUtil.substring(fqn, 0, -1); // remove ended .

                Set<String> stereotypes = TagValueUtil.getStereotypes(value);
                for (String stereotype : stereotypes) {
                    safe &= callback.onPackageStereotypeFound(fqn, stereotype);
                }
                return safe;

            }

            if (tagValueTypeKey) {

                // package tag value

                Matcher matcher = TagValueUtil.getPackageMatcher(key);

                String packageName = matcher.group(1);
                packageName = StringUtil.substring(packageName, 0, -1); // remove ended .
                String tag = matcher.group(3);

                return callback.onPackageTagValueFound(packageName, tag, value);

            }

            throw new InvalidModelPropertiesException(key + " is not a valid package entry.");

        }

        // element tag value or stereotype

        if (stereoTypeKey) {

            // stereotype property

            // check key is ok
            Matcher matcher = TagValueUtil.getStereotypeMatcher(key);

            String fqn = matcher.group(1);
            fqn = StringUtil.substring(fqn, 0, -1); // remove ended .
            // target is class, attribute or operation
            String target = matcher.group(2);
            String targetName = matcher.group(3);

            Set<String> stereotypes = TagValueUtil.getStereotypes(value);
            for (String stereotype : stereotypes) {

                if (CLASS.equals(target)) {
                    safe &= callback.onClassStereotypeFound(fqn, stereotype);
                } else if (ATTRIBUTE.equals(target)) {
                    safe &= callback.onAttributeStereotypeFound(fqn, targetName, stereotype);
                }
            }
            return safe;

        }
        if (tagValueTypeKey) {

            // tag value property

            Matcher matcher = TagValueUtil.getMatcher(key);

            String fqn = matcher.group(1);
            fqn = StringUtil.substring(fqn, 0, -1); // remove ended dot
            // target is class, attribute or operation
            String target = matcher.group(2);
            // name of the target (can be null on class)
            String targetName = matcher.group(3);
            // type is tagvalue
            String type = matcher.group(4).toLowerCase();
            // name of the tag value
            String tag = matcher.group(5);

            if (CLASS.equals(target)) {
                return callback.onClassTagValueFound(fqn, tag, value);
            } else if (ATTRIBUTE.equals(target)) {
                return callback.onAttributeTagValueFound(fqn, targetName, tag, value);
            }
        }

        throw new InvalidModelPropertiesException(key + " is not a valid class or attribute entry.");
    }

    protected boolean isTagValue(String key) {
        return key.contains(TAGVALUE) || key.contains(TAG_VALUE);
    }

    protected boolean isStereotype(String key) {
        return key.contains(STEREOTYPE);
    }

}
