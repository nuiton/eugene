package org.nuiton.eugene.models.extension.io;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Created on 09/10/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public enum ModelExtensionFormat {

    ini {
        @Override
        public ModelExtensionFileParser newParser(boolean strictLoading) {
            return new ModelExtensionFileParserIniImpl(strictLoading);
        }

        @Override
        public ModelExtensionWriter newWriter() {
            return new ModelExtensionWriterIniImpl();
        }
    },
    properties {
        @Override
        public ModelExtensionFileParser newParser(boolean strictLoading) {
            return new ModelExtensionFileParserPropertiesImpl(strictLoading);
        }

        @Override
        public ModelExtensionWriter newWriter() {
            return new ModelExtensionWriterPropertiesImpl();
        }
    };

    public abstract ModelExtensionFileParser newParser(boolean strictLoading);

    public abstract ModelExtensionWriter newWriter();
}
