/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.GeneratorUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * ExternalCacheExtension
 *
 * Created: 2 nov. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public class ExternalCacheExtension {

    private static final Log log = LogFactory.getLog(ExternalCacheExtension.class);

    public static final String OBJECTMODEL_EXTENSION = "externalClassifiers";

    protected Map<String, ObjectModelClassifierImpl> cache;

    public ExternalCacheExtension() {
        cache = new HashMap<>();
    }

    @SuppressWarnings("unchecked")
    public <C extends ObjectModelClassifierImpl> C getCache(
            ObjectModelImplRef reference, Class<C> classifierClass)
            throws RuntimeException {
        ObjectModelClassifierImpl classifier = cache.get(reference.getName());
        C result;
        if (classifier != null &&
            !classifierClass.isAssignableFrom(classifier.getClass())) {
            throw new ClassCastException(
                    "Invalid cast for " + classifierClass.getName());
        }
        if (classifier == null) {
            try {
                result = classifierClass.newInstance();
                addClassifierToCache(reference, result);
                if (log.isDebugEnabled()) {
                    log.debug("Add '" + reference.getName() +
                              "' to external cache");
                }
            } catch (Exception eee) {
                // IllegalAccessException and InstantiationException
                throw new RuntimeException(
                        "Unable to add new '" + classifierClass.getName() +
                        "' to cache for '" + reference.getName() + "'", eee);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Get '" + reference.getName() +
                          "' from external cache");
            }
            result = (C) classifier;
        }
        return result;
    }

    protected void addClassifierToCache(ObjectModelImplRef reference,
                                        ObjectModelClassifierImpl classifier) {
        String fqn = reference.getName();
        String packageName = GeneratorUtil.getParentPackageName(fqn);
        String name = GeneratorUtil.getClassNameFromQualifiedName(fqn);
        classifier.setName(name);
        classifier.setPackage(packageName);
        classifier.postInit(); // to create qualifiedName
        classifier.setExtern(true);
        cache.put(reference.getName(), classifier);
    }


}
