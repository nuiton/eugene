package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * User: agiraudet
 * Date: 11/06/13
 * Time: 14:51
 */
public class DefaultValues implements KeyWords {

    Yaml yaml;

    public DefaultValues() {
        yaml = new Yaml();
    }

    public static Map<String, String> getDefaultValues(String version) {
        Map<String, String> values = new LinkedHashMap<>();

        //retourne les valeurs par defaut en fonction de la version dans une Map
        if (version.equals("0")) {
            values.put(ATTRIBUTE + SEPARATOR + MIN_MULTIPLICITY, "1");
            values.put(ATTRIBUTE + SEPARATOR + MAX_MULTIPLICITY, "1");
            values.put(ATTRIBUTE + SEPARATOR + REVERSE_MAX_MULTIPLICITY, "1");
        }
        /*else if(version.equals(""))
        {
            ;
        }*/

        return values;
    }

    //retourne les valeurs par defaut du fichier YAML passé en parametre
    public Map<String, String> getDefaultValues(File file) throws IOException {
        InputStream inputModel = new FileInputStream(file);
        Object valuesYAML = yaml.load(inputModel);
        inputModel.close();

        Map<String, String> values = new LinkedHashMap<>();
        if (valuesYAML instanceof Map) {
            for (Object entry : ((Map) valuesYAML).entrySet()) {
                if (entry instanceof Map.Entry) {
                    values.put(String.valueOf(((Map.Entry) entry).getKey()), String.valueOf(((Map.Entry) entry).getValue()));
                }
            }
        }

        return values;
    }
}
