package org.nuiton.eugene.models.extension.model;

/*-
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 10/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ModelExtension extends ModelExtensionElement {

    protected final Set<ModelExtensionPackage> packages;
    protected final Set<ModelExtensionClass> classes;

    public ModelExtension(String name) {
        super(name);
        this.packages = new TreeSet<>();
        this.classes = new TreeSet<>();
    }

    public Set<ModelExtensionPackage> getPackages() {
        return packages;
    }

    public Set<ModelExtensionClass> getClasses() {
        return classes;
    }

    public ModelExtensionPackage getOrCreatePackage(final String packageName) {

        ModelExtensionPackage result = null;
        for (ModelExtensionPackage modelExtensionPackage : packages) {
            if (packageName.equals(modelExtensionPackage.name)) {
                result = modelExtensionPackage;
                break;
            }
        }
        if (result == null) {
            result = new ModelExtensionPackage(packageName);
            packages.add(result);
        }
        return result;
    }

    public ModelExtensionClass getOrCreateClass(final String className) {

        ModelExtensionClass result = null;
        for (ModelExtensionClass packageBean : classes) {
            if (className.equals(packageBean.name)) {
                result = packageBean;
                break;
            }
        }
        if (result == null) {
            result = new ModelExtensionClass(className);
            classes.add(result);
        }
        return result;
    }

    public ModelExtensionAttribute getOrCreateClassAttribute(String className, String attributeName) {
        ModelExtensionClass modelExtensionClass = getOrCreateClass(className);
        return modelExtensionClass.getOrCreateAttribute(attributeName);
    }

    public boolean withClasses() {
        return !classes.isEmpty();
    }

    public boolean withPackages() {
        return !packages.isEmpty();
    }
}
