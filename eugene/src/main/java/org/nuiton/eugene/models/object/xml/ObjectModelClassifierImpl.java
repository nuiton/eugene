/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelDependency;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * ObjectModelClassifierImpl.
 *
 * @author chatellier
 * @author cedric
 */
public abstract class ObjectModelClassifierImpl extends ObjectModelElementImpl implements ObjectModelClassifier {

    protected boolean extern;

    protected String qualifiedName;

    protected String packageName;

    protected List<ObjectModelInterface> interfaces;

    protected List<ObjectModelImplRef> interfacesRefs = new ArrayList<>();

    protected List<ObjectModelOperation> operations = new ArrayList<>();

    protected Map<String, ObjectModelAttribute> attributes = new HashMap<>();

    protected List<ObjectModelAttribute> orderedAttributes = new ArrayList<>();

    protected List<ObjectModelDependency> dependencies = new ArrayList<>();

    protected String type;

    protected boolean inner;

    @Override
    public String toString() {
        return "" + getQualifiedName() + " implements " + getInterfaces();
    }

    @Override
    public void postInit() {
        super.postInit();
        qualifiedName = packageName + "." + name;
    }

    public void setExtern(boolean extern) {
        this.extern = extern;
    }

    public void setPackage(String packageName) {
        this.packageName = packageName;
    }

    public void setInner(boolean inner) {
        this.inner = inner;
    }

    @Override
    public boolean isInner() {
        return inner;
    }

    public void addInterface(ObjectModelImplRef ref) {
        //if (ref == null)
        //    return new ObjectModelImplRef();
        interfacesRefs.add(ref);
        //return ref;
    }

    public void addOperation(ObjectModelOperationImpl operation) {
        //if (operation == null)
        //    return new ObjectModelOperationImpl(objectModelImpl, this);
        operation.postInit();
        operation.setDeclaringElement(this);
        operations.add(operation);
        //return operation;
    }

    public void addAttribute(ObjectModelAttributeImpl attribute) {
        attribute.postInit();
        attribute.setDeclaringElement(this);
        attributes.put(attribute.getName(), attribute);
        orderedAttributes.add(attribute);
    }

    public void addDependency(ObjectModelDependencyImpl dependency) {
        dependency.postInit();
        dependency.setClient(this);
        dependencies.add(dependency);
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isExtern() {
        return extern;
    }

    @Override
    public String getPackageName() {
        return packageName;
    }

    @Override
    public String getQualifiedName() {
        return qualifiedName;
    }

    @Override
    public Collection<ObjectModelInterface> getInterfaces() {
        if (interfaces == null) {
            interfaces = new ArrayList<>();
            for (ObjectModelImplRef ref : interfacesRefs) {

                ObjectModelInterfaceImpl interfacez =
                        (ObjectModelInterfaceImpl)
                                objectModelImpl.getInterface(ref.getName());

                if (interfacez == null) { // Interface not exist in model

                    ExternalCacheExtension cache =
                            objectModelImpl.getExtension(
                                    ExternalCacheExtension.OBJECTMODEL_EXTENSION,
                                    ExternalCacheExtension.class);

                    // get external interface from cache (or create it)
                    interfacez =
                            cache.getCache(ref, ObjectModelInterfaceImpl.class);
                }
                interfaces.add(interfacez);
            }
        }
        return interfaces;
    }

    @Override
    public Collection<ObjectModelOperation> getOperations(String name) {
        List<ObjectModelOperation> result = new ArrayList<>();
        for (ObjectModelOperation op : getOperations()) {
            if (name.equals(op.getName())) {
                result.add(op);
            }
        }
        return result;
    }

    @Override
    public Collection<ObjectModelOperation> getOperations() {
        return operations;
    }

    @Override
    public Collection<ObjectModelOperation> getAllOtherOperations(
            boolean distinct) {
        return getAllInterfaceOperations(distinct);
    }

    @Override
    public Collection<ObjectModelOperation> getAllInterfaceOperations(
            boolean distinct) {
        Collection<ObjectModelOperation> result;
        if (distinct) {
            result = new HashSet<>();
        } else {
            result = new LinkedList<>();
        }
        getAllInterfaceOperations(result);
        return result;
    }

    protected Collection<ObjectModelOperation> getAllInterfaceOperations(
            Collection<ObjectModelOperation> result) {
        for (ObjectModelClassifier interfacez : getInterfaces()) {
            result.addAll(interfacez.getOperations());
            ((ObjectModelClassifierImpl) interfacez).getAllInterfaceOperations(result);
        }
        return result;
    }

    @Override
    public Collection<ObjectModelAttribute> getAttributes() {
        return orderedAttributes;
    }

    /**
     * Returns the attribute corresponding to the given name, or null if the
     * class contains no attribute for this name.
     *
     * @return the ObjectModelAttribute of the found attribute, or null if the
     * class contains no attribute for this name.
     */
    @Override
    public ObjectModelAttribute getAttribute(String attributeName) {
        return attributeName == null ? null : attributes.get(attributeName);
    }

    @Override
    public Collection<ObjectModelAttribute> getAllInterfaceAttributes() {
        Collection<ObjectModelAttribute> result = new LinkedList<>();
        getAllInterfaceAttributes(result);
        return result;
    }

    @Override
    public Collection<ObjectModelAttribute> getAllOtherAttributes() {
        Collection<ObjectModelAttribute> result = getAllInterfaceAttributes();
        return result;
    }

    protected Collection<ObjectModelAttribute> getAllInterfaceAttributes(
            Collection<ObjectModelAttribute> result) {
        for (Object o : getInterfaces()) {
            ObjectModelClassifierImpl clazz = (ObjectModelClassifierImpl) o;
            result.addAll(clazz.getAttributes());
            clazz.getAllInterfaceAttributes(result);
        }
        return result;
    }

    @Override
    public Collection<ObjectModelDependency> getDependencies() {
        return dependencies;
    }

    @Override
    public ObjectModelDependency getDependency(String name) {
        if (name.isEmpty()) {
            return null;
        }
        for (ObjectModelDependency dependency : dependencies) {
            if (dependency.getName().equalsIgnoreCase(name)) {
                return dependency;
            }
        }
        return null;
    }

    @Override
    public final boolean isClass() {
        return this instanceof ObjectModelClass;
    }

    @Override
    public final boolean isInterface() {
        return this instanceof ObjectModelInterface;
    }

    @Override
    public final boolean isEnum() {
        return this instanceof ObjectModelEnumeration;
    }
}
