package org.nuiton.eugene.models.extension.tagvalue;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Set;

/**
 * Created on 4/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public interface WithTagValuesOrStereotypes {

    boolean hasStereotype(String stereotypeName);

    Set<String> getStereotypes();

    void addStereotype(String stereotypeName);

    void removeStereotype(String stereotypeName);

    /**
     * Returns the tagValues associated with this element.
     * For each entry, the key is the name of the tagValue, the value is the value of the tagValue :-)
     *
     * @return a Map containing all tagValues associated with this element
     */
    Map<String, String> getTagValues();

    /**
     * Returns the tagValue corresponding to the given name, or null if the element has no associated tagValue for this name.
     *
     * @param tagValue tag value key
     * @return the value of the found tagValue, or null if the element has no associated tagValue for this name.
     */
    String getTagValue(String tagValue);

    /**
     * Adds the given {@code value} associated to the {@code tagValue}.
     *
     * Note: If a previous tag value was definied, then it will be replaced.
     *
     * @param tagValue the name of the tag value
     * @param value    the value to associate
     * @since 2.1.2
     */
    void addTagValue(String tagValue, String value);

    /**
     * Returns whether this element has a tagValue corresponding to the given name, or not.
     *
     * @param tagValue tag value name
     * @return a boolean indicating whether this element has a tagValue corresponding to the given name, or not.
     */
    boolean hasTagValue(String tagValue);

    void removeTagValue(String tagvalue);

}
