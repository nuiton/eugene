/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import com.google.common.collect.ImmutableSet;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelModifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * ObjectModelEnumerationImpl.
 *
 * Created: may 4th 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public class ObjectModelEnumerationImpl extends ObjectModelClassifierImpl implements ObjectModelEnumeration {

    /**
     * Collection of references corresponding to literal values
     */
    private Collection<ObjectModelImplRef> literalRefs = new ArrayList<>();

    /**
     * Add a literal to the ObjectModelEnumeration from Digester
     *
     * @param ref corresponding to a Literal value
     */
    public void addLiteral(ObjectModelImplRef ref) {
        literalRefs.add(ref);
    }

    private static Set<ObjectModelModifier> authorizedModifiers;

    @Override
    protected Set<ObjectModelModifier> getAuthorizedModifiers() {
        if (authorizedModifiers == null) {
            // Nothing special ?
            authorizedModifiers = ImmutableSet.of();
        }
        return authorizedModifiers;
    }

    @Override
    public Collection<String> getLiterals() {
        Collection<String> results = new ArrayList<>();
        for (ObjectModelImplRef ref : literalRefs) {
            results.add(ref.getName());
        }
        return results;
    }

}
