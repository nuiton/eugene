/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import java.util.Collection;

/**
 * Abstraction for the class node of object model trees.
 * This object presents all information concerning the given class.
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public interface ObjectModelClass extends ObjectModelClassifier {

    /**
     * Returns all parent classes for this class.
     *
     * @return a Collection containing all parent ObjectModelClass for this class.
     */
    Collection<ObjectModelClass> getSuperclasses();

    /**
     * Returns all inner classes for this class.
     *
     * @return a Collection containing all inner ObjectModelClass for this class.
     */
    Collection<ObjectModelClassifier> getInnerClassifiers();

    /**
     * Returns the discriminator for the given superclass.
     * (name of the inheritance relation).
     *
     * @param superclass super class to get discriminator
     * @return the discriminator for the given superclass as a String if it exists, null otherwise.
     */
    String getDiscriminator(ObjectModelClass superclass);

    /**
     * Returns all known direct specialized classes for this class.
     *
     * @return a Collection containing all known direct specialized ObjectModelClass for this class.
     */
    Collection<ObjectModelClass> getSpecialisations();

    /**
     * Returns all known direct specialized classes for this class for the
     * specified discriminator.
     *
     * @param discriminator discriminator to get specialisations
     * @return a Collection containing all known direct specialized
     * ObjectModelClass for this class for the specified discriminator.
     */
    Collection<ObjectModelClass> getSpecialisations(String discriminator);

    /**
     * Returns whether this class is abstract or not.
     *
     * @return a boolean indicating whether this class is abstract or not.
     */
    boolean isAbstract();

//    /**
//    * Returns whether this class is inner an other class or not.
//    *
//    * @return a boolean indicating whether this class is inner an other class or not.
//    */
//    boolean isInner();

    /**
     * Returns all operations defined on all Super class extended by this
     * classifier, directly or indirectly. and all interface implemented by the
     * super class.
     *
     * @param distinct if this boolean is true only distinct operation
     *                 are add to list.
     * @return a Collection of ObjectModelOperation
     */
    Collection<ObjectModelOperation> getAllSuperclassOperations(boolean distinct);

} //ObjectModelClass
