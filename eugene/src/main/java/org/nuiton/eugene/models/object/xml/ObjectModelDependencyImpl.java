/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelDependency;

/**
 * ObjectModelDependencyImpl.
 *
 * Created: april 23th 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public class ObjectModelDependencyImpl extends ObjectModelImplRef implements ObjectModelDependency {

    /**
     * Supplier name from XML file
     */
    private String supplierName;

    /**
     * Implementation of client to get the model when supplier is needed
     */
    private ObjectModelClassifierImpl client;

    private ObjectModelClassifier supplier;

    /**
     * Method call for Digester setting properties of Dependency
     *
     * @param supplierName supplier name
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    /**
     * The object instance of supplier is getting from model when supplier is null
     *
     * @return an ObjectModelClassifier corresponding to the supplier of the dependency
     */
    @Override
    public ObjectModelClassifier getSupplier() {
        if (supplier == null) {
            ObjectModelClassifier classifier = client.getModel().getClassifier(supplierName);
            supplier = classifier;
        }
        return supplier;
    }

    public void setClient(ObjectModelClassifierImpl client) {
        this.client = client;
    }

    @Override
    public ObjectModelClassifier getClient() {
        return client;
    }

}
