/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state.xml;

import org.nuiton.eugene.models.state.StateModelSimpleState;

/**
 * StateModelSimpleStateImpl.java
 *
 * @author chatellier
 */
public class StateModelSimpleStateImpl extends StateModelStateImpl implements
        StateModelSimpleState {

    /** init state ? */
    protected boolean initialState;

    /** final state ? */
    protected boolean finalState;

    /**
     * Constructeur
     */
    public StateModelSimpleStateImpl() {
        initialState = finalState = false;
    }

    /**
     * @return the finalState
     */
    @Override
    public boolean isFinal() {
        return finalState;
    }

    /**
     * @param finalState the finalState to set
     */
    public void setFinal(boolean finalState) {
        this.finalState = finalState;
    }

    /**
     * @return the initialState
     */
    @Override
    public boolean isInitial() {
        return initialState;
    }

    /**
     * @param initialState the initialState to set
     */
    public void setInitial(boolean initialState) {
        this.initialState = initialState;
    }

}
