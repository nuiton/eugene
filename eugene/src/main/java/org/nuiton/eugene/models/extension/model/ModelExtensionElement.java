package org.nuiton.eugene.models.extension.model;

/*-
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 10/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ModelExtensionElement implements Comparable<ModelExtensionElement> {

    protected final String name;
    protected final Map<String, String> tagValues;

    public ModelExtensionElement(String name) {
        this.name = name;
        this.tagValues = new TreeMap<>();
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getTagValues() {
        return tagValues;
    }

    public Set<String> getStereotypes() {
        Set<String> result = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : tagValues.entrySet()) {
            if ("true".equals(entry.getValue())) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    @Override
    public int compareTo(ModelExtensionElement o) {
        return name.compareTo(o.name);
    }

}
