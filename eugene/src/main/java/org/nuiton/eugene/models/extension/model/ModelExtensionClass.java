package org.nuiton.eugene.models.extension.model;

/*-
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created on 10/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ModelExtensionClass extends ModelExtensionElement {

    protected final Set<ModelExtensionAttribute> attributes;

    public ModelExtensionClass(String name) {
        super(name);
        this.attributes = new TreeSet<>();
    }

    public Set<ModelExtensionAttribute> getAttributes() {
        return attributes;
    }

    public Map<String, String> getAttributesTagValues() {
        Map<String, String> attributesTagValues = new TreeMap<>();
        for (ModelExtensionAttribute modelExtensionAttribute : attributes) {

            for (Map.Entry<String, String> entry : modelExtensionAttribute.tagValues.entrySet()) {
                if (!"true".equals(entry.getValue())) {
                    attributesTagValues.put(modelExtensionAttribute.name + "." + entry.getKey(), entry.getValue());
                }
            }
        }
        return attributesTagValues;
    }

    public Set<String> getAttributesStereotypes() {
        Set<String> attributesStereotypes = new TreeSet<>();
        for (ModelExtensionAttribute modelExtensionAttribute : attributes) {

            for (String entry : modelExtensionAttribute.getStereotypes()) {
                attributesStereotypes.add(modelExtensionAttribute.name + "." + entry);
            }
        }
        return attributesStereotypes;
    }

    public ModelExtensionAttribute getOrCreateAttribute(String attributeName) {
        ModelExtensionAttribute result = null;
        for (ModelExtensionAttribute modelExtensionAttribute : attributes) {
            if (attributeName.equals(modelExtensionAttribute.name)) {
                result = modelExtensionAttribute;
                break;
            }
        }
        if (result == null) {
            result = new ModelExtensionAttribute(attributeName);
            attributes.add(result);
        }
        return result;
    }
}
