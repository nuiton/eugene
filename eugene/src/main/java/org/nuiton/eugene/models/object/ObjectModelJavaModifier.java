/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.models.object;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * Enum that represents Java possible modifiers
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.4.3
 */
public enum ObjectModelJavaModifier implements ObjectModelModifier {

    STATIC,
    FINAL,
    ABSTRACT,
    TRANSIENT,

    PUBLIC,
    PROTECTED,
    PRIVATE,
    PACKAGE,

    VOLATILE,
    SYNCHRONIZED,
    NATIVE,
    STRICTFP;

    public static final Set<? extends ObjectModelModifier> visibilityModifiers =
            ImmutableSet.of(PUBLIC, PROTECTED, PRIVATE, PACKAGE);

    @Override
    public boolean isVisibility() {
        return visibilityModifiers.contains(this);
    }

    @Override
    public boolean isAssociationType() {
        return false;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String toString() {
        String result = name().toLowerCase();
        if (PACKAGE.equals(this)) {
            result = "";
        }
        return result;
    }

    public static ObjectModelJavaModifier fromVisibility(String name) {
        if (name.equals(PUBLIC.toString())) {
            return PUBLIC;
        } else if (name.equals(PRIVATE.toString())) {
            return PRIVATE;
        } else if (name.equals(PROTECTED.toString())) {
            return PROTECTED;
        } else if (name.equals(PACKAGE.toString())) {
            return PACKAGE;
        } else {
            return null;
        }
    }

}
