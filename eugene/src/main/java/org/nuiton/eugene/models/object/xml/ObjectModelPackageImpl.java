package org.nuiton.eugene.models.object.xml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class ObjectModelPackageImpl implements ObjectModelPackage {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ObjectModelPackageImpl.class);

    protected ObjectModelImpl objectModelImpl;

    protected boolean extern;

    protected String name;

    protected int numberOfSubPackages;

    protected ObjectModelPackage parentPackage;

    protected String documentation;

    protected Set<String> stereotypes = new HashSet<>();

    protected Map<String, String> tagValues = new HashMap<>();

    protected List<String> comments = new ArrayList<>();

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getNumberOfSubPackages() {
        return numberOfSubPackages;
    }

    @Override
    public ObjectModelPackage getParentPackage() {
        return parentPackage;
    }

    @Override
    public String getDescription() {
        return getDocumentation().substring(0, getDocumentation().indexOf("--"));
    }

    @Override
    public String getSourceDocumentation() {
        return getDocumentation().substring(getDocumentation().indexOf("--") + 2);
    }

    @Override
    public Set<String> getStereotypes() {
        return stereotypes;
    }

    @Override
    public boolean hasStereotype(String stereotypeName) {
        return stereotypes.contains(stereotypeName);
    }

    @Override
    public void addStereotype(String stereotype) {
        stereotypes.add(stereotype);
    }

    @Override
    public void removeStereotype(String stereotype) {
        stereotypes.remove(stereotype);
    }

    @Override
    public Map<String, String> getTagValues() {
        return tagValues;
    }

    @Override
    public String getTagValue(String tagValue) {
        return tagValue == null ? null : tagValues.get(tagValue);
    }

    @Override
    public boolean hasTagValue(String tagValue) {
        return tagValues.containsKey(tagValue);
    }

    @Override
    public void addTagValue(String tagValue, String value) {
        String oldValue = getTagValue(tagValue);
        if (StringUtils.isNotEmpty(oldValue)) {
            if (oldValue.equals(value)) {
                // same tag value do not replace it
                return;
            }
            log.warn("Replace tagValue '" + tagValue + "' (old:" + oldValue + ", new: " + value + ")");
        }
        tagValues.put(tagValue, value);
    }

    @Override
    public void removeTagValue(String tagvalue) {
        tagValues.remove(tagvalue);
    }

    @Override
    public List<String> getComments() {
        return comments;
    }

    @Override
    public String getDocumentation() {
        return documentation;
    }

    @Override
    public Iterable<ObjectModelPackage> getPackages() {
        return Iterables.filter(objectModelImpl.getPackages(), IS_PACKAGE_NAME_STARTS_WITH_PREDICATE);
    }

    @Override
    public boolean hasPackage(String packageName) {
        ObjectModelPackage aPackage = objectModelImpl.getPackage(packageName);
        return aPackage != null && IS_PACKAGE_NAME_STARTS_WITH_PREDICATE.apply(aPackage);
    }

    @Override
    public Iterable<ObjectModelClassifier> getClassifiers() {
        return Iterables.filter(objectModelImpl.getClassifiers(), IS_PACKAGE_NAME_EQUALS_PREDICATE);
    }

    @Override
    public ObjectModelClassifier getClassifier(String simpleName) {
        Predicate<ObjectModelClassifier> predicate = newClassifierNameEquals(simpleName);
        Iterable<ObjectModelClassifier> filter = Iterables.filter(objectModelImpl.getClassifiers(), predicate);
        return Iterables.getFirst(filter, null);
    }

    @Override
    public Iterable<ObjectModelClass> getClasses() {
        return Iterables.filter(objectModelImpl.getClasses(), IS_PACKAGE_NAME_EQUALS_PREDICATE);
    }

    @Override
    public ObjectModelClass getClass(String simpleName) {
        Predicate<ObjectModelClassifier> predicate = newClassifierNameEquals(simpleName);
        Iterable<ObjectModelClass> filter = Iterables.filter(objectModelImpl.getClasses(), predicate);
        return Iterables.getFirst(filter, null);
    }

    @Override
    public boolean hasClass(String simpleName) {
        return false;
    }

    @Override
    public Iterable<ObjectModelInterface> getInterfaces() {
        return Iterables.filter(objectModelImpl.getInterfaces(), IS_PACKAGE_NAME_EQUALS_PREDICATE);
    }

    @Override
    public ObjectModelInterface getInterface(String simpleName) {
        Predicate<ObjectModelClassifier> predicate = newClassifierNameEquals(simpleName);
        Iterable<ObjectModelInterface> filter = Iterables.filter(objectModelImpl.getInterfaces(), predicate);
        return Iterables.getFirst(filter, null);
    }

    @Override
    public Iterable<ObjectModelEnumeration> getEnumerations() {
        return Iterables.filter(objectModelImpl.getEnumerations(), IS_PACKAGE_NAME_EQUALS_PREDICATE);
    }

    @Override
    public ObjectModelEnumeration getEnumeration(String simpleName) {
        Predicate<ObjectModelClassifier> predicate = newClassifierNameEquals(simpleName);
        Iterable<ObjectModelEnumeration> filter = Iterables.filter(objectModelImpl.getEnumerations(), predicate);
        return Iterables.getFirst(filter, null);
    }

    public boolean isExtern() {
        return extern;
    }

    public void setExtern(boolean extern) {
        this.extern = extern;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public void addComment(String comment) {
        comments.add(comment);
    }

    public ObjectModelImplRef addStereotype(ObjectModelImplRef stereotype) {
        if (stereotype == null) {
            return new ObjectModelImplRef();
        }
        stereotypes.add(stereotype.getName());
        return stereotype;
    }

    public ObjectModelImplTagValue addTagValue(ObjectModelImplTagValue tagValue) {
        if (tagValue == null) {
            return new ObjectModelImplTagValue();
        }
        addTagValue(tagValue.getName(), tagValue.getValue());
        return tagValue;
    }

    public void setObjectModelImpl(ObjectModelImpl objectModelImpl) {
        this.objectModelImpl = objectModelImpl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentPackage(ObjectModelPackage parentPackage) {
        this.parentPackage = parentPackage;
    }

    public void postInit() {
        numberOfSubPackages = name.split("\\.").length;
    }

    protected final Predicate<ObjectModelClassifier> IS_PACKAGE_NAME_EQUALS_PREDICATE = new PackageNameEqualsPredicate(name);

    protected final Predicate<ObjectModelPackage> IS_PACKAGE_NAME_STARTS_WITH_PREDICATE = new PackageNameStartsWithPredicate(name + ".");

    protected Predicate<ObjectModelClassifier> newClassifierNameEquals(String simpleName) {
        Predicate<ObjectModelClassifier> predicate = Predicates.and(IS_PACKAGE_NAME_EQUALS_PREDICATE, new SimpleNameEqualsPredicate(simpleName));
        return predicate;
    }

    protected static class SimpleNameEqualsPredicate implements Predicate<ObjectModelClassifier> {

        protected final String name;

        public SimpleNameEqualsPredicate(String name) {
            this.name = name;
        }

        @Override
        public boolean apply(ObjectModelClassifier input) {
            return input.getName().equals(name);
        }
    }

    protected static class PackageNameEqualsPredicate implements Predicate<ObjectModelClassifier> {

        protected final String name;

        public PackageNameEqualsPredicate(String name) {
            this.name = name;
        }

        @Override
        public boolean apply(ObjectModelClassifier input) {
            return input.getPackageName().equals(name);
        }
    }

    protected static class PackageNameStartsWithPredicate implements Predicate<ObjectModelPackage> {

        protected final String name;

        public PackageNameStartsWithPredicate(String name) {
            this.name = name;
        }

        @Override
        public boolean apply(ObjectModelPackage input) {
            return input.getName().startsWith(name);
        }
    }
}
