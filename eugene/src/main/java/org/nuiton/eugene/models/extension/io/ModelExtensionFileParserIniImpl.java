package org.nuiton.eugene.models.extension.io;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

/**
 * Created on 09/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
class ModelExtensionFileParserIniImpl extends ModelExtensionFileParser {

    public ModelExtensionFileParserIniImpl(boolean strictLoading) {
        this.strictLoading = strictLoading;
    }

    protected final boolean strictLoading;

    @Override
    public void parse(File file, ModelExtensionFileParserCallback callback) throws IOException {

        INIConfiguration iniConfiguration = new INIConfiguration();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            iniConfiguration.read(reader);
        } catch (ConfigurationException e) {
            throw new IOException("Could not read ini configuration from file: " + file, e);
        }

        SubnodeConfiguration modelSection = iniConfiguration.getSection("model");

        Iterator<String> modelSectionKeys = modelSection.getKeys();
        while (modelSectionKeys.hasNext()) {
            String key = modelSectionKeys.next();
            String value = modelSection.getString(key);

            key = key.replaceAll("\\.\\.", ".");

            if ("true".equals(value)) {

                // stereotype de package
                callback.onModelStereotypeFound(key);
            } else {

                // tag value de package
                callback.onModelTagValueFound(key, value);
            }
        }

        for (String sectionName : iniConfiguration.getSections()) {
            if (sectionName == null) {
                continue;
            }

            SubnodeConfiguration section = iniConfiguration.getSection(sectionName);

            if (sectionName.startsWith("package ")) {
                String packageName = StringUtils.removeStart(sectionName, "package ");
                Iterator<String> keys = section.getKeys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    String value = section.getString(key);

                    key = key.replaceAll("\\.\\.", ".");

                    if ("true".equals(value)) {

                        // stereotype de package
                        callback.onPackageStereotypeFound(packageName, key);
                    } else {

                        // tag value de package
                        callback.onPackageTagValueFound(packageName, key, value);
                    }
                }

            } else if (sectionName.startsWith("class ")) {

                String className = StringUtils.removeStart(sectionName, "class ");
                Iterator<String> keys = section.getKeys();
                while (keys.hasNext()) {

                    String key = keys.next();
                    String value = section.getString(key);

                    key = key.replaceAll("\\.\\.", ".");

                    if ("true".equals(value)) {

                        // stereotype de package
                        int attributeIndex = key.indexOf('.');
                        if (attributeIndex == -1) {

                            // stereotype de classe
                            callback.onClassStereotypeFound(className, key);
                        } else {

                            // stereotype d'attribut
                            callback.onAttributeStereotypeFound(className, key.substring(0, attributeIndex), key.substring(attributeIndex + 1));
                        }

                    } else {

                        // tag value de package
                        int attributeIndex = key.indexOf('.');
                        if (attributeIndex == -1) {

                            // tag value de classe
                            callback.onClassTagValueFound(className, key, value);
                        } else {
                            // tag value d'attribut
                            callback.onAttributeTagValueFound(className, key.substring(0, attributeIndex), key.substring(attributeIndex + 1), value);
                        }
                    }

                }

            }

        }

    }

}
