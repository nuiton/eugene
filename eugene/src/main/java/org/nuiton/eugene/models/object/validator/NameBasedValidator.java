/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.validator;

import org.nuiton.eugene.models.object.ObjectModel;

import java.util.HashMap;
import java.util.Map;

/**
 * NameBasedValidator.
 *
 * Created: 9 mars 2006
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public abstract class NameBasedValidator extends ObjectModelValidator {

    private boolean caseSensitive;

    private Map<String, String> namesAndReasons;

    public NameBasedValidator(ObjectModel model, boolean caseSensitive) {
        super(model);
        this.caseSensitive = caseSensitive;
    }

    private Map<String, String> getNameAndReasons() {
        if (namesAndReasons == null) {
            namesAndReasons = new HashMap<>();
        }
        return namesAndReasons;
    }

    public void addNameAndReason(String name, String reason) {
        if (name != null) {
            if (!caseSensitive) {
                name = name.toLowerCase();
            }
            getNameAndReasons().put(name, reason);
        }
    }

    public void addNamesAndReasons(Map<String, String> namesAndReasons) {
        for (String key : namesAndReasons.keySet()) {
            addNameAndReason(key, namesAndReasons.get(key));
        }
    }

    public boolean containsName(String name) {
        if (!caseSensitive) {
            name = name.toLowerCase();
        }
        return getNameAndReasons().containsKey(name);
    }

    public String getReason(String name) {
        if (!caseSensitive) {
            name = name.toLowerCase();
        }
        return getNameAndReasons().get(name);
    }

} //NameBasedValidator
