/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state.xml;

import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.ModelHelper;
import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.models.state.StateModel;
import org.nuiton.eugene.models.state.StateModelStateChart;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of the {@link StateModel}.
 *
 * @author chatellier
 */
@Component(role = Model.class, hint = "statemodel")
public class StateModelImpl implements StateModel {

    /**
     * List of charts composing this model
     */
    protected List<StateModelStateChart> listStateCharts;

    /**
     * Name of this model
     */
    protected String name;

    /**
     * Version of this model
     */
    protected String version;

    /**
     * Model tagged values
     */
    protected Map<String, String> modelTagValues;

    /**
     * Used to add others specific object to the model
     * The key defined must be unique to get the significative extension associated to
     */
    private Map<String, Object> extensions = new HashMap<>();

    /**
     * Construteur
     */
    public StateModelImpl() {
        listStateCharts = new ArrayList<>();
        modelTagValues = new HashMap<>();
    }

    @Override
    public String getModelType() {
        return ModelHelper.ModelType.STATE.getAlias();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Set<String> getStereotypes() {
        Set<String> result = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : modelTagValues.entrySet()) {
            if ("true".equals(entry.getValue())) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    @Override
    public boolean hasStereotype(String stereotypeName) {
        return modelTagValues.containsKey(stereotypeName);
    }

    @Override
    public void addStereotype(String stereotype) {
        modelTagValues.put(stereotype, "true");
    }

    @Override
    public void removeStereotype(String stereotype) {
        modelTagValues.remove(stereotype);
    }

    /**
     * Set model name.
     *
     * @param name model name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Add chart.
     *
     * @param chart chart
     */
    public void addStateChart(StateModelStateChart chart) {

        // appele apres construction du StateModelStateChartImpl
        // corrige les liens entre les nom d'etat, et les instances d'etat
        ((StateModelStateChartImpl) chart)
                .correctTransitionNameToInstance(null);

        listStateCharts.add(chart);
    }

    @Override
    public List<StateModelStateChart> getStateCharts() {
        return listStateCharts;
    }

    /**
     * Add a list of stateCharts into current model
     *
     * @param charts list
     */
    public void addAllStateCharts(Collection<StateModelStateChart> charts) {
        listStateCharts.addAll(charts);
    }

    @Override
    public Map<String, String> getTagValues() {
        return modelTagValues;
    }

    @Override
    public void addTagValue(String key, String value) {
        modelTagValues.put(key, value);
    }

    @Override
    public String getTagValue(String key) {
        return key == null ? null : modelTagValues.get(key);
    }

    @Override
    public boolean hasTagValue(String tagValue) {
        return modelTagValues.containsKey(tagValue);
    }

    @Override
    public void removeTagValue(String tagvalue) {
        modelTagValues.remove(tagvalue);
    }

    /**
     * Set model version.
     *
     * @param version model version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String getVersion() {
        return version;
    }

    /**
     * Get the extension associated to the reference (unique). Create it if not exist.
     *
     * @param <O>            object type returned
     * @param reference      unique corresponding to the extension to get
     * @param extensionClass class of the extension
     * @return the object value for the extension
     * @throws ClassCastException when extensionClass is not valid
     * @throws RuntimeException   when instantiation problem to create new extension
     */
    @Override
    @SuppressWarnings("unchecked")
    public <O> O getExtension(String reference, Class<O> extensionClass) throws RuntimeException {
        if (reference == null) {
            throw new NullPointerException("reference parameter can not be null in method ObjectModelImpl#getExtension");
        }
        if (extensionClass == null) {
            throw new NullPointerException("extensionClass parameter can not be null in method ObjectModelImpl#getExtension.");
        }
        Object object = extensions.get(reference);
        O result;
        if (object != null && !extensionClass.isAssignableFrom(object.getClass())) {
            throw new ClassCastException("Invalid cast for " + extensionClass.getName());
        }
        if (object == null) {
            try {
                result = extensionClass.newInstance();
            } catch (Exception eee) { // IllegalAccessException and InstantiationException
                throw new RuntimeException("Unable to create new extension '" + extensionClass.getName() +
                                                   "' for '" + reference + "'", eee);
            }
            extensions.put(reference, result);
        } else {
            result = (O) object;
        }
        return result;
    }

}
