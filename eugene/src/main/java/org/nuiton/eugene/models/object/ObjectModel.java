/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


package org.nuiton.eugene.models.object;

import org.nuiton.eugene.models.Model;

import java.util.Collection;
import java.util.List;

/**
 * Abstraction for the root node of object model trees.
 * This an entry point for browsing a model tree. This object offers
 * as well several facilities for a direct access to some of the object model elements.
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public interface ObjectModel extends Model {

    /**
     * Plexus role-hint
     */
    String NAME = "objectmodel";

    /**
     * Returns all packages defined in this model.
     *
     * @return a Collection containing all ObjectModelPackage for this model.
     * @see ObjectModelPackage
     * @since 2.12
     */
    Collection<ObjectModelPackage> getPackages();

    /**
     * Returns the package corresponding to the given name, or null if the model contains no package for this name.
     *
     * @param qualifiedClassifierName - the name of the package to retrieve.
     * @return the ObjectModelPackage of the found given name, or null if the model contains no package for this name.
     * @since 2.12
     */
    ObjectModelPackage getPackage(String qualifiedClassifierName);

    /**
     * Returns the package of the given classifier, or null if the model contains no package for this classifier.
     *
     * @param classifier the classifier of the package to retrieve.
     * @return the ObjectModelPackage for the given classifier, or null if the model contains no package for this classifier.
     * @since 2.12
     */
    ObjectModelPackage getPackage(ObjectModelClassifier classifier);

    /**
     * Indicates whether the model contains the package associated to the given name
     *
     * @param name - the name of the package to retrieve.
     * @return true if the package has been found.
     * @since 2.12
     */
    boolean hasPackage(String name);

    /**
     * Returns all classifiers defined in this model. (Except innerClasses)
     *
     * @return a Collection containing all ObjectModelClassifier for this model.
     * @see ObjectModelClassifier
     */
    Collection<ObjectModelClassifier> getClassifiers();

    /**
     * Returns the classifier corresponding to the given qualified name, or null if the model contains no classifier for this qualified name.
     *
     * @param qualifiedClassifierName - the qualified name of the classifier to retrieve.
     * @return the ObjectModelClassifier of the found classifier, or null if the model contains no classifier for this qualified name.
     */
    ObjectModelClassifier getClassifier(String qualifiedClassifierName);

    /**
     * Returns all classes defined in this model. (Except innerClasses)
     *
     * @return a Collection containing all ObjectModelClass for this model.
     * @see ObjectModelClass
     */
    Collection<ObjectModelClass> getClasses();

    /**
     * Returns the class corresponding to the given qualified name, or null if the model contains no class for this qualified name.
     *
     * @param qualifiedClassName - the qualified name of the class to retrieve.
     * @return the ObjectModelClass of the found class, or null if the model contains no class for this qualified name.
     */
    ObjectModelClass getClass(String qualifiedClassName);


    /**
     * Indicates whether the model contains the class associated to the given className
     *
     * @param qualifiedClassName - the qualified name of the class to retrieve.
     * @return true if the class has been found.
     */
    boolean hasClass(String qualifiedClassName);

    /**
     * Returns all interfaces defined in this model.
     *
     * @return a Collection containing all ObjectModelInterface for this model.
     * @see ObjectModelInterface
     */
    Collection<ObjectModelInterface> getInterfaces();

    /**
     * Returns the interface corresponding to the given qualified name, or null if the model contains no interface for this qualified name.
     *
     * @param qualifiedInterfaceName the qualified name of the interface to retrieve.
     * @return the ObjectModelInterface of the found interface, or null if the model contains no interface for this qualified name.
     */
    ObjectModelInterface getInterface(String qualifiedInterfaceName);


    /**
     * Returns all enumerations defined in this model.
     *
     * @return a Collection containing all ObjectModelEnumeration for this model.
     * @see ObjectModelEnumeration
     */
    Collection<ObjectModelEnumeration> getEnumerations();

    /**
     * Return the enumeration corresponding to the given qualified name
     *
     * @param qualifiedEnumerationName the fully qualified name of the enumeration to retrieve.
     * @return the ObjectModelEnumeration of the found enumeration or null if the model contains no enumeration for this qualified name.
     */
    ObjectModelEnumeration getEnumeration(String qualifiedEnumerationName);

    /**
     * Returns all comments not lied to a particular model element
     *
     * @return a List containing all comments for this model as Strings.
     */
    List<String> getComments();

//    /**
//     * Get the extension associated to the reference (unique)
//     * @param <O> object type returned
//     * @param reference unique corresponding to the extension to get
//     * @param extensionClass class of the extension
//     * @return the object value for the extension
//     * @throws ClassCastException when extensionClass is not valid
//     * @throws IllegalArgumentException for non existing extension with reference
//     */
//     <O> O getExtension(String reference, Class<O> extensionClass)
//            throws ClassCastException, IllegalArgumentException;

} //ObjectModel
