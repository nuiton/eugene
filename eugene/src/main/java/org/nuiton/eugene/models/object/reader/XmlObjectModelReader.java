/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.reader;

import org.apache.commons.digester3.Digester;
import org.nuiton.eugene.ModelHelper;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.xml.DigesterObjectModelRuleSet;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

/**
 * To read object xml model files into an memory object model.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @plexus.component role="org.nuiton.eugene.ModelReader" role-hint="xmlobjectmodel"
 * @since 2.6.3
 */
public class XmlObjectModelReader extends AbstractObjectModelReader {

    protected Digester digester;

    @Override
    public String getInputType() {
        return ModelHelper.ModelInputType.XML.getAlias();
    }

    @Override
    protected void beforeReadFile(File... files) {
        super.beforeReadFile(files);
        digester = new Digester();
        digester.addRuleSet(new DigesterObjectModelRuleSet());
    }

    @Override
    protected void readFileToModel(File file, ObjectModel model) throws IOException {
        try {
            digester.push(model);
            digester.parse(file);
        } catch (SAXException e) {
            throw new IOException("Unable to parse ObjectModel input file : " + file, e);
        }
    }

}
