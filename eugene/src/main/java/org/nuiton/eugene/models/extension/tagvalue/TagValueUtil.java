package org.nuiton.eugene.models.extension.tagvalue;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 24/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class TagValueUtil {

    /**
     * Pattern to define tag values authorized at classifier level in the model
     * properties file.
     *
     * L'expression réguliere match les chaines de type
     * &lt;package.ClassName&gt;.&lt;class|attribute|operation&gt;.[name].&lt;stereotype|tagvalue&gt;.[tag]
     * fr.isisfish.entities.Population.class.stereotype=entity
     * fr.isisfish.entities.Population.class.tagvalue.persistenceType=flatfile
     * fr.isisfish.entities.Population.attribute.name.stereotype=...
     * fr.isisfish.entities.Population.attribute.name.tagvalue.pk=topiaId
     * fr.isisfish.entities.Population.operation.getRegion.stereotype=...
     * fr.isisfish.entities.Population.operation.getRegion.tagvalue.pk=...
     */
    protected static final Pattern TAG_VALUE_PATTERN =
            Pattern.compile("^((?:[_a-zA-Z0-9]+\\.)+(?:_?[A-Z][_a-zA-Z0-9]*\\.)+)(?:(class|attribute|operation))\\.(?:([_a-zA-Z0-9]+)\\.)?(?:(tagvalue|tagValue)?)\\.((?:[_a-zA-Z0-9]+)+(?:\\.?[_a-zA-Z0-9]+)*)?$");

    /**
     * Pattern to define tag values authorized at model level in the model
     * properties file.
     */
    protected static final Pattern MODEL_TAG_VALUE_PATTERN = Pattern.compile(
            "^model\\.(?:(tagvalue|tagValue)?)\\.((?:[_a-zA-Z0-9]+)+(?:\\.?[_a-zA-Z0-9]+)*)$");

    /**
     * Pattern to define tag values authorized at model level in the model
     * properties file.
     */
    protected static final Pattern PACKAGE_TAG_VALUE_PATTERN = Pattern.compile(
            "^package\\.((?:[_a-zA-Z0-9]+\\.)+)(?:(tagvalue|tagValue)?)\\.((?:[_a-zA-Z0-9]+)+(?:\\.?[_a-zA-Z0-9]+)*)$");
    protected static final Pattern PACKAGE_STEREOTYPE_PATTERN = Pattern
            .compile("^package\\.((?:[_a-zA-Z0-9]+\\.)+)?(?:(stereotype)?)$");
    protected static final Pattern STEREOTYPE_PATTERN = Pattern
            .compile("^((?:[_a-zA-Z0-9]+\\.)+(?:_?[a-zA-Z][_a-zA-Z0-9]*\\.)+)(?:(class|attribute|operation))\\.(?:([_a-zA-Z0-9]+)\\.)?(?:(stereotype)?)$");

    public static Matcher getModelMatcher(String key) throws InvalidTagValueSyntaxException {
        Matcher matcher = MODEL_TAG_VALUE_PATTERN.matcher(key);
        if (!matcher.find()) {
            throw new InvalidTagValueSyntaxException();
        }
        return matcher;
    }

    public static Matcher getPackageMatcher(String key) throws InvalidTagValueSyntaxException {
        Matcher matcher = PACKAGE_TAG_VALUE_PATTERN.matcher(key);
        if (!matcher.find()) {
            throw new InvalidTagValueSyntaxException();
        }
        return matcher;
    }

    public static Matcher getMatcher(String key) throws InvalidTagValueSyntaxException {
        Matcher matcher = TAG_VALUE_PATTERN.matcher(key);
        if (!matcher.find()) {
            throw new InvalidTagValueSyntaxException();
        }
        return matcher;
    }

    /**
     * Seek for a tag value amoung elements given using these rules:
     * <ul>
     * <li>Look into {@code elements} and return the first not empty tag value found.</li>
     * <li>If not found, Look into {@code elements} declaringElement (for each
     * element that is a {@link ObjectModelElement} and return the first not empty tag
     * value found.</li>
     * <li>If not found return {@code defaultValue}</li>
     * </ul>
     * <strong>Note:</strong> Order of {@code elements} is important, better then to
     * always starts from specialized to more general level (for example from attribute,
     * to classifier or model).
     *
     * @param tagName  tag name to find
     * @param elements not null elements to test
     * @return found tag value or {@code null} if not found
     * @since 3.0
     */
    public static String findTagValue(TagValueMetadata tagName, WithTagValuesOrStereotypes... elements) {
        String result = findDirectTagValue(tagName, elements);

        if (result != null) {
            return result;
        }

        for (WithTagValuesOrStereotypes element : elements) {
            if (element instanceof ObjectModelElement) {
                // try in declaring element
                ObjectModelElement declaringElement = ((ObjectModelElement) element).getDeclaringElement();
                if (declaringElement != null) {
                    String value = findNotEmptyTagValue(tagName, declaringElement);
                    if (value != null) {
                        return value;
                    }
                }
            }
        }
        return tagName.getDefaultValue();
    }

    /**
     * Seek for a tag value amoung elements given using these rules:
     * <ul>
     * <li>Look into {@code elements} and return the first not empty tag value found.</li>
     * <li>If not found, Look into {@code elements} declaringElement (for each
     * element that is a {@link ObjectModelElement} and return the first not empty tag
     * value found.</li>
     * <li>If not found return {@code defaultValue}</li>
     * </ul>
     * <strong>Note:</strong> Order of {@code elements} is important, better then to
     * always starts from specialized to more general level (for example from attribute,
     * to classifier or model).
     *
     * @param tagName      tag name to find
     * @param defaultValue default value to use if not found
     * @param elements     not null elements to test
     * @return found tag value or {@code defaultValue} if not found
     * @since 3.0
     */
    public static String findTagValue(String tagName, String defaultValue, WithTagValuesOrStereotypes... elements) {
        String result = findDirectTagValue(tagName, elements);

        if (result != null) {
            return result;
        }

        for (WithTagValuesOrStereotypes element : elements) {
            if (element instanceof ObjectModelElement) {
                // try in declaring element
                ObjectModelElement declaringElement = ((ObjectModelElement) element).getDeclaringElement();
                if (declaringElement != null) {
                    String value = findNotEmptyTagValue(tagName, declaringElement);
                    if (value != null) {
                        return value;
                    }
                }
            }
        }
        return defaultValue;
    }

    public static boolean findBooleanTagValue(TagValueMetadata tagName, WithTagValuesOrStereotypes... elements) {
        String value = findTagValue(tagName, elements);
        return value != null && "true".equalsIgnoreCase(value);
    }

    /**
     * Seek for a Boolean tag value.
     *
     * Will first the tag value using the method {@link #findTagValue(TagValueMetadata, WithTagValuesOrStereotypes...)}.
     *
     * If not found, return {@code null}, otherwise return boolean value (case is ignored).
     *
     * <strong>Note:</strong> Order of {@code elements} is important, better then to
     * always starts from specialized to more general level (for example from attribute,
     * to classifier or model).
     *
     * @param tagName  tag name to find
     * @param elements not null elements to test
     * @return found boolean tag value or {@code null} if tag value not found.
     * @since 3.0
     */
    public static Boolean findNullableBooleanTagValue(TagValueMetadata tagName, WithTagValuesOrStereotypes... elements) {
        String value = findTagValue(tagName, elements);
        return value == null ? null : "true".equalsIgnoreCase(value);
    }

    /**
     * Seek for a tag value amoung elements given using these rules:
     * <ul>
     * <li>Look into {@code elements} and return the first not empty tag value found.</li>
     * <li>If not found return {@code defaultValue}</li>
     * </ul>
     * <strong>Note:</strong> Order of {@code elements} is important, better then to
     * always starts from specialized to more general level (for example from attribute,
     * to classifier or model).
     *
     * @param tagName  tag name to find
     * @param elements not null elements to test
     * @return found tag value or {@code null} if not found
     * @since 3.0
     */
    public static String findDirectTagValue(TagValueMetadata tagName, WithTagValuesOrStereotypes... elements) {

        for (WithTagValuesOrStereotypes element : elements) {
            String value = findNotEmptyTagValue(tagName, element);
            if (value != null) {
                return value;
            }
        }

        return tagName.getDefaultValue();
    }

    /**
     * Seek for a tag value amoung elements given using these rules:
     * <ul>
     * <li>Look into {@code elements} and return the first not empty tag value found.</li>
     * <li>If not found return {@code defaultValue}</li>
     * </ul>
     * <strong>Note:</strong> Order of {@code elements} is important, better then to
     * always starts from specialized to more general level (for example from attribute,
     * to classifier or model).
     *
     * @param tagName  tag name to find
     * @param elements not null elements to test
     * @return found tag value or {@code null} if not found
     * @since 3.0
     */
    public static String findDirectTagValue(String tagName, WithTagValuesOrStereotypes... elements) {

        for (WithTagValuesOrStereotypes element : elements) {
            String value = findNotEmptyTagValue(tagName, element);
            if (value != null) {
                return value;
            }
        }

        return null;
    }

    public static String findNotEmptyTagValue(TagValueMetadata tagName, WithTagValuesOrStereotypes element) {

        String value = null;
        if (element != null) {
            if (element instanceof ObjectModelPackage) {
                value = findNotEmptyTagValue(tagName, (ObjectModelPackage) element);
            } else {
                value = element.getTagValue(tagName.getName());
                if (StringUtils.isEmpty(value)) {
                    value = null;
                }
            }
        }
        return value;

    }

    public static String findNotEmptyTagValue(String tagName, WithTagValuesOrStereotypes element) {

        String value = null;
        if (element != null) {
            if (element instanceof ObjectModelPackage) {
                value = findNotEmptyTagValue(tagName, (ObjectModelPackage) element);
            } else {
                value = element.getTagValue(tagName);
                if (StringUtils.isEmpty(value)) {
                    value = null;
                }
            }
        }
        return value;

    }


    protected static String findNotEmptyTagValue(TagValueMetadata tagName, ObjectModelPackage element) {

        String value = element.getTagValue(tagName.getName());
        if (StringUtils.isEmpty(value)) {
            value = null;
        }
        if (value == null && element.getParentPackage() != null) {
            value = findNotEmptyTagValue(tagName, element.getParentPackage());
        }
        return value;

    }

    protected static String findNotEmptyTagValue(String tagName, ObjectModelPackage element) {

        String value = element.getTagValue(tagName);
        if (StringUtils.isEmpty(value)) {
            value = null;
        }
        if (value == null && element.getParentPackage() != null) {
            value = findNotEmptyTagValue(tagName, element.getParentPackage());
        }
        return value;

    }

    public static Matcher getStereotypeMatcher(String key) throws InvalidStereotypeSyntaxException {
        Matcher matcher = STEREOTYPE_PATTERN.matcher(key);
        if (!matcher.find()) {
            throw new InvalidStereotypeSyntaxException();
        }
        return matcher;
    }

    public static Matcher getPackageStereotypeMatcher(String key) throws InvalidStereotypeSyntaxException {
        Matcher matcher = PACKAGE_STEREOTYPE_PATTERN.matcher(key);
        if (!matcher.find()) {
            throw new InvalidStereotypeSyntaxException();
        }
        return matcher;
    }

    public static Set<String> getStereotypes(String value) {
        String[] split = value.split("\\s*,\\s*");
        return ImmutableSet.<String>builder().add(split).build();
    }
}
