package org.nuiton.eugene.models.extension.io;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.NotImplementedException;
import org.nuiton.eugene.models.extension.model.ModelExtension;
import org.nuiton.eugene.models.object.ObjectModel;

import java.io.File;
import java.io.IOException;
import java.io.Writer;

/**
 * Created on 08/10/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public abstract class ModelExtensionWriter {

    public static ModelExtensionWriter newWriter(File file) {
        for (ModelExtensionFormat modelExtensionFormat : ModelExtensionFormat.values()) {
            if (file.getName().endsWith("." + modelExtensionFormat.name())) {
                return modelExtensionFormat.newWriter();
            }
        }
        throw new NotImplementedException("can't find writer for file: " + file);
    }

    public abstract void write(ObjectModel model, Writer writer) throws IOException;

    public abstract void write(ModelExtension model, Writer writer) throws IOException;
}
