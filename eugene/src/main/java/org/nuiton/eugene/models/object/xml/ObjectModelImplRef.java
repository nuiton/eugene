/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

/**
 * ObjectModelImplRef.
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public class ObjectModelImplRef {

    protected String name;

    public void postInit() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ObjectModelImplRef)) {
            return false;
        }
        ObjectModelImplRef oRef = (ObjectModelImplRef) o;
        if (name == null) {
            return oRef.getName() == null;
        }
        return name.equals(oRef.getName());
    }

}
