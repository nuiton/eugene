/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

/**
 * Abstraction for the parameter node of object model trees.
 * This object presents all information concerning the given parameter.
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public interface ObjectModelParameter extends ObjectModelElement {

    /**
     * Returns the type of this parameter.
     *
     * @return the type of this parameter.
     */
    String getType();

    /**
     * Returns the minimal multiplicity of this parameter.
     * The <tt>-1</tt> value means infinite.
     *
     * @return the minimal multiplicity of this parameter.
     */
    int getMinMultiplicity();

    /**
     * Returns the maximal multiplicity of this parameter.
     * The <tt>-1</tt> value means infinite.
     *
     * @return the maximal multiplicity of this parameter.
     */
    int getMaxMultiplicity();

    /**
     * true if this parameter is isOrdered
     *
     * @return {@code true} if this parameter is isOrdered
     */
    boolean isOrdered();

    /**
     * true if this parameter is isUnique
     *
     * @return {@code true} if this parameter is isUnique
     */
    boolean isUnique();

    /**
     * default value of this parameter
     *
     * @return a String for the defaultValue (must be interpreted depending on the parameter type)
     */
    String getDefaultValue();

} //ObjectModelParameter
