package org.nuiton.eugene.models.object.reader;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.ModelHelper;
import org.nuiton.eugene.ModelReader;
import org.nuiton.eugene.models.extension.tagvalue.InvalidStereotypeSyntaxException;
import org.nuiton.eugene.models.extension.tagvalue.InvalidTagValueSyntaxException;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.extension.io.ModelExtensionReader;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelPackageImpl;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Abstract object model reader.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.3
 */
public abstract class AbstractObjectModelReader extends ModelReader<ObjectModel> {

    private static final Log log = LogFactory.getLog(AbstractObjectModelReader.class);

    @Override
    public String getModelType() {
        return ModelHelper.ModelType.OBJECT.getAlias();
    }

    protected abstract void readFileToModel(File file, ObjectModel model) throws IOException;

    protected void beforeReadFile(File... files) {

    }

    @Override
    public ObjectModel read(File... files) throws IOException {

        beforeReadFile(files);

        ObjectModel model = new ObjectModelImpl();

        ModelExtensionReader<ObjectModel> modelExtensionReader = new ModelExtensionReader<>(isVerbose(), strictLoading, model);

        for (File file : files) {

            readFileToModel(file, model);

            addAllSubPackages((ObjectModelImpl) model);

            // recherche est charge le fichier propriete associe au modele
            File dir = file.getParentFile();
            String ext = FileUtil.extension(file);
            String name = FileUtil.basename(file, "." + ext);
            File propFile = new File(dir, name + ".properties");
            if (!propFile.exists()) {
                if (isVerbose()) {
                    log.info("Pas de fichier de propriétés " + propFile + " associé au model");
                }
            } else {
                if (isVerbose()) {
                    log.info("Lecture du fichier de propriétés " + propFile + " associé au model");
                }
                try {
                    modelExtensionReader.read(propFile);
                } catch (InvalidTagValueSyntaxException | InvalidStereotypeSyntaxException e) {
                    // FIXME
                    throw new IllegalStateException(e);
                }
            }

        }


        if (log.isDebugEnabled()) {
            for (ObjectModelClass m : model.getClasses()) {
                log.debug("loaded class in model: " + m.getName());
            }
        }
        return model;
    }

    /**
     * Add all missing sub packages in a model.
     *
     * @param model the model to scan
     */
    protected void addAllSubPackages(ObjectModelImpl model) {

        Set<String> subPackageNames = new LinkedHashSet<>();

        for (ObjectModelPackage aPackage : Sets.newHashSet(model.getPackages())) {

            String aPackageName = aPackage.getName();
            if (verbose) {
                log.info("Treat package: " + aPackageName);
            }

            if (subPackageNames.add(aPackageName)) {

                addSubPackages(model, aPackageName);

            }

        }

    }

    protected void addSubPackages(ObjectModelImpl model, String aPackageName) {

        String subPackageName = null;
        ObjectModelPackageImpl parentPackage = null;

        for (String part : aPackageName.split("\\.")) {

            if (subPackageName == null) {
                subPackageName = part;
            } else {
                subPackageName += "." + part;
            }

            ObjectModelPackageImpl subPackage = (ObjectModelPackageImpl) model.getPackage(subPackageName);

            if (subPackage == null) {

                subPackage = new ObjectModelPackageImpl();
                subPackage.setName(subPackageName);
                model.addPackage(subPackage);
                if (verbose) {
                    log.info("Add sub package: " + subPackageName);
                }

            }

            if (subPackage.getParentPackage() == null && parentPackage != null) {


                if (verbose) {
                    log.info("Set parent package " + parentPackage.getName() + " to " + part);
                }
                subPackage.setParentPackage(parentPackage);
            }

            parentPackage = subPackage;

        }
    }

}
