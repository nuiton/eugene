/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.AbstractGenerator;
import org.nuiton.eugene.MonitorWriter;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;

/**
 * Pour utiliser ce type de générateur il faut implanter au moins une des trois
 * méthodes generateFrom... et le getFilenameFor... associé si l'on souhaite un
 * nom de fichier convenable. Si dans une méthode generateFrom... on utilise pas
 * le Writer alors aucun fichier n'est généré.
 *
 * <pre>
 *          public String getFilenameForClass(ObjectModelClass clazz){
 *          return super.getFilenameForClass(Clazz) + &quot;Service.java&quot;;
 *          }
 *
 *          public void generateFromClass(Writer output, ObjectModelClass clazz) throws IOException{
 *          if(clazz.getType().equals(&quot;service&quot;)){
 *           / *{
 *          public class .... {
 *
 *          }
 *          }* /
 *          }
 *          }
 * </pre>
 *
 * Le nom de l'argument writer doit absolument etre output et pas autre chose si
 * vous souhaitez utiliser le processeur
 * org.codelutin.processor.filters.GeneratorTemplatesFilter pour vous
 * s'implifier l'écriture des templates.
 *
 * Created: 14 mars 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class ObjectModelGenerator extends AbstractGenerator<ObjectModel> {

    /** Logger for this class. */
    private static Log log = LogFactory.getLog(ObjectModelGenerator.class);

    public ObjectModelGenerator() {
    }

    public ObjectModelGenerator(AbstractGenerator<ObjectModel> parent) {
        super(parent);
    }

    /**
     * Par defaut la methode appelle la methode
     * {@link #generateFromModel(Writer, ObjectModel)} puis boucle sur chaque
     * class en appelant la m?thode
     * {@link #generateFromClass(Writer, ObjectModelClass)} puis boucle sur chaque
     * interface en appelant a méthode
     * {@link #generateFromInterface(Writer, ObjectModelInterface)} et enfin sur chaque
     * énumération en appelant la méthode
     * {@link #generateFromEnumeration(Writer, ObjectModelEnumeration)}
     * Le nom de fichier est récupérer pour chacun d'eux en appelant la méthode
     * getFilenameFor.... La methode generateFrom... n'utilise pas le Writer
     * alors le fichier n'est pas généré, si on l'utilise m?me pour ne rien
     * écrire alors le fichier sera généré.
     *
     * @param model   le modele memoire a utiliser
     * @param destDir le repertoire ou generer
     * @throws IOException pour tout pb
     */
    @Override
    public void applyTemplate(ObjectModel model, File destDir) throws IOException {

        if (StringUtils.isEmpty(model.getName())) {
            throw new IllegalArgumentException("No name defined on the model");
        }

        // generateFromModel
        this.model = model;

        String filename = getFilenameForModel(model);

        // generateFromModel
        generateFromElement(model, destDir, filename, ObjectModelType.OBJECT_MODEL);

        // generateFromClassifier
        generateFromElements(model.getClassifiers(), destDir, ObjectModelType.OBJECT_MODEL_CLASSIFIER);

        // generateFromInterface
        generateFromElements(model.getInterfaces(), destDir, ObjectModelType.OBJECT_MODEL_INTERFACE);

        // generateFromClass
        generateFromElements(model.getClasses(), destDir, ObjectModelType.OBJECT_MODEL_CLASS);

        // generateFromEnumeration
        generateFromElements(model.getEnumerations(), destDir, ObjectModelType.OBJECT_MODEL_ENUMERATION);

    }

    /**
     * Parcours une collection d'éléments pour la génération suivant un type d'éléments.
     * Types possibles : ObjectModelClassifier, ObjectModelClass, ObjectModelInterface et
     * ObjectModelEnumeration.
     * Deux méthodes dépendent du type et peuvent être surchargées :
     * getFilenameForXXX et generateFromXXX (XXX étant un type prédéfini pour une méthode existante).
     *
     * @param elements Collection d'éléments d'un des types ci-dessus
     * @param destDir  dossier de destination pour le fichier généré
     * @param type     type explicite d'ObjectModel
     * @see ObjectModelType
     */
    private void generateFromElements(Collection<? extends ObjectModelElement> elements,
                                      File destDir,
                                      ObjectModelType type) {

        for (ObjectModelElement element : elements) {
            String filename = "";
            // Filename depends on type of element (Classifier, Class, Interface or Enumeration)
            switch (type) {
                case OBJECT_MODEL_CLASSIFIER:
                    filename = getFilenameForClassifier((ObjectModelClassifier) element);
                    break;
                case OBJECT_MODEL_INTERFACE:
                    filename = getFilenameForInterface((ObjectModelInterface) element);
                    break;
                case OBJECT_MODEL_CLASS:
                    filename = getFilenameForClass((ObjectModelClass) element);
                    break;
                case OBJECT_MODEL_ENUMERATION:
                    filename = getFilenameForEnumeration((ObjectModelEnumeration) element);
            }

            generateFromElement(element, destDir, filename, type);
        }
    }

    /**
     * Génération pour un élément du modèle (ou le modèle lui-même).
     * Types possibles : ObjectModel, ObjectModelClassifier, ObjectModelClass,
     * ObjectModelInterface et ObjectModelEnumeration.
     * La méthode generateFromXXX dépend du type d'élément et peut être surchargée.
     *
     * @param element  element à généré
     * @param destDir  dossier de destination
     * @param filename nom du fichier de sortie
     * @param type     type d'ObjectModel
     * @see ObjectModelType
     */
    protected void generateFromElement(Object element,
                                       File destDir,
                                       String filename,
                                       ObjectModelType type) {

        // on a maintenant une restriction des elements a generer
        // c'est à dire un filtrage par package
        // effectue un appel pour savoir si on a le droit de generer l'element
        // courant
        if (canGenerateElement(element)) {

            File outputFile = getDestinationFile(destDir, filename);
            if (!isOverwrite() && isNewerThanSource(outputFile)) {
                if (isVerbose()) {
                    if (log.isInfoEnabled()) {
                        log.info("Will not generate " + outputFile + " (up-to-date).");
                    }
                }
                return;
            }
            if (!outputFile.exists()) {
                if (log.isDebugEnabled()) {
                    log.debug("not up-to-date " + outputFile.lastModified()
                              + " <" + outputFile + ">");
                }
            }
            try {
                StringWriter out = new StringWriter();

                try (MonitorWriter monitorOut = new MonitorWriter(out)) {
                    switch (type) {
                        case OBJECT_MODEL:
                            generateFromModel(monitorOut, (ObjectModel) element);
                            break;
                        case OBJECT_MODEL_CLASSIFIER:
                            generateFromClassifier(monitorOut, (ObjectModelClassifier) element);
                            break;
                        case OBJECT_MODEL_INTERFACE:
                            generateFromInterface(monitorOut, (ObjectModelInterface) element);
                            break;
                        case OBJECT_MODEL_CLASS:
                            generateFromClass(monitorOut, (ObjectModelClass) element);
                            break;
                        case OBJECT_MODEL_ENUMERATION:
                            generateFromEnumeration(monitorOut, (ObjectModelEnumeration) element);
                            break;
                    }

                    write(outputFile, monitorOut);

                }

            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Erreur lors de la génération du fichier "
                             + outputFile);
                }
                throw new RuntimeException(
                        "Erreur lors de la génération du fichier "
                        + outputFile, eee);
            }
        }
    }

    /**
     * Test if given element can be generated.
     *
     * An element can be generated if his package is in the
     * {@link #generatedPackages} list or if {@link #generatedPackages} is
     * null or empty.
     *
     * @param element element to test
     * @return generation allowed
     */
    protected boolean canGenerateElement(Object element) {

        boolean canGenerate = true;

        // can get package only for Classifiers
        if (element instanceof ObjectModelClassifier) {
            ObjectModelClassifier classifier = (ObjectModelClassifier) element;
            String classifierPackage = classifier.getPackageName();

            canGenerate = canGeneratePackage(classifierPackage);
        }

        return canGenerate;
    }

    /**
     * Par defaut cette methode retourne le getName du model. Si l'on souhaite
     * utiliser la methode generateFromModel il vaut mieux surcharger cette
     * methode
     *
     * @param model le modele utilise
     * @return le nom du fichier a generer
     */
    public String getFilenameForModel(ObjectModel model) {
        return model.getName();
    }

    /**
     * Par defaut cette methode retourne le QualifiedName convertie en chemin
     * par exemple pour org.codelutin.Toto on aurait org/codelutin/Toto
     *
     * @param model       le modele utilise
     * @param packageName le nom du paquetage
     * @return le repertoire correspondant au paquetage
     */
    public String getFilenameForPackage(ObjectModel model,
                                        String packageName) {
        return packageName.replace('.', File.separatorChar);
    }

    /**
     * Par defaut cette methode retourne le QualifiedName convertie en chemin
     * par exemple pour org.codelutin.Toto on aurait org/codelutin/Toto
     *
     * @param interfacez l'interface utilisee
     * @return le nom du l'interface a generer
     */
    public String getFilenameForInterface(ObjectModelInterface interfacez) {
        return getFilenameForClassifier(interfacez);
    }

    /**
     * Par defaut cette methode retourne le QualifiedName convertie en chemin
     * par exemple pour org.codelutin.Toto on aurait org/codelutin/Toto
     *
     * @param clazz la classe utilisee
     * @return le nom de la classe a generer
     */
    public String getFilenameForClass(ObjectModelClass clazz) {
        return getFilenameForClassifier(clazz);
    }

    /**
     * Par defaut cette methode retourne le QualifiedName convertie en chemin
     * par exemple pour org.codelutin.Toto on aurait org/codelutin/Toto
     *
     * @param classifier le classifier utilisee
     * @return le nom du classifier a generer
     */
    public String getFilenameForClassifier(ObjectModelClassifier classifier) {
        return classifier.getQualifiedName().replace('.', File.separatorChar);
    }

    public String getFilenameForEnumeration(ObjectModelEnumeration enumeration) {
        return getFilenameForClassifier(enumeration);
    }

    public void generateFromModel(Writer output,
                                  ObjectModel input) throws IOException {
    }

    public void generateFromInterface(Writer output,
                                      ObjectModelInterface input) throws IOException {
    }

    public void generateFromEnum(Writer output,
                                 ObjectModelEnumeration input) throws IOException {
    }

    public void generateFromClass(Writer output,
                                  ObjectModelClass input) throws IOException {
    }

    public void generateFromClassifier(Writer output,
                                       ObjectModelClassifier input) throws IOException {
    }

    public void generateFromEnumeration(Writer output,
                                        ObjectModelEnumeration input) throws IOException {
    }
}
