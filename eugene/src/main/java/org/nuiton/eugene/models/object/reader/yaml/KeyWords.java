package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * User: agiraudet
 * Date: 30/05/13
 * Time: 10:10
 */
public interface KeyWords {

    String SEPARATOR = ".";

    String ABSTRACT = "abstract";

    String AGGREGATE = "aggregate";

    String ASSOCIATION_CLASS = "associationclass";

    String ASSOCIATION_CLASS_NAME = "associationclassname";

    String ASSOCIATION_TYPE = "associationtype";

    String ATTRIBUTE = "attribute";

    String BODY_CODE = "bodeycode";

    String CLASS = "class";

    String CLASSIFIER = "classifier";

    String COMMENTS = "comments";

    String COMPOSITE = "composite";

    String DEFAULT_VALUE = "defaultvalue";

    String DESCRIPTION = "description";

    String DOCUMENTATION = "documentation";

    String ENUMERATION = "enumeration";

    String EXTERN = "extern";

    String FINAL = "final";

    String IMPORTS = "imports";

    String INNER = "inner";

    String INTERFACE = "interface";

    String LABEL = "label";

    String LITERALS = "literals";

    String MAX_MULTIPLICITY = "maxmultiplicity";

    String MIN_MULTIPLICITY = "minmultiplicity";

    String NAME = "name";

    String NAVIGABLE = "navigable";

    String OPERATION = "operation";

    String ORDERED = "ordered";

    String ORDERING = "ordering";

    String PACKAGE = "package";

    String PARAMETER = "parameter";

    String PARTICIPANT = "participant";

    String PRIVATE = "private";

    String PROTECTED = "protected";

    String PUBLIC = "public";

    String RETURN_PARAMETER = "returnparameter";

    String REVERSE_ATTRIBUTE_NAME = "reverseattributename";

    String REVERSE_MAX_MULTIPLICITY = "reversemaxmultiplicity";

    String REVERSE_MIN_MULTIPLICITY = "reverseminmultiplicity";

    String REVERSE_ORDERING = "reverseordering";

    String STATIC = "static";

    String STEREOTYPES = "stereotypes";

    String SUPER_CLASSES = "superclasses";

    String SUPER_INTERFACES = "superinterfaces";

    String SYNTAXE = "syntaxe";

    String TAG_VALUES = "tagvalues";

    String TRANSIENT = "transient";

    String TYPE = "type";

    String UNIQUE = "unique";

    String UNORDERED = "unordered";

    String VERSION = "version";

    String VISIBILITY = "visibility";

    String ELEMENT = "element";
}
