/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelParameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * ObjectModelOperationImpl.java
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com Copyright Code Lutin
 */
public class ObjectModelOperationImpl extends ObjectModelElementImpl implements ObjectModelOperation {

    public static final ObjectModelJavaModifier DEFAULT_VISIBILITY = ObjectModelJavaModifier.PUBLIC;

    protected ObjectModelParameter returnParameter;

    protected String transactionLevel = "supports";

    protected List<ObjectModelParameter> parameters = new ArrayList<>();

    protected Set<String> exceptions = new HashSet<>();

    protected String bodyCode = "";

    private static Set<ObjectModelModifier> authorizedModifiers;

    public ObjectModelOperationImpl() {
    }

    @Override
    protected Set<ObjectModelModifier> getAuthorizedModifiers() {
        if (authorizedModifiers == null) {
            // http://docs.oracle.com/javase/specs/jls/se7/html/jls-8.html#jls-8.4.3
            // Annotation public protected private abstract
            // static final synchronized native strictfp
            Set<ObjectModelModifier> modifiers = Sets.newHashSet(
                    (ObjectModelModifier) ObjectModelJavaModifier.ABSTRACT, // Force cast because of generics limitation
                    ObjectModelJavaModifier.STATIC,
                    ObjectModelJavaModifier.FINAL,
                    ObjectModelJavaModifier.SYNCHRONIZED,
                    ObjectModelJavaModifier.NATIVE,
                    ObjectModelJavaModifier.STRICTFP);
            modifiers.addAll(ObjectModelJavaModifier.visibilityModifiers);
            authorizedModifiers = ImmutableSet.copyOf(modifiers);
        }
        return authorizedModifiers;
    }

    public String toString() {
        return getName() + "(" + parameters + ")" + "<<" + getStereotypes()
               + ">> throws " + exceptions + " tagvalue: " + getTagValues();
    }

    /**
     * 2 operations are equal if the have same name and same argument type.
     */
    public boolean equals(Object o) {
        if (o instanceof ObjectModelOperation) {
            ObjectModelOperation op = (ObjectModelOperation) o;
            if (getName().equals(op.getName())
                && getParameters().size() == op.getParameters().size()) {
                for (Iterator<?> i = getParameters().iterator(), opi = op
                        .getParameters().iterator(); i.hasNext()
                                                     && opi.hasNext(); ) {
                    ObjectModelParameter p = (ObjectModelParameter) i.next();
                    ObjectModelParameter pop = (ObjectModelParameter) opi
                            .next();
                    if (!p.getType().equals(pop.getType())) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public void addParameter(ObjectModelParameterImpl parameter) {
        // if (parameter == null)
        // return new ObjectModelParameterImpl(objectModelImpl, this);
        parameter.postInit();
        parameter.setDeclaringElement(this);
        parameters.add(parameter);
        // return parameter;
    }

    public void setVisibility(String visibility) {
        ObjectModelModifier modifier = ObjectModelJavaModifier.fromVisibility(visibility);
        removeModifiers(ObjectModelJavaModifier.visibilityModifiers);
        if (modifier == null) {
            modifier = DEFAULT_VISIBILITY; // default visibility
        }
        addModifier(modifier);
    }

    public void setAbstract(boolean abstractz) {
        addOrRemoveModifier(ObjectModelJavaModifier.ABSTRACT, abstractz);
    }

    public void setReturnParameter(ObjectModelParameterImpl returnParameter) {
        // /if (returnParameter == null)
        // return new ObjectModelParameterImpl(objectModelImpl, this);
        returnParameter.postInit();
        returnParameter.setDeclaringElement(this);
        this.returnParameter = returnParameter;
    }

    public void setBodyCode(String bodyCode) {
        this.bodyCode = bodyCode;
    }

    /**
     * Add some code to current body
     *
     * @param bodyCode code to add
     */
    public void addBodyCode(String bodyCode) {
        this.bodyCode += bodyCode;
    }

    @Override
    public ObjectModelParameter getReturnParameter() {
        return returnParameter;
    }

    @Override
    public String getReturnType() {
        if (returnParameter != null) {
            return returnParameter.getType();
        }
        return "void";
    }

    @Override
    public String getVisibility() {
        String visibility = DEFAULT_VISIBILITY.toString(); // default
        if (modifiers.contains(ObjectModelJavaModifier.PRIVATE)) {
            visibility = ObjectModelJavaModifier.PRIVATE.toString();
        } else if (modifiers.contains(ObjectModelJavaModifier.PROTECTED)) {
            visibility = ObjectModelJavaModifier.PROTECTED.toString();
        }
        if (modifiers.contains(ObjectModelJavaModifier.PACKAGE)) {
            visibility = ObjectModelJavaModifier.PACKAGE.toString();
        }
        return visibility;
    }

    /**
     * Returns whether this operation is abstract or not.
     *
     * @return a boolean indicating whether this operation is abstract or not.
     */
    @Override
    public boolean isAbstract() {
        return modifiers.contains(ObjectModelJavaModifier.ABSTRACT);
    }

    @Override
    public Collection<ObjectModelParameter> getParameters() {
        return parameters;
    }

    /**
     * Add new raised exception.
     *
     * @param raisedParameter exception to add
     */
    public void addExceptionParameter(ObjectModelParameterImpl raisedParameter) {
        raisedParameter.postInit();
        raisedParameter.setDeclaringElement(this);

        exceptions.add(raisedParameter.getType());
    }

    @Override
    public Set<String> getExceptions() {
        return exceptions;
    }

    @Override
    public String getBodyCode() {
        return bodyCode;
    }

}
