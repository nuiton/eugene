/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state.xml;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.RuleSetBase;

/**
 * StateModelDigesterRuleSet
 *
 * Definit principalement :
 * - la classe d'implementation a utiliser pour chaque noeud
 * - la methode a appeler apres chaque noeud
 *
 * Ce jeu de regle ne cree pas l'element racine.
 * Il doit etre cree et ajoute a la pile digester avant l'appel a
 * <tt>Digester.parse(File)</tt>.
 *
 * Exemple:
 * <code>
 * StateModel monModel = new StateModelImpl()
 * Digester d = new Digester();
 * d.push(monModel);
 * d.parse(file);
 * </code>
 *
 * @author chatellier
 */
public class DigesterStateModelRuleSet extends RuleSetBase {

    protected String prefix;

    public DigesterStateModelRuleSet() {
        this("");
    }

    public DigesterStateModelRuleSet(String prefix) {
        super("http://www.codelutin.org/lutingenerator/stateModel");
        this.prefix = prefix;
    }

    public void addRuleInstances(Digester digester) {

        //digester.addObjectCreate("stateModel", StateModelImpl.class);
        //digester.addFactoryCreate("stateModel", DigesterStateModelFactory.class);
        // root element must be present on stask
        digester.addSetProperties("stateModel");

        digester.addObjectCreate("stateModel/stateChart", StateModelStateChartImpl.class);
        digester.addSetProperties("stateModel/stateChart");
        digester.addSetNext("stateModel/stateChart", "addStateChart");

        digester.addObjectCreate("*/state", StateModelSimpleStateImpl.class);
        digester.addSetProperties("*/state");
        digester.addSetNext("*/state", "addState");

        digester.addObjectCreate("*/complexeState", StateModelComplexeStateImpl.class);
        digester.addSetProperties("*/complexeState");
        digester.addSetNext("*/complexeState", "addState");

        digester
                .addObjectCreate("*/transition", StateModelTransitionImpl.class);
        digester.addSetProperties("*/transition");
        digester.addSetNext("*/transition", "addTransition");
    }
}
