/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import org.nuiton.eugene.Transformer;
import org.nuiton.eugene.models.Model;

import java.util.Collection;

/**
 * L'initialisation du modèle de sortie et du générateur de sortie associée,
 * se fait dans la superclass grâce à la méthode init.
 *
 * @param <O> Created: 28 oct. 2009
 * @author Florian Desbois - desbois@codelutin.com
 */
public abstract class ObjectModelTransformer<O extends Model> extends Transformer<ObjectModel, O> {

    /**
     * Le model associé au transformer est le model d'entree. Le modele de sortie
     * peut etre initialiser via la methode {@link Transformer#initOutputModel()}.
     *
     * Il est également possible de surcharger la methode {@link #debugOutputModel()}
     * pour verifier le resultat de la transformation.
     * Par defaut la methode appelle la methode
     * {@link #transformFromModel(ObjectModel)} puis boucle sur chaque
     * class en appelant la methode
     * {@link #transformFromClass(ObjectModelClass)} puis boucle sur chaque
     * interface en appelant a methode
     * {@link #transformFromInterface(ObjectModelInterface)} et enfin sur chaque
     * enumeration en appelant la methode
     * {@link #transformFromEnumeration(ObjectModelEnumeration)}
     */
    @Override
    public void transform() {

        ObjectModel model = getModel();

        // transformFromModel
        transformFromElement(model, ObjectModelType.OBJECT_MODEL);

        // transformFromClassifier
        transformFromElements(model.getClassifiers(), ObjectModelType.OBJECT_MODEL_CLASSIFIER);

        // transformFromInterface
        transformFromElements(model.getInterfaces(), ObjectModelType.OBJECT_MODEL_INTERFACE);

        // transformFromClass
        transformFromElements(model.getClasses(), ObjectModelType.OBJECT_MODEL_CLASS);

        // transformFromEnumeration
        transformFromElements(model.getEnumerations(), ObjectModelType.OBJECT_MODEL_ENUMERATION);

        debugOutputModel();
    }

    protected abstract void debugOutputModel();

    /**
     * Parcours une collection d'éléments pour la transformation suivant un type d'éléments.
     * Types possibles : ObjectModelClassifier, ObjectModelClass, ObjectModelInterface et
     * ObjectModelEnumeration.
     * Une méthode dépend du type et peut être surchargée :
     * transformFromXXX (XXX étant un type prédéfini pour une méthode existante).
     *
     * @param elements Collection d'éléments d'un des types ci-dessus
     * @param type     type explicite d'ObjectModel
     * @see ObjectModelType
     */
    private void transformFromElements(Collection<? extends ObjectModelElement> elements,
                                       ObjectModelType type) {

        for (ObjectModelElement element : elements) {
            transformFromElement(element, type);
        }
    }

    /**
     * Génération pour un élément du modèle (ou le modèle lui-même).
     * Types possibles : ObjectModel, ObjectModelClassifier, ObjectModelClass,
     * ObjectModelInterface et ObjectModelEnumeration.
     * La méthode transformFromXXX dépend du type d'élément et peut être surchargée.
     *
     * @param element element à généré
     * @param type    type d'ObjectModel
     * @see ObjectModelType
     */
    protected void transformFromElement(Object element, ObjectModelType type) {

        switch (type) {
            case OBJECT_MODEL:
                transformFromModel((ObjectModel) element);
                break;
            case OBJECT_MODEL_CLASSIFIER:
                transformFromClassifier((ObjectModelClassifier) element);
                break;
            case OBJECT_MODEL_INTERFACE:
                transformFromInterface((ObjectModelInterface) element);
                break;
            case OBJECT_MODEL_CLASS:
                transformFromClass((ObjectModelClass) element);
                break;
            case OBJECT_MODEL_ENUMERATION:
                transformFromEnumeration((ObjectModelEnumeration) element);
                break;
        }
    }

    public void transformFromModel(ObjectModel model) {
    }

    public void transformFromInterface(ObjectModelInterface interfacez) {
    }

    public void transformFromClass(ObjectModelClass clazz) {
    }

    public void transformFromClassifier(ObjectModelClassifier clazz) {
    }

    public void transformFromEnumeration(ObjectModelEnumeration enumeration) {
    }

}
