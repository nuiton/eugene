package org.nuiton.eugene.models.extension.io;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import org.apache.commons.lang3.NotImplementedException;
import org.nuiton.eugene.models.extension.tagvalue.InvalidStereotypeSyntaxException;
import org.nuiton.eugene.models.extension.tagvalue.InvalidTagValueSyntaxException;

import java.io.File;
import java.io.IOException;

/**
 * Created on 09/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public abstract class ModelExtensionFileParser {

    public static ModelExtensionFileParser newParser(boolean strictLoading, File file) {
        for (ModelExtensionFormat modelExtensionFormat : ModelExtensionFormat.values()) {
            if (file.getName().endsWith("." + modelExtensionFormat.name())) {
                return modelExtensionFormat.newParser(strictLoading);
            }
        }
        throw new NotImplementedException("can't find parser for file: " + file);
    }

    static final String TAGVALUE = "tagvalue";

    static final String TAG_VALUE = "tagValue";

    static final String STEREOTYPE = "stereotype";

    static final String ATTRIBUTE = "attribute";

    static final String CLASS = "class";

    static final String MODEL = "model";

    static final String PACKAGE = "package";

    public abstract void parse(File file, ModelExtensionFileParserCallback callback) throws IOException, InvalidStereotypeSyntaxException, InvalidTagValueSyntaxException;
}
