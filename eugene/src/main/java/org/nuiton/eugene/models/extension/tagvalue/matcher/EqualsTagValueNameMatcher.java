package org.nuiton.eugene.models.extension.tagvalue.matcher;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;

import java.util.Set;

/**
 * Created on 4/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class EqualsTagValueNameMatcher extends TagValueDefinitionMatcher {

    public EqualsTagValueNameMatcher(Set<TagValueMetadata> tagValueDefinitionMap) {
        super(tagValueDefinitionMap);
    }

    @Override
    protected boolean accept(String tagValueName, TagValueMetadata entry) {
        return entry.getName().equals(tagValueName);
    }
}
