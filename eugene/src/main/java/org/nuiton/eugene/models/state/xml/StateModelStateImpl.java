/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state.xml;

import org.nuiton.eugene.models.state.StateModelState;
import org.nuiton.eugene.models.state.StateModelTransition;

import java.util.ArrayList;
import java.util.List;

/**
 * StateModelStateImpl.java
 *
 * @author chatellier
 */
public class StateModelStateImpl implements StateModelState {

    /**
     * State name
     */
    protected String name;

    /**
     * Transition list
     */
    protected List<StateModelTransition> listTransitions;

    /**
     * Constructor
     */
    public StateModelStateImpl() {
        listTransitions = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * Set state name
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Add transition
     *
     * @param transition a transition
     */
    public void addTransition(StateModelTransition transition) {
        listTransitions.add(transition);
    }

    @Override
    public List<StateModelTransition> getTransitions() {
        return listTransitions;
    }

    @Override
    public boolean isComplex() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.nuiton.eugene.models.state.StateModelState#isFinal()
     */
    public boolean isFinal() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.nuiton.eugene.models.state.StateModelState#isInitial()
     */
    public boolean isInitial() {
        return false;
    }
}
