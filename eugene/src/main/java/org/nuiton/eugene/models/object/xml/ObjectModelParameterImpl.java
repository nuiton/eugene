/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import com.google.common.collect.ImmutableSet;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.eugene.models.object.ObjectModelUMLModifier;

import java.util.Set;

/**
 * ObjectModelParameterImpl.
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com Copyright Code Lutin
 */
public class ObjectModelParameterImpl extends ObjectModelElementImpl implements
        ObjectModelParameter {

    protected String type;

    protected int minMultiplicity = 1;

    protected int maxMultiplicity = 1;
//    protected String ordering = "unspecified";

    /**
     * Unique n'est utile que pour les collections pour indiquer que la
     * collection ne peut prendre qu'une valeur identique (Set) si unique
     * est false la collection peut prendre plusieurs fois la meme valeur
     * (List)
     */
    protected String defaultValue;

    private static Set<ObjectModelModifier> authorizedModifiers;

    public static final String PROPERTY_ORDERED = "ordered";

    public ObjectModelParameterImpl() {
        // Do not put in postInit has it has to be done before fields are push into the instance
        addModifier(ObjectModelUMLModifier.UNIQUE); // Unique by default
    }

    @Override
    public void postInit() {
        super.postInit();
    }

    @Override
    protected Set<ObjectModelModifier> getAuthorizedModifiers() {
        if (authorizedModifiers == null) {
            // No particular modifier ?
            authorizedModifiers = ImmutableSet.of(
                    (ObjectModelModifier) ObjectModelUMLModifier.UNIQUE,
                    ObjectModelUMLModifier.ORDERED,
                    ObjectModelUMLModifier.UNIQUE);
        }
        return authorizedModifiers;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMinMultiplicity(int minMultiplicity) {
        this.minMultiplicity = minMultiplicity;
    }

    public void setMaxMultiplicity(int maxMultiplicity) {
        this.maxMultiplicity = maxMultiplicity;
    }

    public void setOrdering(String ordering) {
        setOrdered(PROPERTY_ORDERED.equals(ordering));
//        this.ordering = ordering;
    }

    public void setUnique(boolean unique) {
        addOrRemoveModifier(ObjectModelUMLModifier.UNIQUE, unique);
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public int getMinMultiplicity() {
        return minMultiplicity;
    }

    @Override
    public int getMaxMultiplicity() {
        return maxMultiplicity;
    }

    @Override
    public boolean isOrdered() {
        return modifiers.contains(ObjectModelUMLModifier.ORDERED);
    }

    public void setOrdered(boolean ordered) {
        addOrRemoveModifier(ObjectModelUMLModifier.ORDERED, ordered);
    }

    @Override
    public boolean isUnique() {
        return modifiers.contains(ObjectModelUMLModifier.UNIQUE);
    }

    @Override
    public String toString() {
        return getType() + " " + getName() + "<<" + getStereotypes()
               + ">> tagvalue: " + getTagValues();
    }
}
