package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * User: agiraudet
 * Date: 30/05/13
 * Time: 11:23
 */
//TODO: interface Syntaxe pour futures syntaxes
public class SyntaxePureYaml {

    public static void loadYamlObject(Object modelYAML, YamlObject modelYAMLO) {
        ParserPureYaml.parseModel(modelYAML, modelYAMLO);
    }
}
