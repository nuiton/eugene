/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import java.util.Collection;

/**
 * ObjectModelEnumeration.
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public interface ObjectModelEnumeration extends ObjectModelClassifier {

//    /**
//    * Returns the package name of this enumeration.
//    *
//    * @return the package name of this enumeration.
//    */
//    public String getPackageName();
//
//    /**
//    * Returns the qualified name of this enumeration.
//    * Class qualified name is composed of the package name and the enumeration name.
//    *
//    * @return the qualified name of this enumeration.
//    */
//    public String getQualifiedName();

    /**
     * Returns literals of this enumeration.
     *
     * @return a Collection of String
     */
    Collection<String> getLiterals();

//    /**
//     * Returns all operations defined on this en enumeration.
//     * @see ObjectModelOperation
//     *
//     * @return a Collection containing all ObjectModelOperation for this enumeration.
//     */
//    public Collection<ObjectModelOperation> getOperations();
}
