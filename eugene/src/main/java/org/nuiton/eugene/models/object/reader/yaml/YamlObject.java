package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * User: agiraudet
 * Date: 28/05/13
 * Time: 16:41
 *
 * le YamlObject est un objet intermédiaire et très laxiste, pouvant s'adapter à d'éventuelles évolutions
 * (en cas d'évolutions de la syntaxe YAML ou de l'ObjectModel, les modifications permettant de conserver les interactions seront légères)
 */
public class YamlObject {

    protected Map<String, List<String>> mapStringListString;

    protected Map<String, List<YamlObject>> mapStringListYamlObject;
    //TODO: améliorer noms méthodes

    public YamlObject() {
        mapStringListString = new LinkedHashMap<>();
        mapStringListYamlObject = new LinkedHashMap<>();
    }

    public void addYamlObjectToMap(String key, YamlObject value) {
        if (mapStringListYamlObject.containsKey(key)) {
            mapStringListYamlObject.get(key).add(value);
        } else {
            List<YamlObject> tmp = new LinkedList<>();
            tmp.add(value);
            mapStringListYamlObject.put(key, tmp);
        }
    }

    public void addStringToMap(String key, String value) {
        if (mapStringListString.containsKey(key)) {
            mapStringListString.get(key).add(value);
        } else {
            List<String> tmp = new LinkedList<>();
            tmp.add(value);
            mapStringListString.put(key, tmp);
        }
    }

    public YamlObject getFirstMapStringListYamlObject(String key) {
        if (mapStringListYamlObject.containsKey(key)) {
            if (!mapStringListYamlObject.isEmpty()) {
                return mapStringListYamlObject.get(key).get(0);
            }
        }
        return null;
    }

    public String getFirstMapStringListString(String key) {
        if (mapStringListString.containsKey(key)) {
            if (!mapStringListString.isEmpty()) {
                return mapStringListString.get(key).get(0);
            }
        }
        return null;
    }

    public boolean isUniqueMapStringListYamlObject(String key) {
        return (sizeOfMapStringListYamlObject(key) == 1);
    }

    public boolean isUniqueMapStringListString(String key) {
        return (sizeOfMapStringListString(key) == 1);
    }

    public boolean containsKeyYamlMapStringListYamlObject(String key) {
        return mapStringListYamlObject.containsKey(key);
    }

    public boolean containsKeyMapStringListString(String key) {
        return mapStringListString.containsKey(key);
    }

    public List<YamlObject> getMapStringListYamlObject(String key) {
        if (mapStringListYamlObject.containsKey(key)) {
            if (!mapStringListYamlObject.isEmpty()) {
                return mapStringListYamlObject.get(key);
            }
        }
        //return null;//permet parcours
        return new LinkedList<>();
    }

    public List<String> getMapStringListString(String key) {
        if (mapStringListString.containsKey(key)) {
            if (!mapStringListString.isEmpty()) {
                return mapStringListString.get(key);
            }
        }
        //return null;//permet parcours
        return new LinkedList<>();
    }

    public int sizeOfMapStringListYamlObject(String key) {
        if (mapStringListYamlObject.containsKey(key)) {
            return mapStringListYamlObject.get(key).size();
        }
        return 0;//-1
    }

    public int sizeOfMapStringListString(String key) {
        if (mapStringListString.containsKey(key)) {
            return mapStringListString.get(key).size();
        }
        return 0;//-1
    }

    public Map<String, List<YamlObject>> getMapStringListYamlObject() {
        return mapStringListYamlObject;
    }

    public Map<String, List<String>> getMapStringListString() {
        return mapStringListString;
    }

    public boolean setMapStringListString(String key, String value, String element) {
        if (mapStringListString.containsKey(key)) {
            if (mapStringListString.get(key).contains(value)) {
                mapStringListString.get(key).set(mapStringListString.get(key).indexOf(value), element);
                return true;
            }
        }
        return false;
    }

    public boolean setMapStringListYamlObject(String key, YamlObject value, YamlObject element) {
        if (mapStringListYamlObject.containsKey(key)) {
            if (mapStringListYamlObject.get(key).contains(value)) {
                mapStringListYamlObject.get(key).set(mapStringListYamlObject.get(key).indexOf(value), element);
                return true;
            }
        }
        return false;
    }

    public boolean removeMapStringListString(String key, String value) {
        if (mapStringListString.containsKey(key)) {
            return mapStringListString.get(key).remove(value);
        }
        return false;
    }

    public boolean removeMapStringString(String key) {
        if (mapStringListString.containsKey(key)) {
            mapStringListString.remove(key);
            return true;
        }
        return false;
    }

    public boolean removeMapStringListYamlObject(String key, YamlObject value) {
        if (mapStringListYamlObject.containsKey(key)) {
            return mapStringListYamlObject.get(key).remove(value);
        }
        return false;
    }

    public boolean removeMapStringListYamlObject(String key) {
        if (mapStringListYamlObject.containsKey(key)) {
            mapStringListYamlObject.remove(key);
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof YamlObject) {
            return (mapStringListYamlObject.equals(((YamlObject) o).getMapStringListYamlObject()) && mapStringListString.equals(((YamlObject) o).getMapStringListString()));
        }
        return false;
    }

    @Override
    public String toString() {
        return toString("- ");
    }

    public String toString(String indentation) {
        StringBuilder res = new StringBuilder();

        for (Map.Entry<String, List<String>> entry : mapStringListString.entrySet()) {
            for (String str : entry.getValue()) {
                res.append(indentation).append(entry.getKey()).append(": ").append(str).append("\n");
            }
        }
        for (Map.Entry<String, List<YamlObject>> entry : mapStringListYamlObject.entrySet()) {
            for (YamlObject yobj : entry.getValue()) {
                if (yobj != null) {
                    res.append(indentation).append(entry.getKey()).append(":\n").append(yobj.toString("  " + indentation)).append("\n");
                }
            }
        }
        return res.toString();
    }
}
