package org.nuiton.eugene.models.extension.tagvalue.provider;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import org.nuiton.eugene.models.extension.tagvalue.MismatchTagValueTargetException;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueNotFoundException;

import java.util.Set;

/**
 * Created on 24/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface TagValueMetadatasProvider {

    String getDescription();

    Set<TagValueMetadata> getTagValues();

    void validate(String tagValueName, Class<?> type) throws TagValueNotFoundException, MismatchTagValueTargetException;

    Optional<TagValueMetadata> getTagValue(String tagValueName);

}
