package org.nuiton.eugene.models.extension.tagvalue;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * When a missing tag value is detected.
 *
 * Created on 15/08/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class MissingStereoTypeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    protected final String stereoTypeName;

    protected final String prefixMessage;

    protected final Set<String> stack;

    public MissingStereoTypeException(String stereoTypeName, String prefixMessage, ObjectModelPackage aPackage, ObjectModelClass aClass) {
        this.stereoTypeName = stereoTypeName;
        this.prefixMessage = prefixMessage;
        this.stack = getUsageStack(stereoTypeName, aPackage, aClass);
    }

    @Override
    public String toString() {

        return "\n\n" + prefixMessage
               + "\n=========================================================================================="
               + "\n" + Joiner.on("\n").join(stack)
               + "\n==========================================================================================";

    }

    /**
     * Build the stack of usage of the given stereoType.
     *
     * Order of usage is : model, packages (from root to final package), then class.
     *
     * @param stereoTypeName name of stereotype
     * @param aPackage package used
     * @param aClass class used
     * @return the ordered set of stack usage.
     */
    public LinkedHashSet<String> getUsageStack(String stereoTypeName, ObjectModelPackage aPackage, ObjectModelClass aClass) {
        String suffix = ".stereotype=" + stereoTypeName;
        LinkedHashSet<String> stack = new LinkedHashSet<>();

        stack.add("model" + suffix);

        addPackageStereoTypes(aPackage, suffix, stack);
        stack.add(aClass.getQualifiedName() + ".class" + suffix);

        return stack;
    }

    protected void addPackageStereoTypes(ObjectModelPackage aPackage, String suffix, Set<String> stack) {

        if (aPackage.getParentPackage() != null) {
            addPackageStereoTypes(aPackage.getParentPackage(), suffix, stack);
        }
        stack.add("package." + aPackage.getName() + suffix);

    }

}
