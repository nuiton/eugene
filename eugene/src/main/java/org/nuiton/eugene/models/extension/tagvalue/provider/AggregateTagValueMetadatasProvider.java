package org.nuiton.eugene.models.extension.tagvalue.provider;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import org.nuiton.eugene.models.extension.tagvalue.MismatchTagValueTargetException;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueNotFoundException;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Created on 24/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class AggregateTagValueMetadatasProvider implements TagValueMetadatasProvider, Iterable<TagValueMetadatasProvider> {

    protected final Iterable<TagValueMetadatasProvider> providers;

    /**
     * Obtain a new provider of tag values definitions.
     *
     * @param loader the classloader to use (if none given will use the one of the current thread)
     */
    public AggregateTagValueMetadatasProvider(ClassLoader loader) {
        if (loader == null) {

            // use the current thread loader
            loader = Thread.currentThread().getContextClassLoader();
        }
        this.providers = ServiceLoader.load(TagValueMetadatasProvider.class, loader);
    }

    /**
     * Obtain a new provider of tag values definitions.
     *
     * @param providers the list of providers to use in the factory
     */
    public AggregateTagValueMetadatasProvider(Iterable<TagValueMetadatasProvider> providers) {
        Preconditions.checkNotNull(providers);
        this.providers = providers;
    }

    @Override
    public Iterator<TagValueMetadatasProvider> iterator() {
        return providers.iterator();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Set<TagValueMetadata> getTagValues() {
        ImmutableSet.Builder<TagValueMetadata> builder = ImmutableSet.builder();
        for (TagValueMetadatasProvider tagValuesMetadatasProvider : providers) {
            builder.addAll(tagValuesMetadatasProvider.getTagValues());
        }

        return builder.build();
    }

    @Override
    public Optional<TagValueMetadata> getTagValue(String tagValueName) {
        for (TagValueMetadatasProvider provider : providers) {
            Optional<TagValueMetadata> result = provider.getTagValue(tagValueName);
            if (result.isPresent()) {
                return result;
            }
        }
        return Optional.absent();
    }

    @Override
    public void validate(String tagValueName, Class<?> type) throws TagValueNotFoundException, MismatchTagValueTargetException {
        for (TagValueMetadatasProvider provider : providers) {
            try {
                provider.validate(tagValueName, type);
                return;
            } catch (TagValueNotFoundException e) {
                // do nothing here
            }
        }
        throw new TagValueNotFoundException();
    }

}
