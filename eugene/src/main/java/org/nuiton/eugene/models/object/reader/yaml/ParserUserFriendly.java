package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * User: agiraudet
 * Date: 10/06/13
 * Time: 16:48
 */
public class ParserUserFriendly implements KeyWords {

    protected Map<String, String> imports;

    protected Map<String, String> importsI;

    protected Map<String, String> importsE;

    protected String packageM;

    protected Object modelYAML;

    protected YamlObject modelYAMLO;

    public ParserUserFriendly(Object modelYAML, YamlObject modelYAMLO) {
        this.imports = new LinkedHashMap<>();
        this.importsI = new LinkedHashMap<>();
        this.importsE = new LinkedHashMap<>();
        this.packageM = "default";
        this.modelYAML = modelYAML;
        this.modelYAMLO = modelYAMLO;
    }

    public void parseModel() {
        if (modelYAML instanceof Map) {
            for (Object entry : ((Map) modelYAML).entrySet()) {
                if (entry instanceof Map.Entry) {
                    String key = String.valueOf(((Map.Entry) entry).getKey());
                    Object value = ((Map.Entry) entry).getValue();
                    if (key.equals(PACKAGE)) {
                        modelYAMLO.addStringToMap(PACKAGE, String.valueOf(value));
                        packageM = String.valueOf(value);
                    }

                    if (key.equals(NAME)) {
                        modelYAMLO.addStringToMap(NAME, String.valueOf(value));
                    }

                    if (key.equals(VERSION)) {
                        modelYAMLO.addStringToMap(VERSION, String.valueOf(value));
                    }

                    if (key.equals(IMPORTS)) {
                        if (value instanceof List) {
                            for (Object imp : (List) value) {
                                importsE.put(YamlUtil.afterChar(String.valueOf(imp), '.'), String.valueOf(imp));
                            }
                        }
                    }

                    List<String> tmp = YamlUtil.charParseIgnore(key, ' ', '<', '>');
                    if (tmp.contains(CLASS)) {
                        YamlObject classYAMLO = new YamlObject();
                        parseClass(key, value, classYAMLO);
                        modelYAMLO.addYamlObjectToMap(CLASS, classYAMLO);
                    } else if (tmp.contains(INTERFACE)) {
                        YamlObject interfaceYAMLO = new YamlObject();
                        parseInterface(key, value, interfaceYAMLO);
                        modelYAMLO.addYamlObjectToMap(INTERFACE, interfaceYAMLO);
                    } else if (tmp.contains(ASSOCIATION_CLASS)) {
                        YamlObject associationClassYAMLO = new YamlObject();
                        parseAssociationClass(key, value, associationClassYAMLO);
                        modelYAMLO.addYamlObjectToMap(ASSOCIATION_CLASS, associationClassYAMLO);
                    } else if (tmp.contains(ENUMERATION)) {
                        YamlObject enumerationYAMLO = new YamlObject();
                        parseEnumeration(key, value, enumerationYAMLO);
                        modelYAMLO.addYamlObjectToMap(ENUMERATION, enumerationYAMLO);
                    }
                }
            }
        }
    }

    public void parseClass(String key, Object value, YamlObject classYAMLO) {
        List<String> classPARSE = YamlUtil.charParseIgnore(key, ' ', '<', '>');
        if (classPARSE.contains(CLASS)) {
            classPARSE.remove(classPARSE.indexOf(CLASS));
        }
        if (classPARSE.contains(ABSTRACT)) {
            classYAMLO.addStringToMap(ABSTRACT, String.valueOf(true));
            classPARSE.remove(classPARSE.indexOf(ABSTRACT));
        }
        if (classPARSE.contains(STATIC)) {
            classYAMLO.addStringToMap(STATIC, String.valueOf(true));
            classPARSE.remove(classPARSE.indexOf(STATIC));
        }
        if (classPARSE.contains(EXTERN)) {
            classYAMLO.addStringToMap(EXTERN, String.valueOf(true));
            classPARSE.remove(classPARSE.indexOf(EXTERN));
        }
        if (classPARSE.contains(INNER)) {
            classYAMLO.addStringToMap(INNER, String.valueOf(true));
            classPARSE.remove(classPARSE.indexOf(INNER));
        }
        if (classPARSE.contains(PUBLIC)) {
            classYAMLO.addStringToMap(VISIBILITY, PUBLIC);
            classPARSE.remove(classPARSE.indexOf(PUBLIC));
        } else if (classPARSE.contains(PRIVATE)) {
            classYAMLO.addStringToMap(VISIBILITY, PRIVATE);
            classPARSE.remove(classPARSE.indexOf(PRIVATE));
        } else if (classPARSE.contains(PROTECTED)) {
            classYAMLO.addStringToMap(VISIBILITY, PROTECTED);
            classPARSE.remove(classPARSE.indexOf(PROTECTED));
        }

        for (String str1 : classPARSE) {
            if (str1.contains("<<") && str1.contains(">>")) {
                List<Character> targets = new LinkedList<>();
                targets.add('<');
                targets.add('>');
                String str2 = YamlUtil.removeMultiChar(str1, targets);
                targets = new LinkedList<>();
                targets.add(' ');
                targets.add(',');
                List<String> list = YamlUtil.multiCharParse(str2, targets);
                for (String str3 : list) {
                    classYAMLO.addStringToMap(STEREOTYPES, str3);
                }
            }
        }

        if (classPARSE.size() > 0) {
            String name = classPARSE.get(0);
            classYAMLO.addStringToMap(NAME, name);
            importsI.put(name, packageM + "." + name);
        }

        if (value instanceof Map) {
            for (Object entry : ((Map) value).entrySet()) {
                if (entry instanceof Map.Entry) {
                    String keyP = String.valueOf(((Map.Entry) entry).getKey());
                    Object valueP = ((Map.Entry) entry).getValue();

                    if (keyP.equals(SUPER_CLASSES)) {
                        if (valueP instanceof List) {
                            for (Object superClass : (List) valueP) {
                                classYAMLO.addStringToMap(SUPER_CLASSES, String.valueOf(superClass));
                            }
                        }
                    } else if (keyP.equals(SUPER_INTERFACES)) {
                        if (valueP instanceof List) {
                            for (Object superInterface : (List) valueP) {
                                classYAMLO.addStringToMap(SUPER_INTERFACES, String.valueOf(superInterface));
                            }
                        }
                    } else {
                        if (keyP.contains("(") && keyP.contains(")")) {
                            YamlObject operationYAMLO = new YamlObject();
                            parseOperation(keyP, valueP, operationYAMLO);
                            classYAMLO.addYamlObjectToMap(OPERATION, operationYAMLO);
                        } else {
                            YamlObject attributeYAMLO = new YamlObject();
                            parseAttribute(keyP, valueP, attributeYAMLO);
                            classYAMLO.addYamlObjectToMap(ATTRIBUTE, attributeYAMLO);
                        }
                    }
                }
            }
        }
    }

    public void parseInterface(String key, Object value, YamlObject interfaceYAMLO) {
        List<String> interfacePARSE = YamlUtil.charParseIgnore(key, ' ', '<', '>');
        if (interfacePARSE.contains(INTERFACE)) {
            interfacePARSE.remove(interfacePARSE.indexOf(INTERFACE));
        }
        if (interfacePARSE.contains(ABSTRACT)) {
            interfaceYAMLO.addStringToMap(ABSTRACT, String.valueOf(true));
            interfacePARSE.remove(interfacePARSE.indexOf(ABSTRACT));
        }
        if (interfacePARSE.contains(STATIC)) {
            interfaceYAMLO.addStringToMap(STATIC, String.valueOf(true));
            interfacePARSE.remove(interfacePARSE.indexOf(STATIC));
        }
        if (interfacePARSE.contains(EXTERN)) {
            interfaceYAMLO.addStringToMap(EXTERN, String.valueOf(true));
            interfacePARSE.remove(interfacePARSE.indexOf(EXTERN));
        }
        if (interfacePARSE.contains(INNER)) {
            interfaceYAMLO.addStringToMap(INNER, String.valueOf(true));
            interfacePARSE.remove(interfacePARSE.indexOf(INNER));
        }
        if (interfacePARSE.contains(PUBLIC)) {
            interfaceYAMLO.addStringToMap(VISIBILITY, PUBLIC);
            interfacePARSE.remove(interfacePARSE.indexOf(PUBLIC));
        } else if (interfacePARSE.contains(PRIVATE)) {
            interfaceYAMLO.addStringToMap(VISIBILITY, PRIVATE);
            interfacePARSE.remove(interfacePARSE.indexOf(PRIVATE));
        } else if (interfacePARSE.contains(PROTECTED)) {
            interfaceYAMLO.addStringToMap(VISIBILITY, PROTECTED);
            interfacePARSE.remove(interfacePARSE.indexOf(PROTECTED));
        }

        for (String str1 : interfacePARSE) {
            if (str1.contains("<<") && str1.contains(">>")) {
                List<Character> targets = new LinkedList<>();
                targets.add('<');
                targets.add('>');
                String str2 = YamlUtil.removeMultiChar(str1, targets);
                targets = new LinkedList<>();
                targets.add(' ');
                targets.add(',');
                List<String> list = YamlUtil.multiCharParse(str2, targets);
                for (String str3 : list) {
                    interfaceYAMLO.addStringToMap(STEREOTYPES, str3);
                }
            }
        }

        if (interfacePARSE.size() > 0) {
            String name = interfacePARSE.get(0);
            interfaceYAMLO.addStringToMap(NAME, name);
            importsI.put(name, packageM + "." + name);
        }

        if (value instanceof Map) {
            for (Object entry : ((Map) value).entrySet()) {
                if (entry instanceof Map.Entry) {
                    String keyP = String.valueOf(((Map.Entry) entry).getKey());
                    Object valueP = ((Map.Entry) entry).getValue();

                    if (keyP.equals(SUPER_CLASSES)) {
                        if (valueP instanceof List) {
                            for (Object superClass : (List) valueP) {
                                interfaceYAMLO.addStringToMap(SUPER_CLASSES, String.valueOf(superClass));
                            }
                        }
                    } else if (keyP.equals(SUPER_INTERFACES)) {
                        if (valueP instanceof List) {
                            for (Object superInterface : (List) valueP) {
                                interfaceYAMLO.addStringToMap(SUPER_INTERFACES, String.valueOf(superInterface));
                            }
                        }
                    } else {
                        if (keyP.contains("(") && keyP.contains(")")) {
                            YamlObject operationYAMLO = new YamlObject();
                            parseOperation(keyP, valueP, operationYAMLO);
                            interfaceYAMLO.addYamlObjectToMap(OPERATION, operationYAMLO);
                        } else {
                            YamlObject attributeYAMLO = new YamlObject();
                            parseAttribute(keyP, valueP, attributeYAMLO);
                            interfaceYAMLO.addYamlObjectToMap(ATTRIBUTE, attributeYAMLO);
                        }
                    }
                }
            }
        }
    }

    public void parseAssociationClass(String key, Object value, YamlObject associationClassYAMLO) {
        List<String> associationClassPARSE = YamlUtil.charParseIgnore(key, ' ', '<', '>');
        if (associationClassPARSE.contains(ASSOCIATION_CLASS)) {
            associationClassPARSE.remove(associationClassPARSE.indexOf(ASSOCIATION_CLASS));
        }
        if (associationClassPARSE.contains(ABSTRACT)) {
            associationClassYAMLO.addStringToMap(ABSTRACT, String.valueOf(true));
            associationClassPARSE.remove(associationClassPARSE.indexOf(ABSTRACT));
        }
        if (associationClassPARSE.contains(STATIC)) {
            associationClassYAMLO.addStringToMap(STATIC, String.valueOf(true));
            associationClassPARSE.remove(associationClassPARSE.indexOf(STATIC));
        }
        if (associationClassPARSE.contains(EXTERN)) {
            associationClassYAMLO.addStringToMap(EXTERN, String.valueOf(true));
            associationClassPARSE.remove(associationClassPARSE.indexOf(EXTERN));
        }
        if (associationClassPARSE.contains(INNER)) {
            associationClassYAMLO.addStringToMap(INNER, String.valueOf(true));
            associationClassPARSE.remove(associationClassPARSE.indexOf(INNER));
        }
        if (associationClassPARSE.contains(PUBLIC)) {
            associationClassYAMLO.addStringToMap(VISIBILITY, PUBLIC);
            associationClassPARSE.remove(associationClassPARSE.indexOf(PUBLIC));
        } else if (associationClassPARSE.contains(PRIVATE)) {
            associationClassYAMLO.addStringToMap(VISIBILITY, PRIVATE);
            associationClassPARSE.remove(associationClassPARSE.indexOf(PRIVATE));
        } else if (associationClassPARSE.contains(PROTECTED)) {
            associationClassYAMLO.addStringToMap(VISIBILITY, PROTECTED);
            associationClassPARSE.remove(associationClassPARSE.indexOf(PROTECTED));
        }

        for (String str1 : associationClassPARSE) {
            if (str1.contains("<<") && str1.contains(">>")) {
                List<Character> targets = new LinkedList<>();
                targets.add('<');
                targets.add('>');
                String str2 = YamlUtil.removeMultiChar(str1, targets);
                targets = new LinkedList<>();
                targets.add(' ');
                targets.add(',');
                List<String> list = YamlUtil.multiCharParse(str2, targets);
                for (String str3 : list) {
                    associationClassYAMLO.addStringToMap(STEREOTYPES, str3);
                }
            }
        }

        if (associationClassPARSE.size() > 0) {
            String name = associationClassPARSE.get(0);
            associationClassYAMLO.addStringToMap(NAME, name);
            importsI.put(name, packageM + "." + name);
        }

        if (value instanceof Map) {
            //TODO: utiliser variable locales
            if (((Map) value).containsKey(PARTICIPANT)) {
                if (((Map) value).get(PARTICIPANT) instanceof Map) {
                    for (Object entry : ((Map) ((Map) value).get(PARTICIPANT)).entrySet()) {
                        if (entry instanceof Map.Entry) {
                            List<String> participantPARSE = YamlUtil.charParse(String.valueOf(((Map.Entry) entry).getKey()), ' ');
                            YamlObject participantYAMLO = new YamlObject();
                            if (participantPARSE.contains("\"" + YamlUtil.extract('"', String.valueOf(((Map.Entry) entry).getKey())) + "\"")) {
                                String label = YamlUtil.extract('"', String.valueOf(((Map.Entry) entry).getKey()));

                                participantYAMLO.addStringToMap(LABEL, label);

                                participantPARSE.remove("\"" + YamlUtil.extract('"', String.valueOf(((Map.Entry) entry).getKey())) + "\"");
                            }

                            if (participantPARSE.size() > 0)//ajout type
                            {
                                participantYAMLO.addStringToMap(NAME, participantPARSE.get(0));
                            }
                            if (participantPARSE.size() > 1)//ajout name
                            {
                                participantYAMLO.addStringToMap(ATTRIBUTE, participantPARSE.get(1));
                            }
                            associationClassYAMLO.addYamlObjectToMap(PARTICIPANT, participantYAMLO);
                        }
                    }
                }
                ((Map) value).remove(PARTICIPANT);
            }

            for (Object entry : ((Map) value).entrySet()) {
                if (entry instanceof Map.Entry) {
                    String keyP = String.valueOf(((Map.Entry) entry).getKey());
                    Object valueP = ((Map.Entry) entry).getValue();

                    if (keyP.equals(SUPER_CLASSES)) {
                        if (valueP instanceof List) {
                            for (Object superClass : (List) valueP) {
                                associationClassYAMLO.addStringToMap(SUPER_CLASSES, String.valueOf(superClass));
                            }
                        }
                    } else if (keyP.equals(SUPER_INTERFACES)) {
                        if (valueP instanceof List) {
                            for (Object superInterface : (List) valueP) {
                                associationClassYAMLO.addStringToMap(SUPER_INTERFACES, String.valueOf(superInterface));
                            }
                        }
                    } else {
                        if (keyP.contains("(") && keyP.contains(")")) {
                            YamlObject operationYAMLO = new YamlObject();
                            parseOperation(keyP, valueP, operationYAMLO);
                            associationClassYAMLO.addYamlObjectToMap(OPERATION, operationYAMLO);
                        } else {
                            YamlObject attributeYAMLO = new YamlObject();
                            parseAttribute(keyP, valueP, attributeYAMLO);
                            associationClassYAMLO.addYamlObjectToMap(ATTRIBUTE, attributeYAMLO);
                        }
                    }
                }
            }
        }
    }

    public void parseEnumeration(String key, Object value, YamlObject enumerationYAMLO) {
        List<String> enumerationPARSE = YamlUtil.charParseIgnore(key, ' ', '<', '>');
        if (enumerationPARSE.contains(ENUMERATION)) {
            enumerationPARSE.remove(enumerationPARSE.indexOf(ENUMERATION));
        }
        if (enumerationPARSE.contains(STATIC)) {
            enumerationYAMLO.addStringToMap(STATIC, String.valueOf(true));
            enumerationPARSE.remove(enumerationPARSE.indexOf(STATIC));
        }

        for (String str1 : enumerationPARSE) {
            if (str1.contains("<<") && str1.contains(">>")) {
                List<Character> targets = new LinkedList<>();
                targets.add('<');
                targets.add('>');
                String str2 = YamlUtil.removeMultiChar(str1, targets);
                targets = new LinkedList<>();
                targets.add(' ');
                targets.add(',');
                List<String> list = YamlUtil.multiCharParse(str2, targets);
                for (String str3 : list) {
                    enumerationYAMLO.addStringToMap(STEREOTYPES, str3);
                }
            }
        }
        if (enumerationPARSE.size() > 0) {
            String name = enumerationPARSE.get(0);
            enumerationYAMLO.addStringToMap(NAME, name);
            importsI.put(name, packageM + "." + name);
        }

        if (value instanceof Map) {
            //TODO: utiliser variable locales
            if (((Map) value).containsKey(LITERALS)) {
                if (((Map) value).get(LITERALS) instanceof Map) {
                    for (Object entry : ((Map) ((Map) value).get(LITERALS)).entrySet()) {
                        if (entry instanceof Map.Entry) {
                            enumerationYAMLO.addStringToMap(LITERALS, String.valueOf(((Map.Entry) entry).getKey()));
                        }
                    }
                }
                ((Map) value).remove(LITERALS);
            }
        }

    }

    //TODO: factoriser code
    /*public void parseClassifier(String type, String key, Object value, YamlObject classYAMLO)
    {
        ;
    }*/

    public void parseAttribute(String key, Object value, YamlObject attributeYAMLO) {
        List<String> attributePARSE = YamlUtil.charParseIgnore(key, ' ', '<', '>');
        if (attributePARSE.contains(COMPOSITE)) {
            attributeYAMLO.addStringToMap(ASSOCIATION_TYPE, COMPOSITE);
            attributePARSE.remove(attributePARSE.indexOf(COMPOSITE));
        } else if (attributePARSE.contains(AGGREGATE)) {
            attributeYAMLO.addStringToMap(ASSOCIATION_TYPE, AGGREGATE);
            attributePARSE.remove(attributePARSE.indexOf(AGGREGATE));
        }
        if (attributePARSE.contains(STATIC)) {
            attributeYAMLO.addStringToMap(STATIC, String.valueOf(true));
            attributePARSE.remove(attributePARSE.indexOf(STATIC));
        }
        if (attributePARSE.contains(FINAL)) {
            attributeYAMLO.addStringToMap(FINAL, String.valueOf(true));
            attributePARSE.remove(attributePARSE.indexOf(FINAL));
        }
        if (attributePARSE.contains(UNIQUE)) {
            attributeYAMLO.addStringToMap(UNIQUE, String.valueOf(true));
            attributePARSE.remove(attributePARSE.indexOf(UNIQUE));
        }
        if (attributePARSE.contains(NAVIGABLE)) {
            attributeYAMLO.addStringToMap(NAVIGABLE, String.valueOf(true));
            attributePARSE.remove(attributePARSE.indexOf(NAVIGABLE));
        }
        if (attributePARSE.contains(TRANSIENT)) {
            attributeYAMLO.addStringToMap(TRANSIENT, String.valueOf(true));
            attributePARSE.remove(attributePARSE.indexOf(TRANSIENT));
        }
        if (attributePARSE.contains(PUBLIC)) {
            attributeYAMLO.addStringToMap(VISIBILITY, PUBLIC);
            attributePARSE.remove(attributePARSE.indexOf(PUBLIC));
        } else if (attributePARSE.contains(PROTECTED)) {
            attributeYAMLO.addStringToMap(VISIBILITY, PROTECTED);
            attributePARSE.remove(attributePARSE.indexOf(PUBLIC));
        } else if (attributePARSE.contains(PRIVATE)) {
            attributeYAMLO.addStringToMap(VISIBILITY, PRIVATE);
            attributePARSE.remove(attributePARSE.indexOf(PUBLIC));
        }

        if (attributePARSE.contains(ORDERED)) {
            attributeYAMLO.addStringToMap(ORDERING, ORDERED);
            attributePARSE.remove(attributePARSE.indexOf(ORDERED));
        } else if (attributePARSE.contains(UNORDERED)) {
            attributeYAMLO.addStringToMap(ORDERING, UNORDERED);
            attributePARSE.remove(attributePARSE.indexOf(UNORDERED));
        }

        //TODO: utiliser variables locales
        if (attributePARSE.contains("\"" + YamlUtil.extract('"', key) + "\"")) {
            String label = YamlUtil.extract('"', key);
            attributeYAMLO.addStringToMap(LABEL, label);
            attributePARSE.remove("\"" + YamlUtil.extract('"', key) + "\"");
        }

        if (attributePARSE.contains("[" + YamlUtil.extract('[', ']', key) + "]")) {
            String multiplicity = YamlUtil.extract('[', ']', key);
            if (multiplicity.equals("*")) {
                attributeYAMLO.addStringToMap(MIN_MULTIPLICITY, "0");
                attributeYAMLO.addStringToMap(MAX_MULTIPLICITY, "-1");
            } else if (multiplicity.contains("..")) {
                String min = YamlUtil.beforeChar(multiplicity, '.');
                String max = YamlUtil.afterChar(multiplicity, '.');
                if (min.equals("*")) {
                    min = "-1";
                }
                if (max.equals("*")) {
                    max = "-1";
                }
                attributeYAMLO.addStringToMap(MIN_MULTIPLICITY, min);
                attributeYAMLO.addStringToMap(MAX_MULTIPLICITY, max);
            }
            attributePARSE.remove("[" + YamlUtil.extract('[', ']', key) + "]");
        }

        if (attributePARSE.size() > 0) {
            attributeYAMLO.addStringToMap(TYPE, attributePARSE.get(0));
        }
        if (attributePARSE.size() > 1) {
            attributeYAMLO.addStringToMap(NAME, attributePARSE.get(1));
        }
        if (value != null) {
            //idée1: defaultValue
            //attributeYAMLO.addStringToMap(DEFAULT_VALUE,String.valueOf(value));
            //idée2: reverseMultiplicity & reverseOrdering (pour lien unidirectionnel)
            //idée3: defaultValue après '='
            List<String> valuePARSE = YamlUtil.charParse(String.valueOf(value), ' ');
            if (valuePARSE.contains(ORDERED)) {
                attributeYAMLO.addStringToMap(REVERSE_ORDERING, ORDERED);
                attributePARSE.remove(valuePARSE.indexOf(ORDERED));
            } else if (valuePARSE.contains(UNORDERED)) {
                attributeYAMLO.addStringToMap(REVERSE_ORDERING, UNORDERED);
                attributePARSE.remove(valuePARSE.indexOf(UNORDERED));
            }

            if (valuePARSE.contains("[" + YamlUtil.extract('[', ']', String.valueOf(value)) + "]")) {
                String multiplicity = YamlUtil.extract('[', ']', String.valueOf(value));
                if (multiplicity.equals("*")) {
                    attributeYAMLO.addStringToMap(REVERSE_MIN_MULTIPLICITY, "0");
                    attributeYAMLO.addStringToMap(REVERSE_MAX_MULTIPLICITY, "-1");
                } else if (multiplicity.contains("..")) {
                    String min = YamlUtil.beforeChar(multiplicity, '.');
                    String max = YamlUtil.afterChar(multiplicity, '.');
                    if (min.equals("*")) {
                        min = "-1";
                    }
                    if (max.equals("*")) {
                        max = "-1";
                    }
                    attributeYAMLO.addStringToMap(REVERSE_MIN_MULTIPLICITY, min);
                    attributeYAMLO.addStringToMap(REVERSE_MAX_MULTIPLICITY, max);
                }
                valuePARSE.remove("[" + YamlUtil.extract('[', ']', String.valueOf(value)) + "]");
            }
        }
    }

    public void parseOperation(String key, Object value, YamlObject operationYAMLO) {
        List<Character> ignoreStart = new LinkedList<>();
        ignoreStart.add('(');
        ignoreStart.add('<');
        List<Character> ignoreEnd = new LinkedList<>();
        ignoreEnd.add(')');
        ignoreEnd.add('>');
        List<String> operationPARSE = YamlUtil.charParseMultiIgnore(key, ' ', ignoreStart, ignoreEnd);

        if (operationPARSE.contains(STATIC)) {
            operationYAMLO.addStringToMap(STATIC, String.valueOf(true));
            operationPARSE.remove(operationPARSE.indexOf(STATIC));
        }
        if (operationPARSE.contains(ABSTRACT)) {
            operationYAMLO.addStringToMap(ABSTRACT, String.valueOf(true));
            operationPARSE.remove(operationPARSE.indexOf(ABSTRACT));
        }
        if (operationPARSE.contains(PUBLIC)) {
            operationYAMLO.addStringToMap(VISIBILITY, PUBLIC);
            operationPARSE.remove(operationPARSE.indexOf(PUBLIC));
        } else if (operationPARSE.contains(PROTECTED)) {
            operationYAMLO.addStringToMap(VISIBILITY, PROTECTED);
            operationPARSE.remove(operationPARSE.indexOf(PROTECTED));
        } else if (operationPARSE.contains(PRIVATE)) {
            operationYAMLO.addStringToMap(VISIBILITY, PRIVATE);
            operationPARSE.remove(operationPARSE.indexOf(PRIVATE));
        }

        for (String str1 : operationPARSE) {
            if (str1.contains("(") && str1.contains(")")) {
                operationYAMLO.addStringToMap(NAME, YamlUtil.beforeChar(str1, '('));
                for (String str2 : YamlUtil.charParseIgnore(YamlUtil.extract('(', ')', str1), ',', '<', '>')) {
                    List<String> list = YamlUtil.charParse(str2, ' ');
                    if (list.size() == 2) {
                        YamlObject parameterYAMLO = new YamlObject();
                        parameterYAMLO.addStringToMap(TYPE, list.get(0));
                        parameterYAMLO.addStringToMap(NAME, list.get(1));
                        operationYAMLO.addYamlObjectToMap(PARAMETER, parameterYAMLO);
                    }
                }
            }
        }

        if (operationPARSE.size() > 1) {
            YamlObject returnParameterYAMLO = new YamlObject();
            returnParameterYAMLO.addStringToMap(TYPE, operationPARSE.get(0));
            operationYAMLO.addYamlObjectToMap(RETURN_PARAMETER, returnParameterYAMLO);
        }

        if (value != null) {
            //idée1: returnParameter
            /*YamlObject returnParameterYAMLO = new YamlObject();
            returnParameterYAMLO.addStringToMap(TYPE, String.valueOf(value));
            operationYAMLO.addYamlObjectToMap(RETURN_PARAMETER, returnParameterYAMLO);*/
            //idée2: bodyCode
            operationYAMLO.addStringToMap(BODY_CODE, String.valueOf(value));
        }
    }

    public void resolveImports() {
        imports.putAll(importsE);
        imports.putAll(importsI);
        for (Map.Entry<String, List<YamlObject>> entry : modelYAMLO.getMapStringListYamlObject().entrySet()) {
            for (YamlObject importable : entry.getValue()) {
                if (importable.containsKeyMapStringListString(SUPER_CLASSES)) {
                    for (String value : importable.getMapStringListString(SUPER_CLASSES)) {
                        if (imports.containsKey(value)) {
                            importable.setMapStringListString(SUPER_CLASSES, value, imports.get(value));
                        }
                    }
                }
                if (importable.containsKeyMapStringListString(SUPER_INTERFACES)) {
                    for (String value : importable.getMapStringListString(SUPER_INTERFACES)) {
                        if (imports.containsKey(value)) {
                            importable.setMapStringListString(SUPER_INTERFACES, value, imports.get(value));
                        }
                    }
                }
                if (importable.containsKeyYamlMapStringListYamlObject(ATTRIBUTE)) {
                    for (YamlObject attributeYAMLO : importable.getMapStringListYamlObject(ATTRIBUTE)) {
                        if (attributeYAMLO.containsKeyMapStringListString(TYPE)) {
                            attributeYAMLO.setMapStringListString(TYPE, attributeYAMLO.getFirstMapStringListString(TYPE), resolveType(attributeYAMLO.getFirstMapStringListString(TYPE)));
                        }
                    }
                }
                if (importable.containsKeyYamlMapStringListYamlObject(OPERATION)) {
                    for (YamlObject operationYAMLO : importable.getMapStringListYamlObject(OPERATION)) {
                        if (operationYAMLO.containsKeyYamlMapStringListYamlObject(PARAMETER)) {
                            for (YamlObject parameterYAMLO : operationYAMLO.getMapStringListYamlObject(PARAMETER)) {
                                if (parameterYAMLO.containsKeyMapStringListString(TYPE)) {
                                    parameterYAMLO.setMapStringListString(TYPE, parameterYAMLO.getFirstMapStringListString(TYPE), resolveType(parameterYAMLO.getFirstMapStringListString(TYPE)));
                                }
                            }
                        }
                        if (operationYAMLO.containsKeyYamlMapStringListYamlObject(RETURN_PARAMETER)) {
                            for (YamlObject parameterYAMLO : operationYAMLO.getMapStringListYamlObject(RETURN_PARAMETER)) {
                                if (parameterYAMLO.containsKeyMapStringListString(TYPE)) {
                                    parameterYAMLO.setMapStringListString(TYPE, parameterYAMLO.getFirstMapStringListString(TYPE), resolveType(parameterYAMLO.getFirstMapStringListString(TYPE)));
                                }
                            }
                        }
                    }
                }
                if (importable.containsKeyYamlMapStringListYamlObject(PARTICIPANT)) {
                    for (YamlObject participantYAMLO : importable.getMapStringListYamlObject(PARTICIPANT)) {
                        if (participantYAMLO.containsKeyMapStringListString(NAME)) {
                            participantYAMLO.setMapStringListString(NAME, participantYAMLO.getFirstMapStringListString(NAME), resolveType(participantYAMLO.getFirstMapStringListString(NAME)));
                        }
                    }
                }
            }
        }
    }

    public String browseType(String type) {
        StringBuilder res = new StringBuilder();
        boolean first = true;
        for (String tmp : YamlUtil.charParseIgnore(type, ',', '<', '>')) {
            if (first) {
                first = false;
            } else {
                res.append(",");
            }
            if (tmp.contains("<") && tmp.contains(">")) {
                String ninja = YamlUtil.beforeChar(tmp, '<');
                if (imports.containsKey(ninja)) {
                    res.append(imports.get(ninja));
                } else {
                    //res.append(tmp);
                    res.append(ninja);
                }
                res.append("<");
                res.append(browseType(YamlUtil.extract('<', '>', tmp)));
                res.append(">");
            } else {
                if (imports.containsKey(tmp)) {
                    res.append(imports.get(tmp));
                } else {
                    res.append(tmp);
                }
            }
        }
        return res.toString();
    }

    // String -> java.lang.String
    public String resolveType(String type) {
        List<Character> ignore = new LinkedList<>();
        ignore.add(' ');
        String typePARSE = YamlUtil.removeMultiChar(type, ignore);
        return browseType(typePARSE);
    }

    public void resolveLabels() {
        for (List<YamlObject> objectYAMLO : modelYAMLO.getMapStringListYamlObject().values()) {
            for (YamlObject labelisable : objectYAMLO) {
                if (labelisable.containsKeyYamlMapStringListYamlObject(ATTRIBUTE)) {
                    for (YamlObject attribute : labelisable.getMapStringListYamlObject(ATTRIBUTE)) {
                        if (attribute.containsKeyMapStringListString(TYPE)) {
                            String type = attribute.getFirstMapStringListString(TYPE);
                            if (importsI.containsValue(type)) {
                                if (!attribute.containsKeyMapStringListString(NAVIGABLE)) {
                                    if (attribute.containsKeyMapStringListString(LABEL)) {
                                        String label = attribute.getFirstMapStringListString(LABEL);
                                        YamlObject reverseAttribute = getReverseAttribute(YamlUtil.afterChar(type, '.'), label);
                                        if (reverseAttribute != null) {
                                            attribute.addStringToMap(NAVIGABLE, String.valueOf(true));
                                            reverseAttribute.addStringToMap(NAVIGABLE, String.valueOf(true));
                                            if (attribute.containsKeyMapStringListString(MAX_MULTIPLICITY)) {
                                                reverseAttribute.addStringToMap(REVERSE_MAX_MULTIPLICITY, attribute.getFirstMapStringListString(MAX_MULTIPLICITY));
                                            }
                                            if (reverseAttribute.containsKeyMapStringListString(MAX_MULTIPLICITY)) {
                                                attribute.addStringToMap(REVERSE_MAX_MULTIPLICITY, reverseAttribute.getFirstMapStringListString(MAX_MULTIPLICITY));
                                            }
                                            if (attribute.containsKeyMapStringListString(NAME)) {
                                                reverseAttribute.addStringToMap(REVERSE_ATTRIBUTE_NAME, attribute.getFirstMapStringListString(NAME));
                                            }
                                            if (reverseAttribute.containsKeyMapStringListString(NAME)) {
                                                attribute.addStringToMap(REVERSE_ATTRIBUTE_NAME, reverseAttribute.getFirstMapStringListString(NAME));
                                            }
                                        }
                                    } else {
                                        YamlObject reverseObject = getReverseObject(YamlUtil.afterChar(type, '.'));
                                        if (reverseObject != null) {
                                            YamlObject reverseAttribute = new YamlObject();
                                            attribute.addStringToMap(NAVIGABLE, String.valueOf(true));
                                            reverseAttribute.addStringToMap(NAVIGABLE, String.valueOf(false));
                                            reverseAttribute.addStringToMap(TYPE, imports.get(labelisable.getFirstMapStringListString(NAME)));
                                            if (attribute.containsKeyMapStringListString(MAX_MULTIPLICITY)) {
                                                reverseAttribute.addStringToMap(REVERSE_MAX_MULTIPLICITY, attribute.getFirstMapStringListString(MAX_MULTIPLICITY));
                                            }
                                            if (attribute.containsKeyMapStringListString(NAME)) {
                                                reverseAttribute.addStringToMap(REVERSE_ATTRIBUTE_NAME, attribute.getFirstMapStringListString(NAME));
                                            }
                                            if (attribute.containsKeyMapStringListString(REVERSE_MIN_MULTIPLICITY)) {
                                                reverseAttribute.addStringToMap(MIN_MULTIPLICITY, attribute.getFirstMapStringListString(REVERSE_MIN_MULTIPLICITY));
                                            }
                                            if (attribute.containsKeyMapStringListString(REVERSE_MAX_MULTIPLICITY)) {
                                                reverseAttribute.addStringToMap(MAX_MULTIPLICITY, attribute.getFirstMapStringListString(REVERSE_MAX_MULTIPLICITY));
                                            }
                                            if (attribute.containsKeyMapStringListString(REVERSE_ORDERING)) {
                                                reverseAttribute.addStringToMap(ORDERING, attribute.getFirstMapStringListString(REVERSE_ORDERING));
                                            }
                                            //else set opposit ordering
                                            else if (attribute.containsKeyMapStringListString(ORDERING)) {
                                                if (attribute.getFirstMapStringListString(ORDERING).equals(ORDERED)) {
                                                    reverseAttribute.addStringToMap(ORDERING, UNORDERED);
                                                } else if (attribute.getFirstMapStringListString(ORDERING).equals(UNORDERED)) {
                                                    reverseAttribute.addStringToMap(ORDERING, ORDERED);
                                                }
                                            }
                                            reverseObject.addYamlObjectToMap(ATTRIBUTE, reverseAttribute);
                                        }
                                    }
                                }
                            }
                            /*else//simple attribut
                            {
                                ;
                            }*/
                        }
                    }
                }
                if (labelisable.containsKeyYamlMapStringListYamlObject(PARTICIPANT)) {
                    for (YamlObject participant : labelisable.getMapStringListYamlObject(PARTICIPANT)) {
                        if (participant.containsKeyMapStringListString(NAME)) {
                            String name = participant.getFirstMapStringListString(NAME);
                            if (importsI.containsValue(name))//refClassifier
                            {
                                if (participant.containsKeyMapStringListString(LABEL)) {
                                    String label = participant.getFirstMapStringListString(LABEL);
                                    YamlObject reverseAttribute = getReverseAttribute(YamlUtil.afterChar(name, '.'), label);
                                    if (reverseAttribute != null) {
                                        if (labelisable.containsKeyMapStringListString(NAME)) {
                                            reverseAttribute.addStringToMap(ASSOCIATION_CLASS_NAME, packageM + "." + labelisable.getFirstMapStringListString(NAME));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public YamlObject getReverseAttribute(String type, String label) {
        for (List<YamlObject> objectsYAMLO : modelYAMLO.getMapStringListYamlObject().values()) {
            for (YamlObject objectYAMLO : objectsYAMLO) {
                if (objectYAMLO.containsKeyMapStringListString(NAME)) {
                    String name = objectYAMLO.getFirstMapStringListString(NAME);
                    if (name.equals(type)) {
                        if (objectYAMLO.containsKeyYamlMapStringListYamlObject(ATTRIBUTE)) {
                            for (YamlObject attributeYAMLO : objectYAMLO.getMapStringListYamlObject(ATTRIBUTE)) {
                                if (attributeYAMLO.containsKeyMapStringListString(LABEL)) {
                                    String reverseLabel = attributeYAMLO.getFirstMapStringListString(LABEL);
                                    if (reverseLabel.equals(label)) {
                                        return attributeYAMLO;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public YamlObject getReverseObject(String type) {
        for (List<YamlObject> objectsYAMLO : modelYAMLO.getMapStringListYamlObject().values()) {
            for (YamlObject objectYAMLO : objectsYAMLO) {
                if (objectYAMLO.containsKeyMapStringListString(NAME)) {
                    String name = objectYAMLO.getFirstMapStringListString(NAME);
                    if (name.equals(type)) {
                        return objectYAMLO;
                    }
                }
            }
        }
        return null;
    }

}
