/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


package org.nuiton.eugene.models.object;

/**
 * Modifiers that can be used
 *
 * Created: 3 nov. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public interface ObjectModelModifier {

    // Still present only for backward compatibility. Please use ObjectModelJavaModifier.*
    @Deprecated
    ObjectModelModifier STATIC = ObjectModelJavaModifier.STATIC;

    @Deprecated
    ObjectModelModifier FINAL = ObjectModelJavaModifier.FINAL;

    @Deprecated
    ObjectModelModifier ABSTRACT = ObjectModelJavaModifier.ABSTRACT;

    @Deprecated
    ObjectModelModifier TRANSIENT = ObjectModelJavaModifier.TRANSIENT;

    @Deprecated
    ObjectModelModifier PUBLIC = ObjectModelJavaModifier.PUBLIC;

    @Deprecated
    ObjectModelModifier PROTECTED = ObjectModelJavaModifier.PROTECTED;

    @Deprecated
    ObjectModelModifier PRIVATE = ObjectModelJavaModifier.PRIVATE;

    @Deprecated
    ObjectModelModifier PACKAGE = ObjectModelJavaModifier.PACKAGE;

    // Still present only for backward compatibility. Please use ObjectModelUMLModifier.*
    @Deprecated
    ObjectModelModifier AGGREGATE = ObjectModelUMLModifier.AGGREGATE;

    @Deprecated
    ObjectModelModifier COMPOSITE = ObjectModelUMLModifier.COMPOSITE;

    @Deprecated
    ObjectModelModifier UNIQUE = ObjectModelUMLModifier.UNIQUE;

    @Deprecated
    ObjectModelModifier ORDERED = ObjectModelUMLModifier.ORDERED;

    @Deprecated
    ObjectModelModifier NAVIGABLE = ObjectModelUMLModifier.NAVIGABLE;

    /**
     * @return true is the current ObjectModelModifier is a Java visibility
     */
    boolean isVisibility();

    /**
     * @return true is the current ObjectModelModifier is an UML association
     * type
     */
    boolean isAssociationType();

    /**
     * @return the name of the constant. This refers to Enum.name().
     */
    String getName();

}
