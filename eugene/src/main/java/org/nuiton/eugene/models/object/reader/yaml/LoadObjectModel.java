package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.xml.ObjectModeImplAssociationClassParticipant;
import org.nuiton.eugene.models.object.xml.ObjectModelAssociationClassImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelClassImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelClassifierImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelElementImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelEnumerationImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelImplRef;
import org.nuiton.eugene.models.object.xml.ObjectModelImplSuperClassRef;
import org.nuiton.eugene.models.object.xml.ObjectModelInterfaceImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelOperationImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelParameterImpl;

import java.util.List;
import java.util.Map;

/**
 * User: agiraudet
 * Date: 11/06/13
 * Time: 10:37
 */
public class LoadObjectModel implements KeyWords {

    protected String packageL;

    protected YamlObject modelYAMLO;

    protected ObjectModelImpl modelOM;

    protected Map<String, String> defaultValues;

    public LoadObjectModel(YamlObject modelYAMLO, ObjectModelImpl modelOM, Map<String, String> defaultValues) {
        this.packageL = "default";
        this.modelOM = modelOM;
        this.modelYAMLO = modelYAMLO;
        this.defaultValues = defaultValues;
        //log
        //Log.initLog(modelYAMLO.toString(), "/tmp/log.LoadObjectModel.txt");
        //log
    }

    public void loadModel() {
        String packageYAMLO = modelYAMLO.getFirstMapStringListString(PACKAGE);
        if (packageYAMLO == null) {
            String key = PACKAGE;
            if (defaultValues.containsKey(key)) {
                packageL = defaultValues.get(key);
            }
        } else {
            packageL = packageYAMLO;
        }
        //permet d'afficher le package dans le diagramme plantuml
        modelOM.addTagValue(PACKAGE, packageL);

        String nameYAMLO = modelYAMLO.getFirstMapStringListString(NAME);
        if (nameYAMLO == null) {
            String key = NAME;
            if (defaultValues.containsKey(key)) {
                modelOM.setName(defaultValues.get(key));
            }
        } else {
            modelOM.setName(nameYAMLO);
        }

        //version
        String versionYAMLO = modelYAMLO.getFirstMapStringListString(VERSION);
        if (versionYAMLO == null) {
            String key = VERSION;
            if (defaultValues.containsKey(key)) {
                modelOM.setVersion(defaultValues.get(key));
            }
        } else {
            modelOM.setVersion(versionYAMLO);
        }

        //tagValues
        YamlObject tagValues = modelYAMLO.getFirstMapStringListYamlObject(TAG_VALUES);
        if (tagValues != null) {
            for (Map.Entry<String, List<String>> tagValue : tagValues.getMapStringListString().entrySet()) {
                if (!tagValue.getValue().isEmpty())//taille strictement = 1
                {
                    modelOM.addTagValue(tagValue.getKey(), tagValue.getValue().get(0));
                }
            }
        }

        //classes
        for (YamlObject classYAMLO : modelYAMLO.getMapStringListYamlObject(CLASS)) {
            ObjectModelClassImpl classOM = new ObjectModelClassImpl();
            loadClass(classYAMLO, classOM);
            modelOM.addClass(classOM);
        }
        //interfaces
        for (YamlObject interfaceYAMLO : modelYAMLO.getMapStringListYamlObject(INTERFACE)) {
            ObjectModelInterfaceImpl interfaceOM = new ObjectModelInterfaceImpl();
            loadInterface(interfaceYAMLO, interfaceOM);
            modelOM.addInterface(interfaceOM);
        }
        //classes d'association
        for (YamlObject associationClassYAMLO : modelYAMLO.getMapStringListYamlObject(ASSOCIATION_CLASS)) {
            ObjectModelAssociationClassImpl associationClassOM = new ObjectModelAssociationClassImpl();
            loadAssociationClass(associationClassYAMLO, associationClassOM);
            modelOM.addAssociationClass(associationClassOM);
        }
        //enumerations
        for (YamlObject enumerationYAMLO : modelYAMLO.getMapStringListYamlObject(ENUMERATION)) {
            ObjectModelEnumerationImpl enumerationOM = new ObjectModelEnumerationImpl();
            loadEnumeration(enumerationYAMLO, enumerationOM);
            modelOM.addEnumeration(enumerationOM);
        }
    }

    public void loadElement(YamlObject elementYAMLO, ObjectModelElementImpl elementOM) {
        //name
        String nameYAMLO = elementYAMLO.getFirstMapStringListString(NAME);
        if (nameYAMLO == null) {
            String key = ELEMENT + SEPARATOR + NAME;
            if (defaultValues.containsKey(key)) {
                elementOM.setName(defaultValues.get(key));
            }
        } else {
            elementOM.setName(nameYAMLO);
        }

        //static
        String staticYAMLO = elementYAMLO.getFirstMapStringListString(STATIC);
        if (staticYAMLO == null) {
            String key = ELEMENT + SEPARATOR + STATIC;
            if (defaultValues.containsKey(key)) {
                elementOM.setStatic(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            elementOM.setStatic(Boolean.valueOf(staticYAMLO));
        }

        //documentation
        String documentationYAMLO = elementYAMLO.getFirstMapStringListString(DOCUMENTATION);
        if (documentationYAMLO == null) {
            String key = ELEMENT + SEPARATOR + DOCUMENTATION;
            if (defaultValues.containsKey(key)) {
                elementOM.setDocumentation(defaultValues.get(key));
            }
        } else {
            elementOM.setDocumentation(documentationYAMLO);
        }

        //tagValues
        YamlObject tagValues = elementYAMLO.getFirstMapStringListYamlObject(TAG_VALUES);
        if (tagValues != null) {
            for (Map.Entry<String, List<String>> tagValue : tagValues.getMapStringListString().entrySet()) {
                if (!tagValue.getValue().isEmpty())//taille strictement = 1
                {
                    elementOM.addTagValue(tagValue.getKey(), tagValue.getValue().get(0));
                }
            }
        }

        //comments
        List<String> comments = elementYAMLO.getMapStringListString(COMMENTS);
        for (String comment : comments) {
            elementOM.addComment(comment);
        }

        //stereotypes
        List<String> stereotypes = elementYAMLO.getMapStringListString(STEREOTYPES);
        for (String stereotype : stereotypes) {
            ObjectModelImplRef stereotypeOM = new ObjectModelImplRef();
            stereotypeOM.setName(stereotype);
            elementOM.addStereotype(stereotypeOM);
        }
    }

    public void loadClassifier(YamlObject classifierYAMLO, ObjectModelClassifierImpl classifierOM) {
        loadElement(classifierYAMLO, classifierOM);

        //package
        String packageYAMLO = classifierYAMLO.getFirstMapStringListString(PACKAGE);
        if (packageYAMLO == null) {
            classifierOM.setPackage(packageL);
        } else {
            classifierOM.setPackage(packageYAMLO);
        }

        //extern
        String externYAMLO = classifierYAMLO.getFirstMapStringListString(EXTERN);
        if (externYAMLO == null) {
            String key = CLASSIFIER + SEPARATOR + EXTERN;
            if (defaultValues.containsKey(key)) {
                classifierOM.setExtern(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            classifierOM.setExtern(Boolean.valueOf(externYAMLO));

        }

        //inner
        String innerYAMLO = classifierYAMLO.getFirstMapStringListString(INNER);
        if (innerYAMLO == null) {
            String key = CLASSIFIER + SEPARATOR + INNER;
            if (defaultValues.containsKey(key)) {
                classifierOM.setInner(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            classifierOM.setInner(Boolean.valueOf(innerYAMLO));
        }

        //type
        String typeYAMLO = classifierYAMLO.getFirstMapStringListString(TYPE);
        if (typeYAMLO == null) {
            String key = CLASSIFIER + SEPARATOR + TYPE;
            if (defaultValues.containsKey(key)) {
                classifierOM.setType(defaultValues.get(key));
            }
        } else {
            classifierOM.setType(typeYAMLO);
        }

        //attributes
        for (YamlObject attributeYAMLO : classifierYAMLO.getMapStringListYamlObject(ATTRIBUTE)) {
            ObjectModelAttributeImpl attributeOM = new ObjectModelAttributeImpl();
            loadAttribute(attributeYAMLO, attributeOM);
            classifierOM.addAttribute(attributeOM);
        }

        //operations
        for (YamlObject operationYAMLO : classifierYAMLO.getMapStringListYamlObject(OPERATION)) {
            ObjectModelOperationImpl operationOM = new ObjectModelOperationImpl();
            loadOperation(operationYAMLO, operationOM);
            classifierOM.addOperation(operationOM);
        }

        //superInterfaces
        List<String> superInterfacesYAMLO = classifierYAMLO.getMapStringListString(SUPER_INTERFACES);
        for (String superInterfaceYAMLO : superInterfacesYAMLO) {
            ObjectModelImplRef superInterfaceOM = new ObjectModelImplRef();
            superInterfaceOM.setName(superInterfaceYAMLO);
            classifierOM.addInterface(superInterfaceOM);
        }
    }

    public void loadClass(YamlObject classYAMLO, ObjectModelClassImpl classOM) {
        loadClassifier(classYAMLO, classOM);

        //abstract
        String abstractYAMLO = classYAMLO.getFirstMapStringListString(ABSTRACT);
        if (abstractYAMLO == null) {
            String key = CLASS + SEPARATOR + ABSTRACT;
            if (defaultValues.containsKey(key)) {
                classOM.setAbstract(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            classOM.setAbstract(Boolean.valueOf(abstractYAMLO));
        }

        //superClasses
        List<String> superClassesYAMLO = classYAMLO.getMapStringListString(SUPER_CLASSES);
        for (String superClassYAMLO : superClassesYAMLO) {
            ObjectModelImplSuperClassRef superClassOM = new ObjectModelImplSuperClassRef();
            superClassOM.setName(superClassYAMLO);
            classOM.addSuperclass(superClassOM);
        }
    }

    public void loadInterface(YamlObject interfaceYAMLO, ObjectModelInterfaceImpl interfaceOM) {
        loadClassifier(interfaceYAMLO, interfaceOM);
    }

    public void loadAssociationClass(YamlObject associationClassYAML, ObjectModelAssociationClassImpl associationClassOM) {
        loadClass(associationClassYAML, associationClassOM);

        //TODO: remplacer name par type et attribute par name ? -> confusion
        //TODO: remplacer PARTICIPANT par PARTICIPANTS (List)
        //participants
        for (YamlObject participantYAMLO : associationClassYAML.getMapStringListYamlObject(PARTICIPANT)) {
            ObjectModeImplAssociationClassParticipant participantOM = new ObjectModeImplAssociationClassParticipant();
            participantOM.setAssociationClass(associationClassOM);
            //name
            String nameYAMLO = participantYAMLO.getFirstMapStringListString(NAME);
            if (nameYAMLO == null) {
                String key = ASSOCIATION_CLASS + SEPARATOR + PARTICIPANT + SEPARATOR + NAME;
                if (defaultValues.containsKey(key)) {
                    participantOM.setName(defaultValues.get(key));
                }
            } else {
                participantOM.setName(nameYAMLO);
            }
            //attribute
            String attributeYAMLO = participantYAMLO.getFirstMapStringListString(ATTRIBUTE);
            if (attributeYAMLO == null) {
                String key = ASSOCIATION_CLASS + SEPARATOR + PARTICIPANT + SEPARATOR + ATTRIBUTE;
                if (defaultValues.containsKey(key)) {
                    participantOM.setAttribute(defaultValues.get(key));
                }
            } else {
                participantOM.setAttribute(attributeYAMLO);
            }

            associationClassOM.addParticipant(participantOM);
        }
    }

    public void loadEnumeration(YamlObject enumerationYAMLO, ObjectModelEnumerationImpl enumerationOM) {
        loadElement(enumerationYAMLO, enumerationOM);

        //TODO: remplacer LITERAL par LITERALS (List)
        //lierals
        List<String> literalsYAMLO = enumerationYAMLO.getMapStringListString(LITERALS);
        for (String literalYAMLO : literalsYAMLO) {
            ObjectModelImplRef literalOM = new ObjectModelImplRef();
            literalOM.setName(literalYAMLO);
            enumerationOM.addLiteral(literalOM);
        }

        //package
        String packageYAMLO = enumerationYAMLO.getFirstMapStringListString(PACKAGE);
        if (packageYAMLO == null) {
            enumerationOM.setPackage(packageL);
        } else {
            enumerationOM.setPackage(packageYAMLO);
        }
    }

    public void loadParameter(YamlObject parameterYAMLO, ObjectModelParameterImpl parameterOM) {
        loadElement(parameterYAMLO, parameterOM);

        //ordering
        String orderingYAMLO = parameterYAMLO.getFirstMapStringListString(ORDERING);
        if (orderingYAMLO == null) {
            String key = PARAMETER + SEPARATOR + ORDERING;
            if (defaultValues.containsKey(key)) {
                parameterOM.setOrdering(defaultValues.get(key));
            }
        } else {
            parameterOM.setOrdering(orderingYAMLO);
        }

        //type
        String typeYAMLO = parameterYAMLO.getFirstMapStringListString(TYPE);
        if (typeYAMLO == null) {
            String key = PARAMETER + SEPARATOR + TYPE;
            if (defaultValues.containsKey(key)) {
                parameterOM.setType(defaultValues.get(key));
            }
        } else {
            parameterOM.setType(typeYAMLO);
        }

        //defaultValues
        String defaultValueYAMLO = parameterYAMLO.getFirstMapStringListString(DEFAULT_VALUE);
        if (defaultValueYAMLO == null) {
            String key = PARAMETER + SEPARATOR + DEFAULT_VALUE;
            if (defaultValues.containsKey(key)) {
                parameterOM.setDefaultValue(defaultValues.get(key));
            }
        } else {
            parameterOM.setDefaultValue(defaultValueYAMLO);
        }

        //minMultiplicity
        String minMultiplicityYAMLO = parameterYAMLO.getFirstMapStringListString(MIN_MULTIPLICITY);
        if (minMultiplicityYAMLO == null) {
            String key = PARAMETER + SEPARATOR + MIN_MULTIPLICITY;
            if (defaultValues.containsKey(key)) {
                parameterOM.setMinMultiplicity(Integer.valueOf(defaultValues.get(key)));
            }
        } else {
            parameterOM.setMinMultiplicity(Integer.valueOf(minMultiplicityYAMLO));
        }

        //maxMultiplicity
        String maxMultiplicityYAMLO = parameterYAMLO.getFirstMapStringListString(MAX_MULTIPLICITY);
        if (maxMultiplicityYAMLO == null) {
            String key = PARAMETER + SEPARATOR + MAX_MULTIPLICITY;
            if (defaultValues.containsKey(key)) {
                parameterOM.setMaxMultiplicity(Integer.valueOf(defaultValues.get(key)));
            }
        } else {
            parameterOM.setMaxMultiplicity(Integer.valueOf(maxMultiplicityYAMLO));

        }

        //ordered
        String orderedYAMLO = parameterYAMLO.getFirstMapStringListString(ORDERED);
        if (orderedYAMLO == null) {
            String key = PARAMETER + SEPARATOR + ORDERED;
            if (defaultValues.containsKey(key)) {
                parameterOM.setOrdered(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            parameterOM.setOrdered(Boolean.valueOf(orderedYAMLO));
        }

        //unique
        String uniqueYAMLO = parameterYAMLO.getFirstMapStringListString(UNIQUE);
        if (uniqueYAMLO == null) {
            String key = PARAMETER + SEPARATOR + UNIQUE;
            if (defaultValues.containsKey(key)) {
                parameterOM.setUnique(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            parameterOM.setUnique(Boolean.valueOf(uniqueYAMLO));
        }
    }

    public void loadAttribute(YamlObject attributeYAMLO, ObjectModelAttributeImpl attributeOM) {
        loadParameter(attributeYAMLO, attributeOM);

        //navigable
        String navigableYAMLO = attributeYAMLO.getFirstMapStringListString(NAVIGABLE);
        if (navigableYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + NAVIGABLE;
            if (defaultValues.containsKey(key)) {
                attributeOM.setNavigable(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            attributeOM.setNavigable(Boolean.valueOf(navigableYAMLO));
        }

        //associationType
        String associationTypeYAMLO = attributeYAMLO.getFirstMapStringListString(ASSOCIATION_TYPE);
        if (associationTypeYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + ASSOCIATION_TYPE;
            if (defaultValues.containsKey(key)) {
                attributeOM.setAssociationType(defaultValues.get(key));
            }
        } else {
            attributeOM.setAssociationType(associationTypeYAMLO);
        }

        //final
        String finalYAMLO = attributeYAMLO.getFirstMapStringListString(FINAL);
        if (finalYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + FINAL;
            if (defaultValues.containsKey(key)) {
                attributeOM.setFinal(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            attributeOM.setFinal(Boolean.valueOf(finalYAMLO));
        }

        //static
        String staticYAMLO = attributeYAMLO.getFirstMapStringListString(STATIC);
        if (staticYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + STATIC;
            if (defaultValues.containsKey(key)) {
                attributeOM.setStatic(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            attributeOM.setStatic(Boolean.valueOf(staticYAMLO));
        }

        //associationClassName
        String associationClassNameYAMLO = attributeYAMLO.getFirstMapStringListString(ASSOCIATION_CLASS_NAME);
        if (associationClassNameYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + ASSOCIATION_CLASS_NAME;
            if (defaultValues.containsKey(key)) {
                attributeOM.setAssociationClassName(defaultValues.get(key));
            }
        } else {
            attributeOM.setAssociationClassName(associationClassNameYAMLO);
        }

        //reverseAttributeName
        String reverseAttributeNameYAMLO = attributeYAMLO.getFirstMapStringListString(REVERSE_ATTRIBUTE_NAME);
        if (reverseAttributeNameYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + REVERSE_ATTRIBUTE_NAME;
            if (defaultValues.containsKey(key)) {
                attributeOM.setReverseAttributeName(defaultValues.get(key));
            }
        } else {
            attributeOM.setReverseAttributeName(reverseAttributeNameYAMLO);
        }

        //reverseMaxMultiplicity
        String reverseMaxMultiplicityYAMLO = attributeYAMLO.getFirstMapStringListString(REVERSE_MAX_MULTIPLICITY);
        if (reverseMaxMultiplicityYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + REVERSE_MAX_MULTIPLICITY;
            if (defaultValues.containsKey(key)) {
                attributeOM.setReverseMaxMultiplicity(Integer.valueOf(defaultValues.get(key)));
            }
        } else {
            attributeOM.setReverseMaxMultiplicity(Integer.valueOf(reverseMaxMultiplicityYAMLO));
        }

        //transient
        String transientYAMLO = attributeYAMLO.getFirstMapStringListString(TRANSIENT);
        if (transientYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + TRANSIENT;
            if (defaultValues.containsKey(key)) {
                attributeOM.setTransient(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            attributeOM.setTransient(Boolean.valueOf(transientYAMLO));
        }

        //visibility
        String visibilityYAMLO = attributeYAMLO.getFirstMapStringListString(VISIBILITY);
        if (visibilityYAMLO == null) {
            String key = ATTRIBUTE + SEPARATOR + VISIBILITY;
            if (defaultValues.containsKey(key)) {
                attributeOM.setVisibility(defaultValues.get(key));
            }
        } else {
            attributeOM.setVisibility(visibilityYAMLO);
        }
    }

    public void loadOperation(YamlObject operationYAMLO, ObjectModelOperationImpl operationOM) {
        loadElement(operationYAMLO, operationOM);

        //abstract
        String abstractYAMLO = operationYAMLO.getFirstMapStringListString(ABSTRACT);
        if (abstractYAMLO == null) {
            String key = OPERATION + SEPARATOR + ABSTRACT;
            if (defaultValues.containsKey(key)) {
                operationOM.setAbstract(Boolean.valueOf(defaultValues.get(key)));
            }
        } else {
            operationOM.setAbstract(Boolean.valueOf(abstractYAMLO));
        }

        //visibility
        String visibilityYAMLO = operationYAMLO.getFirstMapStringListString(VISIBILITY);
        if (visibilityYAMLO == null) {
            String key = OPERATION + SEPARATOR + VISIBILITY;
            if (defaultValues.containsKey(key)) {
                operationOM.setVisibility(defaultValues.get(key));
            }
        } else {
            operationOM.setVisibility(visibilityYAMLO);
        }

        //returnParameter
        for (YamlObject returnParameterYAMLO : operationYAMLO.getMapStringListYamlObject(RETURN_PARAMETER)) {
            ObjectModelParameterImpl returnParameterOM = new ObjectModelAttributeImpl();
            loadParameter(returnParameterYAMLO, returnParameterOM);
            operationOM.setReturnParameter(returnParameterOM);
        }

        //parameter
        for (YamlObject parameterYAMLO : operationYAMLO.getMapStringListYamlObject(PARAMETER)) {
            ObjectModelParameterImpl parameterOM = new ObjectModelAttributeImpl();
            loadParameter(parameterYAMLO, parameterOM);
            operationOM.addParameter(parameterOM);
        }

        //bodyCode
        String bodyCodeYAMLO = operationYAMLO.getFirstMapStringListString(BODY_CODE);
        if (bodyCodeYAMLO == null) {
            String key = OPERATION + SEPARATOR + BODY_CODE;
            if (defaultValues.containsKey(key)) {
                operationOM.setBodyCode(defaultValues.get(key));
            }
        } else {
            operationOM.setBodyCode(bodyCodeYAMLO);
        }
    }
}
