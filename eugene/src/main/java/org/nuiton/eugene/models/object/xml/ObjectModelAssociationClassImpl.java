/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * ObjectModelAssociationClassImpl.java
 *
 * @author chatellier
 * @author cedric
 */
public class ObjectModelAssociationClassImpl extends ObjectModelClassImpl
        implements ObjectModelAssociationClass {

    private static Log log = LogFactory
            .getLog(ObjectModelAssociationClassImpl.class);

    protected List<ObjectModelAttribute> participantsAttributes;

    protected List<ObjectModelClassifier> participantsClassifiers;

    protected List<ObjectModeImplAssociationClassParticipant> participantsRefs = new ArrayList<>();

    public ObjectModelAssociationClassImpl() {
    }

    public void addParticipant(
            ObjectModeImplAssociationClassParticipant participant) {
        //if (participant == null)
        //    return new ObjectModeImplAssociationClassParticipant(this);
        participant.postInit();
        participant.setAssociationClass(this);
        participantsRefs.add(participant);
        //return participant;
    }

    /**
     * Returns all participants (that is association ends) attributes for this association class.
     *
     * @return a List containing all participants attributes for this association class.
     * @see ObjectModelAttribute
     */
    @Override
    public List<ObjectModelAttribute> getParticipantsAttributes() {
        if (participantsAttributes == null) {
            parseParticipantsRefs();
        }
        return participantsAttributes;
    }

    /**
     * Returns all participants (that is association ends) classifiers for this association class.
     *
     * @return a List containing all participants classifiers for this association class.
     * @see ObjectModelClassifier
     */
    @Override
    public List<ObjectModelClassifier> getParticipantsClassifiers() {
        if (participantsClassifiers == null) {
            parseParticipantsRefs();
        }
        return participantsClassifiers;
    }

    protected void parseParticipantsRefs() {
        participantsClassifiers = new ArrayList<>();
        participantsAttributes = new ArrayList<>();

        for (ObjectModeImplAssociationClassParticipant ref : participantsRefs) {
            ObjectModelClassifier classifier = objectModelImpl
                    .getClassifier(ref.getName());
            participantsClassifiers.add(classifier);
            ObjectModelAttribute attribute = null;
            //TODO this 
//            if (classifier.isClass()) {
            if (classifier instanceof ObjectModelClass) {
                attribute = classifier.getAttribute(ref.getAttributeName());
                if (attribute == null) {
                    log.warn("WARNING : Attribute " + ref.getAttributeName()
                             + " not found on " + classifier.getQualifiedName());
                    log.warn("WARNING : Assuming there is no navigability in this direction for the "
                             + getQualifiedName() + " association class");
                }
            }
            participantsAttributes.add(attribute);
        }
    }

    public Collection<ObjectModeImplAssociationClassParticipant> getParticipantsRefs() {
        return participantsRefs;
    }

}
