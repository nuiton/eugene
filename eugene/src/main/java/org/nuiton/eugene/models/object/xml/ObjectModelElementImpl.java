/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * ObjectModelElementImpl.
 *
 * @author chatellier
 * @author cedric
 */
public abstract class ObjectModelElementImpl implements ObjectModelElement {

    /** logger */
    private static final Log log = LogFactory.getLog(ObjectModelElementImpl.class);

    protected ObjectModelImpl objectModelImpl;

    protected ObjectModelElement declaringElement;

    protected String name;

    protected String documentation;

    protected Map<String, String> tagValues;

    protected List<String> comments;

    protected Set<ObjectModelModifier> modifiers;

    public ObjectModelElementImpl() {
        tagValues = new HashMap<>();
        comments = new ArrayList<>();
        modifiers = new HashSet<>();
    }

    /** @param objectModelImpl the objectModelImpl */
    public void setObjectModelImpl(ObjectModelImpl objectModelImpl) {
        this.objectModelImpl = objectModelImpl;
    }

    /** @param declaringElement the declaringElement to set */
    public void setDeclaringElement(ObjectModelElement declaringElement) {
        this.declaringElement = declaringElement;
    }

    /**
     * TODO a tester
     *
     * @return the objectModel
     */
    public ObjectModelImpl getModel() {
        if (objectModelImpl != null) {
            return objectModelImpl;
        } else if (declaringElement != null) {
            return ((ObjectModelElementImpl) declaringElement).getModel();
        }
        return null;
    }

    public void postInit() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public ObjectModelImplRef addStereotype(ObjectModelImplRef stereotype) {
        if (stereotype == null) {
            return new ObjectModelImplRef();
        }
        addStereotype(stereotype.getName());
        return stereotype;
    }

    public ObjectModelImplTagValue addTagValue(ObjectModelImplTagValue tagValue) {
        if (tagValue == null) {
            return new ObjectModelImplTagValue();
        }
        addTagValue(tagValue.getName(), tagValue.getValue());
        return tagValue;
    }

    protected void addOrRemoveModifier(ObjectModelModifier modifier, boolean add) {
        if (add) {
            addModifier(modifier);
        } else {
            removeModifier(modifier);
        }
    }

    public void addModifier(ObjectModelModifier... modifiers) {
        if (modifiers == null || (modifiers.length == 1 && modifiers[0] == null)) {
            throw new IllegalArgumentException("Modifier is null");
        }
        for (ObjectModelModifier modifier : modifiers) {
            if (getAuthorizedModifiers().contains(modifier)) {
                this.modifiers.add(modifier);
            } else {
                throw new UnsupportedOperationException("Forbidden modifier: " + modifier.getName());
            }
        }
    }

    protected void removeModifiers(Iterable<? extends ObjectModelModifier> modifiers) {
        removeModifier(Iterables.toArray(modifiers, ObjectModelModifier.class));
    }

    protected void removeModifier(ObjectModelModifier... modifiers) {
        for (ObjectModelModifier modifier : modifiers) {
            this.modifiers.remove(modifier);
        }
    }

    protected abstract Set<ObjectModelModifier> getAuthorizedModifiers();

    public void setStatic(boolean isStatic) {
        addOrRemoveModifier(ObjectModelJavaModifier.STATIC, isStatic);
    }

    public void addComment(String comment) {
        comments.add(comment);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ObjectModelElement getDeclaringElement() {
        return declaringElement;
    }

    @Override
    public String getDocumentation() {
        if (documentation == null && hasTagValue(EugeneCoreTagValues.Store.documentation.getName())) {
            String doc = getTagValue(EugeneCoreTagValues.Store.documentation.getName());
            if (StringUtils.isNotEmpty(doc)) {
                documentation = doc;
            }
        }
        return documentation;
    }

    @Override
    public String getDescription() {
        return getDocumentation().substring(0, getDocumentation().indexOf("--"));
    }

    @Override
    public String getSourceDocumentation() {
        return getDocumentation().substring(getDocumentation().indexOf("--") + 2);
    }

    @Override
    public Set<String> getStereotypes() {
        Set<String> result = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : tagValues.entrySet()) {
            if ("true".equals(entry.getValue())) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    @Override
    public boolean hasStereotype(String stereotypeName) {
        return tagValues.containsKey(stereotypeName);
    }

    @Override
    public void addStereotype(String stereotype) {
        tagValues.put(stereotype, "true");
    }

    @Override
    public void removeStereotype(String stereotype) {
        tagValues.remove(stereotype);
    }

    @Override
    public Map<String, String> getTagValues() {
        return tagValues;
    }

    @Override
    public String getTagValue(String tagValue) {
        return tagValue == null ? null : tagValues.get(tagValue);
    }

    @Override
    public boolean hasTagValue(String tagValue) {
        return tagValues.containsKey(tagValue);
    }

    @Override
    public void removeTagValue(String tagvalue) {
        tagValues.remove(tagvalue);
    }

    @Override
    public void addTagValue(String tagValue, String value) {
        String oldValue = getTagValue(tagValue);
        if (StringUtils.isNotEmpty(oldValue)) {
            if (oldValue.equals(value)) {
                // same tag value do not replace it
                return;
            }
            log.warn("Replace tagValue '" + tagValue + "' (old:" + oldValue + ", new: " + value + ")");
        }
        tagValues.put(tagValue, value);
    }

    @Override
    public boolean isStatic() {
        return modifiers.contains(ObjectModelJavaModifier.STATIC);
    }

    @Override
    public List<String> getComments() {
        return comments;
    }

}
