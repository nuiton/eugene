/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;

import java.util.Iterator;
import java.util.Set;

/**
 * ObjectModelInterfaceImpl.java
 *
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public class ObjectModelInterfaceImpl extends ObjectModelClassifierImpl implements ObjectModelInterface {

    private static Set<ObjectModelModifier> authorizedModifiers;

    @Override
    protected Set<ObjectModelModifier> getAuthorizedModifiers() {
        if (authorizedModifiers == null) {
            // http://docs.oracle.com/javase/specs/jls/se7/html/jls-9.html#jls-9.1.1
            // public protected private abstract static strictfp
            Set<ObjectModelModifier> modifiers = Sets.newHashSet(
                    (ObjectModelModifier) ObjectModelJavaModifier.ABSTRACT, // Force cast because of generics limitation
                    ObjectModelJavaModifier.STATIC,
                    ObjectModelJavaModifier.STRICTFP);
            modifiers.addAll(ObjectModelJavaModifier.visibilityModifiers);
            authorizedModifiers = ImmutableSet.copyOf(modifiers);
        }
        return authorizedModifiers;
    }

    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("interface ").append(getQualifiedName()).append(" ");
        result.append("extends ");
        for (Iterator<?> i = getInterfaces().iterator(); i.hasNext(); ) {
            result.append(((ObjectModelClassifier) i.next()).getName());
            if (i.hasNext()) {
                result.append(", ");
            }
        }
        return result.toString();
    }
}
