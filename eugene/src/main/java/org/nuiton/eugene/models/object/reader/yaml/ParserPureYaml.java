package org.nuiton.eugene.models.object.reader.yaml;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;

/**
 * User: agiraudet
 * Date: 11/06/13
 * Time: 10:25
 */
public class ParserPureYaml {

    //charge recursivement le modelYAML dans le modelYAMLO
    public static void parseModel(Object modelYAML, YamlObject modelYAMLO) {
        if (modelYAML instanceof List) {
            for (Object tmp1 : (List) modelYAML) {
                parseModel(tmp1, modelYAMLO);
            }
        } else if (modelYAML instanceof Map) {
            for (Object tmp1 : ((Map) modelYAML).entrySet()) {
                if (((Map.Entry) tmp1).getValue() instanceof List || ((Map.Entry) tmp1).getValue() instanceof Map) {
                    YamlObject tmp2 = new YamlObject();
                    parseModel(((Map.Entry) tmp1).getValue(), tmp2);
                    modelYAMLO.addYamlObjectToMap(String.valueOf(((Map.Entry) tmp1).getKey()).toLowerCase(), tmp2);
                } else {
                    modelYAMLO.addStringToMap(String.valueOf(((Map.Entry) tmp1).getKey()).toLowerCase(), String.valueOf(((Map.Entry) tmp1).getValue()));
                }
            }
        }
    }
}
