/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.validator;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Validateur de modèle.
 * Cette classe faite pour être surchargée parcours le modèle et appelle
 * différentes méthodes pour tester la validité de ses composants. Chacune de
 * ces méthodes renvoi "true" si la partie du modèle analysée est valide et
 * "false" dans le cas contraire. Lorsqu'une erreur est trouvée, elle est
 * ajoutée au validateur par le biais de la méthode "addError".
 * Puis l'objet faisant appel au validateur peut récupérer la liste des erreurs
 * par la méthode "getErrors".
 * </p>
 * Created: 7 mars 2006
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class ObjectModelValidator {

    protected ObjectModel model;

    private List<String> errors;

    public ObjectModelValidator(ObjectModel model) {
        this.model = model;
        errors = new ArrayList<>();
    }

    /**
     * Renvoie la liste des erreurs constatées pdt la validation. Si aucune
     * erreur n'a été constatée ou si la validation n'a pas été effectuée, la
     * liste renvoyeé sera vide.
     *
     * @return list of errors found while validation.
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * Valide le modèle et renvoie faux si il n'est pas valide
     *
     * @return {@code true} if model is valid, {@code false} otherwise.
     */
    public boolean validate() {
        if (errors.size() > 0) {
            //On recréé une nouvelle liste
            errors = new ArrayList<>();
        }
        boolean isValid = validateModel(model);
        for (ObjectModelClassifier classifier : model.getClassifiers()) {
            isValid &= validateClassifier(classifier);
            if (classifier instanceof ObjectModelInterface) {
                isValid &= validateInterface((ObjectModelInterface) classifier);
            }
            if (classifier instanceof ObjectModelClass) {
                ObjectModelClass clazz = (ObjectModelClass) classifier;
                isValid &= validateClass(clazz);
                for (ObjectModelAttribute objectModelAttribute : clazz.getAttributes()) {
                    isValid &= validateAttribute(objectModelAttribute);
                }
            }
            for (ObjectModelOperation objectModelOperation : classifier.getOperations()) {
                isValid &= validateOperation(objectModelOperation);
            }
        }
        return isValid;
    }

    protected boolean validateModel(ObjectModel model) {

        // check model has a name
        String modelName = model.getName();
        if (StringUtils.isBlank(modelName)) {

            addError(model, "model has no name");
        }
        return true;
    }

    protected boolean validateClassifier(ObjectModelClassifier classifier) {
        return true;
    }

    protected boolean validateInterface(ObjectModelInterface interfacezz) {
        return true;
    }

    protected boolean validateClass(ObjectModelClass clazz) {
        return true;
    }

    protected boolean validateAttribute(ObjectModelAttribute attr) {
        return true;
    }

    protected boolean validateOperation(ObjectModelOperation operation) {
        return true;
    }

    protected void addError(Object onElement, String reason) {
        String elementString = onElement.toString();
        if (onElement instanceof ObjectModelAttribute) {
            ObjectModelAttribute attribute = (ObjectModelAttribute) onElement;
            elementString = ((ObjectModelClassifier) attribute
                    .getDeclaringElement()).getQualifiedName()
                            + "#" + attribute.getName();
        } else if (onElement instanceof ObjectModelClassifier) {
            ObjectModelClassifier classifier = (ObjectModelClassifier) onElement;
            elementString = classifier.getQualifiedName();
        }
        errors.add("[" + elementString + "] " + reason);
    }

} //ObjectModelValidator
