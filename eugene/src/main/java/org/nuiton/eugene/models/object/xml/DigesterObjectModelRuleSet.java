/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.RuleSetBase;

/**
 * DigesterObjectModelRuleSet
 *
 * Definit principalement :
 * - la classe d'implementation a utiliser pour chaque noeud
 * - la methode a appeler apres chaque noeud
 *
 * Ce jeu de regle ne cree pas l'element racine.
 * Il doit etre cree et ajoute a la pile digester avant l'appel a
 * <tt>Digester.parse(File)</tt>.
 *
 * Exemple:
 * <code>
 * ObjectModel monModel = new ObjectModelImpl()
 * Digester d = new Digester();
 * d.push(monModel);
 * d.parse(file);
 * </code>
 *
 * @author chatellier
 */
public class DigesterObjectModelRuleSet extends RuleSetBase {

    protected String prefix;

    public DigesterObjectModelRuleSet() {
        this("");
    }

    public DigesterObjectModelRuleSet(String prefix) {
        super("http://nuiton.org/eugene/objectModel/v1");
        this.prefix = prefix;
    }

    @Override
    public void addRuleInstances(Digester d) {

        //d.addFactoryCreate("objectModel", DigesterObjectModelFactory.class);
        // root element must be present on stask
        d.addSetProperties("objectModel");

        d.addObjectCreate("objectModel/package", ObjectModelPackageImpl.class);
        d.addSetProperties("objectModel/package");
        d.addSetNext("objectModel/package", "addPackage");

        d.addObjectCreate("objectModel/class", ObjectModelClassImpl.class);
        d.addSetProperties("objectModel/class");
        d.addSetNext("objectModel/class", "addClass");

        d.addObjectCreate("objectModel/class/class", ObjectModelClassImpl.class);
        d.addSetProperties("objectModel/class/class");
        d.addSetNext("objectModel/class/class", "addInnerClassifier");

        d.addObjectCreate("objectModel/interface", ObjectModelInterfaceImpl.class);
        d.addSetProperties("objectModel/interface");
        d.addSetNext("objectModel/interface", "addInterface");

        d.addObjectCreate("objectModel/enumeration", ObjectModelEnumerationImpl.class);
        d.addSetProperties("objectModel/enumeration");
        d.addSetNext("objectModel/enumeration", "addEnumeration");

        d.addObjectCreate("objectModel/enumeration/literal", ObjectModelImplRef.class);
        d.addSetProperties("objectModel/enumeration/literal");
        d.addSetNext("objectModel/enumeration/literal", "addLiteral");

        d.addObjectCreate("objectModel/associationClass", ObjectModelAssociationClassImpl.class);
        d.addSetProperties("objectModel/associationClass");
        d.addSetNext("objectModel/associationClass", "addAssociationClass");

        d.addObjectCreate("*/participant", ObjectModeImplAssociationClassParticipant.class);
        d.addSetProperties("*/participant");
        d.addSetNext("*/participant", "addParticipant");

        d.addObjectCreate("*/stereotype", ObjectModelImplRef.class);
        d.addSetProperties("*/stereotype");
        d.addSetNext("*/stereotype", "addStereotype");

        d.addObjectCreate("*/dependency", ObjectModelDependencyImpl.class);
        d.addSetProperties("*/dependency");
        d.addSetNext("*/dependency", "addDependency");

        d.addObjectCreate("*/attribute", ObjectModelAttributeImpl.class);
        d.addSetProperties("*/attribute");
        d.addSetNext("*/attribute", "addAttribute");

        d.addObjectCreate("*/interface", ObjectModelImplRef.class);
        d.addSetProperties("*/interface");
        d.addSetNext("*/interface", "addInterface");

        d.addObjectCreate("*/superclass", ObjectModelImplSuperClassRef.class);
        d.addSetProperties("*/superclass");
        d.addSetNext("*/superclass", "addSuperclass");

        d.addObjectCreate("*/operation", ObjectModelOperationImpl.class);
        d.addSetProperties("*/operation");
        d.addSetNext("*/operation", "addOperation");

        d.addObjectCreate("*/operation/returnParameter", ObjectModelParameterImpl.class);
        d.addSetProperties("*/operation/returnParameter");
        d.addSetNext("*/operation/returnParameter", "setReturnParameter");

        d.addObjectCreate("*/operation/parameter", ObjectModelParameterImpl.class);
        d.addSetProperties("*/operation/parameter");
        d.addSetNext("*/operation/parameter", "addParameter");

        d.addObjectCreate("*/operation/exceptionParameter", ObjectModelParameterImpl.class);
        d.addSetProperties("*/operation/exceptionParameter");
        d.addSetNext("*/operation/exceptionParameter", "addExceptionParameter");

        d.addObjectCreate("*/tagValue", ObjectModelImplTagValue.class);
        d.addSetProperties("*/tagValue");
        d.addSetNext("*/tagValue", "addTagValue");

        d.addObjectCreate("*/comment", String.class);
        d.addSetProperties("*/comment");
        d.addSetNext("*/comment", "addComment");
    }
}
