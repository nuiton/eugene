/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Eugene java extension package : all specific extension for Java generation.
 * <ul>
 * <li>Annotations extension : to add some annotation on elements of a object model.</li>
 * <li>Imports extension : to deal smartly with imports of code to generate.</li>
 * <li>Constants extension : to deal with constants in java generators.</li>
 * <li>codes extension : to add smartly some codes to operations.</li>
 * </ul>
 *
 * @see AnnotationsManager
 * @see ImportsManager
 * @see ConstantsManager
 * @see CodesManager
 */
package org.nuiton.eugene.java.extension;

