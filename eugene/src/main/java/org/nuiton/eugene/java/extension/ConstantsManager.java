/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java.extension;

import org.nuiton.eugene.GeneratorUtil;

import java.util.Map;
import java.util.TreeMap;

/**
 * Manager of constant names.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since ?
 */
public class ConstantsManager {

    /**
     * cache of constant name (values) for property name (keys)
     */
    protected Map<String, String> nameToConstant;

    /**
     * Obtain a constant nmae from a property name and store it in cache
     * the first time it had to build it.
     *
     * @param propertyName the propertyName to convert
     * @return the equivalent constant name
     */
    public String getConstantName(String propertyName) {
        Map<String, String> map = getNameToConstant();
        if (map.containsKey(propertyName)) {
            return map.get(propertyName);
        }
        // convert propertyName to constant name
        String constantName =
                GeneratorUtil.convertVariableNameToConstantName(propertyName);
        map.put(propertyName, constantName);
        return constantName;
    }

    protected Map<String, String> getNameToConstant() {
        if (nameToConstant == null) {
            nameToConstant = new TreeMap<>();
        }
        return nameToConstant;
    }
}
