package org.nuiton.eugene.java.extension;

/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Define a annotation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.4
 */
public class ObjectModelAnnotation {

    protected final String type;

    protected List<ObjectModelAnnotationParameter> parameters;

    public ObjectModelAnnotation(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public List<ObjectModelAnnotationParameter> getParameters() {
        return parameters;
    }

    public void addParameter(ObjectModelAnnotationParameter parameter) {
        if (parameters == null) {
            parameters = new ArrayList<>();
        }
        parameters.add(parameter);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ObjectModelAnnotation)) return false;

        ObjectModelAnnotation that = (ObjectModelAnnotation) o;

        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }
}
