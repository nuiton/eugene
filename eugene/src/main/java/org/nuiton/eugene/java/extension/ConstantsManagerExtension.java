/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java.extension;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3
 */
public class ConstantsManagerExtension {

    /**
     * Extension static used to identify {@code ConstantsManagerExtension} in
     * ObjectModel
     */
    public static final String OBJECTMODEL_EXTENSION = "constants";

    /**
     * the unique manager to use
     */
    protected ConstantsManager manager = new ConstantsManager();

    /**
     * Obtain a constant nmae from a property name and store it in cache
     * the first time it had to build it.
     *
     * @param propertyName the propertyName to convert
     * @return the equivalent constant name
     */
    public String getConstantName(String propertyName) {
        return manager.getConstantName(propertyName);
    }
}
