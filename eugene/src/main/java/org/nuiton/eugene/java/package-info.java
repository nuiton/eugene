/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Eugene java package : all specific class for Java generation.
 * <pre>
 * - ObjectModel Extensions : which are in package extension
 * - Builder : {@link org.nuiton.eugene.java.JavaBuilder} used to fill an ObjectModel in a Java way
 * - Transformer : {@link org.nuiton.eugene.java.ObjectModelTransformerToJava} used to transform an xmi ObjectModel to a java one
 * - Generator : {@link org.nuiton.eugene.java.JavaGenerator} is a template used to write java files (processed by Nuiton-processor)
 * </pre>
 */
package org.nuiton.eugene.java;

