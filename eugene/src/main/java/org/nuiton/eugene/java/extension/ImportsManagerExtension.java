/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java.extension;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.models.object.ObjectModelClassifier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Extension for ObjectModel to manage imports for all classifiers in the model.
 *
 * Created: 2 nov. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ImportsManagerExtension {

    /**
     * Logger.
     */
    private static final Log log =
            LogFactory.getLog(ImportsManagerExtension.class);

    /** Extension static used to identify ImportsManagerExtension in ObjectModel */
    public static final String OBJECTMODEL_EXTENSION = "imports";

    /**
     * Map of ImportsManager with key equals to the classifier qualified name
     * associated to the ImportsManager.
     */
    protected Map<String, ImportsManager> managers;

    /**
     * Get the ImportsManager associated to the classifier.
     *
     * <strong>Note:</strong> if not exist, it will be created.
     *
     * @param classifier reference for the ImportsManager
     * @return the importsManager associated to the classifier (never null)
     */
    public ImportsManager getManager(ObjectModelClassifier classifier) {
        String fqn = classifier.getQualifiedName();
        ImportsManager manager = getManager(fqn);
        return manager;
    }

    /**
     * Get the ImportsManager associated to the given {@code fqn}.
     *
     * <strong>Note:</strong> if not exist, it will be created.
     *
     * @param fqn reference for the ImportsManager
     * @return the importsManager associated to the classifier (never null)
     * @since 2.3.2
     */
    public ImportsManager getManager(String fqn) {
        Map<String, ImportsManager> managers = getManagers();
        ImportsManager manager = managers.get(fqn);
        if (manager == null) {
            manager = new ImportsManager();
            managers.put(fqn, manager);
            if (log.isDebugEnabled()) {
                log.debug("Add new importsManager for : " + fqn);
            }
        }
        return manager;
    }

    /**
     * Get imports for a classifier. The ImportsManager must be defined in the model.
     *
     * @param classifier reference for the imports
     * @return a List of String which contains all imports for the classifier
     */
    public List<String> getImports(ObjectModelClassifier classifier) {
        List<String> imports = getImports(classifier.getQualifiedName(),
                                          classifier.getPackageName()
        );
        return imports;
    }

    /**
     * Get imports for a classifier.
     *
     * The ImportsManager must be defined in the model.
     *
     * @param fqn         reference for the imports
     * @param packageName package name of the fqn
     * @return a List of String which contains all imports for the classifier
     * @since 2.3.2
     */
    public List<String> getImports(String fqn, String packageName) {
        ImportsManager manager = getManager(fqn);
        return manager.getImports(packageName);
    }

    protected Map<String, ImportsManager> getManagers() {
        if (managers == null) {
            managers = new HashMap<>();
        }
        return managers;
    }
}
