/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.java.extension;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.HashMap;
import java.util.Map;

/**
 * Object model extensions to manage verbatim code to attzach to operations.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see CodesManager
 * @since 2.0.2
 */
public class CodesManagerExtension {

    private static final Log log = LogFactory.getLog(CodesManagerExtension.class);

    /** Extension static used to identify CodesManagerExtension in ObjectModel */
    public static final String OBJECTMODEL_EXTENSION = "codes";

    /**
     * Map of CodesManager with key equals to the classifier qualified name
     * associated to the CodesManager
     */
    protected Map<String, CodesManager> managers;

    /**
     * Add the {@code code} for the given {@code operation} of the
     * given {@code classifier}.
     *
     * @param classifier the classifier container of the operation
     * @param operation  the operation on which to add the code
     * @param code       the code to add for the operation
     */
    public void addcode(ObjectModelClassifier classifier,
                        ObjectModelOperation operation,
                        String code) {
        CodesManager codesManager = getManager(classifier);
        codesManager.addCode(operation, code);
    }

    /**
     * Get body code for a operation of the given classifier.
     *
     * The CodesManager must be defined in the model.
     *
     * @param classifier reference for the codes
     * @param operation  the operation to seek
     * @return the body code of the method
     */
    public String getCode(ObjectModelClassifier classifier,
                          ObjectModelOperation operation) {
        CodesManager manager = getManager(classifier);
        return manager.getCode(operation);
    }

    /**
     * Get the CodesManager associated to the classifier.
     *
     * <b>Note:</b> If not exist, it will be created.
     *
     * @param classifier reference for the ImportsManager
     * @return the codesManager associated to the classifier (never null)
     */
    public CodesManager getManager(ObjectModelClassifier classifier) {
        Map<String, CodesManager> managers = getManagers();
        String fqn = classifier.getQualifiedName();
        CodesManager manager = managers.get(fqn);
        if (manager == null) {
            manager = new CodesManager();
            managers.put(fqn, manager);
            if (log.isDebugEnabled()) {
                log.debug("Add new codesManager for : " + fqn);
            }
        }
        return manager;
    }

    protected Map<String, CodesManager> getManagers() {
        if (managers == null) {
            managers = new HashMap<>();
        }
        return managers;
    }
}
