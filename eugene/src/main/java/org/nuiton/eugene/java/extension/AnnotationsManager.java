/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java.extension;

import org.nuiton.eugene.models.object.ObjectModelElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * To manage annotations for any {@link ObjectModelElement} of a classifier.
 *
 * Created: 17 déc. 2009
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.0
 */
public class AnnotationsManager {

//    private static final String[] EMPTY_STRING_ARRAY = new String[]{};

    protected Map<ObjectModelElement, List<ObjectModelAnnotation>> annotations;

    /**
     * Add the {@code annotation} for the given {@code element} of
     * the classifier.
     *
     * @param element    the element where to register the annotation
     * @param annotation the annotation to register
     */
    public void addAnnotation(ObjectModelElement element,
                              ObjectModelAnnotation annotation) {
        Map<ObjectModelElement, List<ObjectModelAnnotation>> map = getAnnotations();
        List<ObjectModelAnnotation> list = map.get(element);
        if (list == null) {
            list = new ArrayList<>();
            map.put(element, list);
        }
        list.add(annotation);
    }

    /**
     * Obtain the array of annotations registred for a given element of
     * the classifier.
     *
     * @param element the element where to search for annotations
     * @return the annotations for the element (empty arry if none found
     */
    public List<ObjectModelAnnotation> getAnnotations(ObjectModelElement element) {
        Map<ObjectModelElement, List<ObjectModelAnnotation>> map = getAnnotations();
        List<ObjectModelAnnotation> list = map.get(element);
        return list == null ? Collections.<ObjectModelAnnotation>emptyList() : list;
    }

    protected Map<ObjectModelElement, List<ObjectModelAnnotation>> getAnnotations() {
        if (annotations == null) {
            annotations = new HashMap<>();
        }
        return annotations;
    }

}
