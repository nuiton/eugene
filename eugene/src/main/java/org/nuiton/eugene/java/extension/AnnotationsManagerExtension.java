/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java.extension;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelElement;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created: 17 déc. 2009
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class AnnotationsManagerExtension {

    private static final Log log = LogFactory.getLog(AnnotationsManagerExtension.class);

    /**
     * Extension static used to identify AnnotationsManagerExtension in
     * ObjectModel.
     */
    public static final String OBJECTMODEL_EXTENSION = "annotations";

    /**
     * Map of AnotationsManager with key equals to the classifier qualified
     * name associated to the AnotationsManager
     */
    protected Map<String, AnnotationsManager> managers;

//    private static final String[] EMPTY_STRING_ARRAY = new String[]{};

    /**
     * Get the registred annotations for the given {@code element} in the
     * given {@code classifier}.
     *
     * <b>Note:</b> The method always returns a {@code none null} value, but
     * an empty array when no annotation when no annotation found for the
     * element.
     *
     * @param classifier the classifier where is the element
     * @param element    the element on which searching annotations
     * @return the list of annotations registred or an empty list if none.
     */
    public List<ObjectModelAnnotation> getAnnotations(ObjectModelClassifier classifier,
                                                      ObjectModelElement element) {
        AnnotationsManager annotationsManager = getManager(classifier);
        List<ObjectModelAnnotation> result = null;
        if (annotationsManager != null) {
            result = annotationsManager.getAnnotations(element);
        }
        return result == null ? Collections.<ObjectModelAnnotation>emptyList() : result;
    }

    /**
     * Get the AnotationsManager associated to the classifier. If not exist,
     * it will be created.
     *
     * @param classifier reference for the AnotationsManager
     * @return the annotationsManager associated to the classifier (never null)
     */
    public AnnotationsManager getManager(ObjectModelClassifier classifier) {
        Map<String, AnnotationsManager> managers = getManagers();
        String fqn = classifier.getQualifiedName();
        AnnotationsManager manager = managers.get(fqn);
        if (manager == null) {
            manager = new AnnotationsManager();
            managers.put(fqn, manager);
            if (log.isDebugEnabled()) {
                log.debug("Add new annotationsManager for : " + fqn);
            }
        }
        return manager;
    }

    protected Map<String, AnnotationsManager> getManagers() {
        if (managers == null) {
            managers = new HashMap<>();
        }
        return managers;
    }
}
