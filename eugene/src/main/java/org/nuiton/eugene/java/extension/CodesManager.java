/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.java.extension;

import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.HashMap;
import java.util.Map;

/**
 * To manage some verbatim code to inject in operations.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.2
 */
public class CodesManager {

    private static final String EMPTY_STRING = "";

    /** store of codes associated to operations */
    protected Map<ObjectModelOperation, StringBuilder> codes;

    /**
     * Add the {@code annotation} for the given {@code element} of
     * the classifier.
     *
     * @param operation the operation on which add the code
     * @param code      the code to add
     */
    public void addCode(ObjectModelOperation operation, String code) {
        Map<ObjectModelOperation, StringBuilder> map = getCodes();

        StringBuilder buffer = map.get(operation);
        if (buffer == null) {
            buffer = new StringBuilder();
            map.put(operation, buffer);
        }
        buffer.append('\n').append(code);
    }

    /**
     * Obtain the codes registred for a given operation of the classifier.
     *
     * @param operation the operation where to search for code
     * @return the code for the operation (empty if none found)
     */
    public String getCode(ObjectModelOperation operation) {
        Map<ObjectModelOperation, StringBuilder> map = getCodes();
        StringBuilder buffer = map.get(operation);
        return buffer == null ? EMPTY_STRING : buffer.toString();
    }

    protected Map<ObjectModelOperation, StringBuilder> getCodes() {
        if (codes == null) {
            codes = new HashMap<>();
        }
        return codes;
    }
}
