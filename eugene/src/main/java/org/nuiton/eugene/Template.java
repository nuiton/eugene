/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.writer.WriterReport;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Template. TODO javadoc
 *
 * Created: 5 août 2004
 *
 * @param <M> Model associated to the generator (input model)
 * @author Cédric Pineau - pineau@codelutin.com
 */
public abstract class Template<M extends Model> implements TemplateConfiguration {

    protected TemplateConfiguration configuration;

    protected List<String> excludeTemplates;

    /**
     * List of package to allow generation.
     *
     * If {@code null} or empty, generate all packages.
     */
    protected List<String> generatedPackages;

    /** Model */
    protected M model;

    protected ResourcesHelper resourcesHelper;

    public abstract void applyTemplate(M model, File destDir) throws IOException;

    public TemplateConfiguration getConfiguration() {
        if (configuration == null) {
            configuration = new DefaultTemplateConfiguration();
        }
        return configuration;
    }

    public void setConfiguration(TemplateConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * @return {@code true} if must overwrite ouput
     * @deprecated since 2.0.2, prefer use the {@link #isOverwrite()} method
     */
    @Deprecated
    public boolean getOverwrite() {
        return isOverwrite();
    }

    @Override
    public boolean isOverwrite() {
        return getConfiguration().isOverwrite();
    }

    @Override
    public boolean isVerbose() {
        return getConfiguration().isVerbose();
    }

    @Override
    public String getEncoding() {
        return getConfiguration().getEncoding();
    }

    @Override
    public ClassLoader getClassLoader() {
        return getConfiguration().getClassLoader();
    }

    @Override
    public long getLastModifiedSource() {
        return getConfiguration().getLastModifiedSource();
    }

    @Override
    public Properties getProperties() {
        return getConfiguration().getProperties();
    }

    @Override
    public <V> V getProperty(String key, Class<V> type) {
        return getConfiguration().getProperty(key, type);
    }

    public String getProperty(String key) {
        return getConfiguration().getProperty(key);
    }

    @Override
    public WriterReport getWriterReport() {
        return getConfiguration().getWriterReport();
    }

    @Override
    public void setProperty(String key, Object value) {
        getConfiguration().setProperty(key, value);
        if (PROP_GENERATED_PACKAGES.equals(key)) {
            // reset cache value
            generatedPackages = null;
        } else if (PROP_EXCLUDE_TEMPLATES.equals(key)) {
            // reset cache value
            excludeTemplates = null;
        }
    }

    protected List<String> getGeneratedPackages() {
        if (generatedPackages == null) {
            generatedPackages = new ArrayList<>();
            String genPackages = getProperty(PROP_GENERATED_PACKAGES);
            if (!StringUtils.isEmpty(genPackages)) {
                //TC-20091125 avoid manual array copy
                generatedPackages.addAll(Arrays.asList(genPackages.split(",")));
            }
        }
        return generatedPackages;
    }

    public List<String> getExcludeTemplates() {
        if (excludeTemplates == null) {
            excludeTemplates = new ArrayList<>();
            String excludes = getProperty(PROP_EXCLUDE_TEMPLATES);
            if (!StringUtils.isEmpty(excludes)) {
                //TC-20091125 avoid manual array copy
                excludeTemplates.addAll(Arrays.asList(excludes.split(",")));
            }
        }
        return excludeTemplates;
    }

    public M getModel() {
        return model;
    }

    protected ResourcesHelper getResourcesHelper() {
        if (resourcesHelper == null) {
            resourcesHelper = new ResourcesHelper(getClassLoader(), isVerbose());
        }
        return resourcesHelper;
    }
}
