/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene;

import org.nuiton.eugene.writer.WriterReport;

import java.util.Properties;

/**
 * Default template configuration
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.2
 */
public class DefaultTemplateConfiguration implements TemplateConfiguration {

    public static final String DEFAULT_ENCONDING = "UTF-8";

    protected Properties properties;

    public DefaultTemplateConfiguration() {
    }

    public DefaultTemplateConfiguration(Properties properties) {
        this.properties = new Properties();
        this.properties.putAll(properties);
    }

    @Override
    public boolean isOverwrite() {
        Boolean value = getProperty(PROP_OVERWRITE, Boolean.class);
        if (value == null) {
            value = true;
            setProperty(PROP_OVERWRITE, value);
        }
        return value;
    }

    @Override
    public boolean isVerbose() {
        Boolean value = getProperty(PROP_VERBOSE, Boolean.class);
        if (value == null) {
            value = false;
            setProperty(PROP_VERBOSE, value);
        }
        return value;
    }

    @Override
    public String getEncoding() {
        String value = getProperty(PROP_ENCODING, String.class);
        if (value == null) {
            value = DEFAULT_ENCONDING;
            setProperty(PROP_ENCODING, value);
        }
        return value;
    }

    @Override
    public ClassLoader getClassLoader() {
        ClassLoader value = getProperty(PROP_CLASS_LOADER, ClassLoader.class);
        if (value == null) {
            value = getClass().getClassLoader();
            setProperty(PROP_CLASS_LOADER, value);
        }
        return value;
    }

    @Override
    public WriterReport getWriterReport() {
        WriterReport report =
                getProperty(PROP_WRITER_REPORT, WriterReport.class);
        return report;
    }

    @Override
    public long getLastModifiedSource() {
        Long value = getProperty(PROP_LAST_MODIFIED_SOURCE, Long.class);
        if (value == null) {
            value = 0l;
            setProperty(PROP_LAST_MODIFIED_SOURCE, value);
        }
        return value;
    }

    @Override
    public Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
        }
        return properties;
    }

    @Override
    public String getProperty(String key) {
        return getProperty(key, String.class);
    }

    @Override
    public <V> V getProperty(String key, Class<V> type) {
        Object o = getProperties().get(key);
        if (o != null && !type.isAssignableFrom(type)) {
            throw new IllegalArgumentException("property [" + key + "] is not of type " + type.getName() + ", but : " + o.getClass().getName());
        }
        return (V) o;
    }

    @Override
    public void setProperty(String key, Object value) {
        getProperties().put(key, value);
    }

    public void setOverwrite(boolean overwrite) {
        setProperty(PROP_OVERWRITE, overwrite);
    }

    public void setVerbose(boolean verbose) {
        setProperty(PROP_VERBOSE, verbose);
    }

    public void setEncoding(String encoding) {
        setProperty(PROP_ENCODING, encoding);
    }

    public void setLoader(ClassLoader loader) {
        setProperty(PROP_CLASS_LOADER, loader);
    }

    public void setLastModifiedSource(long lastModifiedSource) {
        setProperty(PROP_LAST_MODIFIED_SOURCE, lastModifiedSource);
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

}
