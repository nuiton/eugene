/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene;

import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;

import java.io.File;
import java.io.IOException;

/**
 * ModelReader TODO real doc :)
 *
 * Created: 26 oct. 2009
 *
 * @param <M> Model to create from reading input files
 * @author Florian Desbois - desbois@codelutin.com
 */
public abstract class ModelReader<M extends Model> {

    /**
     * A verbose flag to see more things.
     *
     * @since 2.3
     */
    protected boolean verbose;

    /**
     * A flag to load only safe things.
     *
     * Usefull to have a safe loading of a model and his properties file.
     *
     * @since 2.3
     */
    protected boolean strictLoading;

    /**
     * Provider of tag values.
     *
     * @since 2.9
     */
    protected TagValueMetadatasProvider tagValueMetadatasProvider;
    
    /**
     * Gets the type of model.
     *
     * @return the type of model.
     * @see ModelHelper.ModelType
     * @since 2.6.3
     */
    public abstract String getModelType();

    /**
     * Gets the type of input file. For example {@code xml}, or {@code yaml}.
     *
     * @return the type of input file.
     * @since 2.6.3
     */
    public abstract String getInputType();

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public boolean isStrictLoading() {
        return strictLoading;
    }

    public void setStrictLoading(boolean strictLoading) {
        this.strictLoading = strictLoading;
    }

    /**
     * Read files to produce a memory model.
     *
     * @param file files to read
     * @return the new model builded from files
     * @throws IOException if any IO errors while reading files
     */
    public abstract M read(File... file) throws IOException;

}
