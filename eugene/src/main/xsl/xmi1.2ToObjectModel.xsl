<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  EUGene :: EUGene
  %%
  Copyright (C) 2004 - 2010 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xalan="http://xml.apache.org/xslt"
                xmlns:extensions="http://www.codelutin.com/XSLTExtensions"
                xmlns:redirect="http://xml.apache.org/xalan/redirect"
                extension-element-prefixes="extensions redirect"
                xmlns="http://nuiton.org/eugene/objectModel/v1"
                xmlns:packageValidator="xalan://org.nuiton.eugene.PackageValidator"
                xmlns:UML="org.omg.xmi.namespace.UML"
                xmlns:UML2="org.omg.xmi.namespace.UML2">

  <xsl:output method="xml"
              encoding="UTF-8"
              indent="yes"
              xalan:indent-amount="2"/>


  <!-- processing entry point. We'll process everything from that root package path. Everything else is ignored -->
  <xsl:param name="fullPackagePath"/>
  <xsl:param name="extraPackages"/>

  <!-- 
  .
  .
  . Matching templates 
  .
  .
  -->

  <xsl:template match="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement">
    <xsl:element name="objectModel">
      <xsl:attribute name="xsi:schemaLocation">http://nuiton.org/eugene/objectModel/v1 http://doc.nuiton.org/eugene/xsd/v1/objectmodel.xsd</xsl:attribute>
      <xsl:attribute name="name">
        <xsl:value-of select="../@name"/>
      </xsl:attribute>

      <!-- ajout de l'attribut version disponible dans les taggedValue -->
	  <xsl:attribute name="version" xsi:nill="">
		<xsl:variable name="versionTagId">
          <xsl:value-of select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/UML:TagDefinition[@name='version']/@xmi.id"/>
        </xsl:variable>
        <xsl:value-of select="/XMI/XMI.content/UML:Model/UML:ModelElement.taggedValue/UML:TaggedValue[UML:TaggedValue.type/UML:TagDefinition/@xmi.idref = $versionTagId]/UML:TaggedValue.dataValue"/>
      </xsl:attribute>

      <xsl:for-each select="descendant::UML:Namespace.ownedElement/UML:Comment[not(UML:Comment.annotatedElement)]">
        <xsl:call-template name="comment"/>
      </xsl:for-each>

      <xsl:apply-templates>
        <xsl:with-param name="parentLocalPackageName"/>
      </xsl:apply-templates>
    </xsl:element>
  </xsl:template>

  <xsl:template match="text()|attribute"/>

  <xsl:template match="*">
    <xsl:param name="parentLocalPackageName"/>
    <xsl:apply-templates>
      <xsl:with-param name="parentLocalPackageName">
        <xsl:value-of select="$parentLocalPackageName"/>
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="UML:Package">
    <xsl:param name="parentLocalPackageName"/>
    <xsl:variable name="packageName">
      <xsl:value-of select="@name"/>
    </xsl:variable>

    <xsl:variable name="localPackageName">
      <xsl:value-of select="$parentLocalPackageName"/>
      <xsl:value-of select="$packageName"/>
    </xsl:variable>

    <xsl:variable name="localPackageNameDot">
      <xsl:value-of select="concat($localPackageName,'.')"/>
    </xsl:variable>

    <xsl:call-template name="UMLPackage">
      <xsl:with-param name="localPackageName">
        <xsl:value-of select="$localPackageName"/>
      </xsl:with-param>
    </xsl:call-template>

    <xsl:choose>
      <xsl:when test="packageValidator:toContinue($fullPackagePath, $localPackageNameDot, $extraPackages)">
        <xsl:apply-templates>
          <xsl:with-param name="parentLocalPackageName">
            <xsl:value-of select="$localPackageNameDot"/>
          </xsl:with-param>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:when test="packageValidator:isValid($fullPackagePath, $localPackageName, $extraPackages)">
        <xsl:for-each select="UML:Namespace.ownedElement/UML:Class">
          <xsl:call-template name="UMLClass">
            <xsl:with-param name="localPackageName">
              <xsl:value-of select="$localPackageName"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="UML:Namespace.ownedElement/UML:Interface">
          <xsl:call-template name="UMLInterface">
            <xsl:with-param name="localPackageName">
              <xsl:value-of select="$localPackageName"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="UML:Namespace.ownedElement/UML:AssociationClass">
          <xsl:call-template name="UMLAssociationClass">
            <xsl:with-param name="localPackageName">
              <xsl:value-of select="$localPackageName"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="UML:Namespace.ownedElement/UML:Enumeration">
          <xsl:call-template name="UMLEnumeration">
            <xsl:with-param name="localPackageName">
              <xsl:value-of select="$localPackageName"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:apply-templates>
          <xsl:with-param name="parentLocalPackageName">
            <xsl:value-of select="$localPackageNameDot"/>
          </xsl:with-param>
        </xsl:apply-templates>
	</xsl:when>
    </xsl:choose>
  </xsl:template>



  <!-- 
  .
  .
  . Named templates 
  .
  .
  -->

  <xsl:template name="UMLPackage">
    <xsl:param name="localPackageName"/>
    <xsl:variable name="interfaceId">
      <xsl:value-of select="@xmi.id"/>
    </xsl:variable>

    <xsl:element name="package">

      <!-- interface properties -->
      <xsl:attribute name="name">
        <xsl:value-of select="$localPackageName"/>
      </xsl:attribute>

      <xsl:call-template name="extern"/>

      <xsl:call-template name="stereotype"/>

      <xsl:for-each select="UML:ModelElement.taggedValue/UML:TaggedValue">
        <xsl:call-template name="taggedValue"/>
      </xsl:for-each>

      <xsl:for-each select="UML:ModelElement.comment/UML:Comment">
        <xsl:call-template name="UMLComment"/>
      </xsl:for-each>

    </xsl:element>

  </xsl:template>

  <xsl:template name="UMLInterface">
    <xsl:param name="localPackageName"/>
    <xsl:variable name="interfaceId">
      <xsl:value-of select="@xmi.id"/>
    </xsl:variable>

    <xsl:element name="interface">

      <!-- interface properties -->
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

      <xsl:call-template name="extern"/>

      <xsl:attribute name="package">
        <xsl:value-of select="$localPackageName"/>
      </xsl:attribute>

      <xsl:call-template name="dependency"/>

      <xsl:call-template name="stereotype"/>

      <xsl:for-each select="UML:ModelElement.taggedValue/UML:TaggedValue">
        <xsl:call-template name="taggedValue"/>
      </xsl:for-each>

      <xsl:call-template name="UMLGeneralizations">
        <xsl:with-param name="classId">
          <xsl:value-of select="$interfaceId"/>
        </xsl:with-param>
      </xsl:call-template>

      <xsl:for-each select="UML:ModelElement.comment/UML:Comment">
        <xsl:call-template name="UMLComment"/>
      </xsl:for-each>

      <!-- class operations -->
      <xsl:for-each select="UML:Classifier.feature/UML:Operation">
        <xsl:call-template name="UMLOperation"/>
      </xsl:for-each>

    </xsl:element>

  </xsl:template>



  <xsl:template name="UMLClass">
    <xsl:param name="localPackageName"/>
    <xsl:variable name="classId">
      <xsl:value-of select="@xmi.id"/>
    </xsl:variable>

    <xsl:element name="class">

      <!-- class properties -->
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

      <xsl:if test="@isAbstract='true'">
        <xsl:attribute name="abstract">
          <xsl:text>true</xsl:text>
        </xsl:attribute>
      </xsl:if>

      <xsl:call-template name="extern"/>

      <xsl:attribute name="package">
        <xsl:value-of select="$localPackageName"/>
      </xsl:attribute>

      <xsl:call-template name="dependency"/>

      <xsl:call-template name="stereotype"/>

      <xsl:for-each select="UML:ModelElement.taggedValue/UML:TaggedValue">
        <xsl:call-template name="taggedValue"/>
      </xsl:for-each>

      <xsl:call-template name="UMLGeneralizations">
        <xsl:with-param name="classId">
          <xsl:value-of select="$classId"/>
        </xsl:with-param>
      </xsl:call-template>

      <xsl:call-template name="UMLAbstractions">
        <xsl:with-param name="classId">
          <xsl:value-of select="$classId"/>
        </xsl:with-param>
      </xsl:call-template>

      <xsl:for-each select="UML:ModelElement.comment/UML:Comment">
        <xsl:call-template name="UMLComment"/>
      </xsl:for-each>

      <!-- class attributes -->
      <xsl:for-each select="UML:Classifier.feature/UML:Attribute">
        <xsl:call-template name="UMLAttribute"/>
      </xsl:for-each>

      <!-- class operations -->
      <xsl:for-each select="UML:Classifier.feature/UML:Operation">
        <xsl:call-template name="UMLOperation"/>
      </xsl:for-each>

      <!-- inner classes -->
      <xsl:for-each select="UML:Namespace.ownedElement/UML:Class">
        <xsl:call-template name="UMLClass">
            <xsl:with-param name="localPackageName">
              <xsl:value-of select="$localPackageName"/>
            </xsl:with-param>
        </xsl:call-template>
      </xsl:for-each>

      <xsl:call-template name="UMLAssociations">
        <xsl:with-param name="classId">
          <xsl:value-of select="$classId"/>
        </xsl:with-param>
      </xsl:call-template>

    </xsl:element>

  </xsl:template>



  <xsl:template name="UMLAssociationClass">
    <xsl:param name="localPackageName"/>
    <xsl:variable name="classId">
      <xsl:value-of select="@xmi.id"/>
    </xsl:variable>

    <xsl:element name="associationClass">

      <!-- class properties -->
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

      <xsl:if test="@isAbstract='true'">
        <xsl:attribute name="abstract">
          <xsl:text>true</xsl:text>
        </xsl:attribute>
      </xsl:if>

      <xsl:call-template name="extern"/>

      <xsl:attribute name="package">
        <xsl:value-of select="$localPackageName"/>
      </xsl:attribute>

      <xsl:call-template name="stereotype"/>

      <xsl:for-each select="UML:ModelElement.taggedValue/UML:TaggedValue">
        <xsl:call-template name="taggedValue"/>
      </xsl:for-each>

      <xsl:call-template name="UMLGeneralizations">
        <xsl:with-param name="classId">
          <xsl:value-of select="$classId"/>
        </xsl:with-param>
      </xsl:call-template>

      <xsl:call-template name="UMLAbstractions">
        <xsl:with-param name="classId">
          <xsl:value-of select="$classId"/>
        </xsl:with-param>
      </xsl:call-template>

      <xsl:for-each select="UML:ModelElement.comment/UML:Comment">
        <xsl:call-template name="UMLComment"/>
      </xsl:for-each>

      <xsl:for-each select="UML:Association.connection/UML:AssociationEnd">
        <xsl:variable name="participantId">
          <xsl:value-of select="UML:AssociationEnd.participant/*/@xmi.idref"/>
        </xsl:variable>
        <xsl:variable name="id">
          <xsl:value-of select="@xmi.id"/>
        </xsl:variable>
        <xsl:element name="participant">
          <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::*[@xmi.id = $participantId]">
            <xsl:attribute name="name">
              <xsl:call-template name="packagePathOfWithDot"/>
              <xsl:value-of select="@name"/>
            </xsl:attribute>
          </xsl:for-each>
          <xsl:for-each select="../UML:AssociationEnd[@xmi.id!=$id]">
            <xsl:attribute name="attribute">
	          <xsl:value-of select="@name"/>
            </xsl:attribute>
          </xsl:for-each>
        </xsl:element>
      </xsl:for-each>

      <!-- class attributes -->
      <xsl:for-each select="UML:Classifier.feature/UML:Attribute">
        <xsl:call-template name="UMLAttribute"/>
      </xsl:for-each>

      <!-- class operations -->
      <xsl:for-each select="UML:Classifier.feature/UML:Operation">
        <xsl:call-template name="UMLOperation"/>
      </xsl:for-each>

      <xsl:call-template name="UMLAssociations">
        <xsl:with-param name="classId">
          <xsl:value-of select="$classId"/>
        </xsl:with-param>
      </xsl:call-template>

    </xsl:element>

  </xsl:template>

  <xsl:template name="UMLEnumeration">
      <xsl:param name="localPackageName"/>
      <xsl:element name="enumeration">
           <xsl:attribute name="name">
              <xsl:value-of select="@name" />
          </xsl:attribute>

          <xsl:attribute name="package">
            <xsl:value-of select="$localPackageName"/>
          </xsl:attribute>

          <!-- literals -->
          <xsl:for-each select="UML:Enumeration.literal/UML:EnumerationLiteral">
            <xsl:element name="literal">
                <xsl:attribute name="name">
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
            </xsl:element>
          </xsl:for-each>

          <!-- operations -->
          <xsl:for-each select="UML:Classifier.feature/UML:Operation">
            <xsl:call-template name="UMLOperation"/>
          </xsl:for-each>

          <!-- stereotypes -->
          <xsl:call-template name="stereotype"/>
      </xsl:element>
  </xsl:template>

  <xsl:template name="UMLAbstractions">
    <xsl:param name="classId"/>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Abstraction[UML:Dependency.client/UML:Class/@xmi.idref=$classId]">
      <xsl:call-template name="UMLAbstraction"/>
    </xsl:for-each>
  </xsl:template>



  <xsl:template name="UMLAbstraction">
    <xsl:variable name="idInterface">
      <xsl:value-of select="UML:Dependency.supplier/UML:Interface/@xmi.idref"/>
    </xsl:variable>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::*[@xmi.id = $idInterface]">
      <xsl:element name="interface">
        <xsl:attribute name="name">
          <xsl:call-template name="packagePathOfWithDot"/>
          <xsl:value-of select="@name"/>
        </xsl:attribute>
      </xsl:element>
    </xsl:for-each>
    <!-- Cas où une classe hérite d'une classe abstraite -->
    <xsl:variable name="idClass">
      <xsl:value-of select="UML:Dependency.supplier/UML:Class/@xmi.idref"/>
    </xsl:variable>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::*[@xmi.id = $idClass]">
      <xsl:element name="superclass">
        <xsl:attribute name="name">
          <xsl:call-template name="packagePathOfWithDot"/>
          <xsl:value-of select="@name"/>
        </xsl:attribute>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>



  <xsl:template name="UMLGeneralizations">
    <xsl:param name="classId"/>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Generalization[UML:Generalization.child/UML:Class/@xmi.idref=$classId]">
      <xsl:call-template name="UMLClassGeneralization"/>
    </xsl:for-each>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Generalization[UML:Generalization.child/UML:AssociationClass/@xmi.idref=$classId]">
      <xsl:call-template name="UMLClassGeneralization"/>
    </xsl:for-each>
    <!-- Une généralisation peut aussi être une interface qui enrichit une autre -->
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Generalization[UML:Generalization.child/UML:Interface/@xmi.idref=$classId]">
      <xsl:call-template name="UMLInterfaceGeneralization"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="UMLClassGeneralization">
    <xsl:variable name="id">
      <xsl:value-of select="UML:Generalization.parent/UML:Class/@xmi.idref"/>
    </xsl:variable>
    <xsl:variable name="discriminator">
      <xsl:value-of select="@discriminator"/>
    </xsl:variable>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::*[@xmi.id = $id]">
      <xsl:element name="superclass">
        <xsl:attribute name="name">
          <xsl:call-template name="packagePathOfWithDot"/>
          <xsl:value-of select="@name"/>
        </xsl:attribute>
        <xsl:attribute name="discriminator">
          <xsl:value-of select="$discriminator"/>
        </xsl:attribute>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="UMLInterfaceGeneralization">
    <xsl:variable name="id">
      <xsl:value-of select="UML:Generalization.parent/UML:Interface/@xmi.idref"/>
    </xsl:variable>
    <xsl:variable name="discriminator">
      <xsl:value-of select="@discriminator"/>
    </xsl:variable>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::*[@xmi.id = $id]">
      <xsl:element name="interface">
        <xsl:attribute name="name">
          <xsl:call-template name="packagePathOfWithDot"/>
          <xsl:value-of select="@name"/>
        </xsl:attribute>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="UMLAssociations">
    <xsl:param name="classId"/>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Association.connection/UML:AssociationEnd[UML:AssociationEnd.participant/*/@xmi.idref=$classId]">
      <xsl:variable name="id">
        <xsl:value-of select="@xmi.id"/>
      </xsl:variable>
      <xsl:for-each select="../UML:AssociationEnd[@xmi.id!=$id]">
        <xsl:call-template name="UMLAssociation"/>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>



  <xsl:template name="UMLAssociation">
<!-- BB    <xsl:if test="@isNavigable='true'"> -->
      <xsl:element name="attribute">
        <xsl:if test="@name!=''">
          <xsl:attribute name="name">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name="visibility">
          <xsl:value-of select="@visibility"/>
        </xsl:attribute>

        <xsl:variable name="asClassId">
          <xsl:value-of select="ancestor::UML:AssociationClass/@xmi.id"/>
        </xsl:variable>

        <xsl:if test="$asClassId">
          <xsl:for-each select="ancestor::UML:AssociationClass">
            <xsl:attribute name="associationClassName">
              <xsl:call-template name="packagePathOfWithDot"/>
              <xsl:value-of select="@name"/>
            </xsl:attribute>
          </xsl:for-each>
        </xsl:if>

        <xsl:variable name="idParticipant">
          <xsl:value-of select="UML:AssociationEnd.participant/*/@xmi.idref"/>
        </xsl:variable>
        <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::*[@xmi.id = $idParticipant]">
          <xsl:attribute name="type">
            <xsl:call-template name="packagePathOfWithDot"/>
            <xsl:value-of select="@name"/>
          </xsl:attribute>
        </xsl:for-each>

        <xsl:variable name="id">
          <xsl:value-of select="@xmi.id"/>
        </xsl:variable>
        <xsl:for-each select="../UML:AssociationEnd[@xmi.id!=$id]">
          <xsl:attribute name="reverseAttributeName">
            <xsl:value-of select="@name"/>
          </xsl:attribute>
          <xsl:if test="@aggregation='aggregate' or @aggregation='composite'">
            <xsl:attribute name="associationType">
              <xsl:value-of select="@aggregation"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:variable name="reverseMaxMultiplicity">
            <xsl:value-of select="UML:AssociationEnd.multiplicity/UML:Multiplicity/UML:Multiplicity.range/UML:MultiplicityRange/@upper"/>
          </xsl:variable>
          <xsl:if test="$reverseMaxMultiplicity!=''">
            <xsl:attribute name="reverseMaxMultiplicity">
              <xsl:value-of select="$reverseMaxMultiplicity"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:for-each>

        <xsl:if test="UML:AssociationEnd.multiplicity">
          <xsl:variable name="minMultiplicity">
            <xsl:value-of select="UML:AssociationEnd.multiplicity/UML:Multiplicity/UML:Multiplicity.range/UML:MultiplicityRange/@lower"/>
          </xsl:variable>
          <xsl:if test="$minMultiplicity!=''">
            <xsl:attribute name="minMultiplicity">
              <xsl:value-of select="$minMultiplicity"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:variable name="maxMultiplicity">
            <xsl:value-of select="UML:AssociationEnd.multiplicity/UML:Multiplicity/UML:Multiplicity.range/UML:MultiplicityRange/@upper"/>
          </xsl:variable>
          <xsl:if test="$maxMultiplicity!=''">
            <xsl:attribute name="maxMultiplicity">
              <xsl:value-of select="$maxMultiplicity"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:if>

        <xsl:attribute name="navigable">
          <xsl:value-of select="@isNavigable"/>
        </xsl:attribute>
        <xsl:attribute name="ordering">
          <xsl:value-of select="@ordering"/>
        </xsl:attribute>

        <xsl:for-each select="UML:ModelElement.taggedValue/UML:TaggedValue">
          <xsl:call-template name="taggedValue"/>
        </xsl:for-each>

        <xsl:call-template name="stereotype"/>

      </xsl:element>
<!-- BB    </xsl:if> -->
  </xsl:template>



  <xsl:template name="UMLAttribute">

    <xsl:element name="attribute">

      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

	  <!-- BB: attribute entity must have associationType, we put 'composite'
	       because composite is already defined for attribute association, but
	       perhaps other value like internal is better. If we change that
	       we must change template too -->
      <xsl:attribute name="associationType">
        <xsl:text>composite</xsl:text>
      </xsl:attribute>

      <xsl:attribute name="visibility">
        <xsl:value-of select="@visibility"/>
      </xsl:attribute>

      <xsl:if test="@ownerScope='classifier'">
        <xsl:attribute name="static">
          <xsl:text>true</xsl:text>
        </xsl:attribute>
      </xsl:if>

      <xsl:if test="@changeability='frozen'">
        <xsl:attribute name="final">
          <xsl:text>true</xsl:text>
        </xsl:attribute>
      </xsl:if>

	  <xsl:attribute name="type">
		  <xsl:call-template name="fullClassName">
		  	<xsl:with-param name="childpath">UML:StructuralFeature.type</xsl:with-param>
		  </xsl:call-template>
	  </xsl:attribute>

      <xsl:if test="UML:StructuralFeature.multiplicity">
        <xsl:variable name="minMultiplicity">
          <xsl:value-of select="UML:StructuralFeature.multiplicity/UML:Multiplicity/UML:Multiplicity.range/UML:MultiplicityRange/@lower"/>
        </xsl:variable>
        <xsl:if test="$minMultiplicity!=''">
          <xsl:attribute name="minMultiplicity">
            <xsl:value-of select="$minMultiplicity"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:variable name="maxMultiplicity">
          <xsl:value-of select="UML:StructuralFeature.multiplicity/UML:Multiplicity/UML:Multiplicity.range/UML:MultiplicityRange/@upper"/>
        </xsl:variable>
        <xsl:if test="$maxMultiplicity!=''">
          <xsl:attribute name="maxMultiplicity">
            <xsl:value-of select="$maxMultiplicity"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:if>

      <xsl:for-each select="UML:ModelElement.taggedValue/UML:TaggedValue">
        <xsl:call-template name="taggedValue"/>
      </xsl:for-each>

      <xsl:for-each select="UML:ModelElement.comment/UML:Comment">
        <xsl:call-template name="UMLComment"/>
      </xsl:for-each>

      <!-- default value of attribute -->
      <xsl:if test="UML:Attribute.initialValue">
        <xsl:attribute  name="defaultValue">
          <xsl:value-of select="UML:Attribute.initialValue/UML:Expression/@body"/>
        </xsl:attribute>
      </xsl:if>

      <xsl:call-template name="stereotype"/>

    </xsl:element>
  </xsl:template>



  <xsl:template name="UMLOperation">
    <xsl:element name="operation">
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:attribute name="visibility">
        <xsl:value-of select="@visibility"/>
      </xsl:attribute>

      <xsl:if test="@isAbstract='true'">
        <xsl:attribute name="abstract">
          <xsl:text>true</xsl:text>
        </xsl:attribute>
      </xsl:if>

      <xsl:if test="@ownerScope='classifier'">
        <xsl:attribute name="static">
          <xsl:text>true</xsl:text>
        </xsl:attribute>
      </xsl:if>

      <xsl:for-each select="UML:ModelElement.taggedValue/UML:TaggedValue">
        <xsl:call-template name="taggedValue"/>
      </xsl:for-each>

      <!-- operation parameters -->
      <xsl:for-each select="UML:BehavioralFeature.parameter/UML:Parameter">
        <xsl:choose>
          <xsl:when test="@kind='return'">
            <xsl:call-template name="UMLReturnParameter"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="UMLParameter"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>

      <xsl:for-each select="UML:ModelElement.comment/UML:Comment">
        <xsl:call-template name="UMLComment"/>
      </xsl:for-each>

      <!-- exceptions -->
      <xsl:variable name="operationId">
        <xsl:value-of select="@xmi.id"/>
      </xsl:variable>

      <xsl:for-each select="/XMI/XMI.content/UML:A_context_raisedSignal/UML:Operation[@xmi.idref = $operationId]">
        <xsl:variable name="exceptionId">
            <xsl:value-of select="concat(following::UML:Exception/@xmi.idref,following::UML:Signal/@xmi.idref)"/>
        </xsl:variable>
        <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Exception[@xmi.id = $exceptionId]">
            <xsl:call-template name="exception"/>
        </xsl:for-each>
        <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Signal[@xmi.id = $exceptionId]">
            <xsl:call-template name="exception"/>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:call-template name="stereotype"/>

    </xsl:element>
  </xsl:template>

  <xsl:template name="exception">
    <xsl:element name="exceptionParameter">
      <xsl:attribute name="name">
        <xsl:value-of select="@name" />
      </xsl:attribute>
      <xsl:attribute name="type">
        <xsl:call-template name="packagePathOfWithDot" />
        <xsl:value-of select="@name" />
      </xsl:attribute>
    </xsl:element>
  </xsl:template>

  <xsl:template name="UMLParameter">
    <xsl:element name="parameter">
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

        <xsl:attribute name="type">
            <xsl:call-template name="fullClassName">
                <xsl:with-param name="childpath">UML:Parameter.type</xsl:with-param>
            </xsl:call-template>
        </xsl:attribute>

        <xsl:for-each select="UML:ModelElement.taggedValue/UML:TaggedValue">
            <xsl:call-template name="taggedValue"/>
        </xsl:for-each>

    </xsl:element>
  </xsl:template>


  <xsl:template name="UMLReturnParameter">
    <xsl:element name="returnParameter">

      <xsl:variable name="id">
        <xsl:value-of select="concat(UML:Parameter.type/*/@xmi.idref,UML2:TypedElement.type/*/@xmi.idref)"/>
      </xsl:variable>

        <xsl:attribute name="type">
		  <xsl:call-template name="fullClassName">
		  	<xsl:with-param name="childpath">UML:Parameter.type</xsl:with-param>
		  </xsl:call-template>
        </xsl:attribute>

    </xsl:element>
  </xsl:template>


  <xsl:template name="UMLComment">
    <xsl:variable name="id">
      <xsl:value-of select="@xmi.idref"/>
    </xsl:variable>
    <xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::*[@xmi.id = $id]">
      <xsl:call-template name="comment"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="comment">
    <xsl:element name="comment">
	  <xsl:value-of select="@body"/>
    </xsl:element>
  </xsl:template>

  <xsl:template name="extern">
    <xsl:for-each select="UML:ModelElement.stereotype/UML:Stereotype">
      <xsl:variable name="id">
        <xsl:value-of select="@xmi.idref"/>
      </xsl:variable>
      <xsl:variable name="name">
        <xsl:value-of select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Stereotype[@xmi.id = $id]/@name"/>
      </xsl:variable>
      <xsl:if test="$name='extern'">
        <xsl:attribute name="extern">
          <xsl:text>true</xsl:text>
        </xsl:attribute>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="stereotype">
    <xsl:for-each select="UML:ModelElement.stereotype/UML:Stereotype">
      <xsl:variable name="id">
        <xsl:value-of select="@xmi.idref"/>
      </xsl:variable>
      <xsl:variable name="name">
        <xsl:value-of select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::UML:Stereotype[@xmi.id = $id]/@name"/>
      </xsl:variable>
      <xsl:if test="$name!='extern'">
        <xsl:element name="stereotype">
          <xsl:attribute name="name">
            <xsl:value-of select="$name"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="dependency">
    <xsl:for-each select="UML:Namespace.ownedElement/UML:Dependency">
      <xsl:element name="dependency">
        <xsl:attribute name="name">
          <xsl:value-of select="@name"/>
        </xsl:attribute>
        <xsl:attribute name="supplierName">
          <xsl:call-template name="fullClassName">
	     <xsl:with-param name="childpath">UML:Dependency.supplier</xsl:with-param>
          </xsl:call-template>
        </xsl:attribute>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="taggedValue">
    <xsl:variable name="tagId">
      <xsl:value-of select="UML:TaggedValue.type/UML:TagDefinition/@xmi.idref"/>
    </xsl:variable>
    <xsl:variable name="tagName">
	  <xsl:call-template name="fullClassName">
	  	<xsl:with-param name="childpath">UML:TaggedValue.type</xsl:with-param>
	  </xsl:call-template>
    </xsl:variable>
    <xsl:element name="tagValue">
      <xsl:attribute name="name">
        <xsl:value-of select="$tagName"/>
      </xsl:attribute>
      <xsl:attribute name="value">
        <xsl:value-of select="UML:TaggedValue.dataValue"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>


  <xsl:template name="fullClassName">
  	<xsl:param name="childpath"/>

	<xsl:if test="count(child::*[name() = $childpath]) = 1">
		<xsl:choose>
			<xsl:when test="child::*[name() = $childpath]/*/@xmi.idref">
				<xsl:variable name="id" select="child::*[name() = $childpath]/*/@xmi.idref"/>
				<xsl:for-each select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/descendant::*[@xmi.id = $id]">
				    <xsl:call-template name="packagePathOfWithDot"/>
				    <xsl:value-of select="@name"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:when test="child::*[name() = $childpath]/*/@href">
				<xsl:variable name="href" select="child::*[name() = $childpath]/*/@href"/>
				<xsl:variable name="external-url" select="substring-before($href, '#')"/>
				<xsl:variable name="external-id" select="substring-after($href, '#')"/>
				<xsl:for-each select="document($external-url)//descendant::*[@xmi.id=$external-id]">
					<xsl:call-template name="packagePathOfWithDot"/>
					<xsl:value-of select="@name"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
  </xsl:template>

  <xsl:template name="packagePathOfWithDot">
    <xsl:if test="name()!='UML:DataType'">
      <xsl:for-each select="ancestor::UML:Package">
        <xsl:value-of select="@name"/>
        <xsl:value-of select="string('.')"/>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
