<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  EUGene :: EUGene
  %%
  Copyright (C) 2004 - 2010 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xalan="http://xml.apache.org/xslt"
    xmlns:extensions="http://www.codelutin.com/XSLTExtensions"
    xmlns:redirect="http://xml.apache.org/xalan/redirect"
    extension-element-prefixes="extensions redirect"
    xmlns="http://nuiton.org/eugene/objectModel/v1"
    xmlns:packageValidator="xalan://org.nuiton.eugene.PackageValidator"
    xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:uml="http://www.eclipse.org/uml2/2.1.0/UML">

    <xsl:output method="xml" encoding="UTF-8" indent="yes"
        xalan:indent-amount="2" />

    <!--
        processing entry point. We'll process everything from that root
        package path. Everything else is ignored
    -->
    <xsl:param name="fullPackagePath" />
    <xsl:param name="extraPackages" />

    <!-- 
	. Matching templates 
	-->

    <xsl:template match="uml:Model">
        <xsl:element name="objectModel">
            <xsl:attribute name="xsi:schemaLocation">http://nuiton.org/eugene/objectModel/v1 http://doc.nuiton.org/eugene/xsd/v1/objectmodel.xsd</xsl:attribute>
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>

            <xsl:apply-templates>
                <xsl:with-param name="parentLocalPackageName" />
            </xsl:apply-templates>
        </xsl:element>
    </xsl:template>

    <xsl:template match="text()|attribute" />

    <xsl:template match="*">
        <xsl:param name="parentLocalPackageName" />
        <xsl:apply-templates>
            <xsl:with-param name="parentLocalPackageName">
                <xsl:value-of select="$parentLocalPackageName" />
            </xsl:with-param>
        </xsl:apply-templates>
    </xsl:template>

    <!-- uml:Package -->
    <xsl:template match="packagedElement[@xmi:type='uml:Package']">
        <xsl:param name="parentLocalPackageName" />
        
        <!--  Documentation... -->
        <xsl:call-template name="taggedValue" />

        <xsl:variable name="packageName">
            <xsl:value-of select="@name" />
        </xsl:variable>
        
        <xsl:variable name="parentLocalPackageNameDot">
            <xsl:if test="$parentLocalPackageName=''">
                <xsl:value-of select="$parentLocalPackageName" />
            </xsl:if>
            <xsl:if test="$parentLocalPackageName!=''">
                <xsl:value-of select="concat($parentLocalPackageName,'.')" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="localPackageName">
            <xsl:value-of select="$parentLocalPackageNameDot" />
            <xsl:value-of select="$packageName" />
        </xsl:variable>

        <xsl:apply-templates>
            <xsl:with-param name="parentLocalPackageName">
                <xsl:value-of select="$localPackageName" />
            </xsl:with-param>
        </xsl:apply-templates>

    </xsl:template>

    <!-- uml:Class -->
    <xsl:template match="packagedElement[@xmi:type='uml:Class']">
        <xsl:param name="parentLocalPackageName" />
        <xsl:call-template name="UMLClass">
            <xsl:with-param name="parentLocalPackageName" select="$parentLocalPackageName" />
        </xsl:call-template>
    </xsl:template>
        
    <xsl:template name="UMLClass">
        <xsl:param name="parentLocalPackageName" />

        <xsl:element name="class">
            <!-- class properties -->
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>

            <xsl:attribute name="package">
                <xsl:value-of select="$parentLocalPackageName" />
            </xsl:attribute>

            <xsl:if test="@isAbstract='true'">
                <xsl:attribute name="abstract">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:if>

            <!-- stereotypes -->
            <xsl:for-each select="//child::*[@base_Class = current()/@xmi:id]">
                <xsl:element name="stereotype">
                    <xsl:attribute name="name">
                        <xsl:value-of select="local-name()" />
                    </xsl:attribute>
                </xsl:element>
            </xsl:for-each>

            <!-- dependencies -->
            <xsl:call-template name="UMLDependencies">
                <xsl:with-param name="parent" select="current()" />
            </xsl:call-template>

            <!-- interfaces -->
            <xsl:apply-templates select="interfaceRealization" />

            <!-- extends -->
            <xsl:apply-templates select="generalization" />

            <!-- attributes -->
            <xsl:apply-templates select="ownedAttribute" />

            <!-- operations -->
            <xsl:apply-templates select="ownedOperation" />

            <!-- inner classes -->
            <xsl:for-each select="nestedClassifier[@xmi:type='uml:Class']">
                <xsl:call-template name="UMLClass">
                    <xsl:with-param name="parentLocalPackageName" select="$parentLocalPackageName" />
                </xsl:call-template>
            </xsl:for-each>

            <!-- associations , Association & AssociationClass -->
            <xsl:call-template name="UMLAssociations" />

            <!--  Documentation... -->
            <xsl:call-template name="taggedValue" />
        </xsl:element>
    </xsl:template>

    <!-- uml:AssociationClass -->
    <xsl:template match="packagedElement[@xmi:type='uml:AssociationClass']">
        <xsl:param name="parentLocalPackageName" />

        <xsl:element name="associationClass">
            <!-- class properties -->
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>

            <xsl:attribute name="package">
                <xsl:value-of select="$parentLocalPackageName" />
            </xsl:attribute>

            <xsl:if test="@isAbstract='true'">
                <xsl:attribute name="abstract">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:if>

            <!-- stereotypes -->
            <xsl:for-each select="//child::*[@base_Class = current()/@xmi:id]">
                <xsl:element name="stereotype">
                    <xsl:attribute name="name">
                        <xsl:value-of select="local-name()" />
                    </xsl:attribute>
                </xsl:element>
            </xsl:for-each>

            <!-- dependencies -->
            <xsl:call-template name="UMLDependencies">
                <xsl:with-param name="parent" select="current()" />
            </xsl:call-template>

            <!-- interfaces -->
            <xsl:apply-templates select="interfaceRealization" />

            <!-- extends -->
            <xsl:apply-templates select="generalization" />

            <!-- attributes -->
            <xsl:apply-templates select="ownedAttribute" />

            <!-- operations -->
            <xsl:apply-templates select="ownedOperation" />

            <!-- associations -->
            <xsl:call-template name="Participant" />

            <!--  Documentation... -->
            <xsl:call-template name="taggedValue" />
        </xsl:element>
    </xsl:template>

    <!-- uml:Interface -->
    <xsl:template match="packagedElement[@xmi:type='uml:Interface']">
        <xsl:param name="parentLocalPackageName" />

        <xsl:element name="interface">
            <!-- interface properties -->
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>

            <xsl:attribute name="package">
                <xsl:value-of select="$parentLocalPackageName" />
            </xsl:attribute>

            <xsl:if test="@isAbstract='true'">
                <xsl:attribute name="abstract">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:if>

            <!-- stereotypes -->
            <xsl:for-each
                select="//child::*[@base_Interface = current()/@xmi:id]">
                <xsl:element name="stereotype">
                    <xsl:attribute name="name">
                        <xsl:value-of select="local-name()" />
                    </xsl:attribute>
                </xsl:element>
            </xsl:for-each>

            <!-- dependencies -->
            <xsl:call-template name="UMLDependencies">
                <xsl:with-param name="parent" select="current()" />
            </xsl:call-template>

            <!-- extends -->
            <xsl:apply-templates select="generalization" />

            <!-- operations -->
            <xsl:apply-templates select="ownedOperation" />

            <!--  Documentation... -->
            <xsl:call-template name="taggedValue" />

        </xsl:element>
    </xsl:template>

    <!-- interfaceRealization -->
    <xsl:template match="interfaceRealization">
        <xsl:element name="interface">
            <xsl:attribute name="name">
                <xsl:call-template name="supplierName">
                    <xsl:with-param name="parent" select="current()" />
                </xsl:call-template>
            </xsl:attribute>
        </xsl:element>
    </xsl:template>

    <xsl:template match="generalization">

        <xsl:element name="superclass">
            <xsl:attribute name="name">
				
                <xsl:choose>
                    <xsl:when test="general">
                        <xsl:call-template name="fullClassName">
                            <xsl:with-param name="node" select="general" />
                        </xsl:call-template>
                    </xsl:when>
					
                    <xsl:when test="@general">
                        <xsl:call-template name="fullClassName2">
                            <xsl:with-param name="node"
                select="//uml:Model/descendant::packagedElement[@xmi:type='uml:Class'][@xmi:id=current()/@general]" />
                        </xsl:call-template>
                    </xsl:when>
                </xsl:choose>
				
            </xsl:attribute>
        </xsl:element>
    </xsl:template>

    <!-- uml:Enumeration -->
    <xsl:template match="packagedElement[@xmi:type='uml:Enumeration']">
        <xsl:param name="parentLocalPackageName" />
        <xsl:element name="enumeration">
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>
            <xsl:attribute name="package">
                <xsl:value-of select="$parentLocalPackageName" />
            </xsl:attribute>

            <!-- literals -->
            <xsl:apply-templates select="ownedLiteral" />

            <!-- operations -->
            <xsl:apply-templates select="ownedOperation" />
            
            <!--  Documentation... -->
            <xsl:call-template name="taggedValue" />
            
        </xsl:element>
    </xsl:template>

    <xsl:template match="ownedLiteral">
        <xsl:element name="literal">
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>
			
            <!--  Documentation... Not yet supported by the memory representation-->
            <!--
            <xsl:call-template name="taggedValue" /> 
            -->
        </xsl:element>
    </xsl:template>

    <xsl:template match="ownedAttribute">

        <xsl:element name="attribute">
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>

            <xsl:call-template name="multiplicities">
                <xsl:with-param name="parent" select="current()" />
            </xsl:call-template>

            <xsl:if test="@isStatic">
                <xsl:attribute name="static">
                    <xsl:value-of select="@isStatic" />
                </xsl:attribute>
            </xsl:if>

            <xsl:choose>
                <xsl:when test="@visibility">
                    <xsl:attribute name="visibility">
                        <xsl:value-of select="@visibility" />
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="visibility">public</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="UMLParameter" />
            <xsl:call-template name="taggedValue" />
        </xsl:element>
    </xsl:template>

    <xsl:template match="ownedOperation">

        <xsl:element name="operation">
            <xsl:attribute name="name">
                <xsl:value-of select="@name" />
            </xsl:attribute>

            <xsl:choose>
                <xsl:when test="@visibility">
                    <xsl:attribute name="visibility">
                        <xsl:value-of select="@visibility" />
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="visibility">public</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:if test="@isAbstract='true'">
                <xsl:attribute name="abstract">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:if>

            <xsl:if test="@isStatic">
                <xsl:attribute name="static">
                    <xsl:value-of select="@isStatic" />
                </xsl:attribute>
            </xsl:if>

            <!-- operation parameters -->
            <xsl:for-each select="ownedParameter">
                <xsl:choose>
                    <xsl:when test="@direction = 'return'">
                        <xsl:element name="returnParameter">
                            <xsl:call-template name="UMLParameter" />
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="parameter">
                            <xsl:attribute name="name">
                                <xsl:value-of select="@name" />
                            </xsl:attribute>
                            <xsl:call-template name="UMLParameter" />
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>

            <!-- operation exceptions -->
            <xsl:choose>
                <xsl:when test="raisedException">
                    <xsl:for-each select="raisedException">
                        <xsl:element name="exceptionParameter">
                            <xsl:attribute name="type">
                                <xsl:call-template name="fullClassName">
                                    <xsl:with-param name="node" select="." />
                                </xsl:call-template>
                            </xsl:attribute>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:when>
                <xsl:when test="@raisedException">
                    <xsl:call-template name="ownedOperation-raisedException">
                        <xsl:with-param name="value"
                            select="string(@raisedException)" />
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>

            <!-- stereotypes -->
            <xsl:for-each select="//child::*[@base_Operation = current()/@xmi:id]">
                <xsl:element name="stereotype">
                    <xsl:attribute name="name">
                        <xsl:value-of select="local-name()" />
                    </xsl:attribute>
                </xsl:element>
            </xsl:for-each>
            
            <!--  Documentation... -->
            <xsl:call-template name="taggedValue" />
            
        </xsl:element>
    </xsl:template>

    <!-- 
	. Named templates 
	-->

    <!-- find reference listed in ownedOperation/@raisedException -->
    <xsl:template name="ownedOperation-raisedException">
        <xsl:param name="value" />

        <xsl:choose>
            <xsl:when test="contains($value, ' ')">
                <xsl:call-template name="ownedOperation-raisedException">
                    <xsl:with-param name="value"
                        select="substring-before($value, ' ')" />
                </xsl:call-template>
                <xsl:call-template name="ownedOperation-raisedException">
                    <xsl:with-param name="value"
                        select="substring-after($value, ' ')" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$value != ''">
                <xsl:element name="exceptionParameter">
                    <xsl:attribute name="type">
                        <xsl:call-template name="fullClassName2">
                            <xsl:with-param name="node"
                        select="//uml:Model/descendant::packagedElement[@xmi:type='uml:Class'][@xmi:id=$value]" />
                        </xsl:call-template>
                    </xsl:attribute>
                </xsl:element>
            </xsl:when>
        </xsl:choose>

    </xsl:template>

    <xsl:template name="UMLDependencies">
        <xsl:param name="parent" />
        <xsl:for-each select="//packagedElement[@xmi:type = 'uml:Dependency'][@client = $parent/@xmi:id]">
            <xsl:element name="dependency">
                <xsl:attribute name="name">
                    <xsl:value-of select="@name" />
                </xsl:attribute>

                <xsl:attribute name="supplierName">
                    <xsl:call-template name="supplierName">
                        <xsl:with-param name="parent" select="current()" />
                    </xsl:call-template>
                </xsl:attribute>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="supplierName">
        <xsl:param name="parent" />
        <xsl:choose>
            <xsl:when test="supplier">
                <xsl:call-template name="fullClassName">
                    <xsl:with-param name="node" select="supplier" />
                </xsl:call-template>
            </xsl:when>

            <xsl:when test="@supplier">
                <xsl:call-template name="fullClassName2">
                    <xsl:with-param name="node"
        select="//uml:Model/descendant::packagedElement[@xmi:id=$parent/@supplier]" />
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- 
	called by ownedAttribute and ownedOperation
	-->
    <xsl:template name="UMLParameter">

        <xsl:choose>
            <xsl:when test="type">
                <xsl:attribute name="type">
                    <xsl:call-template name="fullClassName">
                        <xsl:with-param name="node" select="type" />
                    </xsl:call-template>
                </xsl:attribute>
            </xsl:when>

            <xsl:when test="@type">
                <xsl:attribute name="type">
                    <xsl:variable name="typeref"
                    select="//uml:Model/descendant::packagedElement[@xmi:type='uml:Enumeration' or @xmi:type='uml:DataType' or @xmi:type='uml:Class' or @xmi:type='uml:Interface' or @xmi:type='uml:PrimitiveType'][@xmi:id=current()/@type]" />
                    <xsl:call-template name="fullClassName2">
                        <xsl:with-param name="node" select="$typeref" />
                    </xsl:call-template>
                </xsl:attribute>
            </xsl:when>
        </xsl:choose>

        <xsl:call-template name="multiplicities">
            <xsl:with-param name="parent" select="current()" />
        </xsl:call-template>

        <xsl:if test="@isOrdered">
            <xsl:attribute name="ordering">
                <xsl:text>ordered</xsl:text>
            </xsl:attribute>
        </xsl:if>

        <xsl:if test="@isUnique">
            <xsl:attribute name="unique">
                <xsl:value-of select="@isUnique" />
            </xsl:attribute>
        </xsl:if>

        <xsl:if test="defaultValue/@value">
            <xsl:attribute name="defaultValue">
                <xsl:value-of select="defaultValue/@value" />
            </xsl:attribute>
        </xsl:if>

    </xsl:template>

    <!-- 
    	taggedValue. e.i. documentation
    -->
    <xsl:template name="taggedValue">
        <xsl:for-each select="eAnnotations/details">
            <xsl:element name="tagValue">
                <xsl:attribute name="name">
                    <xsl:value-of select="@key" />
                </xsl:attribute>
                <xsl:attribute name="value">
                    <xsl:value-of select="@value" />
                </xsl:attribute>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <!-- 
        uml:Association
    -->
    <xsl:template name="UMLAssociations">
        <xsl:for-each
            select="following-sibling::packagedElement[@xmi:type='uml:Association']/ownedEnd[@type=current()/@xmi:id]">


            <xsl:variable name="reversenode" select="current()"/>

            <xsl:variable name="endnode"
                select="parent::packagedElement/ownedEnd[@association=current()/@association][not(@type) or @type != current()/@type]" />

            <xsl:if test="$endnode">

                <xsl:element name="attribute">
                    <xsl:if test="$endnode/@name != ''">
                        <xsl:attribute name="name">
                            <xsl:value-of select="$endnode/@name" />
                        </xsl:attribute>
                    </xsl:if>

                    <xsl:choose>
                        <xsl:when test="$endnode/type">
                            <xsl:attribute name="type">
                                <xsl:call-template name="fullClassName">
                                    <xsl:with-param name="node" select="$endnode/type" />
                                </xsl:call-template>
                            </xsl:attribute>
                        </xsl:when>

                        <xsl:when test="$endnode/@type">
                            <xsl:attribute name="type">
                                <xsl:variable name="typeref"
                                select="//uml:Model/descendant::packagedElement[@xmi:type='uml:Enumeration' or @xmi:type='uml:DataType' or @xmi:type='uml:Class' or @xmi:type='uml:Interface'][@xmi:id=$endnode/@type]" />
                                <xsl:call-template name="fullClassName2">
                                    <xsl:with-param name="node" select="$typeref" />
                                </xsl:call-template>
                            </xsl:attribute>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:attribute name="reverseAttributeName">
                        <xsl:value-of select="$reversenode/@name" />
                    </xsl:attribute>

                    <xsl:attribute name="reverseMaxMultiplicity">
                        <xsl:choose>
                            <xsl:when test="$reversenode/upperValue/@value">
                                <xsl:choose>
                                    <xsl:when test="$reversenode/upperValue/@value='*'">
                                        <xsl:text>-1</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$reversenode/upperValue/@value" />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>-1</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>

                    <xsl:if test="$endnode/@aggregation">
                        <xsl:attribute name="associationType">
                            <xsl:value-of select="$endnode/@aggregation" />
                        </xsl:attribute>
                    </xsl:if>

                    <xsl:call-template name="multiplicities">
                        <xsl:with-param name="parent" select="$endnode" />
                    </xsl:call-template>

                    <xsl:attribute name="navigable">
                        <xsl:value-of select="contains(parent::packagedElement/@navigableOwnedEnd, $endnode/@xmi:id)"/>
                    </xsl:attribute>

                    <xsl:if test="$endnode/@isOrdered">
                        <xsl:attribute name="ordering">
                            <xsl:text>ordered</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    
                    <!-- stereotypes -->
                    <xsl:for-each select="//child::*[@base_Association = current()/parent::packagedElement/@xmi:id]">
                        <xsl:element name="stereotype">
                            <xsl:attribute name="name">
                                <xsl:value-of select="local-name()" />
                            </xsl:attribute>
                        </xsl:element>
                    </xsl:for-each>

                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="multiplicities">
        <xsl:param name="parent" />

        <xsl:if test="$parent/lowerValue">
            <xsl:attribute name="minMultiplicity">
                <xsl:choose>
                    <xsl:when test="$parent/lowerValue/@value">
                        <xsl:value-of select="$parent/lowerValue/@value" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>0</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
        </xsl:if>

        <xsl:if test="$parent/upperValue">
            <xsl:attribute name="maxMultiplicity">
                <xsl:choose>
                    <xsl:when test="$parent/upperValue/@value">
                        <xsl:choose>
                            <xsl:when test="$parent/upperValue/@value='*'">
                                <xsl:text>-1</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="$parent/upperValue/@value" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>-1</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
        </xsl:if>
    </xsl:template>

    <xsl:template name="Participant">
        <xsl:for-each select="ownedEnd">

            <xsl:variable name="endnode"
                select="child::ownedEnd[@association=current()/@association][not(@type) or @type != current()/@type]" />
            <xsl:if test="$endnode">
                <xsl:element name="participant">
                    <xsl:choose>
                        <xsl:when test="$endnode/type">
                            <xsl:attribute name="type">
                                <xsl:call-template
                                name="fullClassName">
                                    <xsl:with-param
                                name="node" select="$endnode/type" />
                                </xsl:call-template>
                            </xsl:attribute>
                        </xsl:when>

                        <xsl:when test="$endnode/@type">
                            <xsl:attribute name="type">
                                <xsl:variable name="typeref"
                                select="//uml:Model/descendant::packagedElement[@xmi:type='uml:Enumeration' or @xmi:type='uml:DataType' or @xmi:type='uml:Class' or @xmi:type='uml:Interface'][@xmi:id=$endnode/@type]" />
                                <xsl:call-template
                                name="fullClassName2">
                                    <xsl:with-param
                                name="node" select="$typeref" />
                                </xsl:call-template>
                            </xsl:attribute>
                        </xsl:when>
                    </xsl:choose>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <!--
        node param must be a Xpath node. the node designed by node param
        must have @xmi:type. walks thru the model in order to print
        super package names. print fullClassName of type designed by
        node param.
    -->
    <xsl:template name="fullClassName2">
        <xsl:param name="node" />

        <xsl:choose>
            <xsl:when test="$node[@xmi:type='uml:DataType']">
                <xsl:call-template name="fullClassName2">
                    <xsl:with-param name="node"
                        select="$node/parent::packagedElement[@xmi:type = 'uml:Package']" />
                </xsl:call-template>
                <xsl:value-of select="$node/@name" />
            </xsl:when>

            <xsl:when test="$node[@xmi:type='uml:PrimitiveType']">
                <xsl:call-template name="fullClassName2">
                    <xsl:with-param name="node"
                        select="$node/parent::packagedElement[@xmi:type = 'uml:Package']" />
                </xsl:call-template>
                <xsl:value-of select="$node/@name" />
            </xsl:when>

            <xsl:when test="$node[@xmi:type='uml:Enumeration']">
                <xsl:call-template name="fullClassName2">
                    <xsl:with-param name="node"
                        select="$node/parent::packagedElement[@xmi:type = 'uml:Package']" />
                </xsl:call-template>
                <xsl:value-of select="$node/@name" />
            </xsl:when>

            <xsl:when test="$node[@xmi:type = 'uml:Class']">
                <xsl:call-template name="fullClassName2">
                    <xsl:with-param name="node"
                        select="$node/parent::packagedElement[@xmi:type = 'uml:Package']" />
                </xsl:call-template>
                <xsl:value-of select="$node/@name" />
            </xsl:when>

            <xsl:when test="$node[@xmi:type = 'uml:Interface']">
                <xsl:call-template name="fullClassName2">
                    <xsl:with-param name="node"
                        select="$node/parent::packagedElement[@xmi:type = 'uml:Package']" />
                </xsl:call-template>
                <xsl:value-of select="$node/@name" />
            </xsl:when>

            <xsl:when test="$node[@xmi:type='uml:Package']">
                <xsl:choose>
                    <xsl:when
                        test="$node/parent::packagedElement[@xmi:type = 'uml:Package']">
                        <xsl:call-template name="fullClassName2">
                            <xsl:with-param name="node"
                                select="$node/parent::packagedElement[@xmi:type = 'uml:Package']" />
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="fullClassName2">
                            <xsl:with-param name="node"
                                select="$node/parent::uml:Model" />
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select="concat($node/@name, '.')" />
            </xsl:when>

            <!--
                <xsl:when test="$node[name()='uml:Model']">
                <xsl:value-of select="concat($modelName, '.')"/>
                </xsl:when>
            -->
        </xsl:choose>
    </xsl:template>

    <!--
        node param must be a Xpath node. the node designed by node param
        must have @xmi:type and @href attributes. print fullClassName of
        type designed by node param (helped by fullClassName2 template).
    -->
    <xsl:template name="fullClassName">
        <xsl:param name="node" />

        <xsl:variable name="typeref" select="$node/@href" />
        <xsl:variable name="xmi-type" select="$node/@xmi:type" />
        <xsl:variable name="external-url"
            select="substring-before($typeref, '#')" />
        <xsl:variable name="external-id"
            select="substring-after($typeref, '#')" />

        <xsl:choose>
            <xsl:when test="$external-url=''">
                <!-- internal reference -->
                <xsl:call-template name="fullClassName2">
                    <xsl:with-param name="node"
                        select="//uml:Model/descendant::packagedElement[@xmi:type=$xmi-type][@xmi:id=$external-id]" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <!-- Dans les modeles externes, le cas est different, FQN seulement pour les types non primitifs -->
                <xsl:choose>
                    <xsl:when test="$node/@xmi:type = 'uml:PrimitiveType'">
                        <xsl:value-of select="document($external-url)/descendant::packagedElement[@xmi:type=$xmi-type][@xmi:id=$external-id]/@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="fullClassName2">
                            <xsl:with-param name="node" select="document($external-url)/descendant::packagedElement[@xmi:type=$xmi-type][@xmi:id=$external-id]" />
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
              <!-- / fin du cas particulier -->
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

</xsl:stylesheet>
