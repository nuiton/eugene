<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!--
  #%L
  EUGene :: EUGene
  %%
  Copyright (C) 2004 - 2010 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xslt"
                xmlns:extensions="http://www.codelutin.com/XSLTExtensions"
                xmlns:redirect="http://xml.apache.org/xalan/redirect"
                extension-element-prefixes="extensions redirect"
                xmlns="http://www.codelutin.org/eugene/stateModel"
                xmlns:packageValidator="xalan://org.nuiton.eugene.PackageValidator"
                xmlns:UML="org.omg.xmi.namespace.UML"
				xmlns:UML2="org.omg.xmi.namespace.UML2">

  <xsl:output method="xml" 
              encoding="UTF-8"
              indent="yes" 
              xalan:indent-amount="2"/>

            
  <!-- processing entry point. We'll process everything from that root package
	   path. Everything else is ignored -->
  <!--<xsl:param name="fullPackagePath"/>
  <xsl:param name="extraPackages"/>-->

  <!-- ignoring text, comment, etc... -->
  <xsl:template match="text()|attribute"/>

  <!--<xsl:template match="*">
    <xsl:param name="parentLocalPackageName"/>
    <xsl:apply-templates> 
      <xsl:with-param name="parentLocalPackageName">
        <xsl:value-of select="$parentLocalPackageName"/>
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>-->
	
  <xsl:template match="/XMI/XMI.content/UML:Model">
    <xsl:element name="stateModel">
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
	  
	  <xsl:attribute name="version">
		<xsl:variable name="versionTagId">
          <xsl:value-of select="/XMI/XMI.content/UML:Model/UML:Namespace.ownedElement/UML:TagDefinition[@name='version']/@xmi.id"/>
        </xsl:variable>
        <xsl:value-of select="/XMI/XMI.content/UML:Model/UML:ModelElement.taggedValue/UML:TaggedValue[UML:TaggedValue.type/UML:TagDefinition/@xmi.idref = $versionTagId]/UML:TaggedValue.dataValue"/>
      </xsl:attribute>
      
      <xsl:apply-templates select="UML:Namespace.ownedElement/*">
		  <xsl:with-param name="parentLocalPackageName"/>
	  </xsl:apply-templates>
    </xsl:element>
  </xsl:template>
	

  <!-- package names management -->
  <xsl:template match="UML:Package">
    <xsl:param name="parentLocalPackageName"/>
    <xsl:variable name="packageName">
      <xsl:value-of select="@name"/>
    </xsl:variable>

    <xsl:variable name="localPackageName">
      <xsl:value-of select="$parentLocalPackageName"/>
      <xsl:value-of select="$packageName"/>
    </xsl:variable>

    <xsl:variable name="localPackageNameDot">
      <xsl:value-of select="concat($localPackageName,'.')"/>
    </xsl:variable>
	  
	<xsl:apply-templates select="UML:Namespace.ownedElement/UML:Package"> 
      <xsl:with-param name="parentLocalPackageName">
		  <xsl:value-of select="$localPackageNameDot"/>
		  </xsl:with-param>
    </xsl:apply-templates>
	
	<xsl:apply-templates select="UML:Namespace.ownedElement/UML:StateMachine"> 
      <xsl:with-param name="parentLocalPackageName">
		  <xsl:value-of select="$localPackageName"/>
		  </xsl:with-param>
    </xsl:apply-templates>
		  
  </xsl:template>
	
  <!-- UML:StateMachine : one use case -->
  <xsl:template match="UML:StateMachine">
	<xsl:param name="parentLocalPackageName"/>
	<xsl:element name="stateChart">
	  <!-- name attribute -->
	  <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

      <!-- package attribute -->
      <xsl:attribute name="package">
        <xsl:value-of select="$parentLocalPackageName"/>
      </xsl:attribute>
	  
	  <!-- process states -->
	  <!-- only state without stereotype -->
	  <xsl:apply-templates select="UML:StateMachine.top/UML:CompositeState[@name='top']/UML:CompositeState.subvertex/*[count(child::UML:ModelElement.stereotype)=0]"/>

	  <!-- process transitions -->
	  <!--<xsl:apply-templates select="UML:StateMachine.transitions"/>-->
	</xsl:element>
  </xsl:template>
  
  <!-- UML:StateMachine.top = set of states of this case -->
  <!-- UML:CompositeState : idem -->
  <!--<xsl:template match="UML:StateMachine.top">
	<xsl:element name="states">
	  <xsl:apply-templates select="UML:CompositeState/UML:CompositeState.subvertex/*"/>
	</xsl:element>
  </xsl:template>-->

  <!-- UML:CompositeState = composite state -->
  <xsl:template match="UML:CompositeState">
	<xsl:element name="complexeState">
	  <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

	  <!-- process all state of this complexe state -->
	  <!-- without stereotype -->
      <xsl:apply-templates select="UML:CompositeState.subvertex/*[count(child::UML:ModelElement.stereotype)=0]"/>
	</xsl:element>
  </xsl:template>
	
  <!-- UML:Pseudostate = init state -->
  <xsl:template match="UML:Pseudostate">
	<xsl:element name="state">
	  <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:attribute name="initial">true</xsl:attribute>
		
      <!-- process all outgoing transition -->
      <xsl:apply-templates select="UML:StateVertex.outgoing/*"/>
	</xsl:element>
  </xsl:template>

  <!-- UML:SimpleState = normal state -->
  <xsl:template match="UML:SimpleState">
	<xsl:element name="state">
	  <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
		
      <!-- TODO init action ? 
	  <xsl:apply-templates select="UML:State.entry/UML:CallAction"> 
        <xsl:with-param name="parentLocalPackageName"/>
      </xsl:apply-templates> -->

	  <!-- process all outgoing transition -->
      <xsl:apply-templates select="UML:StateVertex.outgoing/*"/>
	</xsl:element>
  </xsl:template>

  <!-- UML:FinalState = final state -->
  <xsl:template match="UML:FinalState">
	<xsl:element name="state">
	  <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:attribute name="final">true</xsl:attribute>
	</xsl:element>
  </xsl:template>
	
  <!-- UML:Transition = a transition -->
  <xsl:template match="UML:Transition">
	<xsl:element name="transition">

	  <!-- id of this transition -->
      <xsl:variable name="thisTransitionId">
		  <xsl:value-of select="@xmi.idref"/>
		</xsl:variable>
	  
      <!-- event -->
	  <xsl:attribute name="event">
        <xsl:variable name="eventId">
	  	  <xsl:value-of select="ancestor::UML:StateMachine/UML:StateMachine.transitions/UML:Transition[@xmi.id=$thisTransitionId]/UML:Transition.trigger/UML:SignalEvent/@xmi.idref"/>
	    </xsl:variable>
	    <xsl:value-of select="ancestor::UML:Package/UML:Namespace.ownedElement/UML:SignalEvent[@xmi.id=$eventId]/@name"/>
	  </xsl:attribute>
		
	  <!-- toState attribute -->
      <xsl:attribute name="toState">
		<xsl:variable name="destStateId">
		  <xsl:value-of select="ancestor::UML:StateMachine/UML:StateMachine.transitions/UML:Transition[@xmi.id=$thisTransitionId]/UML:Transition.target/child::*[position()=1]/@xmi.idref"/>
	    </xsl:variable>
		<xsl:value-of select="../../../*[@xmi.id=$destStateId]/@name"/>
      </xsl:attribute>
	  
	  <!-- transition's actions -->
	  <!--<xsl:apply-templates select="UML:Transition.effect/*"> 
        <xsl:with-param name="parentLocalPackageName"/>
      </xsl:apply-templates>-->
	</xsl:element>
  </xsl:template>
	
  <!-- UML:CallAction : action -->
  <!--<xsl:template match="UML:CallAction">
	<xsl:element name="action">
	  <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
	</xsl:element>
  </xsl:template>-->
</xsl:stylesheet>
