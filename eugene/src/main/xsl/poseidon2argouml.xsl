<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!--
  #%L
  EUGene :: EUGene
  %%
  Copyright (C) 2004 - 2010 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:UML="org.omg.xmi.namespace.UML"
  xmlns:UML2="org.omg.xmi.namespace.UML2"
  >

  <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="@*|node()">
      <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
    </xsl:template>

  <xsl:template match="//UML:Parameter/UML2:TypedElement.type">
    <xsl:element name="UML:Parameter.type" namespace="org.omg.xmi.namespace.UML">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

 
  <xsl:template match="//UML:Attribute/UML2:TypedElement.type">
    <xsl:element name="UML:StructuralFeature.type" namespace="org.omg.xmi.namespace.UML">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="//UML:Comment[count(.//UML2:*) > 0]">
  </xsl:template>

  <xsl:template match="UML2:BehavioredClassifier.ownedBehavior">
  </xsl:template>

  <xsl:template match="UML2:BehavioredClassifier.ownedTrigger">
  </xsl:template>

  <xsl:template match="UML:Comment.annotatedElement">
  </xsl:template>

  <xsl:template match="UML:Collaboration">
  </xsl:template>
  
  <xsl:template match="UML:Diagram">
  </xsl:template>
  
  <xsl:template match="UML:GraphConnector">
  </xsl:template>
  
  <xsl:template match="UML:GraphNode">
  </xsl:template>
  
  <xsl:template match="UML:Property">
  </xsl:template>
  
</xsl:stylesheet>
