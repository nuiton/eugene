/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.TestHelper;
import org.nuiton.eugene.models.object.reader.XmlObjectModelReader;
import org.nuiton.util.FileUtil;
import org.nuiton.util.Resource;
import org.nuiton.util.ResourceResolver;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Test de la feuille de style "xmi2.1ToObjectModel.xsl"
 * sur l'exemple TestXMI21.uml
 *
 * @author chatellier
 */
public class XMI21ToObjectModelTest {

    protected static File destinationDirectory;

    @BeforeClass
    public static void beforeTest() throws IOException {
        destinationDirectory = TestHelper.getTestBasedir(XMI21ToObjectModelTest.class);
        FileUtil.createDirectoryIfNecessary(destinationDirectory);
    }

//    @Before
//    public void setUp() {
//        destinationDirectory = new File("target", "xmi");
//        destinationDirectory.mkdirs();
//    }

    /**
     * Apply XSLT Transformation.
     *
     * @param xmiFile
     * @return transformed file
     * @throws IOException
     * @throws TransformerException
     */
    protected File transformXMI(File xmiFile, String outModel)
            throws IOException, TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();

        URL xsl = Resource.getURL("xmi2.1ToObjectModel.xsl");

        File result = new File(destinationDirectory, outModel);

        Transformer transformer = factory.newTransformer(new StreamSource(xsl
                                                                                  .openStream()));

        String basePath = xmiFile.getParent();
        transformer.setURIResolver(new ResourceResolver(basePath));

        try (FileOutputStream out = new FileOutputStream(result)) {
            transformer.transform(new StreamSource(xmiFile), new StreamResult(
                    out));
        }

        return result;
    }

    /**
     * Load model into memory.
     *
     * @param modelFile
     * @return object model
     */
    protected ObjectModel loadModel(File modelFile) throws IOException {
        XmlObjectModelReader generator = new XmlObjectModelReader();
        ObjectModel model = generator.read(modelFile);
        return model;
//        ObjectModelReader reader = new ObjectModelReader();
//        ObjectModel model = reader.read(modelFile);
//        ObjectModelGenerator generator = new ObjectModelGenerator();
//        generator.generate(new File[] { modelFile }, new File("output"));
//        ObjectModel objectModel = generator.getModel();
//        return objectModel;
    }

    /**
     * Apply xslt on xmi model, and load it.
     *
     * @param modelName model to load
     * @throws URISyntaxException
     * @throws TransformerException
     * @throws IOException
     */
    protected ObjectModel xmiToObjectModel(String modelName) throws URISyntaxException, IOException, TransformerException {
        File xmiFile = new File(Resource.getURL("xmi/2.1/" + modelName + ".uml")
                                        .toURI());

        File objectModelFile = transformXMI(xmiFile, modelName + ".objectmodel");

        ObjectModel model = loadModel(objectModelFile);
        return model;
    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on it.
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testXSLTestXMI21() throws URISyntaxException, IOException,
            TransformerException {

        ObjectModel model = xmiToObjectModel("TestXMI21");

        assertNotNull(model);
        assertEquals("XMITest21", model.getName());
        //FIXME check there is an xmi enumeration since we changed enumeration to classifier 
//        assertEquals(15, model.getClassifiers().size());
        assertEquals(16, model.getClassifiers().size());

        // ClassB
        ObjectModelClass clazzB = model.getClass("org.nuiton.eugene.test21.ClassB");
        assertNotNull(clazzB);
        ObjectModelAttribute attrCost = clazzB.getAttribute("cost");
        assertNotNull(attrCost);
        assertEquals("Cost attribute comment", attrCost.getTagValue("documentation"));

        // ClassA
        ObjectModelClass clazzA = model.getClass("org.nuiton.eugene.test21.ClassA");
        assertNotNull(clazzA);
        ObjectModelAttribute attrName = clazzA.getAttribute("name");
        assertNotNull(attrName);
        assertEquals(2, attrName.getMinMultiplicity());
        assertEquals(-1, attrName.getMaxMultiplicity());
//        assertTrue(attrName.isOrdered());
//        assertTrue(attrName.isUnique());
    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on it.
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testXSLVpod() throws URISyntaxException, IOException,
            TransformerException {

        ObjectModel model = xmiToObjectModel("vpod");

        assertNotNull(model);
        assertEquals("org::sharengo::s4a::storage::http::vpod", model.getName());
        assertEquals(4, model.getClassifiers().size());

        // ClassB
        ObjectModelClass clazzVpodMappingDao = model.getClass("daos.VpodMappingDao");
        assertNotNull(clazzVpodMappingDao);
        assertTrue(clazzVpodMappingDao.hasStereotype("Dao"));
        List<ObjectModelOperation> opFindByVpodId = new ArrayList<>();
        opFindByVpodId.addAll(clazzVpodMappingDao.getOperations("findByVpodId"));
        assertEquals("entities.VpodMapping", opFindByVpodId.get(0).getReturnType());
        List<ObjectModelParameter> opFindByVpodIdParams = new ArrayList<>();
        opFindByVpodIdParams.addAll(opFindByVpodId.get(0).getParameters());
        assertEquals("String", opFindByVpodIdParams.get(0).getType());
    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on it.
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testXSLWithStereotype() throws URISyntaxException, IOException,
            TransformerException {

        ObjectModel model = xmiToObjectModel("cmsLink");

        assertNotNull(model);
        assertEquals("org::sharengo::utils::container::link", model.getName());
        //FIXME check there is an xmi enumeration since we changed enumeration to classifier
        assertEquals(9, model.getClassifiers().size());
//        assertEquals(8, model.getClassifiers().size());

        // LinkEntity
        ObjectModelClass clazzLinkEntity = model.getClass("org.sharengo.utils.container.link.entities.LinkEntity");
        assertNotNull(clazzLinkEntity);
        assertTrue(clazzLinkEntity.hasStereotype("Entity"));
        ObjectModelAttribute attrDefinition = clazzLinkEntity.getAttribute("target");
        assertNotNull(attrDefinition);
        assertTrue(attrDefinition.hasStereotype("Embedded"));

        // LinkEntity
        ObjectModelClass clazzLinkSrv = model.getClass("org.sharengo.utils.container.link.services.LinkSrv");
        assertNotNull(clazzLinkSrv);
        assertTrue(clazzLinkSrv.hasStereotype("Service"));
        List<ObjectModelOperation> opFindAllByContent = new ArrayList<>();
        opFindAllByContent.addAll(clazzLinkSrv.getOperations("findAllByContent"));
        assertEquals(1, opFindAllByContent.size());
        assertTrue(opFindAllByContent.get(0).hasStereotype("Remote"));
    }

    @Ignore("Test doesn't work on Windows OS ticket:http://nuiton.org/issues/3136")
    @Test
    public void testExtractCmsCore() throws Exception {

        ObjectModel model = xmiToObjectModel("cmsCore");
        assertNotNull(model);

        // There was a problem with sub package
        ObjectModelClass clazzFacetViewSrv = model.getClass("org.sharengo.cms.core.services.facet.FacetViewSrv");
        assertNotNull(clazzFacetViewSrv);
        assertEquals(11, clazzFacetViewSrv.getOperations().size());

        // Test attribute type FQN
        ObjectModelClass clazzContentDefSearchDto = model.getClass("org.sharengo.cms.core.dtos.ContentDefSearchDto");
        assertNotNull(clazzContentDefSearchDto);
        ObjectModelAttribute attrFromDate = clazzContentDefSearchDto.getAttribute("fromDate");
        assertNotNull(attrFromDate);
        assertEquals("Date", attrFromDate.getType());
        ObjectModelAttribute attrContentDefId = clazzContentDefSearchDto.getAttribute("contentDefId");
        assertNotNull(attrContentDefId);
        assertEquals("String", attrContentDefId.getType());
        ObjectModelAttribute attrAttributes = clazzContentDefSearchDto.getAttribute("attributes");
        assertNotNull(attrAttributes);
        assertEquals("org.sharengo.cms.core.dtos.AttributeSearchDto", attrAttributes.getType());

        ObjectModelClass clazzContentDefDto = model.getClass("org.sharengo.cms.core.dtos.AttributeDef");
        assertNotNull(clazzContentDefDto);
        ObjectModelAttribute attrIdContentDef = clazzContentDefDto.getAttribute("idContentDef");
        assertNotNull(attrIdContentDef);
        assertEquals("String", attrIdContentDef.getType());
    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on embedded primitive type
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testXSLTestXMI21EmbeddedPrimitiveType() throws URISyntaxException, IOException,
            TransformerException {

        ObjectModel model = xmiToObjectModel("TestXMI21");

        assertNotNull(model);
        assertEquals("XMITest21", model.getName());

        // ClassB
        ObjectModelClass clazzB = model.getClass("org.nuiton.eugene.test21.ClassB");
        assertNotNull(clazzB);

        //primitiveType double
        ObjectModelAttribute attrDouble = clazzB.getAttribute("double");
        assertNotNull(attrDouble);
        assertEquals("org.nuiton.eugene.test21.double", attrDouble.getType());

        // ClassC
        ObjectModelClass clazzC = model.getClass("org.nuiton.eugene.test21.ClassC");
        assertNotNull(clazzC);

        //dataType formula
        ObjectModelAttribute attrFormula = clazzC.getAttribute("formula");
        assertNotNull(attrFormula);
        assertEquals("org.codelutin.types.Formula", attrFormula.getType());

    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on embedded primitive type
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testXSLTestXMI21MultiplicityClass() throws URISyntaxException, IOException,
            TransformerException {

        ObjectModel model = xmiToObjectModel("TestXMI21");

        // ClassB
        ObjectModelClass multiClazz = model.getClass("org.nuiton.eugene.test21.MultiplicityClass");

        // strings
        ObjectModelAttribute strings = multiClazz.getAttribute("strings");
        assertNotNull(strings);
        assertEquals("String", strings.getType());
        assertEquals(1, strings.getMinMultiplicity());
        assertEquals(-1, strings.getMaxMultiplicity());
        assertTrue(strings.isUnique());
        assertFalse(strings.isOrdered());
        assertNull(strings.getReverseAttribute());
        assertTrue(strings.isNavigable());

        // doubles
        ObjectModelAttribute doubles = multiClazz.getAttribute("doubles");
        assertNotNull(doubles);
        assertEquals("org.nuiton.eugene.test21.double", doubles.getType());
        assertEquals(1, doubles.getMinMultiplicity());
        assertEquals(5, doubles.getMaxMultiplicity());
        assertTrue(doubles.isUnique());
        assertTrue(doubles.isOrdered());

        // dataTypes
        ObjectModelAttribute dataTypes = multiClazz.getAttribute("dataTypes");
        assertNotNull(dataTypes);
        assertEquals("org.nuiton.eugene.test21.DataType1", dataTypes.getType());
        assertEquals(0, dataTypes.getMinMultiplicity());
        assertEquals(-1, dataTypes.getMaxMultiplicity());
        assertFalse(dataTypes.isUnique());
        assertTrue(dataTypes.isOrdered());

        // getStrings
        ObjectModelOperation op = multiClazz.getOperations("getStrings").iterator().next();
        assertNotNull(op);
        assertEquals("String", op.getReturnType());
        assertNotNull(op.getReturnParameter());
        assertEquals("String", op.getReturnParameter().getType());
        assertEquals(1, op.getReturnParameter().getMinMultiplicity());
        assertEquals(-1, op.getReturnParameter().getMaxMultiplicity());
        assertTrue(op.getReturnParameter().isUnique());
        assertTrue(op.getReturnParameter().isOrdered());


        // getDoubles
        op = multiClazz.getOperations("getDoubles").iterator().next();
        assertNotNull(op);
        assertEquals("org.nuiton.eugene.test21.double", op.getReturnType());
        assertNotNull(op.getReturnParameter());
        assertEquals("org.nuiton.eugene.test21.double", op.getReturnParameter().getType());
        assertEquals(2, op.getReturnParameter().getMinMultiplicity());
        assertEquals(-1, op.getReturnParameter().getMaxMultiplicity());
        assertFalse(op.getReturnParameter().isUnique());
        assertTrue(op.getReturnParameter().isOrdered());

        // getDataTypes
        op = multiClazz.getOperations("getDataTypes").iterator().next();
        assertNotNull(op);
        assertEquals("org.nuiton.eugene.test21.DataType1", op.getReturnType());
        assertNotNull(op.getReturnParameter());
        assertEquals("org.nuiton.eugene.test21.DataType1", op.getReturnParameter().getType());
        assertEquals(0, op.getReturnParameter().getMinMultiplicity());
        assertEquals(-1, op.getReturnParameter().getMaxMultiplicity());
        assertTrue(op.getReturnParameter().isUnique());
        assertFalse(op.getReturnParameter().isOrdered());

        // addAndGetAll
        op = multiClazz.getOperations("addAndGetAll").iterator().next();
        assertNotNull(op);
        assertEquals("org.nuiton.eugene.test21.DataType1", op.getReturnType());
        assertNotNull(op.getReturnParameter());
        assertEquals("org.nuiton.eugene.test21.DataType1", op.getReturnParameter().getType());
        assertEquals(3, op.getReturnParameter().getMinMultiplicity());
        assertEquals(19, op.getReturnParameter().getMaxMultiplicity());
        assertTrue(op.getReturnParameter().isUnique());
        assertFalse(op.getReturnParameter().isOrdered());

        // param strings
        ObjectModelParameter param = findParameter(op, "strings");
        assertNotNull(param);
        assertEquals("String", param.getType());
        assertEquals(1, param.getMinMultiplicity());
        assertEquals(-1, param.getMaxMultiplicity());
        assertTrue(param.isUnique());
        assertFalse(param.isOrdered());

        // param doubles
        param = findParameter(op, "doubles");
        assertNotNull(param);
        assertEquals("org.nuiton.eugene.test21.double", param.getType());
        assertEquals(5, param.getMinMultiplicity());
        assertEquals(-1, param.getMaxMultiplicity());
        assertTrue(param.isUnique());
        assertTrue(param.isOrdered());

        // param dataTypes
        param = findParameter(op, "dataTypes");
        assertNotNull(param);
        assertEquals("org.nuiton.eugene.test21.DataType1", param.getType());
        assertEquals(1, param.getMinMultiplicity());
        assertEquals(18, param.getMaxMultiplicity());
        assertFalse(param.isUnique());
        assertTrue(param.isOrdered());

    }

    private ObjectModelParameter findParameter(ObjectModelOperation op, String name) {
        for (ObjectModelParameter param : op.getParameters()) {
            if (name.equals(param.getName())) {
                return param;
            }
        }
        return null;
    }


    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on embedded primitive type
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testXSLTestXMI21StaticAndDefaultValueAttribute() throws URISyntaxException, IOException,
            TransformerException {

        ObjectModel model = xmiToObjectModel("TestXMI21");

        // StaticClass
        ObjectModelClass multiClazz = model.getClass("org.nuiton.eugene.test21.StaticClass");

        // strings
        ObjectModelAttribute strings = multiClazz.getAttribute("azerty");
        assertNotNull(strings);
        assertEquals("String", strings.getType());
        assertTrue(strings.isStatic());
        assertEquals("azerty", strings.getDefaultValue());

        // static operation
        ObjectModelClass classD = model.getClass("org.nuiton.eugene.test21.ClassD");
        assertNotNull(classD);
        List<ObjectModelOperation> operations = (List<ObjectModelOperation>) classD.getOperations("getInstance");
        assertEquals(operations.size(), 1);
        assertTrue(operations.get(0).isStatic());
    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on embedded primitive type
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testTestXMI21SuperClasses() throws URISyntaxException, IOException,
            TransformerException {

        ObjectModel model = xmiToObjectModel("TestXMI21");

        // StaticClass
        ObjectModelClass multiClazz = model.getClass("org.nuiton.eugene.test21.StaticClass");

        // SubClass
        ObjectModelClass subClazz = model.getClass("org.nuiton.eugene.test21.SubClass");
        assertNotNull(subClazz);
        assertNotNull(subClazz.getSuperclasses());
        assertEquals(1, subClazz.getSuperclasses().size());
        assertEquals(multiClazz, subClazz.getSuperclasses().iterator().next());

        // TODO test attributes
    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * Test for InnerClasses
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws TransformerException
     */
    @Test
    public void testTestXMI21InnerClasses() throws URISyntaxException, IOException,
            TransformerException {

        ObjectModel model = xmiToObjectModel("TestXMI21");

        // OuterClass
        ObjectModelClass outer = model.getClass("org.nuiton.eugene.test21.OuterClass");

        List<ObjectModelClassifier> inners = (List<ObjectModelClassifier>) outer.getInnerClassifiers();
        assertNotNull(inners);
        assertEquals(inners.size(), 1);

        // InnerClass
        ObjectModelClass inner = (ObjectModelClass) inners.get(0);
        assertNotNull(inner);
        assertNotNull(inner.getDeclaringElement());
        assertEquals(inner.getDeclaringElement().getName(), "OuterClass");
        assertTrue(inner.isInner());
    }

    /**
     * Apply XSL stylesheet on TestXMI21
     * Check documentation tags on model, class, attribute, operation and enumerations
     */
    @Test
    public void testTestXMI21Documentation() throws URISyntaxException, IOException,
            TransformerException {

        EugeneCoreTagValues eugeneTagValues = new EugeneCoreTagValues();

        ObjectModel model = xmiToObjectModel("TestXMI21");

        assertNotNull(model);
        assertEquals("XMITest21", model.getName());
        assertEquals("model doc!", eugeneTagValues.getDocumentationTagValue(model));

        // ClassC
        ObjectModelClass clazzC = model.getClass("org.nuiton.eugene.test21.ClassC");
        assertNotNull(clazzC);
        assertEquals("Class C Comment", clazzC.getDocumentation());

        ObjectModelAttribute attrCost = clazzC.getAttribute("formula");
        assertNotNull(attrCost);
        assertEquals("azerty", attrCost.getDocumentation());

        ObjectModelOperation opLaunchExpression = clazzC.getOperations("launchException").iterator().next();
        assertNotNull(opLaunchExpression);
        assertEquals("launchExcep comment", opLaunchExpression.getDocumentation());

        assertFalse(model.getEnumerations().isEmpty());

        ObjectModelEnumeration myenum = model.getEnumeration("org.nuiton.eugene.test21.MyEnumeration");
        assertNotNull(myenum);
        assertEquals("MyEnumeration is just a simply enumeration in order to test documentation in enumeration.", myenum.getDocumentation());


        // ClassB
        ObjectModelClass clazzB = model.getClass("org.nuiton.eugene.test21.ClassB");
        assertNotNull(clazzB);
        assertEquals("This is some documentation\non multiple lines !\n\nVery hard to do !", clazzB.getDocumentation());

    }

}
