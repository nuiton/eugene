/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


package org.nuiton.eugene.models.object.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelBuilder;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.eugene.models.object.ObjectModelUMLModifier;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * ObjectModelBuilder
 * Created: 3 nov. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public class ObjectModelBuilderTest {

    private static final Log log =
            LogFactory.getLog(ObjectModelBuilderTest.class);

    /**
     * Test of getModel method, of class ObjectModelBuilder.
     * Prerequisite : instanciation of ObjectModelBuilder.
     * - ObjectModel created in ObjectModelBuilder constructor. ObjectModel name will be set.
     */
    @Test
    public void testGetModel() {
        log.debug("getModel");

        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        Assert.assertNotNull(builder.getModel());
        Assert.assertEquals("TestModel", builder.getModel().getName());
    }

    /**
     * Test of addTagValue method, of class ObjectModelBuilder.
     * Prerequisite : none.
     * - Add a tag value to the model
     * - Add a tag value to an element
     */
    @Test
    public void testAddTagValue() {
        log.debug("addTagValue");

        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        builder.addTagValue("fr.ifremer.isisfish.types.Month",
                            "fr.ifremer.isisfish.types.hibernate.MonthType");

        // model.tagvalue.fr.ifremer.isisfish.types.Month=fr.ifremer.isisfish.types.hibernate.MonthType
        ObjectModel model = builder.getModel();
        Assert.assertEquals("fr.ifremer.isisfish.types.hibernate.MonthType",
                            model.getTagValue("fr.ifremer.isisfish.types.Month"));

        // fr.isisfish.entities.Population.attribute.name.tagvalue.pk=topiaId
        ObjectModelClass clazz = builder.createClass("Population", "fr.isisfish.entities");
        ObjectModelAttribute attr = builder.addAttribute(clazz, "name", "java.lang.String");
        builder.addTagValue(attr, "pk", "topiaId");

        Assert.assertEquals("topiaId", attr.getTagValue("pk"));
    }

    /**
     * Test of createClass method, of class ObjectModelBuilder.
     * Prerequisite : none.
     * - Create a class with name and packageName. Qualified name of the class = name + packageName.
     */
    @Test
    public void testCreateClass() {
        log.debug("createClass");

        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClass clazz = builder.createClass("Person", "org.chorem.bonzoms");

        Assert.assertNotNull(clazz);
        Assert.assertEquals("Person", clazz.getName());
        Assert.assertEquals("org.chorem.bonzoms", clazz.getPackageName());
        Assert.assertEquals("org.chorem.bonzoms.Person", clazz.getQualifiedName());
    }

    /** Test of createInterface method, of class ObjectModelBuilder. */
    //@Test
    public void testCreateInterface() {
        log.debug("createInterface");
    }

    /**
     * Test of addAttribute method, of class ObjectModelBuilder.
     * Prerequisite : existing classifier in model.
     * - Add an attribute with no default value
     */
    @Test
    public void testAddAttributeWithoutDefaultValue() {
        log.debug("addAttribute");
        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");
        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelAttribute attribute = builder.addAttribute(classifier, "firstName", "java.lang.String");
        Assert.assertEquals("", attribute.getDefaultValue());
    }

    /**
     * Test of addAttribute method, of class ObjectModelBuilder.
     * Prerequisite : existing classifier in model.
     * - Add a public attribute without default value
     * - Add a private static attribute with default value
     */
    @Test
    public void testAddAttribute() {
        log.debug("addAttribute");
        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelAttribute attribute = builder.addAttribute(classifier, "firstName", "java.lang.String", "",
                                                              ObjectModelJavaModifier.PUBLIC);

        Assert.assertNotNull(attribute);
        Assert.assertEquals("firstName", attribute.getName());
        Assert.assertEquals("java.lang.String", attribute.getType());
        Assert.assertEquals("public", attribute.getVisibility());
        Assert.assertEquals("", attribute.getDefaultValue());
        Assert.assertFalse(attribute.isFinal());
        Assert.assertFalse(attribute.isStatic());

        attribute = builder.addAttribute(classifier, "roles", "java.lang.List<org.chorem.bonzoms.Role>",
                                         "new java.lang.ArrayList<org.chorem.bonzoms.Role>()",
                                         ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.STATIC);

        Assert.assertEquals("new java.lang.ArrayList<org.chorem.bonzoms.Role>()", attribute.getDefaultValue());
        Assert.assertTrue(attribute.isStatic());
    }

    /**
     * Test of addOperation method, of class ObjectModelBuilder.
     * Prerequisite : existing classifier in model.
     * - Add simple public method
     * - Add abstract method
     */
    @Test
    public void testAddOperation() {
        log.debug("addOperation");
        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelOperation result = builder.addOperation(classifier,
                                                           "methodName", "java.util.List<java.lang.String>", ObjectModelJavaModifier.PUBLIC);
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getName(), "methodName");
        Assert.assertEquals(result.getReturnType(), "java.util.List<java.lang.String>");
        Assert.assertEquals(result.getVisibility(), "public");
        Assert.assertFalse(result.isAbstract());

        result = builder.addOperation(classifier, "addPropertyChangeListener", "void",
                                      ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.ABSTRACT);
        Assert.assertTrue(result.isAbstract());
    }

    /**
     * Test of setOperationBody method, of class ObjectModelBuilder.
     * Prerequisite : existing classifier in model and operation associated to this classifier.
     * - Add a body to an existing operation in a classifier
     */
    @Test
    public void testSetOperationBody() {
        log.debug("setOperationBody");

        ObjectModelBuilder builder = new ObjectModelBuilder("modelName");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelOperation operation = builder.addOperation(classifier, "setName", "java.lang.String",
                                                              ObjectModelJavaModifier.PUBLIC);

        builder.setOperationBody(operation, "this.name = name");
        Assert.assertNotNull(operation.getBodyCode());
        Assert.assertEquals(operation.getBodyCode(), "this.name = name");
    }

    /**
     * Test of addInterface method, of class ObjectModelBuilder.
     * Prerequisite : existing classifier in model.
     * - Add an interface not included in the model to a classifier.
     * - TODO : Add an existing interface in the model to a classfier.
     */
    @Test
    public void testAddInterface() {
        log.debug("addInterface");

        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        String interfaceQualifiedName = "java.io.Serializable";
        builder.addInterface(classifier, interfaceQualifiedName);

        Assert.assertNotNull(classifier.getInterfaces());
        Assert.assertEquals(classifier.getInterfaces().size(), 1);
    }

    /**
     * Test of addSuperclass method, of class ObjectModelBuilder.
     * Prerequisite : existing classifier in model.
     * - Add a superclass not included in the model to a classifier.
     * - TODO : Add an existing superclass in the model to a classfier.
     */
    @Test
    public void testAddSuperclass() {
        log.debug("addSuperclass");
        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClass clazz = builder.createClass("Person", "org.chorem.bonzoms");

        String superclassQualifiedName = "org.chorem.bonzoms.Party";

        builder.addSuperclass(clazz, superclassQualifiedName);
        Assert.assertEquals(1, clazz.getSuperclasses().size());
    }

    /**
     * Test of addParameter method, of class ObjectModelBuilder.
     * Prerequisite : existing classifier in model and operation associated to this classifier.
     * - Add a parameter to an existing operation in a classifier
     */
    @Test
    public void testAddParameter() {
        log.debug("addParameter");

        ObjectModelBuilder builder = new ObjectModelBuilder("modelName");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelOperation operation = builder.addOperation(classifier, "setName", "java.lang.String",
                                                              ObjectModelJavaModifier.PUBLIC);

        ObjectModelParameter param = builder.addParameter(operation, "java.lang.String", "name");
        Assert.assertNotNull(param);
        Assert.assertEquals(param.getName(), "name");
        Assert.assertEquals(param.getType(), "java.lang.String");

        List<ObjectModelParameter> listParams = (List<ObjectModelParameter>) operation.getParameters();
        ObjectModelParameter param1 = listParams.get(0);
        Assert.assertEquals(param1, param);
    }

    /**
     * Test of addException method, of class ObjectModelBuilder.
     * Prerequisite : existing classifier in model and operation associated to this classifier.
     * - Add an exception to an existing operation in a classifier
     */
    @Test
    public void testAddException() {
        log.debug("addException");

        ObjectModelBuilder builder = new ObjectModelBuilder("modelName");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelOperation operation = builder.addOperation(classifier, "setName", "java.lang.String",
                                                              ObjectModelJavaModifier.PUBLIC);

        builder.addException(operation, "java.lang.IllegalArgumentException");
        Set<String> exceptions = operation.getExceptions();
        Assert.assertNotNull(exceptions);
        Assert.assertEquals(exceptions.size(), 1);
        Iterator<String> it = exceptions.iterator();
        Assert.assertNotNull(it.hasNext());
        Assert.assertEquals(it.next(), "java.lang.IllegalArgumentException");
    }

    /** Test of setDocumentation method, of class ObjectModelBuilder. */
    //@Test
    public void testSetDocumentation() {
        log.debug("setDocumentation");
    }

    /**
     * Test of addAssociation method, of class ObjectModelBuilder.
     * Prerequisite : two existing classifier in model.
     * - Add an ordered navigable composite association 1..1 from Poll to Vote
     */
    @Test
    public void testAddAssociation() {
        log.debug("addAssociation");

        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClass classA = builder.createClass("Poll", "org.chorem.pollen.business.persistence");

        ObjectModelClass classB = builder.createClass("Vote", "org.chorem.pollen.business.persistence");

        ObjectModelAttribute assoc = builder.addAssociation(classA, classB, "poll", 1, 1,
                                                            ObjectModelUMLModifier.COMPOSITE, ObjectModelUMLModifier.NAVIGABLE, ObjectModelUMLModifier.ORDERED);

        Assert.assertNotNull(assoc);
        Assert.assertTrue(assoc.isNavigable());
        Assert.assertTrue(assoc.isComposite());
        Assert.assertTrue(assoc.isOrdered());
        Assert.assertEquals("poll", assoc.getName());
        Assert.assertEquals(1, assoc.getMinMultiplicity());
        Assert.assertEquals(1, assoc.getMaxMultiplicity());
        Assert.assertEquals("org.chorem.pollen.business.persistence.Vote", assoc.getType());

    }

    /**
     * Test of addReverseAssociation method, of class ObjectModelBuilder.
     * Prerequisite : two existing classifier in model and attribute association existing for one of the
     * classifier.
     * - Add a non navigable association 0..* from Vote to Poll
     */
    @Test
    public void testAddReverseAssociation() {
        log.debug("addReverseAssociation");

        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClass classA = builder.createClass("Poll", "org.chorem.pollen.business.persistence");

        ObjectModelClass classB = builder.createClass("Vote", "org.chorem.pollen.business.persistence");

        ObjectModelAttribute assocA = builder.addAssociation(classA, classB, "poll", 1, 1,
                                                             ObjectModelUMLModifier.COMPOSITE, ObjectModelUMLModifier.NAVIGABLE, ObjectModelUMLModifier.ORDERED);

        ObjectModelAttribute assocB = builder.addReverseAssociation(assocA, "vote", 0, -1);

        Assert.assertEquals(assocA, assocB.getReverseAttribute());
        Assert.assertEquals(assocB, assocA.getReverseAttribute());
    }

    /**
     * Test of createAssociationClass method, of class ObjectModelBuilder.
     * Prerequisite : two existing classifier in model and attribute association existing for both of the
     * classifiers.
     * - Add an association class VoteToChoice between Vote and Choice classes
     */
    @Test
    public void testCreateAssociationClass() {
        log.debug("createAssociationClass");

        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClass classA = builder.createClass("Vote", "org.chorem.pollen.business.persistence");

        ObjectModelClass classB = builder.createClass("Choice", "org.chorem.pollen.business.persistence");

        ObjectModelAttribute assocA = builder.addAssociation(classA, classB, "vote", 0, -1,
                                                             ObjectModelUMLModifier.NAVIGABLE, ObjectModelUMLModifier.ORDERED);

        ObjectModelAttribute assocB = builder.addReverseAssociation(assocA, "choice", 1, -1,
                                                                    ObjectModelUMLModifier.NAVIGABLE, ObjectModelUMLModifier.ORDERED);

        ObjectModelAssociationClass assocClass = builder.createAssociationClass("VoteToChoice",
                                                                                "org.chorem.pollen.business.persistence", assocA, assocB);

        List<ObjectModelClassifier> classifiers = assocClass.getParticipantsClassifiers();

        Assert.assertEquals(2, classifiers.size());
        Assert.assertTrue(classifiers.contains(classA));
        Assert.assertTrue(classifiers.contains(classB));
    }

    /** Test of addStereotype method, of class ObjectModelBuilder. */
    @Test
    public void testAddStereotype() {
        log.debug("addStereotype");

        ObjectModelBuilder builder = new ObjectModelBuilder("TestModel");

        ObjectModelClass classA = builder.createClass("Vote", "org.chorem.pollen.business.persistence");

        Assert.assertEquals(0, classA.getStereotypes().size());

        builder.addStereotype(classA, "entity");

        Assert.assertEquals(1, classA.getStereotypes().size());

    }

}
