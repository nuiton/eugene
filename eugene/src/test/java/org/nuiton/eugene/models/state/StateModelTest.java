/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.Resource;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * StateModelTest.java
 *
 * @author chatellier
 */
public class StateModelTest {

    /** model */
    protected StateModel stateModel;

    @Before
    public void setUp() throws URISyntaxException {

        File[] testFiles = {
                new File(Resource.getURL("models/statemodel/project.statemodel").toURI()),
                new File(Resource.getURL("models/statemodel/contact.statemodel").toURI())
        };

        //TC-20091220 use reader api instead of deprecated generator one
        StateModelReader reader = new StateModelReader();
        stateModel = reader.read(testFiles);

//        StateModelGenerator generator = new StateModelGenerator();
//        generator.generate(testFiles, new File("output"));
//        stateModel = generator.getModel();
    }

    /** various test */
    @Test
    public void testCorrectStateModel() {
        List<StateModelStateChart> lstChart = stateModel.getStateCharts();

        // trois diagrammes
        Assert.assertTrue(lstChart.size() == 3);

        // les 3 ont des états
        for (StateModelStateChart chart : lstChart) {
            Assert.assertFalse(chart.getStates().isEmpty());
        }

        // diagramme contactManagementUseCase
        StateModelStateChart cmChart = null;
        for (StateModelStateChart smsc : lstChart) {
            if ("contactManagementUseCase".equals(smsc.getName())) {
                cmChart = smsc;
            }
        }
        Assert.assertNotNull(cmChart);

        // package
        Assert.assertEquals(cmChart.getPackageName(), "org.codelutin.chorem.web.contactManagement");

        // six etats
        Collection<StateModelState> lstStates = cmChart.getStates();
        Assert.assertEquals(lstStates.size(), 6);

        // le premier etat est initial
        StateModelState initState = null;
        StateModelState sfUCState = null;
        for (StateModelState state : lstStates.toArray(new StateModelState[lstStates.size()])) {
            if ("initContact".equals(state.getName())) {
                initState = state;
            }
            if ("societyFormUC".equals(state.getName())) {
                sfUCState = state;
            }
        }

        // test init
        Assert.assertNotNull(initState);
        Assert.assertFalse(initState.isComplex());
        StateModelSimpleState sInitState = (StateModelSimpleState) initState;
        Assert.assertTrue(sInitState.isInitial());

        // les 3eme etat est complexe et a 4 etat
        Assert.assertNotNull(sfUCState);
        StateModelComplexState cpxState = (StateModelComplexState) sfUCState;
        Assert.assertEquals(cpxState.getStates().size(), 4);

        // test sur un etat
        Collection<StateModelState> lstStatesCpxState = cpxState.getStates();
        StateModelState sfState = null;
        for (StateModelState state : lstStatesCpxState.toArray(new StateModelState[lstStatesCpxState.size()])) {
            if ("societyForm".equals(state.getName())) {
                sfState = state;
            }
        }
        Assert.assertNotNull(sfState);
        Assert.assertEquals(sfState.getTransitions().size(), 2);

        // test transition
        StateModelTransition trEventCancel = null;
        for (StateModelTransition tr : sfState.getTransitions()) {
            if ("cancel".equals(tr.getEvent())) {
                trEventCancel = tr;
            }
        }
        Assert.assertNotNull(trEventCancel);
        Assert.assertNotNull(trEventCancel.getDestinationState());
        Assert.assertEquals(trEventCancel.getDestinationState().getName(), "finalSocietyFormCancel");

        // tagged value test
        Map<String, String> tagValues = stateModel.getTagValues();
        Assert.assertNotNull(tagValues);
        String tagUseCaseEngineExtendedClass = tagValues.get("usecaseengineextendedclass");
        Assert.assertNotNull(tagUseCaseEngineExtendedClass);
        Assert.assertEquals(tagUseCaseEngineExtendedClass, "BasePage");
    }
}
