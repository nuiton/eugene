/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.models.object.reader.XmlObjectModelReader;
import org.nuiton.util.Resource;

import java.io.File;
import java.net.URL;
import java.util.Collection;

/**
 * ModelMergeTest.
 *
 * Created: 18 mai 2005
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class ModelMergeTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ModelMergeTest.class);

    @Test
    public void testMerge1() throws Exception {
        parcourirModel(genModel(1), 4);
    }

    @Test
    public void testMerge2() throws Exception {
        parcourirModel(genModel(2), 4);
    }

    @Test
    public void testMerge3() throws Exception {
        parcourirModel(genModel(3), 4);
    }

    @Test
    public void testMerge4() throws Exception {
        parcourirModel(genModel(4), 4);
    }

    @Test
    public void testMerge5() throws Exception {
        parcourirModel(genModel(5), 4);
    }

    @Test
    public void testMerge6() throws Exception {
        parcourirModel(genModel(6), 4);
    }

    protected ObjectModel genModel(int num) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("\n\n============================================\n\t\ttestMerge" + num + "\n============================================");
        }
//        ObjectModelGenerator generator = new ObjectModelGenerator();
        File[] files = new File[num];
        for (int j = 1; j < num + 1; j++) {
            URL url = Resource.getURL("models/objectmodel/security" + j + ".objectmodel");
            files[j - 1] = new File(url.toURI());
        }

        XmlObjectModelReader generator = new XmlObjectModelReader();
        ObjectModel model = generator.read(files);
        return model;
//        ObjectModelReader reader = new ObjectModelReader();
//        ObjectModel objectModel = reader.read(files);
//        return objectModel;

//        generator.generate(files, new File("target"));
//        return generator.getModel();
    }

    protected void parcourirModel(ObjectModel model, int expectedSize) {
        Collection<ObjectModelClass> classes = model.getClasses();
        Assert.assertEquals(expectedSize, classes.size());
        if (!log.isDebugEnabled()) {
            return;
        }
        StringBuilder buffer = new StringBuilder();
        buffer.append("\n:::: model tag: ").append(model.getTagValues());
        for (Object o3 : classes) {
            ObjectModelClass clazz = (ObjectModelClass) o3;
            buffer.append("\n********* Class : ").append(clazz.getQualifiedName());
            buffer.append("\n--- Attributs :");
            for (Object o2 : clazz.getAttributes()) {
                buffer.append("\n\t").append(o2);
            }
            buffer.append("\n--- Operations :");
            for (Object o1 : clazz.getOperations()) {
                buffer.append("\n\t").append(o1);
            }
            buffer.append("\n--- Interfaces :");
            for (Object o : clazz.getInterfaces()) {
                buffer.append("\n\t").append(o);
            }
            buffer.append("\n--- Superclasses :");
            for (Object o : clazz.getSuperclasses()) {
                buffer.append("\n\t").append(o);
            }
        }
        log.debug(buffer.toString());
    }
}
