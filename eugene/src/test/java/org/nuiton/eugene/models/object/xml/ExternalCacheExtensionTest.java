/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


package org.nuiton.eugene.models.object.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.nuiton.eugene.models.object.ObjectModelInterface;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * ExternalCacheExtensionTest
 * Created: 2 nov. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public class ExternalCacheExtensionTest {

    private static final Log log =
            LogFactory.getLog(ExternalCacheExtensionTest.class);

    /**
     * Test of getCache method, of class ExternalCacheExtension.
     * Prerequisite : Existing model
     * - Add an external interface to cache
     * - No double add, get the existing one
     */
    @Test
    public void testGetCache() {
        log.debug("getCache");

        ObjectModelImpl model = new ObjectModelImpl();
        model.setName("TestModel");

        ObjectModelImplRef refInterface = new ObjectModelImplRef();
        refInterface.setName("java.io.Serializable");

        // Creation if not exist
        ExternalCacheExtension cache =
                model.getExtension(ExternalCacheExtension.OBJECTMODEL_EXTENSION, ExternalCacheExtension.class);

        ObjectModelInterface interfacez =
                cache.getCache(refInterface, ObjectModelInterfaceImpl.class);

        assertNotNull(interfacez);

        ObjectModelInterface interfacez2 =
                cache.getCache(refInterface, ObjectModelInterfaceImpl.class);

        assertEquals(interfacez, interfacez2);
    }

    /**
     * Test of addClassifierToCache method, of class ExternalCacheExtension.
     * Prerequisite : Existing model
     * - Add a classifier which is an interface
     * - The interface getting is the same as the first added classifier
     */
    @Test
    public void testAddClassifierToCache() {
        log.debug("addClassifierToCache");

        ObjectModelImpl model = new ObjectModelImpl();
        model.setName("TestModel");

        ObjectModelImplRef refInterface = new ObjectModelImplRef();
        refInterface.setName("java.io.Serializable");

        ObjectModelClassifierImpl classifier = new ObjectModelInterfaceImpl();

        ExternalCacheExtension cache =
                model.getExtension(ExternalCacheExtension.OBJECTMODEL_EXTENSION, ExternalCacheExtension.class);

        cache.addClassifierToCache(refInterface, classifier);

        assertEquals(classifier.getQualifiedName(), "java.io.Serializable");

        ObjectModelInterfaceImpl interfacez = cache.getCache(refInterface, ObjectModelInterfaceImpl.class);

        assertEquals(interfacez, classifier);
    }

}
