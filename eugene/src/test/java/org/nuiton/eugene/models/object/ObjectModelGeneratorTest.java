/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.object.xml.ObjectModelClassImpl;

/**
 * Test des fonctions de ObjectModelGenerator.
 *
 * @author chatellier
 */
public class ObjectModelGeneratorTest {

    @Test
    public void testCanGenerate() {

        ObjectModelGenerator generator = new ObjectModelGenerator();

        // test with null generated list
//        Assert.assertTrue(generator.canGenerateElement(new ObjectModelElementImpl()));
        Assert.assertTrue(generator.canGenerateElement(new ObjectModelClassImpl()));

//        List<String> generatedPackages = new ArrayList<String>();
//        generatedPackages.add("org.nuiton.eugene");

        generator.setProperty(Template.PROP_GENERATED_PACKAGES, "org.nuiton.eugene");
//        generator.setGeneratedPackages(generatedPackages);

        // Still true
//        Assert.assertTrue(generator.canGenerateElement(new ObjectModelElementImpl()));
        // become false
        boolean b = generator.canGenerateElement(new ObjectModelClassImpl());
        Assert.assertFalse(b);

        ObjectModelClassImpl testClass = new ObjectModelClassImpl();
        testClass.setPackage("org.nuiton.eugene");
        Assert.assertTrue(generator.canGenerateElement(testClass));

        testClass.setPackage("org.nuiton");
        Assert.assertFalse(generator.canGenerateElement(testClass));

        testClass.setPackage("org.nuiton.eugene.entities");
        Assert.assertTrue(generator.canGenerateElement(testClass));

        testClass.setPackage("org.nuiton.eugene2");
        Assert.assertFalse(generator.canGenerateElement(testClass));

        testClass.setPackage("org.nuiton.eugene2.entities");
        Assert.assertFalse(generator.canGenerateElement(testClass));

        // test avec les generators recursifs
        // sans faire de set sur le fils
        ObjectModelGenerator childGenerator = new ObjectModelGenerator(generator);

        testClass.setPackage("org.nuiton.eugene");
        Assert.assertTrue(childGenerator.canGenerateElement(testClass));

        testClass.setPackage("org.nuiton");
        Assert.assertFalse(childGenerator.canGenerateElement(testClass));

        testClass.setPackage("org.nuiton.eugene.entities");
        Assert.assertTrue(childGenerator.canGenerateElement(testClass));

        testClass.setPackage("org.nuiton.eugene2");
        Assert.assertFalse(childGenerator.canGenerateElement(testClass));

        testClass.setPackage("org.nuiton.eugene2.entities");
        Assert.assertFalse(childGenerator.canGenerateElement(testClass));

    }
}
