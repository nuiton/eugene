/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.java.extension.ImportsManagerExtension;

/**
 * @author Florian Desbois - desbois@codelutin.com
 */
public class ObjectModelImplTest {

    private static final Log log = LogFactory.getLog(ObjectModelImplTest.class);

//    /**
//     * Test of setName method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testSetName() {
//        System.out.println("setName");
//        String name = "";
//        ObjectModelImpl instance = new ObjectModelImpl();
//        instance.setName(name);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addClass method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testAddClass() {
//        System.out.println("addClass");
//        ObjectModelClassImpl clazz = null;
//        ObjectModelImpl instance = new ObjectModelImpl();
//        instance.addClass(clazz);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addAssociationClass method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testAddAssociationClass() {
//        System.out.println("addAssociationClass");
//        ObjectModelAssociationClassImpl clazz = null;
//        ObjectModelImpl instance = new ObjectModelImpl();
//        instance.addAssociationClass(clazz);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addComment method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testAddComment() {
//        System.out.println("addComment");
//        String comment = "";
//        ObjectModelImpl instance = new ObjectModelImpl();
//        instance.addComment(comment);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getName method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetName() {
//        System.out.println("getName");
//        ObjectModelImpl instance = new ObjectModelImpl();
//        String expResult = "";
//        String result = instance.getName();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getClassifiers method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetClassifiers() {
//        System.out.println("getClassifiers");
//        ObjectModelImpl instance = new ObjectModelImpl();
//        Collection expResult = null;
//        Collection result = instance.getClassifiers();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getClassifier method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetClassifier() {
//        System.out.println("getClassifier");
//        String qualifiedClassifierName = "";
//        ObjectModelImpl instance = new ObjectModelImpl();
//        ObjectModelClassifier expResult = null;
//        ObjectModelClassifier result = instance.getClassifier(qualifiedClassifierName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getClasses method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetClasses() {
//        System.out.println("getClasses");
//        ObjectModelImpl instance = new ObjectModelImpl();
//        Collection expResult = null;
//        Collection result = instance.getClasses();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getClass method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetClass() {
//        System.out.println("getClass");
//        String qualifiedClassName = "";
//        ObjectModelImpl instance = new ObjectModelImpl();
//        ObjectModelClass expResult = null;
//        ObjectModelClass result = instance.getClass(qualifiedClassName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of hasClass method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testHasClass() {
//        System.out.println("hasClass");
//        String qualifiedClassName = "";
//        ObjectModelImpl instance = new ObjectModelImpl();
//        boolean expResult = false;
//        boolean result = instance.hasClass(qualifiedClassName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addInterface method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testAddInterface() {
//        System.out.println("addInterface");
//        ObjectModelInterfaceImpl interfacez = null;
//        ObjectModelImpl instance = new ObjectModelImpl();
//        instance.addInterface(interfacez);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getInterface method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetInterface() {
//        System.out.println("getInterface");
//        String qualifiedInterfaceName = "";
//        ObjectModelImpl instance = new ObjectModelImpl();
//        ObjectModelInterface expResult = null;
//        ObjectModelInterface result = instance.getInterface(qualifiedInterfaceName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getInterfaces method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetInterfaces() {
//        System.out.println("getInterfaces");
//        ObjectModelImpl instance = new ObjectModelImpl();
//        Collection expResult = null;
//        Collection result = instance.getInterfaces();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addEnumeration method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testAddEnumeration() {
//        System.out.println("addEnumeration");
//        ObjectModelEnumerationImpl enumeration = null;
//        ObjectModelImpl instance = new ObjectModelImpl();
//        instance.addEnumeration(enumeration);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getEnumerations method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetEnumerations() {
//        System.out.println("getEnumerations");
//        ObjectModelImpl instance = new ObjectModelImpl();
//        Collection expResult = null;
//        Collection result = instance.getEnumerations();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getEnumeration method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetEnumeration() {
//        System.out.println("getEnumeration");
//        String qualifiedEnumerationName = "";
//        ObjectModelImpl instance = new ObjectModelImpl();
//        ObjectModelEnumeration expResult = null;
//        ObjectModelEnumeration result = instance.getEnumeration(qualifiedEnumerationName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getComments method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetComments() {
//        System.out.println("getComments");
//        ObjectModelImpl instance = new ObjectModelImpl();
//        List expResult = null;
//        List result = instance.getComments();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of mergeClassifiers method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testMergeClassifiers() {
//        System.out.println("mergeClassifiers");
//        ObjectModelClassifierImpl initialClazzifier = null;
//        ObjectModelClassifierImpl additionalClazzifier = null;
//        ObjectModelImpl instance = new ObjectModelImpl();
//        instance.mergeClassifiers(initialClazzifier, additionalClazzifier);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addTagValue method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testAddTagValue() {
//        System.out.println("addTagValue");
//        ObjectModelImplTagValue tagValue = null;
//        ObjectModelImpl instance = new ObjectModelImpl();
//        ObjectModelImplTagValue expResult = null;
//        ObjectModelImplTagValue result = instance.addTagValue(tagValue);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getTagValues method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetTagValues() {
//        System.out.println("getTagValues");
//        ObjectModelImpl instance = new ObjectModelImpl();
//        Map expResult = null;
//        Map result = instance.getTagValues();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getTagValue method, of class ObjectModelImpl.
//     */
//    @Test
//    public void testGetTagValue() {
//        System.out.println("getTagValue");
//        String tagValue = "";
//        ObjectModelImpl instance = new ObjectModelImpl();
//        String expResult = "";
//        String result = instance.getTagValue(tagValue);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getExtension method, of class ObjectModelImpl.
     * - Create and get ImportsManagerExtension
     * - No double creation
     */
    @Test
    public void testGetExtension() {
        log.debug("getExtension");

        ObjectModelImpl model = new ObjectModelImpl();
        model.setName("TestModel");

        // Creation if not exist
        ImportsManagerExtension result =
                model.getExtension(ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        Assert.assertNotNull(result);

        ImportsManagerExtension result2 =
                model.getExtension(ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        Assert.assertEquals(result, result2);
    }

}
