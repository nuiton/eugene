/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.eugene.TestHelper;
import org.nuiton.eugene.models.object.reader.XmlObjectModelReader;
import org.nuiton.util.FileUtil;
import org.nuiton.util.Resource;
import org.nuiton.util.ResourceResolver;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Test de la feuille de style "xmi1.2ToObjectModel.xsl"
 * sur l'exemple isis-fish.xmi
 *
 * @author chatellier
 */
public class XMI12ToObjectModelTest {

    protected static File destinationDirectory;

    @BeforeClass
    public static void beforeTest() throws IOException {
        destinationDirectory = TestHelper.getTestBasedir(XMI12ToObjectModelTest.class);
        FileUtil.createDirectoryIfNecessary(destinationDirectory);
    }

//    @Before
//    public void setUp() {
//        destinationDirectory = new File("target", "xmi");
//        destinationDirectory.mkdirs();
//    }

    /**
     * Apply XSLT Transformation.
     *
     * @param xmiFile
     * @param modelFile
     * @return transformed file
     * @throws IOException
     * @throws TransformerException
     */
    protected File transformXMI(File xmiFile, String modelFile)
            throws IOException, TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();

        URL xsl = Resource.getURL("xmi1.2ToObjectModel.xsl");

        File result = new File(destinationDirectory, modelFile);

        Transformer transformer = factory.newTransformer(new StreamSource(xsl
                                                                                  .openStream()));

        transformer.setURIResolver(new ResourceResolver());

        try (FileOutputStream out = new FileOutputStream(result)) {
            transformer.transform(new StreamSource(xmiFile), new StreamResult(out));
        }
        return result;
    }

    /**
     * Load model into memory.
     *
     * @param modelFile
     * @return object model
     * @throws IOException
     */
    protected ObjectModel loadModel(File modelFile) throws IOException {
//        ObjectModelGenerator generator = new ObjectModelGenerator();
        XmlObjectModelReader generator = new XmlObjectModelReader();
        ObjectModel model = generator.read(modelFile);
        return model;
//        ObjectModelReader reader = new ObjectModelReader();
//        ObjectModel model = reader.read(modelFile);
//        return model;
//        generator.applyTemplate(model);
//        generator.generate(new File[] { modelFile }, new File("output"));
//        ObjectModel objectModel = generator.getModel();
//        return objectModel;
    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on it.
     *
     * @throws Exception
     */
    @Test
    public void testXSLIsis() throws Exception {

        File xmiFile = new File(Resource.getURL("xmi/1.2/isis-fish.xmi")
                                        .toURI());

        File objectModelFile = transformXMI(xmiFile, "isis-fish.objectmodel");

        ObjectModel model = loadModel(objectModelFile);

        Assert.assertNotNull(model);
        Assert.assertEquals("IsisFish", model.getName());
        Assert.assertEquals(62, model.getClassifiers().size());
    }

    /**
     * Apply XSL stylesheet on a topcased model.
     * And make test on it.
     *
     * @throws Exception
     */
    @Test
    public void testXSLTopia() throws Exception {

        File xmiFile = new File(Resource.getURL("xmi/1.2/topiatest.xmi")
                                        .toURI());

        File objectModelFile = transformXMI(xmiFile, "topiatest.objectmodel");

        ObjectModel model = loadModel(objectModelFile);

        // Test for model version
        Assert.assertEquals("1.2", model.getVersion());

        Assert.assertNotNull(model);
        Assert.assertEquals("TopiaTest", model.getName());
        Assert.assertEquals(22, model.getClassifiers().size());

        // Test for aggregation attribute (relation)
        ObjectModelClass personneClass = model.getClass("org.nuiton.topiatest.Personne");
        Assert.assertNotNull(personneClass);
        ObjectModelAttribute addressAttr = personneClass.getAttribute("address");
        Assert.assertNotNull(addressAttr);
        Assert.assertTrue(addressAttr.isAggregate());

        // Test for inner class
        ObjectModelClass storeClass = model.getClass("org.nuiton.topiatest.Store");
        Assert.assertNotNull(storeClass);
        List<ObjectModelClassifier> inners = (List<ObjectModelClassifier>) storeClass.getInnerClassifiers();
        Assert.assertNotNull(inners);
        Assert.assertEquals(inners.size(), 1);
        ObjectModelClass rowClass = (ObjectModelClass) inners.get(0);
        Assert.assertNotNull(rowClass);
        Assert.assertEquals(rowClass.getDeclaringElement().getName(), "Store");
    }

    /**
     * Apply XSL stylesheet on an Argouml model.
     * And make test on it.
     *
     * @throws Exception
     */
    @Test
    public void testXSLDependency() throws Exception {

        File xmiFile = new File(Resource.getURL("xmi/1.2/dependency.xmi")
                                        .toURI());

        File objectModelFile = transformXMI(xmiFile, "dependency.objectmodel");

        ObjectModel model = loadModel(objectModelFile);

        Assert.assertNotNull(model);
        Assert.assertEquals("DependencyTest", model.getName());
        Assert.assertEquals(4, model.getClassifiers().size());

        int nbDependencies = 0;
        for (ObjectModelClassifier classifier : model.getClassifiers()) {
            nbDependencies += classifier.getDependencies().size();
            for (ObjectModelDependency dependency : classifier.getDependencies()) {
                Assert.assertNotNull(dependency.getSupplier());
            }
        }

        Assert.assertEquals(4, nbDependencies);
    }

    /**
     * Apply XSL stylesheet on an Argouml model.
     * And make test on it.
     *
     * @throws Exception
     */
    @Test
    public void testXSLEnumeration() throws Exception {

        File xmiFile = new File(Resource.getURL("xmi/1.2/enumeration.xmi")
                                        .toURI());

        File objectModelFile = transformXMI(xmiFile, "enumeration.objectmodel");

        ObjectModel model = loadModel(objectModelFile);

        Assert.assertNotNull(model);
        Assert.assertEquals("EnumerationTest", model.getName());
        Assert.assertEquals(1, model.getEnumerations().size());

        for (ObjectModelEnumeration enumeration : model.getEnumerations()) {
            // 1 seule énumeration avec 3 literals et 2 opérations
            Assert.assertNotNull(enumeration.getQualifiedName());
            Assert.assertEquals(3, enumeration.getLiterals().size());
            Assert.assertEquals(2, enumeration.getOperations().size());
        }
    }

}
