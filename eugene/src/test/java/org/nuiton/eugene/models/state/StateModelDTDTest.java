/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.state;

import org.junit.Test;
import org.nuiton.util.Resource;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * StateModelTest.java
 *
 * @author chatellier
 */
public class StateModelDTDTest {

    /**
     * Test la validation des DTD sur les modeles XML.
     *
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws URISyntaxException           FIXME this test doesn't valid anything
     */
    @Test
    public void testCorrectStateModel() throws ParserConfigurationException, SAXException, IOException, URISyntaxException {

        File[] testFiles = {
                new File(Resource.getURL("models/statemodel/project.statemodel").toURI()),
                new File(Resource.getURL("models/statemodel/contact.statemodel").toURI())
        };

        for (File file : testFiles) {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(true);
            factory.setNamespaceAware(true);
            SAXParser parser = factory.newSAXParser();

            // --- Set Input source

            try (FileInputStream stream = new FileInputStream(file)) {
                InputSource source = new InputSource(stream);
                // --- parse
                XMLReader reader = parser.getXMLReader();
                reader.setContentHandler(new DefaultHandler());
                reader.setErrorHandler(new DefaultHandler());
                reader.setEntityResolver(new EntityResolver() {

                    @Override
                    public InputSource resolveEntity(String publicId, String systemId)
                            throws SAXException, IOException {

                        String dtd = systemId.substring(systemId.lastIndexOf("/"));

                        URL url = Resource.getURL("dtd/" + dtd);
                        InputSource source;
                        try {
                            source = new InputSource(new FileInputStream(new File(url.toURI())));
                        } catch (URISyntaxException e) {
                            throw new SAXException(e);
                        }

                        return source;
                    }
                });
                reader.parse(source);
            }
        }
    }
}
