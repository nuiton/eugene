package org.nuiton.eugene;

/*-
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.eugene.models.extension.tagvalue.MismatchTagValueTargetException;
import org.nuiton.eugene.models.extension.tagvalue.TagValueNotFoundException;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.xml.ObjectModelClassImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelPackageImpl;

/**
 * Created on 24/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class EugeneCoreTagValuesTest {

    protected EugeneCoreTagValues provider;

    @Before
    public void setUp() throws Exception {
        provider = new EugeneCoreTagValues();
    }

    @Test
    public void validate() throws Exception {
        validate(EugeneCoreTagValues.Store.constantPrefix.getName(), true, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelClass.class, ObjectModelEnumeration.class);
        validate(EugeneCoreTagValues.Store.i18n.getName(), true, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelClass.class, ObjectModelEnumeration.class);
        validate(EugeneCoreTagValues.Store.version.getName(), true, ObjectModel.class);

        validate(EugeneCoreTagValues.Store.constantPrefix.getName(), false, ObjectModelOperation.class, ObjectModelAttribute.class);
        validate(EugeneCoreTagValues.Store.i18n.getName(), false, ObjectModelOperation.class, ObjectModelAttribute.class);
        validate(EugeneCoreTagValues.Store.version.getName(), false, ObjectModelPackage.class, ObjectModelClassifier.class, ObjectModelOperation.class, ObjectModelAttribute.class);

        validate(EugeneCoreTagValues.Store.ordered.name(), true, ObjectModelAttribute.class);
        validate(EugeneCoreTagValues.Store.ordered.name(), false, ObjectModel.class, ObjectModelOperation.class, ObjectModelClassifier.class, ObjectModelClass.class, ObjectModelEnumeration.class, ObjectModelInterface.class);

        long l = System.nanoTime();
        validate(EugeneCoreTagValues.Store.constantPrefix.getName() + l, false, ObjectModelOperation.class, ObjectModelPackage.class, ObjectModelAttribute.class);
        validate(EugeneCoreTagValues.Store.i18n.getName() + l, false, ObjectModelOperation.class, ObjectModelPackage.class, ObjectModelAttribute.class);
        validate(EugeneCoreTagValues.Store.version.getName() + l, false, ObjectModelClassifier.class, ObjectModelPackage.class, ObjectModelOperation.class, ObjectModelAttribute.class);

        validate(EugeneCoreTagValues.Store.ordered.name() + l, false, ObjectModel.class, ObjectModelOperation.class, ObjectModelClassifier.class, ObjectModelClass.class, ObjectModelEnumeration.class, ObjectModelInterface.class);

    }

    @Test
    public void testDefaultValue() throws Exception {
        Assert.assertEquals("PROPERTY_", provider.getConstantPrefixTagValue(new ObjectModelClassImpl(), new ObjectModelPackageImpl(), new ObjectModelImpl()));
        Assert.assertEquals(false, provider.isGenerateBooleanGetMethods(new ObjectModelClassImpl(), new ObjectModelPackageImpl(), new ObjectModelImpl()));
        // No default value
        Assert.assertNull(provider.getI18nPrefixTagValue(new ObjectModelClassImpl(), new ObjectModelPackageImpl(), new ObjectModelImpl()));
    }

    protected void validate(String name, boolean expected, Class<?>... types) {
        for (Class<?> type : types) {

            try {
                provider.validate(name, type);
                Assert.assertTrue(expected);
            } catch (TagValueNotFoundException | MismatchTagValueTargetException e) {
                Assert.assertFalse(expected);
            }
        }
    }
}