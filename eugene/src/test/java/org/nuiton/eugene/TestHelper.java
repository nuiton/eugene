/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;

import java.io.File;

/**
 * Helper for all eugene tests.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3
 */
@Ignore
// this is not a test :)
public class TestHelper {

    private static final Log log = LogFactory.getLog(TestHelper.class);

    protected static File testBasedir;

    protected static File targetdir;

    protected static File dirDatabase;

    public static File getTestWorkdir() {
        if (testBasedir == null) {
            String base = System.getProperty("java.io.tmpdir");
            if (base == null || base.isEmpty()) {
                base = new File("").getAbsolutePath();
            }
            testBasedir = new File(base);
            log.info("basedir for test " + testBasedir);
        }
        return testBasedir;
    }

    public static File getTestBasedir(Class<?> testClass) {
        File dir = getTestWorkdir();
        File result = new File(dir, testClass.getName());
        return result;
    }

}
