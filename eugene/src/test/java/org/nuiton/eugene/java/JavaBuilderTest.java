/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.GeneratorUtilTest;
import org.nuiton.eugene.java.extension.ImportsManagerExtension;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelBuilderTest;

import java.util.List;

/**
 * JavaBuilderTest
 * Created: 29 oct. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public class JavaBuilderTest {

    private static final Log log = LogFactory.getLog(JavaBuilderTest.class);

    /**
     * Test of getModel.
     * No test needed : model created in modelBuilder instanciated in constructor (ObjectModelBuilder).
     *
     * @see ObjectModelBuilderTest#testGetModel()
     */
    @Test
    public void testGetModel() {
        log.debug("getModel");

        JavaBuilder builder = new JavaBuilder("TestModel");

        ObjectModel model = builder.getModel();
        Assert.assertNotNull(model);
    }

    /**
     * Test of addImport method, of class JavaBuilder.
     * Prerequisite : existing classifier in model.
     * - Add a simple type to import.
     * - Add a type to import with generic (List of Category) : two imports are added.
     * NOTE : Use of ImportsManager addImport() method and GeneratorUtil getTypesList() method.
     *
     * @see GeneratorUtilTest#testGetTypesList()
     */
    @Test
    public void testAddImport() {
        log.debug("addImport");
        JavaBuilder builder = new JavaBuilder("TestModel");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");
        String imports = "java.beans.PropertyChangeListener";
        builder.addImport(classifier, imports);

        ObjectModel model = builder.getModel();
        ImportsManagerExtension importsExtension =
                model.getExtension(ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        String type = "java.util.List<org.chorem.cash.Category>"; // type add : 2 imports added
        builder.addImport(classifier, type);
        Assert.assertEquals(3, importsExtension.getImports(classifier).size());
    }

    /**
     * Test of createClass method, of class JavaBuilder.
     * No test needed : call of createClass in ObjectModelBuilder
     *
     * @see ObjectModelBuilderTest#testCreateClass()
     */
    @Test
    public void testCreateClass() {
        log.debug("createClass");

        JavaBuilder builder = new JavaBuilder("TestModel");

        ObjectModelClass modelClass = builder.createClass("Person", "org.chorem.bonzoms");
        Assert.assertNotNull(modelClass);
        Assert.assertEquals("Person", modelClass.getName());
        Assert.assertEquals("org.chorem.bonzoms", modelClass.getPackageName());
    }

    /**
     * Test of createAbstractClass method, of class JavaBuilder.
     * Prerequisite : none.
     * - Create an abstract class.
     */
    @Test
    public void testCreateAbstractClass() {
        log.debug("createAbstractClass");
        JavaBuilder builder = new JavaBuilder("TestModel");

        ObjectModelClass clazz = builder.createAbstractClass("Person", "org.chorem.bonzoms");
        Assert.assertNotNull(clazz);
        Assert.assertEquals("Person", clazz.getName());
        Assert.assertEquals("org.chorem.bonzoms", clazz.getPackageName());
        Assert.assertTrue(clazz.isAbstract());
    }

    /**
     * Test of setSuperClass method, of class JavaBuilder.
     * Only one superclass can be set.
     * Check imports on superclass qualified name
     *
     * @see ObjectModelBuilderTest#testAddSuperclass()
     */
    @Test
    public void testSetSuperClass() {
        log.debug("setSuperClass");
        JavaBuilder builder = new JavaBuilder("TestModel");

        ObjectModelClass clazz = builder.createClass("Person", "org.chorem.bonzoms");

        String superclassQualifiedName = "org.chorem.bonzoms.Party";
        builder.setSuperClass(clazz, superclassQualifiedName);

        superclassQualifiedName = "org.chorem.bonzoms.NamedElement";
        builder.setSuperClass(clazz, superclassQualifiedName);

        Assert.assertEquals(1, clazz.getSuperclasses().size());

        // Check imports
        ImportsManagerExtension ext = builder.getModel().getExtension(
                ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        List<String> imports = ext.getImports(clazz);
        Assert.assertEquals(0, imports.size());
    }

    /**
     * Test of addInterface method, of class JavaBuilder.
     * Check imports on adding interface : imports added on interfaceQualifiedName
     *
     * @see ObjectModelBuilderTest#testAddInterface()
     */
    @Test
    public void testAddInterface() {
        log.debug("addInterface");
        JavaBuilder builder = new JavaBuilder("TestModel");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        String interfaceQualifiedName = "java.io.Serializable";
        builder.addInterface(classifier, interfaceQualifiedName);

        // Check imports
        ImportsManagerExtension ext = builder.getModel().getExtension(
                ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        List<String> imports = ext.getImports(classifier);
        Assert.assertEquals(1, imports.size());
        Assert.assertTrue(imports.contains("java.io.Serializable"));
    }

    /**
     * Test of addConstant method, of class JavaBuilder.
     * Prerequisite : existing classifier in model.
     * - Add a public constant attribute (static, final)
     */
    @Test
    public void testAddConstant() {
        log.debug("addConstant");
        JavaBuilder builder = new JavaBuilder("TestModel");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelAttribute constant = builder.addConstant(classifier, "GENDER_MALE", "boolean", "true",
                                                            ObjectModelJavaModifier.PUBLIC);

        Assert.assertTrue(constant.isFinal());
        Assert.assertTrue(constant.isStatic());
    }

    /**
     * Test of addAttribute method, of class JavaBuilder.
     * Check imports on adding attribute : imports added on type and value.
     *
     * @see ObjectModelBuilderTest#testAddAttribute()
     */
    @Test
    public void testAddAttribute() {
        log.debug("addAttribute");
        JavaBuilder builder = new JavaBuilder("TestModel");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        builder.addAttribute(classifier, "firstName", "java.lang.String", "",
                             ObjectModelJavaModifier.PUBLIC);

        builder.addAttribute(classifier, "roles", "java.util.List<org.chorem.bonzoms.Role>",
                             "new ArrayList<Role>()",
                             ObjectModelJavaModifier.PRIVATE, ObjectModelJavaModifier.STATIC);

        // ANO#474 : manage manually imports for defaultValue
        builder.addImport(classifier, "java.util.ArrayList");

        // Check imports
        ImportsManagerExtension ext = builder.getModel().getExtension(
                ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        List<String> imports = ext.getImports(classifier);
        // no imports for org.chorem.bonzoms.Role and java.lang.String
        Assert.assertEquals(2, imports.size());
        Assert.assertTrue(imports.contains("java.util.ArrayList"));
        Assert.assertTrue(imports.contains("java.util.List"));
    }

    /**
     * Test of addAttribute method, of class JavaBuilder.
     * Prerequisite : existing classifier in model.
     * - Add an attribute from an existing one
     */
    @Test
    public void testAddAttributeObjectModelClassifierObjectModelAttribute() {
        log.debug("addAttribute");
        JavaBuilder builder = new JavaBuilder("modelName");

        ObjectModelAttributeImpl attribute = new ObjectModelAttributeImpl();
        attribute.setName("name");
        attribute.setType("java.lang.String");
        attribute.setVisibility("public");
        attribute.setStatic(false);
        attribute.setFinal(true);
        attribute.setDefaultValue("\"\"");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelAttribute result = builder.addAttribute(classifier, attribute);

        Assert.assertNotSame(attribute, result);
    }

    /**
     * Test of addConstructor method, of class JavaBuilder.
     * Prerequisite : existing class in model.
     * - Add a constructor to an existing class.
     * NOTE : operation.getReturnType doesn't get the good type (ie. "void")
     */
    @Test
    public void testAddConstructor() {
        log.debug("addConstructor");
        JavaBuilder builder = new JavaBuilder("modelName");

        ObjectModelClass clazz = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelOperation operation = builder.addConstructor(clazz, ObjectModelJavaModifier.PUBLIC);
        Assert.assertNotNull(operation);
        Assert.assertEquals(operation.getName(), "Person");
        Assert.assertNull(operation.getReturnParameter());
        Assert.assertFalse(operation.isAbstract());
    }

    /**
     * Test of addOperation method, of class JavaBuilder.
     * Check imports on adding operation : returnType of the operation
     *
     * @see ObjectModelBuilderTest#testAddOperation()
     */
    @Test
    public void testAddOperation() {
        log.debug("addOperation");

        JavaBuilder builder = new JavaBuilder("modelName");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        builder.addOperation(classifier,
                             "methodName", "java.util.List<java.lang.String>", ObjectModelJavaModifier.PUBLIC);

        builder.addOperation(classifier,
                             "addPropertyChangeListener", null,
                             ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.ABSTRACT);

        // Check imports
        ImportsManagerExtension ext = builder.getModel().getExtension(
                ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        List<String> imports = ext.getImports(classifier);
        Assert.assertEquals(1, imports.size());
        Assert.assertTrue(imports.contains("java.util.List"));
    }

    /**
     * Test of addParameter method, of class JavaBuilder.
     * Check imports on adding parameter : type of the parameter
     *
     * @see ObjectModelBuilderTest#testAddParameter()
     */
    @Test
    public void testAddParameter() {
        log.debug("addParameter");

        JavaBuilder builder = new JavaBuilder("modelName");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelOperation operation = builder.addOperation(classifier, "setName", "java.lang.String",
                                                              ObjectModelJavaModifier.PUBLIC);

        builder.addParameter(operation, "java.lang.String", "name");

        // Check imports
        ImportsManagerExtension ext = builder.getModel().getExtension(
                ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        List<String> imports = ext.getImports(classifier);
        Assert.assertEquals(0, imports.size());

    }

    /**
     * Test of addException method, of class JavaBuilder.
     * Check imports on adding exception : exception name
     *
     * @see ObjectModelBuilderTest#testAddException()
     */
    @Test
    public void testAddException() {
        log.debug("addException");

        JavaBuilder builder = new JavaBuilder("modelName");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelOperation operation = builder.addOperation(classifier, "setName", "java.lang.String",
                                                              ObjectModelJavaModifier.PUBLIC);

        builder.addException(operation, "java.lang.IllegalArgumentException");

        // Check imports
        ImportsManagerExtension ext = builder.getModel().getExtension(
                ImportsManagerExtension.OBJECTMODEL_EXTENSION, ImportsManagerExtension.class);

        List<String> imports = ext.getImports(classifier);
        Assert.assertEquals(0, imports.size());
    }

    /**
     * Test of setOperationBody method, of class JavaBuilder.
     * No body code for an abstract operation : throw an IllegalArgumentException
     *
     * @see ObjectModelBuilderTest#testSetOperationBody()
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetOperationBody() {
        log.debug("setOperationBody");

        JavaBuilder builder = new JavaBuilder("modelName");

        ObjectModelClassifier classifier = builder.createClass("Person", "org.chorem.bonzoms");

        ObjectModelOperation operation = builder.addOperation(classifier, "setName", "java.lang.String",
                                                              ObjectModelJavaModifier.PUBLIC, ObjectModelJavaModifier.ABSTRACT);


        builder.setOperationBody(operation, "this.name = name");

    }

}
