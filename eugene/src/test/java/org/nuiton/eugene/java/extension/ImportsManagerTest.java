/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java.extension;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class ImportsManagerTest {

    private ImportsManager mgr;

    @Before
    public void setUp() throws Exception {
        mgr = new ImportsManager();
    }

    @After
    public void tearDown() throws Exception {
        mgr.clearImports();
        mgr = null;
    }

    @Test
    public void getReturnType() throws Exception {
        String actual;
        String expected;

        expected = null;
        actual = mgr.getReturnType(null);
        Assert.assertEquals(expected, actual);

        expected = "List";
        actual = mgr.getReturnType("java.util.List");
        Assert.assertEquals(expected, actual);

        expected = "List<Integer>";
        actual = mgr.getReturnType("java.util.List<Integer>");
        Assert.assertEquals(expected, actual);


        expected = "List<List<Integer>>";
        actual = mgr.getReturnType("java.util.List<java.util.List<Integer>>");
        Assert.assertEquals(expected, actual);

        expected = "Map<Integer, Integer>";
        actual = mgr.getReturnType("java.util.Map< Integer,Integer>");
        Assert.assertEquals(expected, actual);

        expected = "Map<Integer, List<Integer>>";
        actual = mgr.getReturnType("java.util.Map<Integer,java.util.List<Integer>>");
        Assert.assertEquals(expected, actual);

        expected = "<O> Map<O, List<Integer>>";
        actual = mgr.getReturnType("<O> java.util.Map<O,   java.util.List<Integer>>");
        Assert.assertEquals(expected, actual);

        expected = "<O extends java.util.List> Map<O, List<Toto>>";
        actual = mgr.getReturnType("<O extends java.util.List> java.util.Map<O,   java.util.List<a.b.Toto>>");
        Assert.assertEquals(expected, actual);

    }

    @Test
    public void testAddImport() throws Exception {

        Assert.assertFalse(mgr.addImport((String) null)); // Not accept null value

        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.Toto"));
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.Toto")); //Repeat to test acceptance
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.Tutu"));
        Assert.assertFalse(mgr.addImport("org.chorem.eugene.Tutu")); //Conflict, package differs

        Assert.assertTrue(mgr.addImport("void"));
        Assert.assertTrue(mgr.addImport("boolean"));
        Assert.assertTrue(mgr.addImport("Byte"));
        Assert.assertTrue(mgr.addImport("java.lang.Toto"));
        Assert.assertFalse(mgr.addImport("java.lang.sub.Toto"));

        Assert.assertTrue(mgr.addImport(Serializable.class));
        Assert.assertFalse(mgr.addImport("org.nuiton.eugene.Serializable"));

        Assert.assertTrue(mgr.addImport(File.class.getName() + "[]"));
        Assert.assertTrue(mgr.addImport(File.class)); // was already import just above
    }

    @Test
    public void testGetType() throws Exception {
        Assert.assertEquals("Toto", mgr.getType("org.nuiton.eugene.Toto"));
        Assert.assertEquals("Toto", mgr.getType("org.nuiton.eugene.Toto")); //Repeat to test acceptance
        Assert.assertEquals("Tutu", mgr.getType("org.nuiton.eugene.Tutu"));
        Assert.assertEquals("Tutu<Toto>", mgr.getType("org.nuiton.eugene.Tutu<org.nuiton.eugene.Toto>"));

        Assert.assertEquals("org.chorem.eugene.Tutu", mgr.getType("org.chorem.eugene.Tutu")); //Conflict, package differs

        Assert.assertEquals("void", mgr.getType("void"));
        Assert.assertEquals("boolean", mgr.getType("boolean"));
        Assert.assertEquals("Byte", mgr.getType("Byte"));
        Assert.assertEquals("Toto", mgr.getType("java.lang.Toto"));
        Assert.assertEquals("java.lang.sub.Toto", mgr.getType("java.lang.sub.Toto")); // Conflict, package differs

        Assert.assertEquals("File[]", mgr.getType(File.class.getName() + "[]"));
        Assert.assertEquals("File", mgr.getType(File.class.getName()));
    }

    @Test
    public void testGetImportsWithWildcard() throws Exception {
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.*"));
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.Toto"));
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.subpackage.Tata"));
        List<String> imports = mgr.getImports("org.nuiton.eugene");
        Assert.assertEquals(1, imports.size());
        Assert.assertEquals("org.nuiton.eugene.subpackage.Tata", imports.get(0));

        imports = mgr.getImports("org.nuiton");
        Assert.assertEquals(3, imports.size());
        // Check using alphabetic sort
        Assert.assertEquals("org.nuiton.eugene.*", imports.get(0));
        Assert.assertEquals("org.nuiton.eugene.Toto", imports.get(1));
        Assert.assertEquals("org.nuiton.eugene.subpackage.Tata", imports.get(2));
    }

    @Test
    public void testGetImportsWithGeneric() throws Exception {
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.Toto<String, java.util.File>"));
        List<String> imports = mgr.getImports("org.nuiton.eugene");
        Assert.assertEquals(1, imports.size());
        Assert.assertEquals("java.util.File", imports.get(0));
    }


    @Test
    public void testGetImports() throws Exception {
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.Toto"));
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.Tutu"));
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.subpackage.Tata"));
        List<String> imports = mgr.getImports("org.nuiton.eugene");
        Assert.assertEquals(1, imports.size());
        Assert.assertEquals("org.nuiton.eugene.subpackage.Tata", imports.get(0));

        imports = mgr.getImports("org.nuiton");
        Assert.assertEquals(3, imports.size());
        // Check using alphabetic sort
        Assert.assertEquals("org.nuiton.eugene.Toto", imports.get(0));
        Assert.assertEquals("org.nuiton.eugene.Tutu", imports.get(1));
        Assert.assertEquals("org.nuiton.eugene.subpackage.Tata", imports.get(2));

        imports = mgr.getImports("org.chorem");
        Assert.assertEquals(3, imports.size());

        imports = mgr.getImports("org.nuiton.eugene.subpackage");
        Assert.assertEquals(2, imports.size());
        Assert.assertEquals("org.nuiton.eugene.Toto", imports.get(0));
        Assert.assertEquals("org.nuiton.eugene.Tutu", imports.get(1));

        Assert.assertTrue(mgr.addImport("java.lang.String"));
        imports = mgr.getImports("org.nuiton.eugene.subpackage");
        Assert.assertEquals(2, imports.size());
        Assert.assertEquals("org.nuiton.eugene.Toto", imports.get(0));
        Assert.assertEquals("org.nuiton.eugene.Tutu", imports.get(1));

        Assert.assertTrue(mgr.addImport("void"));
        Assert.assertTrue(mgr.addImport("boolean"));
        Assert.assertTrue(mgr.addImport("Byte"));
        Assert.assertTrue(mgr.addImport("java.lang.Toto"));
        Assert.assertFalse(mgr.addImport("java.lang.sub.Titi")); // Read as started, import is refused
        Assert.assertTrue(mgr.addImport("org.nuiton.eugene.Tutu")); //Already present, must not reject
        imports = mgr.getImports("org.nuiton.eugene.subpackage");
        Assert.assertEquals(2, imports.size());

    }

}
