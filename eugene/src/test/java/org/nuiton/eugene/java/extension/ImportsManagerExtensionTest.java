/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java.extension;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.models.object.xml.ObjectModelClassImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;

import java.util.List;


/**
 * ImportsManagerExtensionTest
 *
 * Created: 2 nov. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public class ImportsManagerExtensionTest {


    private static final Log log =
            LogFactory.getLog(ImportsManagerExtensionTest.class);

    /**
     * Test of getManager method, of class ImportsManagerExtension.
     * Prerequisite : Existing ObjectModel containing a classifier
     * - Get an import Manager for a classifier
     * - Test unique : one ImportsManager by classifier
     */
    @Test
    public void testGetManager() {
        log.debug("getManager");

        ImportsManagerExtension managers = new ImportsManagerExtension();

        ObjectModelImpl model = new ObjectModelImpl();
        model.setName("TestModel");

        ObjectModelClassImpl clazz = new ObjectModelClassImpl();
        clazz.setName("Person");
        clazz.setPackage("org.chorem.bonzoms");
        model.addClass(clazz);

        ImportsManager manager = managers.getManager(clazz);
        Assert.assertNotNull(manager);

        ImportsManager manager2 = managers.getManager(clazz);
        Assert.assertEquals(manager, manager2);
    }

    /**
     * Test of getImports method, of class ImportsManagerExtension.
     * Prerequisite : Existing ObjectModel containing a classifier, imports set for this classifier
     * - Get imports for an existing classifier with its ImportsManager
     */
    @Test
    public void testGetImports() {
        log.debug("getImports");

        ImportsManagerExtension managers = new ImportsManagerExtension();

        ObjectModelImpl model = new ObjectModelImpl();
        model.setName("TestModel");

        ObjectModelClassImpl clazz = new ObjectModelClassImpl();
        clazz.setName("Person");
        clazz.setPackage("org.chorem.bonzoms");
        model.addClass(clazz);

        ImportsManager manager = managers.getManager(clazz);
        manager.addImport("java.util.List");

        List<String> imports = managers.getImports(clazz);
        Assert.assertNotNull(imports);
        Assert.assertEquals(imports.size(), 1);
        Assert.assertEquals(imports.get(0), "java.util.List");
    }

}
