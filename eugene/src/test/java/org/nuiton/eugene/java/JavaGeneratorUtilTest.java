/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.java;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.models.object.xml.ObjectModelOperationImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelParameterImpl;
import org.nuiton.util.StringUtil;

import java.beans.Introspector;

/**
 * Test class {@link JavaGeneratorUtil}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3.2
 */
public class JavaGeneratorUtilTest {

    private static final Log log = LogFactory.getLog(JavaGeneratorUtilTest.class);

    @Test
    public void splitGeneric() throws Exception {

        String[] actual;
        String[] expected;

        expected = null;
        actual = JavaGeneratorUtil.splitGeneric(null);
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.List"};
        actual = JavaGeneratorUtil.splitGeneric("java.util.List");
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.List", "Integer"};
        actual = JavaGeneratorUtil.splitGeneric("java.util.List<Integer>");
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.List", "java.util.List<Integer>"};
        actual = JavaGeneratorUtil.splitGeneric("java.util.List<java.util.List<Integer>>");
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.Map", "Integer", "Integer"};
        actual = JavaGeneratorUtil.splitGeneric("java.util.Map<Integer, Integer>");
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.Map", "Integer", "java.util.List<Integer>"};
        actual = JavaGeneratorUtil.splitGeneric("java.util.Map<Integer, java.util.List<Integer>>");
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.Map", "Integer", "java.util.Map<Integer, Integer>"};
        actual = JavaGeneratorUtil.splitGeneric("java.util.Map<Integer, java.util.Map<Integer, Integer>>");
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.Map", "Integer", "java.util.Map<Integer, Integer, Toto<A, B, C>>", "X", "Y<X>"};
        actual = JavaGeneratorUtil.splitGeneric("java.util.Map<Integer, java.util.Map<Integer, Integer, Toto<A, B, C>>, X, Y<X>>");
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.Map", "X<Y<Z>>", "java.util.Map<Integer, Integer, Toto<A, B, C>>", "X", "Y<X>"};
        actual = JavaGeneratorUtil.splitGeneric("java.util.Map< X<Y<Z>>, java.util.Map<Integer, Integer, Toto<A, B, C>>, X, Y<X>>");
        Assert.assertArrayEquals(expected, actual);
    }


    @Test
    public void joinGeneric() throws Exception {
        String actual;
        String expected;

        expected = null;
        actual = JavaGeneratorUtil.joinGeneric((String) null);
        Assert.assertEquals(expected, actual);

        expected = null;
        actual = JavaGeneratorUtil.joinGeneric(StringUtil.EMPTY_STRING_ARRAY);
        Assert.assertEquals(expected, actual);

        expected = "java.util.List";
        actual = JavaGeneratorUtil.joinGeneric("java.util.List");
        Assert.assertEquals(expected, actual);

        expected = "java.util.List<Integer>";
        actual = JavaGeneratorUtil.joinGeneric("java.util.List", "Integer");
        Assert.assertEquals(expected, actual);


        expected = "java.util.List<java.util.List<Integer>>";
        actual = JavaGeneratorUtil.joinGeneric("java.util.List", "java.util.List<Integer>");
        Assert.assertEquals(expected, actual);

        expected = "java.util.Map<Integer, Integer>";
        actual = JavaGeneratorUtil.joinGeneric("java.util.Map", "Integer", "Integer");
        Assert.assertEquals(expected, actual);

        expected = "java.util.Map<Integer, java.util.List<Integer>>";
        actual = JavaGeneratorUtil.joinGeneric("java.util.Map", "Integer", "java.util.List<Integer>");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void splitFqnList() throws Exception {

        String[] actual;
        String[] expected;

        expected = null;
        actual = JavaGeneratorUtil.splitFqnList(null, ',');
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.List"};
        actual = JavaGeneratorUtil.splitFqnList("java.util.List", ',');
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"Integer", "java.util.List<Integer>"};
        actual = JavaGeneratorUtil.splitFqnList("Integer, java.util.List<Integer>", ',');
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.List", "java.util.List<java.util.List<Integer>>"};
        actual = JavaGeneratorUtil.splitFqnList("java.util.List, java.util.List<java.util.List<Integer>>", ',');
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.List", "java.util.Map<Integer, Integer>"};
        actual = JavaGeneratorUtil.splitFqnList("java.util.List, java.util.Map<Integer, Integer>", ',');
        Assert.assertArrayEquals(expected, actual);


        expected = new String[]{"java.util.Map< X<Y<Z>>>", "java.util.Map<Integer, Integer, Toto<A, B, C>>", "X", "Y<X>"};
        actual = JavaGeneratorUtil.splitFqnList("java.util.Map< X<Y<Z>>>, java.util.Map<Integer, Integer, Toto<A, B, C>>, X, Y<X>", ',');
        Assert.assertArrayEquals(expected, actual);

        expected = new String[]{"java.util.Map< X<Y<Z>>>", "java.util.Map<Integer, Integer, Toto<A, B, C>>", "X", "Y<X>"};
        actual = JavaGeneratorUtil.splitFqnList("java.util.Map< X<Y<Z>>>| java.util.Map<Integer, Integer, Toto<A, B, C>>| X| Y<X>", '|');
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void capitalizeJavaBeanPropertyName() {

        assertCapitalizeJavaBeanPropertyName("a", "A");
        assertCapitalizeJavaBeanPropertyName("ab", "Ab");
        assertCapitalizeJavaBeanPropertyName("aB", "aB");
        assertCapitalizeJavaBeanPropertyName("AB", "AB");
    }

    protected void assertCapitalizeJavaBeanPropertyName(String original, String expected) {

        String actual = JavaGeneratorUtil.capitalizeJavaBeanPropertyName(original);
        Assert.assertEquals(expected, actual);
        Assert.assertEquals(Introspector.decapitalize(actual), original);
    }

    @Test
    public void testGetOperationParametersListDeclaration() {
        if (log.isDebugEnabled()) {
            log.debug("getOperationParametersListDeclaration");
        }

        ObjectModelOperationImpl op = new ObjectModelOperationImpl();

        // Without any params
        String result = JavaGeneratorUtil.getOperationParametersListDeclaration(op);
        Assert.assertEquals("", result);

        // With one param
        ObjectModelParameterImpl param1 = new ObjectModelParameterImpl();
        param1.setType("String");
        param1.setName("param1");
        op.addParameter(param1);

        result = JavaGeneratorUtil.getOperationParametersListDeclaration(op);
        Assert.assertEquals("String param1", result);

        // With two params
        ObjectModelParameterImpl param2 = new ObjectModelParameterImpl();
        param2.setType("Date");
        param2.setName("param2");
        op.addParameter(param2);

        result = JavaGeneratorUtil.getOperationParametersListDeclaration(op);
        Assert.assertEquals("String param1, Date param2", result);
    }
}
