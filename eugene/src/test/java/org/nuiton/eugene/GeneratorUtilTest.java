/*
 * #%L
 * EUGene :: EUGene
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


package org.nuiton.eugene;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.eugene.models.object.xml.ObjectModelOperationImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelParameterImpl;

import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * @author Florian Desbois - desbois@codelutin.com
 */
public class GeneratorUtilTest {

    private static Log log = LogFactory.getLog(GeneratorUtilTest.class);

    @Test
    public void testGetOperationParametersListName() {
        if (log.isDebugEnabled()) {
            log.debug("getOperationParametersListName");
        }

        ObjectModelOperationImpl op = new ObjectModelOperationImpl();

        // Without any params
        String result = GeneratorUtil.getOperationParametersListName(op);
        Assert.assertEquals("", result);

        // With one param
        ObjectModelParameterImpl param1 = new ObjectModelParameterImpl();
        param1.setName("param1");
        op.addParameter(param1);

        result = GeneratorUtil.getOperationParametersListName(op);
        Assert.assertEquals("param1", result);

        // With two params
        ObjectModelParameterImpl param2 = new ObjectModelParameterImpl();
        param2.setName("param2");
        op.addParameter(param2);

        result = GeneratorUtil.getOperationParametersListName(op);
        Assert.assertEquals("param1, param2", result);
    }


    /** Test of getSimpleName method, of class GeneratorUtil. */
    @Test
    public void testGetSimpleName() {
        if (log.isDebugEnabled()) {
            log.debug("getSimpleName");
        }

        String str = "List";
        String expResult = "List";
        String result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.Regex";
        expResult = "Regex";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "<T> java.util.List<T>";
        expResult = "<T> List<T>";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.List<? extends org.chorem.bonzoms.Bonzoms>";
        expResult = "List<? extends Bonzoms>";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.List<org.chorem.jtimer.Jtimer>";
        expResult = "List<Jtimer>";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.Set<java.util.Collection<java.util.Collection<java.util.Collection" +
              "<java.lang.String>>>>";
        expResult = "Set<Collection<Collection<Collection<String>>>>";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.Map<org.chorem.jtimer.Jtimer, java.util.Collection<java.lang.String>>";
        expResult = "Map<Jtimer, Collection<String>>";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "new java.util.HashMap<org.chorem.jtimer.Jtimer, T extends java.lang.String>()";
        expResult = "new HashMap<Jtimer, T extends String>()";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Class<O>";
        expResult = "Class<O>";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "2.0";
        expResult = "2.0";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "\"eric.chatellier\"";
        expResult = "\"eric.chatellier\"";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "<T extends org.nuiton.topia.TopiaEntity, D extends org.nuiton.topia.TopiaDAO<? super T>> D";
        expResult = "<T extends TopiaEntity, D extends TopiaDAO<? super T>> D";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Object...";
        expResult = "Object...";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Class<A>...";
        expResult = "Class<A>...";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.lang.Class<A>...";
        expResult = "Class<A>...";
        result = GeneratorUtil.getSimpleName(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getSimpleName method with removing generics definition,
     * of class GeneratorUtil.
     */
    @Test
    public void testGetSimpleNameAndRemoveGenericsDefinition() {
        if (log.isDebugEnabled()) {
            log.debug("getSimpleName");
        }

        String str = "List";
        String expResult = "List";
        String result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.Regex";
        expResult = "Regex";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "<T> java.util.List<T>";
        expResult = "List<T>";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.List<? extends org.chorem.bonzoms.Bonzoms>";
        expResult = "List<? extends Bonzoms>";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.List<org.chorem.jtimer.Jtimer>";
        expResult = "List<Jtimer>";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.Set<java.util.Collection<java.util.Collection<java.util.Collection" +
              "<java.lang.String>>>>";
        expResult = "Set<Collection<Collection<Collection<String>>>>";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.Map<org.chorem.jtimer.Jtimer, java.util.Collection<java.lang.String>>";
        expResult = "Map<Jtimer, Collection<String>>";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "new java.util.HashMap<org.chorem.jtimer.Jtimer, T extends java.lang.String>()";
        expResult = "new HashMap<Jtimer, T extends String>()";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Class<O>";
        expResult = "Class<O>";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "2.0";
        expResult = "2.0";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "\"eric.chatellier\"";
        expResult = "\"eric.chatellier\"";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "<T extends org.nuiton.topia.TopiaEntity, D extends org.nuiton.topia.TopiaDAO<? super T>> D";
        expResult = "D";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Object...";
        expResult = "Object...";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Class<A>...";
        expResult = "Class<A>...";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.lang.Class<A>...";
        expResult = "Class<A>...";
        result = GeneratorUtil.getSimpleName(str, true);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);
    }


    /** Test of removeGenericDefinition method, of class GeneratorUtil. */
    @Test
    public void testRemoveGenericDefinition() {
        if (log.isDebugEnabled()) {
            log.debug("removeGenericDefinition");
        }

        String str = "List";
        String expResult = "List";
        String result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.Regex";
        expResult = "java.util.Regex";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "<T> java.util.List<T>";
        expResult = "java.util.List<T>";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "  <T> java.util.List<T> ";
        expResult = "java.util.List<T>";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);


        str = "java.util.List<? extends org.chorem.bonzoms.Bonzoms>";
        expResult = "java.util.List<? extends org.chorem.bonzoms.Bonzoms>";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.util.List<org.chorem.jtimer.Jtimer>";
        expResult = "java.util.List<org.chorem.jtimer.Jtimer>";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Class<O>";
        expResult = "Class<O>";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "2.0";
        expResult = "2.0";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "\"eric.chatellier\"";
        expResult = "\"eric.chatellier\"";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "<T extends org.nuiton.topia.TopiaEntity, D extends org.nuiton.topia.TopiaDAO<? super T>> D";
        expResult = "D";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Object...";
        expResult = "Object...";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "Class<A>...";
        expResult = "Class<A>...";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);

        str = "java.lang.Class<A>...";
        expResult = "java.lang.Class<A>...";
        result = GeneratorUtil.removeGenericDefinition(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + result);
        }
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void testGetTypesList() {
        String str = "List";
        Set<String> results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(1, results.size());

        str = "java.util.Regex";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(1, results.size());

        str = "java.util.List<? extends org.chorem.bonzoms.Bonzoms>";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(2, results.size());

        str = "java.util.List<org.chorem.jtimer.Jtimer>";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(2, results.size());

        str = "java.util.Set<java.util.Collection<java.util.Collection<java.util.Collection" +
              "<java.lang.String>>>>";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(3, results.size());

        str = "java.util.Map<org.chorem.jtimer.Jtimer, java.util.Collection<java.lang.String>>";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(4, results.size());

        str = "new java.util.HashMap<org.chorem.jtimer.Jtimer, T extends java.lang.String>()";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(3, results.size());

        str = "java.util.Map<org.chorem.jtimer.Jtimer, java.util.Collection<String>>";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(4, results.size());

        str = "java.util.List<java.util.Map<java.util.Date, java.lang.Integer>>";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(4, results.size());

        str = "java.util.List<java.util.Map<java.util.Date, java.lang.Integer>>";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(4, results.size());

        str = "java.util.TreeMap<java.util.ArrayList<java.util.Date>, java.util.SortedSet<java.lang.Number, java.lang.Double>>";
        results = GeneratorUtil.getTypesList(str);
        if (log.isDebugEnabled()) {
            log.debug(str + " -> " + results);
        }
        Assert.assertEquals(6, results.size());

//        str = "<T extends org.nuiton.topia.TopiaEntity> T";
//        results = GeneratorUtil.getTypesList(str);
//        log.debug(str + " -> " + results);
//        assertEquals(results.size(), 1);
    }

    @Test
    public void testConvertVariableNameToConstantName() {

        String actual;

        actual = GeneratorUtil.convertVariableNameToConstantName("aProperty");
        Assert.assertEquals("A_PROPERTY", actual);

        actual = GeneratorUtil.convertVariableNameToConstantName("AProperty");
        Assert.assertEquals("APROPERTY", actual);

        String variableName = "abc";
        String expResult = "ABC";
        String result = GeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);

        variableName = "ABC";
        expResult = "ABC";
        result = GeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);

        variableName = "abC";
        expResult = "AB_C";
        result = GeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);

        variableName = "AbC";
        expResult = "AB_C";
        result = GeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);

        variableName = "AbC";
        expResult = "AB_C";
        result = GeneratorUtil.convertVariableNameToConstantName(variableName);
        assertEquals(expResult, result);
    }
}
