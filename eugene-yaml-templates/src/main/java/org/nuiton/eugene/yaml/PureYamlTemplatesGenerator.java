package org.nuiton.eugene.yaml;

/*
 * #%L
 * EUGene :: YAML templates
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.*;
import org.nuiton.eugene.models.object.reader.yaml.KeyWords;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * @author agiraudet - giraudet@codelutin.com
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.yaml.PureYamlTemplatesGenerator"
 * @since 2.6.4
 */

public class PureYamlTemplatesGenerator extends ObjectModelGenerator implements KeyWords {

    //TODO: tenir compte des valeurs par defaut : afficher seulement les valeurs différentes des valeurs par défaut

    @Override
    public void generateFromModel(Writer output, ObjectModel input) throws IOException {
        output.write("%YAML 1.1\n---\n- syntaxe: 1.0\n\n");
        if (input.getName() != null) {
            output.write("- " + NAME + ": " + input.getName() + "\n");
        }
        if (input.getVersion() != null) {
            output.write("- " + VERSION + ": " + input.getVersion() + "\n");
        }
        if (input.getTagValues() != null && !input.getTagValues().isEmpty()) {
            output.write("- " + TAG_VALUES + ":\n");
            for (Map.Entry<String, String> entry : input.getTagValues().entrySet()) {
                output.write("  - " + entry.getKey() + ": " + entry.getValue() + "\n");
            }
        }
        output.write("\n");
        //classifier
        if (input.getClasses() != null && !input.getClasses().isEmpty()) {
            for (ObjectModelClass objectModelClass : input.getClasses()) {
                //associationClass
                if (objectModelClass instanceof ObjectModelAssociationClass) {
                    generateFromAssociationClass(output, (ObjectModelAssociationClass) objectModelClass);
                }
                //class
                else {
                    generateFromClass(output, objectModelClass);
                }
            }
        }
        //interface
        if (input.getInterfaces() != null && !input.getInterfaces().isEmpty()) {
            for (ObjectModelInterface objectModelInterface : input.getInterfaces()) {
                generateFromInterface(output, objectModelInterface);
            }
        }
        //enumeration
        if (input.getEnumerations() != null && !input.getEnumerations().isEmpty()) {
            for (ObjectModelEnumeration objectModelEnumeration : input.getEnumerations()) {
                generateFromEnumeration(output, objectModelEnumeration);
            }
        }
    }

    @Override
    public String getFilenameForModel(ObjectModel model) {
        return model.getName() + ".yamlobjectmodel";
    }

    public void generateFromElement(Writer output, ObjectModelElement input, String indent) throws IOException {
        if (input.getName() != null) {
            output.write(indent + "- " + NAME + ": " + input.getName() + "\n");
        }
        if (input.getStereotypes() != null && !input.getStereotypes().isEmpty()) {
            for (String stereotype : input.getStereotypes()) {
                output.write(indent + "- " + STEREOTYPES + ": " + stereotype + "\n");
            }
        }
        if (input.getComments() != null && !input.getComments().isEmpty()) {
            for (String comment : input.getComments()) {
                output.write(indent + "- " + COMMENTS + ": " + comment + "\n");
            }
        }
        /*if(input.getDescription() != null)
        {
            output.write(indent+"- "+DESCRIPTION+": "+input.getDescription()+"\n");
        }*/
        if (input.getDocumentation() != null) {
            output.write(indent + "- " + DOCUMENTATION + ": " + input.getDocumentation() + "\n");
        }
        if (input.getTagValues() != null && !input.getTagValues().isEmpty()) {
            output.write("- " + TAG_VALUES + ":\n");
            for (Map.Entry<String, String> entry : input.getTagValues().entrySet()) {
                output.write("  - " + entry.getKey() + ": " + entry.getValue() + "\n");
            }
        }
        output.write(indent + "- " + STATIC + ": " + String.valueOf(input.isStatic()) + "\n");
    }

    public void generateFromClassifier(Writer output, ObjectModelClassifier input) throws IOException {
        if (input.getInterfaces() != null && !input.getInterfaces().isEmpty()) {
            for (ObjectModelInterface objectModelInterface : input.getInterfaces()) {
                //tester null
                output.write("  - " + SUPER_INTERFACES + ": " + objectModelInterface.getPackageName() + "." + objectModelInterface.getName());
            }
        }
        generateFromElement(output, input, "  ");
        output.write("  - " + INNER + ": " + String.valueOf(input.isInner()) + "\n");
        if (input.getPackageName() != null) {
            output.write("  - " + PACKAGE + ": " + input.getPackageName() + "\n");
        }
        if (input.getAttributes() != null && !input.getAttributes().isEmpty()) {
            for (ObjectModelAttribute attribute : input.getAttributes()) {
                generateFromAttribute(output, attribute);
            }
        }
        if (input.getOperations() != null && !input.getOperations().isEmpty()) {
            for (ObjectModelOperation operation : input.getOperations()) {
                generateFromOperation(output, operation);
            }
        }
    }

    public void generateFromClass(Writer output, ObjectModelClass input) throws IOException {
        output.write("- " + CLASS + ":\n");
        output.write("  - " + ABSTRACT + ": " + String.valueOf(input.isAbstract()) + "\n");
        if (input.getSuperclasses() != null && !input.getSuperclasses().isEmpty()) {
            for (ObjectModelClass objectModelClass : input.getSuperclasses()) {
                //tester null
                output.write("  - " + SUPER_CLASSES + ": " + objectModelClass.getPackageName() + "." + objectModelClass.getName());
            }
        }
        generateFromClassifier(output, input);
        output.write("\n");
    }

    public void generateFromInterface(Writer output, ObjectModelInterface input) throws IOException {
        output.write("- " + INTERFACE + ":\n");
        generateFromClassifier(output, input);
        output.write("\n");
    }

    public void generateFromAssociationClass(Writer output, ObjectModelAssociationClass input) throws IOException {
        output.write("- " + ASSOCIATION_CLASS + ":\n");
        if (input.getSuperclasses() != null && !input.getSuperclasses().isEmpty()) {
            for (ObjectModelClass objectModelClass : input.getSuperclasses()) {
                //tester null
                output.write("  - " + SUPER_CLASSES + ": " + objectModelClass.getPackageName() + "." + objectModelClass.getName());
            }
        }
        generateFromClassifier(output, input);
        output.write("  - " + ABSTRACT + ": " + String.valueOf(input.isAbstract()) + "\n");
        if (input.getParticipantsAttributes() != null && !input.getParticipantsAttributes().isEmpty()) {
            for (ObjectModelAttribute attribute : input.getParticipantsAttributes()) {
                output.write("  - " + PARTICIPANT + ":\n");
                //tester null
                if (attribute.getType() != null) {
                    output.write("    - " + NAME + ": " + attribute.getType() + "\n");
                }
                if (attribute.getName() != null) {
                    output.write("    - " + TYPE + ": " + attribute.getName() + "\n");
                }
            }
        }
        output.write("\n");
    }

    public void generateFromEnumeration(Writer output, ObjectModelEnumeration input) throws IOException {
        output.write("- " + ENUMERATION + ":\n");
        generateFromElement(output, input, "  ");
    }

    public void generateFromParameter(Writer output, ObjectModelParameter input, String indent) throws IOException {
        generateFromElement(output, input, indent);
        if (input.isOrdered()) {
            output.write(indent + "- " + ORDERING + ": " + ORDERED + "\n");
        } else {
            output.write(indent + "- " + ORDERING + ": " + UNORDERED + "\n");
        }
        if (input.getType() != null) {
            output.write(indent + "- " + TYPE + ": " + input.getType() + "\n");
        }
        if (input.getDefaultValue() != null) {
            output.write(indent + "- " + DEFAULT_VALUE + ": " + input.getDefaultValue() + "\n");
        }
        output.write(indent + "- " + MIN_MULTIPLICITY + ": " + String.valueOf(input.getMinMultiplicity()) + "\n");
        output.write(indent + "- " + MAX_MULTIPLICITY + ": " + String.valueOf(input.getMaxMultiplicity()) + "\n");
        output.write(indent + "- " + UNIQUE + ": " + String.valueOf(input.isUnique()) + "\n");
    }

    public void generateFromAttribute(Writer output, ObjectModelAttribute input) throws IOException {
        output.write("  - " + ATTRIBUTE + ":\n");
        generateFromParameter(output, input, "    ");
        output.write("    - " + NAVIGABLE + ": " + String.valueOf(input.isNavigable()) + "\n");
        output.write("    - " + TRANSIENT + ": " + String.valueOf(input.isTransient()) + "\n");
        output.write("    - " + FINAL + ": " + String.valueOf(input.isFinal()) + "\n");
        output.write("    - " + REVERSE_MAX_MULTIPLICITY + ": " + String.valueOf(input.getReverseMaxMultiplicity()) + "\n");
        if (input.getVisibility() != null) {
            output.write("    - " + VISIBILITY + ": " + input.getVisibility() + "\n");
        }
        if (input.isComposite()) {
            output.write("    - " + ASSOCIATION_TYPE + ": " + COMPOSITE + "\n");
        } else if (input.isAggregate()) {
            output.write("    - " + ASSOCIATION_TYPE + ": " + AGGREGATE + "\n");
        }
        if (input.getAssociationClass() != null) {
            output.write("    - " + ASSOCIATION_CLASS_NAME + ": " + input.getAssociationClass().getPackageName() + "." + input.getAssociationClass().getName() + "\n");
        }
    }

    public void generateFromOperation(Writer output, ObjectModelOperation input) throws IOException {
        output.write("  - " + ATTRIBUTE + ":\n");
        generateFromElement(output, input, "  ");
        output.write("    - " + ABSTRACT + ": " + String.valueOf(input.isAbstract()) + "\n");
        if (input.getVisibility() != null) {
            output.write("    - " + VISIBILITY + ": " + input.getVisibility() + "\n");
        }
        if (input.getBodyCode() != null) {
            output.write("    - " + BODY_CODE + ": " + input.getBodyCode() + "\n");
        }
        if (input.getReturnParameter() != null) {
            output.write("    - " + RETURN_PARAMETER + ":\n");
            generateFromParameter(output, input.getReturnParameter(), "      ");
        }
        if (input.getParameters() != null && !input.getParameters().isEmpty()) {
            for (ObjectModelParameter objectModelParameter : input.getParameters()) {
                output.write("    - " + PARAMETER + ":\n");
                generateFromParameter(output, objectModelParameter, "      ");
            }
        }

    }
}
