package org.nuiton.eugene.yaml;

/*
 * #%L
 * EUGene :: YAML templates
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelGenerator;
import org.nuiton.eugene.models.object.reader.yaml.KeyWords;

import java.io.IOException;
import java.io.Writer;

/**
 * @author agiraudet - giraudet@codelutin.com
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.yaml.UserFriendlyTemplatesGenerator"
 * @since 2.6.4
 */
public class UserFriendlyTemplatesGenerator extends ObjectModelGenerator implements KeyWords {

    @Override
    public void generateFromModel(Writer output, ObjectModel input) throws IOException {
        ;//TODO
    }

    @Override
    public String getFilenameForModel(ObjectModel model) {
        return model.getName() + ".yamlobjectmodel";
    }
}
