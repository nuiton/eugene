.. -
.. * #%L
.. * EUGene :: Maven plugin
.. * %%
.. * Copyright (C) 2006 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=====
Usage
=====

:Authors: Tony Chemit
:Contact: chemit@codelutin.com
:Revision: $Revision$
:Date: $Date$

.. contents::

Abstract
~~~~~~~~

This page describes the usage of the goals of the eugene plugin.

generate
--------

This goal permits to launch a smart (understood as intelligent) generation,
chaining different generation phases.

Following the maven spirit of using convention (and then default configuration)
you should always try to respect the plugin convention to avoid big headaches...

modelType
*********

An invariant of the goal is the type of model to treate given by the
configuration property *modelType*.

At the moment there is two types of model :

- objectmodel
- statemodel

inputs
******

To define what to generate, you must fill the *inputs* configuration.

Each inputs entry is bind to a phase of generation (see next section to learn
more about phases).

An input entry is defined by :

- a phase (zargo, xmi or model)
- a input directory
- an include pattern

Example:

::

    <execution>
        <configuration>
            <inputs>zargo</inputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

If you want to use more than one entry, you can also write :


::

    <execution>
        <configuration>
            <inputs>
                <input>zargo</input>
                <input>xmi</input>
            </inputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

An input can have three forms :

- <input>XXX</input> : use the XXX phase with its default configuration
- <input>dir:includes</input> : use the given directory with the given phase and detects from the includes pattern the phase to use
- <input>XXX:dir:includes</input> : use the XXX phase with the given directory and includes pattern

Example:

::

    <execution>
        <configuration>
            <inputs>zargo</inputs>
            <inputs>src/main/myzargo:**/*.zargo</inputs>
            <inputs>zargo:src/main/myzargo2:**/*.zargo2</inputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>


phases
******

A phase (also called a writer) aims to generate something with possibility to
fire a *next* chained phase.

At the moment we have defined several phases of generation :

- zargo
- xmi
- model

Each phase has an input protocol and an output protocol, says :

::

  zargo --> xmi
  xmi   --> model
  model --> no output protocol (at the moment)

So if you gives and zargo phase, the plugin will try to chain zargo --> xmi --> model.

It is possible to skip some phase using the *skipInputs* configuration property.

Example:

::

    <execution>
        <configuration>
            <inputs>zargo</inputs>
            <skipInputs>model</skipInputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

will launch phases zargo then xmi but not the model phase.

Finally, there is some specific properties that can be filled for each phase.

zargo phase
~~~~~~~~~~~

This phase permits to take some zargo files to extract the xmi files from them.

Example:

::

    <execution>
        <configuration>
            <inputs>zargo</inputs>
            <skipInputs>xmi,model</skipInputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

will detect all zargo files (using the default configuration for zargo phase)
and extract their xmi files.

The default configuration for the zargo phase is :

- input directory : src/main/xmi
- include files : **/*.zargo
- output directory : target/generated-sources/xmi

This phase has no specific configuration properties.

xmi phase
~~~~~~~~~

This phase permits to take some xmi files to build some objectmoldel (or
statemodel) files.

Example:

::

    <execution>
        <configuration>
            <inputs>xmi</inputs>
            <resolver>org.nuiton.util.FasterCachedResourceResolver</resolver>
            <skipInputs>model</skipInputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

will detect all xmi files (using the default configuration for xmi phase) and
create model files from them.

The default configuration for the xmi phase is :

- input directory : src/main/xmi
- include files : **/*.xmi
- output directory : target/generated-sources/modles

This phase defines some specific configuration properties :

- *fullPackagePath*
- *resolver*

model phase
~~~~~~~~~~~

This phase permits to take some model files to generate some files (java and
others) using the given templates.

Example:

::

    <execution>
        <configuration>
            <inputs>model:target/generated-sources/models:observe.objectmodel</inputs>
            <templates>
                org.nuiton.topia.generator.TopiaMetaTransformer,
                org.nuiton.topia.generator.InterfaceTransformer
            </templates>    
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

The default configuration for the model phase is :

- input directory : src/main/models
- include files : **/*.objectmodel
- output directory : target/generated-sources/java


This phase defines some specific configuration properties :

- *defaultPackage*
- *templates*
- *excludeTemplates*
- *generatedPackages*

available-data
--------------

This goal help users to determine all types of data that can be used with their
current configuration of plugin.

The goal can only be used by direct invocation in console and not in the pom as
its purpose is only to display some stuff in the console.

Just type

::

   mvn eugene:available-data

to obtain all data available.

Example:

::

    [INFO] [eugene:available-data {execution: default-cli}]
    [INFO] Get datas for data types : [modeltype, writer, modelreader, modeltemplate]
    Found 2 modeltypes :
     [statemodel] with implementation 'org.nuiton.eugene.models.state.xml.StateModelImpl'
     [objectmodel] with implementation 'org.nuiton.eugene.models.object.xml.ObjectModelImpl'
    Found 3 writers :
     [xmi] with implementation 'org.nuiton.eugene.plugin.writer.XmiChainedFileWriter
      inputProtocol             : xmi
      outputProtocol            : model
      defaultIncludes           : **/*.xmi
      defaultInputDirectory     : src/main/xmi
      defaultTestInputDirectory : src/test/xmi'
     [model2Java] with implementation 'org.nuiton.eugene.plugin.writer.ModelChainedFileWriter
      inputProtocol             : model
      outputProtocol            : null
      defaultIncludes           : **/*.*model
      defaultInputDirectory     : src/main/models
      defaultTestInputDirectory : src/test/models'
     [zargo2xmi] with implementation 'org.nuiton.eugene.plugin.writer.ZargoChainedFileWriter
      inputProtocol             : zargo
      outputProtocol            : xmi
      defaultIncludes           : **/*.zargo
      defaultInputDirectory     : src/main/xmi
      defaultTestInputDirectory : src/test/xmi'
    Found 2 modelreaders :
     [statemodel] with implementation 'org.nuiton.eugene.models.state.StateModelReader'
     [objectmodel] with implementation 'org.nuiton.eugene.models.object.ObjectModelReader'
    Found one modeltemplate :
     [org.nuiton.eugene.java.JavaGenerator] with implementation 'org.nuiton.eugene.java.JavaGenerator'

transform-flat-properties-to-compact
------------------------------------

This goal transform some input files (model extension file with properties file format) to a unique file using a compact format (objectmodel-ext).

Example of a such file:

::

  [model]
  modeTagValue value
  modelStereotype

  [package]
  fr.ird.observe.entities
    packageTagValue value
    packageStereotype
  [class]
  fr.ird.observe.entities.CommentableEntity
    classTagValue value
    classStereotype
    attribute.attributeTagValue value
    attribute.attributeStereotype

transform-compact-to-flat-properties
------------------------------------

This goal transform some input files (compact format) to a unique file using the classical properties file format.
