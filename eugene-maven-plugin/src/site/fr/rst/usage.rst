.. -
.. * #%L
.. * EUGene :: Maven plugin
.. * %%
.. * Copyright (C) 2006 - 2011 CodeLutin, Chatellier Eric
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=====
Usage
=====

:Authors: Tony Chemit
:Contact: chemit@codelutin.com
:Revision: $Revision$
:Date: $Date$

.. contents::

generate
--------

Ce 'goal' permet de lancer une génération intelligente, en chaînant les
différentes phases de génération.

En suivant les conventions de maven (et la configuration par défaut), il est
conseillé de respecter au maximum les cnventions de plugin afin d'éviter de gros
mals de crâne...

modelType
*********

Propriété invariante du 'goal', *modelType* est le type de modèle à traiter.

Il y a actuellement deux types de modèles :

- objectmmodel
- statemodel

inputs
******

Pour définir ce qu'il faut générer, vous devez remplir la configuration des
*inputs*.

Chaque entrée des 'inputs' est associée à une phase de génération (référez-vous
à la section suivante pour en savoir plus sur les phases).

Une entrée des 'inputs' est définie par :

- une phase (zargo, xmi ou model)
- un répertoire contenant les fichiers à inclure
- un 'pattern' d'inclusion

Example:

::

    <execution>
        <configuration>
            <inputs>zargo</inputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

Si vous souhaitez utiliser plusieurs entrées, vous pouvez aussi écrire :

::

    <execution>
        <configuration>
            <inputs>
                <input>zargo</input>
                <input>xmi</input>
            </inputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

Une entrée peut avoir cinq formes :

- <input>XXX</input> : utilise la phase XXX avec sa configuration par défaut.
- <input>dir:includes</input> : utilise le répertoire donné et utilise le 'pattern' d'exclusion pour définir la phase à utiliser.
- <input>classpath:XXX:dir:includes</input> : utilise la phase XXX avec recherche dans le classpath à partir du répertoire et le 'pattern d'inclusion' donnés.
- <input>dir:includes</input> : utilise le répertoire donné et utilise le 'pattern' d'exclusion pour définir la phase à utiliser.
- <input>classpath:XXX:dir:includes</input> : utilise la phase XXX avec recherche dans le classpath et le 'pattern d'inclusion' donnés.

Example:

::

    <execution>
        <configuration>
            <inputs>zargo</inputs>
            <inputs>src/main/myzargo:**/*.zargo</inputs>
            <inputs>classpath:/myzargo:.*\.zargo</inputs>
            <inputs>zargo:src/main/myzargo:**/*.zargo</inputs>
            <inputs>classpath:zargo:/myzargo:.*\.zargo</inputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>


Utilisation du classpath (depuis 2.2)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On autorise désormais la recherche de fichiers dans le classpath, cela est util
par exemple, lorsque le modèle est dans différents modules d'un projet maven.

On distingue deux sortes de recherche dans le class-path :

- Une recherche exacte on on donne directement le nom du fichier recherché.

  Dans l'exemple suivant, on recherche exactement le fichier /xmi/myModel.zargo
  dans le class-path :

::

    <execution>
        <configuration>
            <inputs>
                <input>classpath:zargo:/xmi:myModel.zargo</input>
            </inputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

- Une recherche non exacte (avec des patterns), dans ce cas la pattern
  d'inclusion (contrairement au cas des fichiers) doit être un expression
  régulière :

  Dans l'exemple suivant, on veut traiter tous les fichiers zargo contenus dans
  le class-path :

::

    <execution>
        <configuration>
            <inputs>
                <input>classpath:zargo:/:.*\.zargo</input>
            </inputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

Techniquement le plugin va rechercher les fichiers à traiter dans le class-path
et les extraire vers un répertoire précis (donné par le paramètre extractDirectory/inputType)
du mojo.

En reprenant notre premier exemple, cela donnera :

::

  myProject
  .
  `-- target
      `-- extracted-sources
          `-- zargo
              `-- xmi
                  |-- myModel.properties
                  `-- myModel.zargo


Une fois ces données extrait, le plugin utilisera ces données comme un input
classique sur fichiers :

::

  <input>zargo:target/extracted-sources/zargo:xmi/myModel.zargo</input>

Note::

  On notera que les répertoires dont viennent les données du class-path sont
  conservés pour éviter toute collision.
  
phases
******

Une phase (aussi appellée un 'writer' a pour but de générer quelque chose avec
la possibilité de déclencher la phase chaînée *suivante*.

Pour le moment, nous avons défini quelques phases de génération :

- zargo
- xmi
- model

Chaque phase a un protocole d'entrée et un protocole de sortie :

::

  zargo --> xmi
  xmi   --> model
  model --> no output protocol (at the moment)

Donc si vous donnez la phase zargo, le plugin essaiera de chaîner zargo --> xmi --> model.

Il est possible de ne pas effectuer une phase en utilisant la propriété *skipInputs*.

Example:

::

    <execution>
        <configuration>
            <inputs>zargo</inputs>
            <skipInputs>model</skipInputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

lancera les phases zargo et xmi mais pas la phase model.

Il y a aussi quelques propriété spécifiques à chaque phase.

Phase zargo
~~~~~~~~~~~

Cette phase permet de prendre des fichiers zargo pour en extraire les fichiers xmi.

Example:

::

    <execution>
        <configuration>
            <inputs>zargo</inputs>
            <skipInputs>xmi,model</skipInputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

détectera tous les fichiers zargo (en utilisant la configuration par défaut de
la phase zargo) et extraira les fichiers xmi.

La configuration par défaut de la phase zargo est :

- répertoire source : src/main/xmi
- 'pattern' d'inclusion : **/*.zargo
- répertoire de sortie : target/generated-sources/xmi

Cette phase n'a pas de configuration spécifique.

Phase xmi
~~~~~~~~~

Cette phase permet de prendre des fichiers xmi pour construire des fichiers
objectmodel (ou statemodel).

Example:

::

    <execution>
        <configuration>
            <inputs>xmi</inputs>
            <resolver>org.nuiton.util.FasterCachedResourceResolver</resolver>
            <skipInputs>model</skipInputs>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

détectera tous les fichiers xmi (en utilisant la configuration par défaut de la
phase xmi) et créera  des fichiers model.

La configuration pour la phase xmi est :

- répertoire source : src/main/xmi
- 'pattern' d'inclusion : **/*.xmi
- répertoire de sortie : target/generated-sources/modles

Cette phase défini quelques propriétés spécifiques :

- *fullPackagePath* : le package généré.
- *resolver* : le resolver de ressource à utiliser.

Phase model
~~~~~~~~~~~

Cette phase permet de prendre des fichiers model pour générer des fichiers (java
ou autre) en utilisant des templates.

Example:

::

    <execution>
        <configuration>
            <inputs>model:target/generated-sources/models:observe.objectmodel</inputs>
            <templates>
                org.nuiton.topia.generator.TopiaMetaTransformer,
                org.nuiton.topia.generator.InterfaceTransformer
            </templates>
        </configuration>
        <goals>
            <goal>generate</goal>
        </goals>
    </execution>

La configuration par défaut pour la phase model est :

- répertoire source : src/main/models
- 'pattern' d'inclusion : **/*.objectmodel
- répertoire de sortie : target/generated-sources/java

Cette phase défini des propriétés spécifiques :

- *defaultPackage* : package par défaut
- *templates* : templates à utiliser
- *excludeTemplates* : templates à exclure
- *generatedPackages* : packages à générer

available-data
--------------

Ce 'goal' aide les utilisateurs à déterminer quel type de donnée peut-être
utilisé avec la configuration actuelle du plugin.

Ce 'goal' ne peut être utilisé que par invocation directe en console et non dans
votre pom car son objectif est d'afficher des informations dans la console.

Lancez seulement

::xmi

   mvn eugene:available-data

pour obtenir toutes les informations disponibles.

Example:

::

    [INFO] [eugene:available-data {execution: default-cli}]
    [INFO] Get datas for data types : [modeltype, writer, modelreader, modeltemplate]
    Found 2 modeltypes :
     [statemodel] with implementation 'org.nuiton.eugene.models.state.xml.StateModelImpl'
     [objectmodel] with implementation 'org.nuiton.eugene.models.object.xml.ObjectModelImpl'
    Found 3 writers :
     [xmi] with implementation 'org.nuiton.eugene.plugin.writer.XmiChainedFileWriter
      inputProtocol             : xmi
      outputProtocol            : model
      defaultIncludes           : **/*.xmi
      defaultInputDirectory     : src/main/xmi
      defaultTestInputDirectory : src/test/xmi'
     [model2Java] with implementation 'org.nuiton.eugene.plugin.writer.ModelChainedFileWriter
      inputProtocol             : model
      outputProtocol            : null
      defaultIncludes           : **/*.*model
      defaultInputDirectory     : src/main/models
      defaultTestInputDirectory : src/test/models'
     [zargo2xmi] with implementation 'org.nuiton.eugene.plugin.writer.ZargoChainedFileWriter
      inputProtocol             : zargo
      outputProtocol            : xmi
      defaultIncludes           : **/*.zargo
      defaultInputDirectory     : src/main/xmi
      defaultTestInputDirectory : src/test/xmi'
    Found 2 modelreaders :
     [statemodel] with implementation 'org.nuiton.eugene.models.state.StateModelReader'
     [objectmodel] with implementation 'org.nuiton.eugene.models.object.ObjectModelReader'
    Found one modeltemplate :
     [org.nuiton.eugene.java.JavaGenerator] with implementation 'org.nuiton.eugene.java.JavaGenerator'

transform-flat-properties-to-compact
------------------------------------

Ce 'goal' transforme des fichiers d'exention du modèle au format properties vers un format plus compact (objectmodel-ext).

Exemple de fichier sous ce format :

::

  [model]
  modeTagValue value
  modelStereotype

  [package]
  fr.ird.observe.entities
    packageTagValue value
    packageStereotype
  [class]
  fr.ird.observe.entities.CommentableEntity
    classTagValue value
    classStereotype
    attribute.attributeTagValue value
    attribute.attributeStereotype

transform-compact-to-flat-properties
------------------------------------

Ce 'goal' transforme des fichiers d'extension du modèle au compact vers le format classique de fichier de properties.
