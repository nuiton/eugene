.. -
.. * #%L
.. * EUGene :: Maven plugin
.. * %%
.. * Copyright (C) 2006 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

===================
Maven-eugene-plugin
===================

.. contents::

Présentation
------------

Le plugin eugene-maven-plugin permet l'utilisation depuis maven de
Eugene. Pour plus de détails sur Eugene veuillez consulter 
l'adresse suivante http://doc.nuiton.org/eugene/eugene

Nouvautés dans la version 2.2
-----------------------------

Recherche de fichiers dans le class-path
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il est possible désormais d'utiliser des fichiers contenus dans le class-path,
que ce soit des modèles zargo, xmi ou model.

La page d'`usage`_ explique comment cela fonctionne.

Respect des hiérarchies répertoires
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On respecte conserve désormais de bout en bout les répertoires relatifs dont
sont extrait les données lors d'un cycle de génération et ceci afin d'éviter
toute collision de noms de fichiers et mieux s'y retrouver.

Meilleure copie des ressources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'algorithme de recopie des rsesources a été revu et est désormais spécifique à
chaque type de writer.

Globalement, pour tous les writers fournis par le plugin, à chaque fichier
à traiter par un writer, on y associe un fichier de properties du même nom.

Ce mécanisme n'est pas actuellement extensible mais pourrait le devenir si le
besoin est présent.

Utilisation dans le pom.xml
---------------------------

Voir la `page`_ de détail des goals ou bien la page d'`usage`_ des goals.

.. _page: plugin-info.html
.. _usage: usage.html
