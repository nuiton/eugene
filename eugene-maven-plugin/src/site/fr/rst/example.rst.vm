.. -
.. * #%L
.. * EUGene :: Maven plugin
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2006 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

========
Examples
========


:Authors: Jean Couteau
:Contact: couteau@codelutin.com
:Revision: $Revision$
:Date: $Date$


.. contents::

Cette page regroupe deux examples de configuration/utilisation du plugin
dans un pom. Le premier example est très simple, le second est plus complexe
et est censé ammener une complète compréhension du plugin combiné à la
documentation des usages_ .

.. _usages: usage.html

Example simple
--------------

Cet exemple va générer les entitées du package org.nuiton.eugene.demopackage à
partir des fichiers zargo présents dans le répertoire src/main/xmi.

::

    <plugin>
        <groupId>org.nuiton.eugene</groupId>
        <artifactId>eugene-maven-plugin</artifactId>
        <version>${project.version}</version>

        <executions>

        <execution>
                <phase>generate-sources</phase>
                <!-- By default, generation from ObjectModel -->
                <configuration>
                    <!-- Corresponding to extracted package from zargo file -->
                    <fullPackagePath>org.nuiton.eugene.demopackage</fullPackagePath>
                    <!-- DefaultPackage used for DAOHelper generation -->
                    <defaultPackage>org.nuiton.eugene.demopackage</defaultPackage>
                    <!-- Use topia templates -->
                    <templates>
                        org.nuiton.topia.generator.TopiaMetaTransformer,
                        org.nuiton.topia.generator.InterfaceTransformer,
                        org.nuiton.topia.generator.BeanTransformer
                    </templates>
                </configuration>
                <goals>
                    <goal>generate</goal>
                </goals>
            </execution>
        </executions>
        <-- dependency to topia to use their template -->
        <dependencies>
            <dependency>
                <groupId>org.nuiton.topia</groupId>
                <artifactId>topia-persistence</artifactId>
                <version>${topiaVersion}</version>
            </dependency>
        </dependencies>
    </plugin>

Example complexe
----------------

Cet exemple utilise deux fichiers zargo en entrée et va les générer différemment.

Les deux premières phases sont communes, ensuite on distingues deux executions,
une par fichier spécifiant les différents templates à utiliser pour chaque
fichier model.

::

    <plugin>
        <groupId>org.nuiton.eugene</groupId>
        <artifactId>eugene-maven-plugin</artifactId>
        <version>${project.version}</version>
        <configuration>
            <defaultPackage>org.nuiton.eugene.demopackage</defaultPackage>
            <fullPackagePath>org.nuiton.eugene.demopackage</fullPackagePath>
        </configuration>

        <executions>

            <!-- Execution that transforms zargo files to objectmodel by using
            the full run without the model phase -->
            <execution>
                <phase>generate-sources</phase>
                <id>toModel</id>
                <configuration>
                    <inputs>zargo</inputs>
                    <resolver>org.nuiton.util.FasterCachedResourceResolver</resolver>
                    <skipInputs>model</skipInputs>
                </configuration>
                <goals>
                    <goal>generate</goal>
                </goals>
            </execution>

            <-- Execution that transforms the entities.objectmodel file using
            specific templates from topia -->
            <execution>
                <phase>generate-sources</phase>
                <id>model-to-entities</id>
                <configuration>
                    <inputs>model:target/generated-sources/models:entities.objectmodel</inputs>
                    <templates>
                        org.nuiton.topia.generator.TopiaMetaTransformer,
                        org.nuiton.topia.generator.InterfaceTransformer
                    </templates>
                </configuration>
                <goals>
                    <goal>generate</goal>
                </goals>
            </execution>

            <-- Execution that transforms the beans.objectmodel file using
            other specific templates from topia -->
            <execution>
                <phase>generate-sources</phase>
                <id>model-to-bean</id>
                <configuration>
                    <inputs>
                        <input>model:target/generated-sources/models:beans.objectmodel</input>
                    </inputs>
                    <templates>org.nuiton.topia.generator.BeanTransformer</templates>
                </configuration>
                <goals>
                    <goal>generate</goal>
                </goals>
            </execution>

        </executions>

        <!-- dependency to use topia templates -->
        <dependencies>
            <dependency>
                <groupId>org.nuiton.topia</groupId>
                <artifactId>topia-persistence</artifactId>
                <version>${topiaVersion}</version>
                <scope>compile</scope>
            </dependency>
        </dependencies>
    </plugin>
