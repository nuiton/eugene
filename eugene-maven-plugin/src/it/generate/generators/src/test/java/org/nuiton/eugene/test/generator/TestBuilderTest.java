/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
 * *##% 
 * EUGene Test
 * Copyright (C) 2007 - 2009 CodeLutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * ##%*
 */
package org.nuiton.eugene.test.generator;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelOperation;

/**
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public class TestBuilderTest {

    private static final Log log = LogFactory.getLog(TestBuilderTest.class);

    public TestBuilderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of build method, of class TestBuilder.
     */
    @Test
    public void testBuild() {
        System.out.println("build");
        TestBuilder instance = new TestBuilder();

        instance.build();
        
        ObjectModel result = instance.getModel();
        assertNotNull(result);
        assertEquals(result.getClasses().size(), 2);
        ObjectModelClass clazz = result.getClass("org.chorem.bonzoms.Person");
        assertNotNull(clazz);
        assertEquals(clazz.getAttributes().size(), 3);
        assertEquals(clazz.getOperations().size(), 2);
        List<ObjectModelOperation> operations = (List<ObjectModelOperation>)clazz.getOperations();
        ObjectModelOperation operation = operations.get(0);
        log.debug("Body code [" + operation.getName() + "] : " + operation.getBodyCode());
        assertFalse(operation.getBodyCode().isEmpty());
    }

}
