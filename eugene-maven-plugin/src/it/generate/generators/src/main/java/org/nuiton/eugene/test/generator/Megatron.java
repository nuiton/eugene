/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.test.generator;

import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelClass;

/**
 * Megatron
 *
 * Chainage des transformer : Modele de depart -> transformation dans BeanTransformer = modele d'entrée de Megatron
 *
 * Created: 12 nov. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.test.generator.Megatron"
 */
public class Megatron extends ObjectModelTransformerToJava {

    public Megatron() {
        super();
    }

    /*
            CAS modele de sortie vide : modele d'entree transformee par BeanTransformer
    */
    @Override
    protected ObjectModelTransformerToJava initPreviousTransformer() {
        return new BeanTransformer();
    }

    @Override
    public void transformFromClass(ObjectModelClass clazz) {

        

    }

}
