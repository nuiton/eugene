/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
 * *##% 
 * EUGene Test
 * Copyright (C) 2007 - 2009 CodeLutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * ##%*
 */

package org.nuiton.eugene.test.generator;

import java.io.File;
import org.nuiton.eugene.ModelReader;
import org.nuiton.eugene.models.object.ObjectModel;

/**
 * TestReader
 *
 * Created: 27 oct. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */
public class TestReader extends ModelReader<ObjectModel> {


    @Override
    public ObjectModel read(File[] file) {
        TestBuilder builder = new TestBuilder();

        builder.build();

        return builder.getModel();
    }

}
