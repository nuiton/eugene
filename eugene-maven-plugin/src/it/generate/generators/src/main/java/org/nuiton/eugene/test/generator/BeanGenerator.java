/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* BeanGenerator.java
*
* Created: 17 avril 2009
*
* @author Tony Chemit - chemit@codelutin.com
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
* @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.topia.generator.BeanGenerator"
*/

package org.nuiton.eugene.test.generator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.java.extension.ImportsManager;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelDependency;
import org.nuiton.eugene.models.object.ObjectModelGenerator;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelParameter;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * DTO generator
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.test.generator.BeanGenerator"
 */
public class BeanGenerator extends ObjectModelGenerator {
    /** Tag pour ajouter une annotation à un champ */
    public static final String TAG_ANNOTATION = "annotation";
    /** Stéréotype pour les objets devant être générées sous forme d'entités */
    public static final String STEREOTYPE_ENTITY = "entity";
    /** Stéréotype pour les objets devant être générées sous forme de DTO */
    public static final String STEREOTYPE_DTO = "dto";
    /** Stéréotype pour les objets devant être générées sous forme de bean */
    public static final String STEREOTYPE_BEAN = "bean";
    /**
     * Logger for this class
     */
    private static final Log log = LogFactory.getLog(BeanGenerator.class);

    public BeanGenerator() {
        super();
    }

    @Override
    public String getFilenameForClass(ObjectModelClass clazz) {
        return clazz.getQualifiedName().replace('.', File.separatorChar) + ".java";
    }

    @Override
    public void generateFromClass(Writer output, ObjectModelClass clazz) throws IOException {
        if (!clazz.hasStereotype(STEREOTYPE_BEAN) &&
                !clazz.hasStereotype(STEREOTYPE_DTO)) {
            return;
        }
        //
        // première phase : calcul des variables
        //
        String copyright = "";
        String clazzName = clazz.getName();
        String abstractStr = isAbstract(clazz) ? " abstract " : " ";
        boolean needGetEntityMethod = false;
        boolean generateToString = true;

        ImportsManager imports = new ImportsManager();

        String extendClass = "";
        Iterator<ObjectModelClass> j = clazz.getSuperclasses().iterator();
        if (j.hasNext()) {
        	ObjectModelClassifier p = j.next();
            imports.addImport(p.getQualifiedName());
            extendClass += p.getName();
        }
        String implInterface = "";
        for (Iterator<ObjectModelInterface> i=clazz.getInterfaces().iterator(); i.hasNext();) {
        	ObjectModelClassifier parentInterface = i.next();
            imports.addImport(parentInterface.getQualifiedName());
        	implInterface += parentInterface.getName();
        	if (i.hasNext()) {
        		implInterface += ", ";
        	}
        }
        // Add Serializable implements for DTO generation
        if (clazz.hasStereotype(STEREOTYPE_DTO)) {
            imports.addImport(Serializable.class);
            if (!implInterface.isEmpty()) {
                implInterface += ", ";
            }
            implInterface += Serializable.class.getName();
        }
        String svUID = "1L";

        List<ObjectModelAttribute> attributes = new ArrayList<ObjectModelAttribute>();
        List<ObjectModelAttribute> multipleAttr = new ArrayList<ObjectModelAttribute>();

        setAttributesForDTO(clazz, attributes,imports);  
        
        boolean needListInImport=false;

        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            if (attr.isNavigable()) {
                attributes.add(attr);
                imports.addImport(attr.getType());
                if (GeneratorUtil.isNMultiplicity(attr)) {
                    multipleAttr.add(attr);
                    ObjectModelClass attrEntity = null;
                    if (model.hasClass(attr.getType())) {
                        attrEntity = model.getClass(attr.getType());
                    }
                    boolean isEntity = (attrEntity != null && attrEntity.hasStereotype(STEREOTYPE_ENTITY));
                    needGetEntityMethod |= isEntity;
                    if (JavaGeneratorUtil.isOrdered(attr)) {
                        needListInImport = true;
                    } 
                }
            }
        }

        imports.addImport(java.beans.PropertyChangeListener.class.getName());
        imports.addImport(java.beans.PropertyChangeSupport.class.getName());

        for (ObjectModelOperation operation : clazz.getOperations()) {
            imports.addImport(operation.getReturnType());
            for (ObjectModelParameter parameter : operation.getParameters()) {
                imports.addImport(parameter.getType());
            }
        }

        if (needGetEntityMethod) {
            imports.addImport("org.nuiton.topia.persistence.TopiaEntity");
        }
        if (!multipleAttr.isEmpty()) {
            imports.addImport(Collection.class);
        }
        if (needListInImport) {
            imports.addImport(List.class);
        }
        if (generateToString) {
            imports.addImport(org.apache.commons.lang3.builder.ToStringBuilder.class);
        }
        
        boolean sortAttribute = true;
        if (sortAttribute) {
            Comparator<ObjectModelAttribute> comp = new Comparator<ObjectModelAttribute>(){

                @Override
                public int compare(ObjectModelAttribute o1, ObjectModelAttribute o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            };
            java.util.Collections.sort(attributes,comp);
            java.util.Collections.sort(multipleAttr,comp);
        }
        //
        // seconde phase : génération
        //
        
        if (StringUtils.isNotEmpty(copyright)) {
/*{<%=copyright%>
}*/
        }
        
/*{package <%=clazz.getPackageName()%>;
 
 }*/

        if (log.isDebugEnabled()) {
            log.debug("imports for class <" + clazzName + ">");
        }
        //for (String anImport : imports) {
        for (String anImport : imports.getImports(clazz.getPackageName())) {
            if (log.isDebugEnabled()) {
                log.debug("import " + anImport);
            }
/*{import <%=anImport%>;
}*/
        }
/*{
public<%=abstractStr%>class <%=clazzName%>}*/

/*
 * Définition de la super classe : il ne doit y avoir qu'une
 */        
        if (extendClass.length() > 0) {
/*{ extends <%=extendClass%>}*/
        }
        
        if (implInterface.length() > 0) {
/*{ implements <%=implInterface%> {

}*/
        } else {
        	/*{ {

}*/
        }

        
        // TODO Calculer un serialVersionUID si il n'y en a pas
        if (svUID != null) {
/*{    public static final long serialVersionUID = <%=svUID%>;

}*/
        }
        generateInterfaceOperations(output, clazz);
        generateAttributes(output, attributes);        
/*{    protected final PropertyChangeSupport pcs;

    /**
     * Default constructor of <%=clazzName%>.
     *)
    public <%=clazzName%>() {
        pcs = new PropertyChangeSupport(this);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

}*/
        generateGetters(output, attributes);
        generateSetters(output, attributes);
        generateGetChild(output, multipleAttr);
        generateAddChild(output, multipleAttr);
        generateRemoveChild(output, multipleAttr);
        if (generateToString) {
            generateToString(output, clazz);
        }
        if (!multipleAttr.isEmpty()) {
/*{
    
    protected <T> T getChild(Collection<T> childs, int index) {
        if (childs != null) {
            int i = 0;
            for (T o : childs) {
                if (index == i) {
                    return o;
                }
                i++;
            }
        }
        return null;
    }

 }*/
            if (needGetEntityMethod) {
/*{    protected <T extends TopiaEntity> T getEntity(Collection<T> childs, String topiaId) {
        if (childs != null) {
            for (T o : childs) {
                if (topiaId.equals(o.getTopiaId())) {
                    return o;
                }
            }
        }
        return null;
    }
 }*/            
            }
        }

/*{
    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
      pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

} //<%=clazz.getName()%>
}*/
    }

    protected void generateAttributes(Writer output, List<ObjectModelAttribute> attributes) throws IOException {
        
        for (ObjectModelAttribute attr : attributes) {
            
            if (!(attr.isNavigable()
                   || attr.hasAssociationClass())) {
                continue;
            }

            if (GeneratorUtil.hasDocumentation(attr)) {
/*{    /**
     * <%=attr.getDocumentation()%>
     *)
}*/
            }
            String annotation = attr.getTagValue(TAG_ANNOTATION);
            if (annotation != null && annotation.length() > 0) {
/*{    <%=annotation%>
}*/
            }
            String attrName = attr.getName();
            String attrVisibility = attr.getVisibility();
            String attrType = attr.getType();            
            if (attr.hasAssociationClass()) {
                String assocAttrName = getAssocAttrName(attr);
                attrName = GeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
                attrType = attr.getAssociationClass().getName();
            }
            int dot = attrType.lastIndexOf(".");
            if (dot>-1) {
                attrType = attrType.substring(dot + 1);
            }
            if (GeneratorUtil.isNMultiplicity(attr)) {
                attrType = JavaGeneratorUtil.getAttributeInterfaceType(attr, attrType, true);
            }
            
/*{    <%=attrVisibility%> <%=attrType%> <%=attrName%>;
}*/
        }        
    }

    /**
     * Renvoie le nom de l'attribut de classe d'association en fonction des cas:
     * Si l'attribut porte le même nom que le type (extrémité inverse de
     * l'association), on lui ajoute le nom de la classe d'association
     *
     * @param attr l'attribut a traiter
     * @return le nom de l'attribut de classe d'association
     */
    public static String getAssocAttrName(ObjectModelAttribute attr) {
        String typeName = attr.getType().substring(
                attr.getType().lastIndexOf(".") + 1);
        String result = attr.getName();
        if (attr.getName().equalsIgnoreCase(typeName)) {
            result += StringUtils.capitalize(attr.getAssociationClass().getName());
        }
        return result;
    }

    protected void generateGetters(Writer output, List<ObjectModelAttribute> attributes) throws IOException {
        /*
         * Définition des getteurs et setteurs
         */
        for (ObjectModelAttribute attr : attributes) {
            
            if (!attr.isNavigable()) {
                continue;
            }

            String attrName = attr.getName();
            String attrType = attr.getType();
            if (attr.hasAssociationClass()) {
                String assocAttrName = getAssocAttrName(attr);
                attrName = GeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
                attrType = attr.getAssociationClass().getName();
            }
            String attrNameCapitalized = StringUtils.capitalize(attrName);
            int dot = attrType.lastIndexOf(".");
            if (dot>-1) {
                attrType = attrType.substring(dot + 1);
            }
            if (GeneratorUtil.isNMultiplicity(attr)) {
                attrType = JavaGeneratorUtil.getAttributeInterfaceType(attr, attrType, true);
            }            
/*{    public <%=attrType%> get<%=attrNameCapitalized%>() {
        return <%=attrName%>;
    }

}*/
        }
    }

    protected void generateSetters(Writer output, List<ObjectModelAttribute> attributes) throws IOException {
        /*
         * Définition des getteurs et setteurs
         */
        for (ObjectModelAttribute attr : attributes) {
            
            if (!attr.isNavigable()) {
                continue;
            }

            String attrName = attr.getName();
            String attrType = attr.getType();

            if (attr.hasAssociationClass()) {
                String assocAttrName = getAssocAttrName(attr);
                attrName = GeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
                attrType = attr.getAssociationClass().getName();
            }
            String attrNameCapitalized = StringUtils.capitalize(attrName);
            int dot = attrType.lastIndexOf(".");
            if (dot>-1) {
                attrType = attrType.substring(dot + 1);
            }
            if (GeneratorUtil.isNMultiplicity(attr)) {
                attrType  = JavaGeneratorUtil.getAttributeInterfaceType(attr, attrType, true);
            }            
/*{    public void set<%=attrNameCapitalized%>(<%=attrType%> newValue) {
        <%=attrType%> oldValue = get<%=attrNameCapitalized%>();
        this.<%=attrName%> = newValue;
        firePropertyChange("<%=attrName%>", oldValue, newValue);
    }

}*/
        }
    }

    protected void generateGetChild(Writer output, List<ObjectModelAttribute> multipleAttr) throws IOException {
        
        for (ObjectModelAttribute attr : multipleAttr) {

            String attrName = attr.getName();
            String attrNameCapitalized = StringUtils.capitalize(attrName);
            String attrType = attr.getType();
            int dot = attrType.lastIndexOf(".");
            if (dot>-1) {
                attrType = attrType.substring(dot + 1);
            }
            ObjectModelClass attrEntity=null;
            if (model.hasClass(attr.getType())) {
                attrEntity = model.getClass(attr.getType());
            }
            boolean isEntity = (attrEntity != null && attrEntity.hasStereotype("entity"));
/*{    public <%=attrType%> get<%=attrNameCapitalized%>(int index) {
 	    <%=attrType%> o = getChild(<%=attrName%>, index);
        return o;
    }

}*/
            if (isEntity) {
/*{    public <%=attrType%> get<%=attrNameCapitalized%>(String topiaId) {
        <%=attrType%> o = getEntity(<%=attrName%>, topiaId);
        return o;
    }

}*/
            }
        }
    }
    protected void generateAddChild(Writer output, List<ObjectModelAttribute> multipleAttr) throws IOException {
        for (ObjectModelAttribute attr : multipleAttr) {
            
            String attrName = attr.getName();
            String attrNameCapitalized = StringUtils.capitalize(attrName);
            String attrType = attr.getType();
            int dot = attrType.lastIndexOf(".");
            if (dot>-1) {
                attrType = attrType.substring(dot + 1);
            }            
/*{    public <%=attrType%> add<%=attrNameCapitalized%>(<%=attrType%> <%=attrName%>) {
 	    get<%=attrNameCapitalized%>().add(<%=attrName%>);
        firePropertyChange("<%=attrName%>", null, <%=attrName%>);
        return <%=attrName%>;
    }

}*/
        }
    }

    protected void generateRemoveChild(Writer output, List<ObjectModelAttribute> multipleAttr) throws IOException {
        for (ObjectModelAttribute attr : multipleAttr) {
            String attrName = attr.getName();
            String attrNameCapitalized = StringUtils.capitalize(attrName);
            String attrType = attr.getType();
            int dot = attrType.lastIndexOf(".");
            if (dot>-1) {
                attrType = attrType.substring(dot + 1);
            }
/*{    public boolean remove<%=attrNameCapitalized%>(<%=attrType%> <%=attrName%>) {
        boolean  removed = get<%=attrNameCapitalized%>().remove(<%=attrName%>);
        if (removed) {
            firePropertyChange("<%=attrName%>", <%=attrName%>, null);
        }
        return removed;
    }

}*/
        }
    }

    protected void generateInterfaceOperations(Writer output, ObjectModelClassifier classifier) throws IOException {
        for (ObjectModelOperation op : classifier.getOperations()) {
        	String opName = op.getName();
/*{    /**
}*/
            if (GeneratorUtil.hasDocumentation(op)) {
            	String opDocumentation = op.getDocumentation();
/*{     * <%=opName%> : <%=opDocumentation%>
}*/
            }
            Collection<ObjectModelParameter> params = op.getParameters();
            for (ObjectModelParameter param : params) {
            	String paramName = param.getName();
            	String paramDocumentation = param.getDocumentation();
/*{     * @param <%=paramName%> <%=paramDocumentation%>
 }*/
            }
            String opVisibility = op.getVisibility();
            String opType = op.getReturnType();
/*{     *)
    <%=opVisibility%> abstract <%=opType%> <%=opName%>(}*/
            String comma = "";
            for (ObjectModelParameter param : params) {
            	String paramName = param.getName();
            	String paramType = param.getType();
/*{<%=comma%><%=paramType%> <%=paramName%>}*/
                comma = ", ";
            }
/*{)}*/
            Set<String> exceptions = op.getExceptions();
            comma = " throws ";
            for (String exception : exceptions) {
/*{<%=comma%><%=exception%>}*/
                comma = ", ";
            }
/*{;

}*/
        }
    }
    protected void generateToString(Writer output, ObjectModelClass clazz) throws IOException {
/*{
    @Override
    public String toString() {
        String result = new ToStringBuilder(this).
}*/
        for (ObjectModelAttribute attr : clazz.getAttributes()) {
            if (!(attr.isNavigable() || attr.hasAssociationClass())) {
                continue;
            }
            //FIXME possibilité de boucles (non directes)
            String attrName = attr.getName();
/*{         append("<%=attrName%>", this.<%=attrName%>).
}*/
        }
/*{         toString();
        return result;
    }
 }*/
    }

    protected boolean isAbstract(ObjectModelClass clazz) {
        if (clazz.isAbstract()) {
            return true;
        }
        return !clazz.getOperations().isEmpty();
    }

    /**
     * Dependecy gestion for DTO generation.
     * All primitives attributes (and dates) of dependencies entities of the DTO are
     * copied in the DTO. This method only prepare a list of attributes to be generated.
     * @param clazz DTO ObjectModelClass
     * @param attributes list of attributes for the generation (may be not empty)
     * @param imports the ImportsManager used to generate the header imports of the DTO
     * @return the same list of attributes in parameter with attributes from entities dependencies
     * @see org.nuiton.eugene.java.extension.ImportsManager
     * @see org.nuiton.eugene.models.object.ObjectModelDependency
     */
    private List<ObjectModelAttribute> setAttributesForDTO(ObjectModelClass clazz,
            List<ObjectModelAttribute> attributes, ImportsManager imports) {

        if (clazz.hasStereotype(STEREOTYPE_DTO)) {
            if (log.isInfoEnabled()) {
                log.info("DTO dependency gestion");
            }
            for (ObjectModelDependency dependency : clazz.getDependencies()) {
                ObjectModelClass supplier = (ObjectModelClass)dependency.getSupplier();

                // ENTITY dependency
                // Copy all primitives attributes from the Entity (supplier) to the DTO
                // Prepare a list to future generation of all object generated attributes
                if (supplier.hasStereotype(STEREOTYPE_ENTITY)) {
                    if (log.isInfoEnabled()) {
                        log.info("Create primitive and date fields in DTO from Entity : "
                                + supplier.getQualifiedName());
                    }
                    for (ObjectModelAttribute attr : supplier.getAttributes()) {
                        if (isPrimitiveType(attr) || isDateType(attr)) {
                            attributes.add(attr);
                            imports.addImport(attr.getType());
                        }
                        if (GeneratorUtil.isNMultiplicity(attr)) {
                            imports.addImport("java.util.Collection");
                        }
                    }
                }
            }
        }
        return attributes;
    }

    private static final Set<String> numberTypes = new HashSet<String>();
    private static final Set<String> textTypes = new HashSet<String>();
    private static final Set<String> booleanTypes = new HashSet<String>();
    private static final Set<String> primitiveTypes = new HashSet<String>();

    static {
        numberTypes.add("byte");
        numberTypes.add("java.lang.Byte");
        numberTypes.add("Byte");
        numberTypes.add("short");
        numberTypes.add("java.lang.Short");
        numberTypes.add("Short");
        numberTypes.add("int");
        numberTypes.add("java.lang.Integer");
        numberTypes.add("Integer");
        numberTypes.add("long");
        numberTypes.add("java.lang.Long");
        numberTypes.add("Long");
        numberTypes.add("float");
        numberTypes.add("java.lang.Float");
        numberTypes.add("Float");
        numberTypes.add("double");
        numberTypes.add("java.lang.Double");
        numberTypes.add("Double");

        textTypes.add("char");
        textTypes.add("java.lang.Char");
        textTypes.add("Char");
        textTypes.add("java.lang.String");
        textTypes.add("String");

        booleanTypes.add("boolean");
        booleanTypes.add("java.lang.Boolean");
        booleanTypes.add("Boolean");

        primitiveTypes.addAll(numberTypes);
        primitiveTypes.addAll(textTypes);
        primitiveTypes.addAll(booleanTypes);
    }

    public static boolean isNumericType(ObjectModelAttribute attr) {
        return numberTypes.contains(attr.getType());
    }

    public static boolean isTextType(ObjectModelAttribute attr) {
        return textTypes.contains(attr.getType());
    }

    public static boolean isDateType(ObjectModelAttribute attr) {
        return "java.util.Date".equals(attr.getType());
    }

    public static boolean isBooleanType(ObjectModelAttribute attr) {
        return booleanTypes.contains(attr.getType());
    }

    public static boolean isPrimitiveType(ObjectModelAttribute attr) {
        return primitiveTypes.contains(attr.getType());
    }
    
} //BeanGenerator
