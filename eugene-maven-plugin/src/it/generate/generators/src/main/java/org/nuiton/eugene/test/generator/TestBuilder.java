/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
 * *##% 
 * EUGene Test
 * Copyright (C) 2007 - 2009 CodeLutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * ##%*
 */

package org.nuiton.eugene.test.generator;

import org.nuiton.eugene.java.JavaBuilder;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/
/**
 * TestBuilder
 *
 * Created: 25 oct. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */
public class TestBuilder extends JavaBuilder {

    public TestBuilder() {
        super("TestModel");
    }

    //@Override
    public void build() {
        createRole();
        createPerson();
    }

    private void createRole() {
        ObjectModelClass roleClass = createClass("Role", "org.chorem.bonzoms");

        addAttribute(roleClass, "name", "java.lang.String");

        //this.addImportForClassifier(roleClass, Date.class);
        addAttribute(roleClass, "fromDate", "java.util.Date");
        addAttribute(roleClass, "thruDate", "java.util.Date");
    }

    private void createPerson() {
        ObjectModelClass personneClass = createClass("Person", "org.chorem.bonzoms");

        addAttribute(personneClass, "lastName", "java.lang.String");
        addAttribute(personneClass, "firstName", "java.lang.String", "\"2.0\"");

        //this.addImportForClassifier(personneClass, List.class);
        addAttribute(personneClass, "roles", "java.util.List<org.chorem.bonzoms.Role>",
                "new java.util.ArrayList<org.chorem.bonzoms.Role>()");


        ObjectModelOperation setLastName = addOperation(personneClass, "setLastName", "void",
                ObjectModelModifier.PUBLIC);
        addParameter(setLastName, "java.lang.String", "lastName");
        setOperationBody(setLastName, ""
    /*{
        this.lastName = lastName;
    }*/
        );

        ObjectModelOperation getLastName = addOperation(personneClass, "getLastName", "java.lang.String",
                ObjectModelModifier.PUBLIC);
        setOperationBody(getLastName, ""
    /*{
        return this.lastName;
    }*/
        );
    }

}
