/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
 * *##% 
 * EUGene Test
 * Copyright (C) 2007 - 2009 CodeLutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * ##%*
 */
package org.nuiton.eugene.test.generator;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.*;

import java.util.Iterator;

/*{generator option: parentheses = false}*/

/*{generator option: writeString = +}*/
/**
 * BeanTransformer
 *
 * Created: 28 oct. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.eugene.test.generator.BeanTransformer"
 */
public class BeanTransformer extends ObjectModelTransformerToJava {

    public BeanTransformer() {
        super();
    }

    @Override
    public void transformFromClass(ObjectModelClass clazz) {
        if (!clazz.hasStereotype(BeanGenerator.STEREOTYPE_BEAN) &&
                !clazz.hasStereotype(BeanGenerator.STEREOTYPE_DTO)) {
            return;
        }

        ObjectModelClass resultClass;
        if (!clazz.getOperations().isEmpty()) {
            resultClass = createAbstractClass(clazz.getName(), clazz.getPackageName());
        } else {
            resultClass = createClass(clazz.getName(), clazz.getPackageName());
        }

        createForDTO(resultClass, clazz);

        // Set superclass
        Iterator<ObjectModelClass> j = clazz.getSuperclasses().iterator();
        if (j.hasNext()) {
            ObjectModelClass p = j.next();
            setSuperClass(resultClass, p.getQualifiedName());
        }

        // Add interfaces from inputModel
        for (ObjectModelInterface parentInterface : clazz.getInterfaces()) {
            addInterface(resultClass, parentInterface.getQualifiedName());
        }

        createListeners(resultClass, clazz);

        boolean hasEntity = false;
        boolean hasMultipleAttribute = false;

        // Add attributes with getter/setter
        for (ObjectModelAttribute attr : clazz.getAttributes()) {

            if (attr.isNavigable()/* || attr.hasAssociationClass()*/) {
                String attrType = attr.getType();
                String simpleType = GeneratorUtil.getSimpleName(attrType);
                String attrName = attr.getName();
                String attrNameCapitalized = StringUtils.capitalize(attrName);

                // multiple attribute
                if (GeneratorUtil.isNMultiplicity(attr)) {
                    hasMultipleAttribute = true;

                    // Add getChild
                    ObjectModelOperation getChild = addOperation(resultClass, "get" + attrNameCapitalized,
                            attrType, ObjectModelModifier.PUBLIC);
                    addParameter(getChild, "int", "index");
                    setOperationBody(getChild, ""
    /*{
        <%=simpleType%> o = getChild(<%=attrName%>, index);
        return o;
    }*/
                    );

                    // Add getEntity
                    ObjectModelClass attrEntity = null;
                    if (getModel().hasClass(attr.getType())) {
                        attrEntity = getModel().getClass(attr.getType());
                    }
                    boolean isEntity = (attrEntity != null && attrEntity.hasStereotype(BeanGenerator.STEREOTYPE_ENTITY));

                    if (isEntity) {
                        hasEntity = true;
                        ObjectModelOperation getChildEntity = addOperation(resultClass, "get" + attrNameCapitalized,
                            attrType, ObjectModelModifier.PUBLIC);
                        addParameter(getChildEntity, String.class.getName(), "topiaId");
                        setOperationBody(getChildEntity, ""
    /*{
        <%=simpleType%> o = getEntity(<%=attrName%>, topiaId);
        return o;
    }*/
                        );
                    }

                    // Add addChild
                    ObjectModelOperation addChild = addOperation(resultClass, "add" + attrNameCapitalized,
                            attrType, ObjectModelModifier.PUBLIC);
                    addParameter(addChild, attrType, attrName);
                    setOperationBody(addChild, ""

    /*{
        get<%=attrNameCapitalized%>().add(<%=attrName%>);
        firePropertyChange("<%=attrName%>", null, <%=attrName%>);
        return <%=attrName%>;
    }*/
                    );

                    // Add removeChild
                    ObjectModelOperation removeChild = addOperation(resultClass, "remove" + attrNameCapitalized,
                            "boolean", ObjectModelModifier.PUBLIC);
                    addParameter(removeChild, attrType, attrName);
                    setOperationBody(removeChild, ""

    /*{
        boolean  removed = get<%=attrNameCapitalized%>().remove(<%=attrName%>);
        if (removed) {
            firePropertyChange("<%=attrName%>", <%=attrName%>, null);
        }
        return removed;
    }*/
                    );

                    // Change type for Multiple attribute
                    attrType = JavaGeneratorUtil.getAttributeInterfaceType(attr, true);
                    simpleType = GeneratorUtil.getSimpleName(attrType);
                }

                if (attr.hasAssociationClass()) {
                    String assocAttrName = BeanGenerator.getAssocAttrName(attr);
                    attrName = GeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
                    attrType = attr.getAssociationClass().getName();
                }

                // Add attribute
                String visibility = attr.getVisibility();
                addAttribute(resultClass, attrName, attrType, "", ObjectModelModifier.toValue(visibility));

                // Add getter
                ObjectModelOperation getter = addOperation(resultClass, "get" + attrNameCapitalized, attrType,
                        ObjectModelModifier.PUBLIC);
                setOperationBody(getter, ""
    /*{
        return this.<%=attrName%>;
    }*/
                );

                // Add setter
                ObjectModelOperation setter = addOperation(resultClass, "set" + attrNameCapitalized, "void",
                        ObjectModelModifier.PUBLIC);
                addParameter(setter, attrType, "newValue");
                setOperationBody(setter, ""
    /*{
        <%=simpleType%> oldValue = get<%=attrNameCapitalized%>();
        this.<%=attrName%> = newValue;
        firePropertyChange("<%=attrName%>", oldValue, newValue);
    }*/
                );

            }
        }

        // Add helper methods
        if (hasMultipleAttribute) {
            ObjectModelOperation getChild = addOperation(resultClass, "getChild", "<T> T",
                    ObjectModelModifier.PROTECTED);
            addParameter(getChild, "java.util.Collection<T>", "childs");
            addParameter(getChild, "int", "index");
            setOperationBody(getChild, ""
    /*{
        if (childs != null) {
            int i = 0;
            for (T o : childs) {
                if (index == i) {
                    return o;
                }
                i++;
            }
        }
        return null;
    }*/
            );
        }

        if (hasEntity) {
            ObjectModelOperation getEntity = addOperation(resultClass, "getEntity",
                    "<T extends org.nuiton.topia.persistence.TopiaEntity> T", ObjectModelModifier.PROTECTED);
            addParameter(getEntity, "java.util.Collection<T>", "childs");
            addParameter(getEntity, "java.lang.String", "topiaId");
            setOperationBody(getEntity, ""
    /*{
        if (childs != null) {
            for (T o : childs) {
                if (topiaId.equals(o.getTopiaId())) {
                    return o;
                }
            }
        }
        return null;
    }*/
            );
        }

        // Add operations
        for (ObjectModelOperation op : clazz.getOperations()) {
            String visibility = op.getVisibility();
            ObjectModelOperation resultOperation = addOperation(resultClass, op.getName(), op.getReturnType(),
                     ObjectModelModifier.toValue(visibility), ObjectModelModifier.ABSTRACT);

            for (ObjectModelParameter param : op.getParameters()) {
                addParameter(resultOperation, param.getType(), param.getName());
            }

            for (String exception : op.getExceptions()) {
                addException(resultOperation, exception);
            }
        }


    }

    private void createForDTO(ObjectModelClass resultClass, ObjectModelClass inputClass) {

        // Add Serializable implements for DTO generation
        if (inputClass.hasStereotype(BeanGenerator.STEREOTYPE_DTO)) {
            addInterface(resultClass, "java.io.Serializable");
        }

        String svUID = "1L";
        if (svUID != null) {
            addConstant(resultClass, "serialVersionUID", "long", svUID, ObjectModelModifier.PUBLIC);
        }
    }

    protected void createListeners(ObjectModelClass resultClass, ObjectModelClass inputClass) {

        addAttribute(resultClass, "pcs", "java.beans.PropertyChangeSupport", "",
                ObjectModelModifier.PROTECTED, ObjectModelModifier.FINAL);

        // Default constructor
        ObjectModelOperation constructor = addConstructor(resultClass, ObjectModelModifier.PUBLIC);
        setOperationBody(constructor, ""
    /*{
        pcs = new PropertyChangeSupport(this);
    }*/
        );

        // Add PropertyListener
        String propType = "java.beans.PropertyChangeListener";
        String strType = String.class.getName();
        String objectType = Object.class.getName();

        ObjectModelOperation addPropertyChangeListener = addOperation(resultClass,
                "addPropertyChangeListener", "void", ObjectModelModifier.PUBLIC);
        addParameter(addPropertyChangeListener, propType, "listener");
        setOperationBody(addPropertyChangeListener, ""
    /*{
        pcs.addPropertyChangeListener(listener);
    }*/
        );

        ObjectModelOperation addPropertyChangeListenerPlus = addOperation(resultClass,
                "addPropertyChangeListener", "void", ObjectModelModifier.PUBLIC);
        addParameter(addPropertyChangeListenerPlus, strType, "propertyName");
        addParameter(addPropertyChangeListenerPlus, propType, "listener");
        setOperationBody(addPropertyChangeListenerPlus, ""
    /*{
        pcs.addPropertyChangeListener(propertyName, listener);
    }*/
        );

        ObjectModelOperation removePropertyChangeListener = addOperation(resultClass,
                "removePropertyChangeListener", "void", ObjectModelModifier.PUBLIC);
        addParameter(removePropertyChangeListener, propType, "listener");
        setOperationBody(removePropertyChangeListener, ""
    /*{
        pcs.removePropertyChangeListener(listener);
    }*/
        );

        ObjectModelOperation removePropertyChangeListenerPlus = addOperation(resultClass,
                "removePropertyChangeListener", "void", ObjectModelModifier.PUBLIC);
        addParameter(removePropertyChangeListenerPlus, strType, "propertyName");
        addParameter(removePropertyChangeListenerPlus, propType, "listener");
        setOperationBody(removePropertyChangeListenerPlus, ""
    /*{
        pcs.removePropertyChangeListener(propertyName, listener);
    }*/
        );

        ObjectModelOperation firePropertyChange = addOperation(resultClass,
                "firePropertyChange", "void", ObjectModelModifier.PROTECTED);
        addParameter(firePropertyChange, strType, "propertyName");
        addParameter(firePropertyChange, objectType, "oldValue");
        addParameter(firePropertyChange, objectType, "newValue");
        setOperationBody(firePropertyChange, ""
    /*{
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }*/
        );
    }


}
