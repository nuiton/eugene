/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

// Zargo to xmi

assert new File(basedir, 'target/generated-sources-xmi').exists();
assert new File(basedir, 'target/generated-sources-xmi/xmi').exists();
assert new File(basedir, 'target/generated-sources-xmi/xmi/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-xmi/xmi/topiatest.xmi').exists();

assert !new File(basedir, 'target/generated-sources-xmi/models').exists();
assert !new File(basedir, 'target/generated-sources-xmi/java').exists();

// Zargo to xmi 2

assert new File(basedir, 'target/generated-sources-xmi2').exists();

assert new File(basedir, 'target/generated-sources-xmi2/xmi').exists();
assert new File(basedir, 'target/generated-sources-xmi2/xmi/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-xmi2/xmi/topiatest.xmi').exists();

assert !new File(basedir, 'target/generated-sources-xmi2/models').exists();
assert !new File(basedir, 'target/generated-sources-xmi2/java').exists();

return true;

