/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

// Zargo to java

assert new File(basedir, 'target/generated-sources-zargo2Java').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/xmi').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/xmi/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/xmi/topiatest.xmi').exists();

assert new File(basedir, 'target/generated-sources-zargo2Java/models').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/models/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/models/topiatest.objectmodel').exists();

assert new File(basedir, 'target/generated-sources-zargo2Java/java').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/java/org/nuiton/topiatest/beangen').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/java/org/nuiton/topiatest/beangen/Voiture.java').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/java/org/nuiton/topiatest/beangen/Roue.java').exists();
assert new File(basedir, 'target/generated-sources-zargo2Java/java/org/nuiton/topiatest/beangen/Siege.java').exists();

// Xmi to java


assert new File(basedir, 'target/generated-sources-xmi2Java').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/models').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/models/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/models/topiatest.objectmodel').exists();

assert new File(basedir, 'target/generated-sources-xmi2Java/models').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/models/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/models/topiatest.objectmodel').exists();

assert new File(basedir, 'target/generated-sources-xmi2Java/java').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/java/org/nuiton/topiatest/beangen2').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/java/org/nuiton/topiatest/beangen2/Voiture.java').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/java/org/nuiton/topiatest/beangen2/Roue.java').exists();
assert new File(basedir, 'target/generated-sources-xmi2Java/java/org/nuiton/topiatest/beangen2/Siege.java').exists();

// ObjectModel to java

assert new File(basedir, 'target/generated-sources-objectModel2Java').exists();

assert new File(basedir, 'target/generated-sources-objectModel2Java/java').exists();
assert new File(basedir, 'target/generated-sources-objectModel2Java/java/org/nuiton/topiatest/beangen').exists();
assert new File(basedir, 'target/generated-sources-objectModel2Java/java/org/nuiton/topiatest/beangen/Voiture.java').exists();
assert new File(basedir, 'target/generated-sources-objectModel2Java/java/org/nuiton/topiatest/beangen/Roue.java').exists();
assert new File(basedir, 'target/generated-sources-objectModel2Java/java/org/nuiton/topiatest/beangen/Siege.java').exists();

return true;

