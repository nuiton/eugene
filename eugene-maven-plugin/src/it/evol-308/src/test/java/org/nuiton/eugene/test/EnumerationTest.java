/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.test;

import org.junit.Assert;
import org.junit.Test;

public class EnumerationTest {

    @Test
    public void testEnumeration() {
        Assert.assertNotNull(MyEnumeration.values());
        Assert.assertEquals(3,MyEnumeration.values().length);
        Assert.assertNotNull(MyEnumeration.one);
        Assert.assertNotNull(MyEnumeration.two);
        Assert.assertNotNull(MyEnumeration.three);

    }
}
