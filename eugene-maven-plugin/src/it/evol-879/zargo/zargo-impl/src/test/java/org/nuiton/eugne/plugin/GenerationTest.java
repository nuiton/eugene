/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.plugin;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

public class GenerationTest {

    static File target;

    File tmp;

    String extractdDir, generateDir;

    public static File getBaseDir() {
        // Search basedir from maven environment
        String basedirPath = System.getenv("basedir");
        if (basedirPath == null) {
            basedirPath = new File("").getAbsolutePath();
        }

        File result = new File(basedirPath);
        return result;
    }

    @BeforeClass
    public static void init() {
        File basedir = getBaseDir();
        target = new File(basedir, "target");
    }

    public void setTestNumber(int i) {
        extractdDir = "extracted-sources" + i;
        generateDir = "generated-sources" + i;

    }

    protected void checkExist(File dir, String... files) {
        for (String file : files) {
            File f = new File(dir, file);
            Assert.assertTrue("File " + file + " should exists.", f.exists());
        }
    }

//    <id>Test 1 : from classpath:/xmi with resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:zargo:/xmi:topiatest.zargo</inputs>
//      <outputDirectory>target/generated-sources1</outputDirectory>
//      <extractDirectory>target/extracted-sources1</extractDirectory>
//    </configuration>

//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    |-- extracted-sources1
//    |   `-- zargo
//    |       `-- xmi
//    |           |-- topiatest.properties
//    |           `-- topiatest.zargo
//    `-- generated-sources1
//        |-- models
//        |   |-- topiatest.objectmodel
//        |   `-- topiatest.properties
//        `-- xmi
//            |-- topiatest.properties
//            `-- topiatest.xmi


    @Test
    public void test1() {
        setTestNumber(1);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "zargo");
        checkExist(tmp = new File(tmp, "zargo"), "xmi");
        checkExist(new File(tmp, "xmi"), "topiatest.properties", "topiatest.zargo");

        checkExist(tmp = new File(target, generateDir), "models", "xmi");
        checkExist(new File(tmp, "models"), "topiatest.properties", "topiatest.objectmodel");
        checkExist(new File(tmp, "xmi"), "topiatest.properties", "topiatest.xmi");

    }

//    <id>Test 2 : from classpath:/xmi with resources (with no protocol)</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:/xmi:topiatest.zargo</inputs>
//      <outputDirectory>target/generated-sources2</outputDirectory>
//      <extractDirectory>target/extracted-sources</extractDirectory>
//    </configuration>

//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    |-- extracted-sources2
//    |   `-- zargo
//    |       `-- xmi
//    |           |-- topiatest.properties
//    |           `-- topiatest.zargo
//    `-- generated-sources2
//        |-- models
//        |   |-- topiatest.objectmodel
//        |   `-- topiatest.properties
//        `-- xmi
//            |-- topiatest.properties
//            `-- topiatest.xmi


    @Test
    public void test2() {
        setTestNumber(2);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "zargo");
        checkExist(tmp = new File(tmp, "zargo"), "xmi");
        checkExist(new File(tmp, "xmi"), "topiatest.properties", "topiatest.zargo");

        checkExist(tmp = new File(target, generateDir), "models", "xmi");
        checkExist(new File(tmp, "models"), "topiatest.properties", "topiatest.objectmodel");
        checkExist(new File(tmp, "xmi"), "topiatest.properties", "topiatest.xmi");
    }

//    <id>Test 3 : from classpath:/xmi2 without resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:zargo:/xmi2:topiatest2.zargo</inputs>
//      <outputDirectory>target/generated-sources2</outputDirectory>
//      <extractDirectory>target/extracted-sources2</extractDirectory>
//    </configuration>


//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    |-- extracted-sources3
//    |   `-- zargo
//    |       `-- xmi2
//    |           `-- topiatest2.zargo
//    `-- generated-sources3
//        |-- models
//        |   `-- topiatest2.objectmodel
//        `-- xmi
//            `-- topiatest2.xmi


    @Test
    public void test3() {
        setTestNumber(3);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "zargo");
        checkExist(tmp = new File(tmp, "zargo"), "xmi2");
        checkExist(new File(tmp, "xmi2"), "topiatest2.zargo");

        checkExist(tmp = new File(target, generateDir), "models", "xmi");

        checkExist(new File(tmp, "models"), "topiatest2.objectmodel");
        checkExist(new File(tmp, "xmi"), "topiatest2.xmi");
    }

//    <id>Test 4 : from classpath:/xmi2 without resources (with no protocol)</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:/xmi2:topiatest2.zargo</inputs>
//      <outputDirectory>target/generated-sources4</outputDirectory>
//      <extractDirectory>target/extracted-sources4</extractDirectory>
//    </configuration>


//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    |-- extracted-sources4
//    |   `-- zargo
//    |       `-- xmi2
//    |           `-- topiatest2.zargo
//    `-- generated-sources4
//        |-- models
//        |   `-- topiatest2.objectmodel
//        `-- xmi
//            `-- topiatest2.xmi


    @Test
    public void test4() {
        setTestNumber(4);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "zargo");
        checkExist(tmp = new File(tmp, "zargo"), "xmi2");
        checkExist(new File(tmp, "xmi2"), "topiatest2.zargo");

        checkExist(tmp = new File(target, generateDir), "models", "xmi");
        checkExist(new File(tmp, "models"), "topiatest2.objectmodel");
        checkExist(new File(tmp, "xmi"), "topiatest2.xmi");
    }

//    <id>Test 5 : from classpath:/ without resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:zargo:/:topiatest3.zargo</inputs>
//      <outputDirectory>target/generated-sources5</outputDirectory>
//      <extractDirectory>target/extracted-sources5</extractDirectory>
//    </configuration>


//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    |-- extracted-sources5
//    |   `-- zargo
//    |       `-- topiatest3.zargo
//    `-- generated-sources5
//        |-- models
//        |   `-- topiatest3.objectmodel
//        `-- xmi
//            `-- topiatest3.xmi

    @Test
    public void test5() {
        setTestNumber(5);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "zargo");
        checkExist(new File(tmp, "zargo"), "topiatest3.zargo");

        checkExist(tmp = new File(target, generateDir), "models", "xmi");
        checkExist(new File(tmp, "models"), "topiatest3.objectmodel");
        checkExist(new File(tmp, "xmi"), "topiatest3.xmi");
    }
//    <id>Test 6 : from classpath:/ without resources (with no protocol)</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:/:topiatest3.zargo</inputs>
//      <outputDirectory>target/generated-sources6</outputDirectory>
//      <extractDirectory>target/extracted-sources6</extractDirectory>
//    </configuration>


//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    |-- extracted-sources6
//    |   `-- zargo
//    |       `-- topiatest3.zargo
//    `-- generated-sources6
//        |-- models
//        |   `-- topiatest3.objectmodel
//        `-- xmi
//            `-- topiatest3.xmi

    @Test
    public void test6() {
        setTestNumber(6);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "zargo");
        checkExist(new File(tmp, "zargo"), "topiatest3.zargo");

        checkExist(tmp = new File(target, generateDir), "models", "xmi");
        checkExist(new File(tmp, "models"), "topiatest3.objectmodel");
        checkExist(new File(tmp, "xmi"), "topiatest3.xmi");
    }

//    <id>Test 7 : from src/main/xmi with resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>src/main/xmi:topiatest4.zargo</inputs>
//      <outputDirectory>target/generated-sources7</outputDirectory>
//      <extractDirectory>target/extracted-sources7</extractDirectory>
//    </configuration>


//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    `-- generated-sources7
//        |-- models
//        |   |-- topiatest4.objectmodel
//        |   `-- topiatest4.properties
//        `-- xmi
//            |-- topiatest4.properties
//            `-- topiatest4.xmi

    @Test
    public void test7() {
        setTestNumber(7);
        checkExist(target, generateDir);

        checkExist(tmp = new File(target, generateDir), "models", "xmi");
        checkExist(new File(tmp, "models"), "topiatest4.objectmodel", "topiatest4.properties");
        checkExist(new File(tmp, "xmi"), "topiatest4.xmi", "topiatest4.properties");
    }

//    <id>Test 8 : from classpath:/*.zargo</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:zargo:/:.*\.zargo</inputs>
//      <outputDirectory>target/generated-sources8</outputDirectory>
//      <extractDirectory>target/extracted-sources8</extractDirectory>
//    </configuration>


//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    |-- extracted-sources8
//    |   `-- zargo
//    |       |-- topiatest3.zargo
//    |       |-- xmi
//    |       |   |-- topiatest.properties
//    |       |   `-- topiatest.zargo
//    |       `-- xmi2
//    |           `-- topiatest2.zargo
//    `-- generated-sources8
//        |-- models
//        |   |-- topiatest3.objectmodel
//        |   |-- xmi
//        |   |   |-- topiatest.objectmodel
//        |   |   `-- topiatest.properties
//        |   `-- xmi2
//        |       `-- topiatest2.objectmodel
//        `-- xmi
//            |-- topiatest3.xmi
//            |-- xmi
//            |   |-- topiatest.properties
//            |   `-- topiatest.xmi
//            `-- xmi2
//                `-- topiatest2.xmi

    @Test
    public void test8() {
        setTestNumber(8);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "zargo");
        checkExist(tmp = new File(tmp, "zargo"), "xmi", "xmi2", "topiatest3.zargo");
        checkExist(new File(tmp, "xmi"), "topiatest.zargo", "topiatest.properties");
        checkExist(new File(tmp, "xmi2"), "topiatest2.zargo");

        checkExist(tmp = new File(target, generateDir), "models", "xmi");
        File tmp2;

        checkExist(tmp2 = new File(tmp, "models"), "topiatest3.objectmodel", "xmi", "xmi2");
        checkExist(new File(tmp2, "xmi"), "topiatest.objectmodel", "topiatest.properties");
        checkExist(new File(tmp2, "xmi2"), "topiatest2.objectmodel");

        checkExist(tmp2 = new File(tmp, "xmi"), "topiatest3.xmi", "xmi", "xmi2");
        checkExist(new File(tmp2, "xmi"), "topiatest.xmi", "topiatest.properties");
        checkExist(new File(tmp2, "xmi2"), "topiatest2.xmi");
    }

//    <id>Test 9 : from classpath:/*.zargo (with no protocol)</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:/:.*\.zargo</inputs>
//      <outputDirectory>target/generated-sources9</outputDirectory>
//      <extractDirectory>target/extracted-sources9</extractDirectory>
//    </configuration>


//    eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
//    |-- extracted-sources9
//    |   `-- zargo
//    |       |-- topiatest3.zargo
//    |       |-- xmi
//    |       |   |-- topiatest.properties
//    |       |   `-- topiatest.zargo
//    |       `-- xmi2
//    |           `-- topiatest2.zargo
//    `-- generated-sources9
//        |-- models
//        |   |-- topiatest3.objectmodel
//        |   |-- xmi
//        |   |   |-- topiatest.objectmodel
//        |   |   `-- topiatest.properties
//        |   `-- xmi2
//        |       `-- topiatest2.objectmodel
//        `-- xmi
//            |-- topiatest3.xmi
//            |-- xmi
//            |   |-- topiatest.properties
//            |   `-- topiatest.xmi
//            `-- xmi2
//                `-- topiatest2.xmi

    @Test
    public void test9() {
        setTestNumber(9);
        checkExist(target, extractdDir, generateDir);


        checkExist(tmp = new File(target, extractdDir), "zargo");

        checkExist(tmp = new File(tmp, "zargo"), "xmi", "xmi2", "topiatest3.zargo");

        checkExist(new File(tmp, "xmi"), "topiatest.zargo", "topiatest.properties");
        checkExist(new File(tmp, "xmi2"), "topiatest2.zargo");

        checkExist(tmp = new File(target, generateDir), "models", "xmi");

        File tmp2;
        checkExist(tmp2 = new File(tmp, "models"), "topiatest3.objectmodel", "xmi", "xmi2");
        checkExist(new File(tmp2, "xmi"), "topiatest.objectmodel", "topiatest.properties");
        checkExist(new File(tmp2, "xmi2"), "topiatest2.objectmodel");

        checkExist(tmp2 = new File(tmp, "xmi"), "topiatest3.xmi", "xmi", "xmi2");
        checkExist(new File(tmp2, "xmi"), "topiatest.xmi", "topiatest.properties");
        checkExist(new File(tmp2, "xmi2"), "topiatest2.xmi");
    }

}
