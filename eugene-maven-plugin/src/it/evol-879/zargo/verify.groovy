/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
outputDir = new File(basedir, 'zargo-impl/target/generated-sources');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources/zargo');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'xmi').exists();
assert new File(outputEDir, 'xmi/topiatest.zargo').exists();
assert new File(outputEDir, 'xmi/topiatest.properties').exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest.xmi').exists();
assert new File(outputDir, 'xmi/topiatest.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest.objectmodel').exists();
assert new File(outputDir, 'models/topiatest.properties').exists();

outputDir = new File(basedir, 'zargo-impl/target/generated-sources_2');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources_2/zargo');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'xmi').exists();
assert new File(outputEDir, 'xmi/topiatest.zargo').exists();
assert new File(outputEDir, 'xmi/topiatest.properties').exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest.xmi').exists();
assert new File(outputDir, 'xmi/topiatest.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest.objectmodel').exists();
assert new File(outputDir, 'models/topiatest.properties').exists();

outputDir = new File(basedir, 'zargo-impl/target/generated-sources2');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources2/zargo');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'xmi2').exists();
assert new File(outputEDir, 'xmi2/topiatest2.zargo').exists();
assert !new File(outputEDir, 'xmi2/topiatest2.properties').exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest2.xmi').exists();
assert !new File(outputDir, 'xmi/topiatest2.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest2.objectmodel').exists();
assert !new File(outputDir, 'models/topiatest2.properties').exists();

outputDir = new File(basedir, 'zargo-impl/target/generated-sources2_2');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources2_2/zargo');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'xmi2').exists();
assert new File(outputEDir, 'xmi2/topiatest2.zargo').exists();
assert !new File(outputEDir, 'xmi2/topiatest2.properties').exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest2.xmi').exists();
assert !new File(outputDir, 'xmi/topiatest2.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest2.objectmodel').exists();
assert !new File(outputDir, 'models/topiatest2.properties').exists();

outputDir = new File(basedir, 'zargo-impl/target/generated-sources3');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources3/zargo');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'topiatest3.zargo').exists();
assert !new File(outputEDir, 'topiatest3.properties').exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest3.xmi').exists();
assert !new File(outputDir, 'xmi/topiatest3.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest3.objectmodel').exists();
assert !new File(outputDir, 'models/topiatest3.properties').exists();

outputDir = new File(basedir, 'zargo-impl/target/generated-sources3_2');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources3_2/zargo');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'topiatest3.zargo').exists();
assert !new File(outputEDir, 'topiatest3.properties').exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest3.xmi').exists();
assert !new File(outputDir, 'xmi/topiatest3.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest3.objectmodel').exists();
assert !new File(outputDir, 'models/topiatest3.properties').exists();

outputDir = new File(basedir, 'zargo-impl/target/generated-sources4');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources4/zargo');

assert outputDir.exists();
assert !outputEDir.exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest4.xmi').exists();
assert new File(outputDir, 'xmi/topiatest4.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest4.objectmodel').exists();
assert new File(outputDir, 'models/topiatest4.properties').exists();

outputDir = new File(basedir, 'zargo-impl/target/generated-sources5');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources5/zargo');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'xmi').exists();
assert new File(outputEDir, 'xmi/topiatest.zargo').exists();
assert new File(outputEDir, 'xmi/topiatest.properties').exists();

assert new File(outputEDir, 'xmi2').exists();
assert new File(outputEDir, 'xmi2/topiatest2.zargo').exists();
assert !new File(outputEDir, 'xmi2/topiatest2.properties').exists();

assert new File(outputEDir, 'topiatest3.zargo').exists();
assert !new File(outputEDir, 'topiatest3.properties').exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest.xmi').exists();
assert new File(outputDir, 'xmi/topiatest.properties').exists();

assert new File(outputDir, 'xmi/topiatest2.xmi').exists();
assert !new File(outputDir, 'xmi/topiatest2.properties').exists();

assert new File(outputDir, 'xmi/topiatest3.xmi').exists();
assert !new File(outputDir, 'xmi/topiatest3.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest.objectmodel').exists();
assert new File(outputDir, 'models/topiatest.properties').exists();

assert new File(outputDir, 'models/topiatest2.objectmodel').exists();
assert !new File(outputDir, 'models/topiatest2.properties').exists();

assert new File(outputDir, 'models/topiatest3.objectmodel').exists();
assert !new File(outputDir, 'models/topiatest3.properties').exists();


outputDir = new File(basedir, 'zargo-impl/target/generated-sources5_2');
outputEDir = new File(basedir, 'zargo-impl/target/extracted-sources5_2/zargo');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'xmi').exists();
assert new File(outputEDir, 'xmi/topiatest.zargo').exists();
assert new File(outputEDir, 'xmi/topiatest.properties').exists();

assert new File(outputEDir, 'xmi2').exists();
assert new File(outputEDir, 'xmi2/topiatest2.zargo').exists();
assert !new File(outputEDir, 'xmi2/topiatest2.properties').exists();

assert new File(outputEDir, 'topiatest3.zargo').exists();
assert !new File(outputEDir, 'topiatest3.properties').exists();

assert new File(outputDir, 'xmi').exists();
assert new File(outputDir, 'xmi/topiatest.xmi').exists();
assert new File(outputDir, 'xmi/topiatest.properties').exists();

assert new File(outputDir, 'xmi/topiatest2.xmi').exists();
assert !new File(outputDir, 'xmi/topiatest2.properties').exists();

assert new File(outputDir, 'xmi/topiatest3.xmi').exists();
assert !new File(outputDir, 'xmi/topiatest3.properties').exists();

assert new File(outputDir, 'models').exists();
assert new File(outputDir, 'models/topiatest.objectmodel').exists();
assert new File(outputDir, 'models/topiatest.properties').exists();

assert new File(outputDir, 'models/topiatest2.objectmodel').exists();
assert !new File(outputDir, 'models/topiatest2.properties').exists();

assert new File(outputDir, 'models/topiatest3.objectmodel').exists();
assert !new File(outputDir, 'models/topiatest3.properties').exists();
*/
return true;
