On teste ici le goal generate avec un fichier zargo dans le class-path :

Le result doit être

eugene-maven-plugin/target/its/evol-879/zargo/zargo-impl/target/
|-- extracted-sources
|   `-- zargo
|       `-- xmi
|           |-- topiatest.properties
|           `-- topiatest.zargo
|-- extracted-sources1
|   `-- zargo
|       `-- xmi
|           |-- topiatest.properties
|           `-- topiatest.zargo
|-- extracted-sources3
|   `-- zargo
|       `-- xmi2
|           `-- topiatest2.zargo
|-- extracted-sources4
|   `-- zargo
|       `-- xmi2
|           `-- topiatest2.zargo
|-- extracted-sources5
|   `-- zargo
|       `-- topiatest3.zargo
|-- extracted-sources6
|   `-- zargo
|       `-- topiatest3.zargo
|-- extracted-sources8
|   `-- zargo
|       |-- topiatest3.zargo
|       |-- xmi
|       |   |-- topiatest.properties
|       |   `-- topiatest.zargo
|       `-- xmi2
|           `-- topiatest2.zargo
|-- extracted-sources9
|   `-- zargo
|       |-- topiatest3.zargo
|       |-- xmi
|       |   |-- topiatest.properties
|       |   `-- topiatest.zargo
|       `-- xmi2
|           `-- topiatest2.zargo
|-- generated-sources1
|   |-- models
|   |   |-- topiatest.objectmodel
|   |   `-- topiatest.properties
|   `-- xmi
|       |-- topiatest.properties
|       `-- topiatest.xmi
|-- generated-sources2
|   |-- models
|   |   |-- topiatest.objectmodel
|   |   `-- topiatest.properties
|   `-- xmi
|       |-- topiatest.properties
|       `-- topiatest.xmi
|-- generated-sources3
|   |-- models
|   |   `-- topiatest2.objectmodel
|   `-- xmi
|       `-- topiatest2.xmi
|-- generated-sources4
|   |-- models
|   |   `-- topiatest2.objectmodel
|   `-- xmi
|       `-- topiatest2.xmi
|-- generated-sources5
|   |-- models
|   |   `-- topiatest3.objectmodel
|   `-- xmi
|       `-- topiatest3.xmi
|-- generated-sources6
|   |-- models
|   |   `-- topiatest3.objectmodel
|   `-- xmi
|       `-- topiatest3.xmi
|-- generated-sources7
|   |-- models
|   |   |-- topiatest4.objectmodel
|   |   `-- topiatest4.properties
|   `-- xmi
|       |-- topiatest4.properties
|       `-- topiatest4.xmi
|-- generated-sources8
|   |-- models
|   |   |-- topiatest3.objectmodel
|   |   |-- xmi
|   |   |   |-- topiatest.objectmodel
|   |   |   `-- topiatest.properties
|   |   `-- xmi2
|   |       `-- topiatest2.objectmodel
|   `-- xmi
|       |-- topiatest3.xmi
|       |-- xmi
|       |   |-- topiatest.properties
|       |   `-- topiatest.xmi
|       `-- xmi2
|           `-- topiatest2.xmi
|-- generated-sources9
|   |-- models
|   |   |-- topiatest3.objectmodel
|   |   |-- xmi
|   |   |   |-- topiatest.objectmodel
|   |   |   `-- topiatest.properties
|   |   `-- xmi2
|   |       `-- topiatest2.objectmodel
|   `-- xmi
|       |-- topiatest3.xmi
|       |-- xmi
|       |   |-- topiatest.properties
|       |   `-- topiatest.xmi
|       `-- xmi2
|           `-- topiatest2.xmi
