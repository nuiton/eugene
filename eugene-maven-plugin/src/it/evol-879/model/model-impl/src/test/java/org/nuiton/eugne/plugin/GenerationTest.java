/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.plugin;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

public class GenerationTest {

    static File target;

    File tmp;

    String extractdDir, generateDir;

    public static File getBaseDir() {
        // Search basedir from maven environment
        String basedirPath = System.getenv("basedir");
        if (basedirPath == null) {
            basedirPath = new File("").getAbsolutePath();
        }

        File result = new File(basedirPath);
        return result;
    }

    @BeforeClass
    public static void init() {
        File basedir = getBaseDir();
        target = new File(basedir, "target");
    }

    public void setTestNumber(int i) {
        extractdDir = "extracted-sources" + i;
        generateDir = "generated-sources" + i;

    }

    protected void checkExist(File dir, String... files) {
        for (String file : files) {
            File f = new File(dir, file);
            Assert.assertTrue("File " + file + " should exists.", f.exists());
        }
    }

//    <id>Test 1 : from classpath:/models with resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:model:/models:topiatest.objectmodel
//      </inputs>
//      <outputDirectory>target/generated-sources1</outputDirectory>
//      <extractDirectory>target/extracted-sources1</extractDirectory>
//    </configuration>

    //    eugene-maven-plugin/target/its/evol-879/model/model-impl/target/
//    |-- extracted-sources1
//    |   `-- model
//    |       `-- models
//    |           |-- topiatest.objectmodel
//    |           `-- topiatest.properties
//    `-- generated-sources1
//        `-- java
//            `-- org
    @Test
    public void test1() {
        setTestNumber(1);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "model");
        checkExist(tmp = new File(tmp, "model"), "models");
        checkExist(new File(tmp, "models"), "topiatest.objectmodel", "topiatest.properties");

        checkExist(tmp = new File(target, generateDir), "java");
        checkExist(tmp = new File(tmp, "java"), "org");
    }

//    <id>Test 2 : from classpath:/models2 without resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <!--<skipInputs>model</skipInputs>-->
//      <inputs>classpath:model:/models2:topiatest.objectmodel
//      </inputs>
//      <outputDirectory>target/generated-sources2</outputDirectory>
//      <extractDirectory>target/extracted-sources2</extractDirectory>
//    </configuration>

    //    eugene-maven-plugin/target/its/evol-879/model/model-impl/target/
//    |-- extracted-sources2
//    |   `-- model
//    |       `-- models2
//    |           `-- topiatest2.objectmodel
//    `-- generated-sources2
//        `-- java
//            `-- org
    @Test
    public void test2() {
        setTestNumber(2);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "model");
        checkExist(tmp = new File(tmp, "model"), "models2");
        checkExist(new File(tmp, "models2"), "topiatest2.objectmodel");

        checkExist(tmp = new File(target, generateDir), "java");
        checkExist(tmp = new File(tmp, "java"), "org");
    }


//    <id>Test 3 : from classpath:/ without resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <!--<skipInputs>model</skipInputs>-->
//      <inputs>classpath:model:/:topiatest.objectmodel</inputs>
//      <outputDirectory>target/generated-sources3</outputDirectory>
//      <extractDirectory>target/extracted-sources3</extractDirectory>
//    </configuration>

    //    eugene-maven-plugin/target/its/evol-879/model/model-impl/target/
//    |-- extracted-sources3
//    |   `-- model
//    |       `-- topiatest3.objectmodel
//    `-- generated-sources3
//        `-- java
//            `-- org
    @Test
    public void test3() {
        setTestNumber(3);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "model");
        checkExist(new File(tmp, "model"), "topiatest3.objectmodel");

        checkExist(tmp = new File(target, generateDir), "java");
        checkExist(tmp = new File(tmp, "java"), "org");
    }

//    <id>Test 4 : from src/main/models with resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <!--<skipInputs>model</skipInputs>-->
//      <inputs>src/main/models:topiatest.objectmodel</inputs>
//      <outputDirectory>target/generated-sources4</outputDirectory>
//      <extractDirectory>target/extracted-sources4</extractDirectory>
//    </configuration>
//    eugene-maven-plugin/target/its/evol-879/model/model-impl/target/
//    |-- extracted-sources4
//    `-- generated-sources4
//        `-- java
//            `-- org
    @Test
    public void test4() {
        setTestNumber(4);
        checkExist(target, generateDir);

        checkExist(tmp = new File(target, generateDir), "java");
        checkExist(tmp = new File(tmp, "java"), "org");
    }

}
