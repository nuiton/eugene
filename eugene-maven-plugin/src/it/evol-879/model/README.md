On teste ici le goal generate avec un fichier model dans le class-path :

Le résultat désiré est :

eugene-maven-plugin/target/its/evol-879/model/model-impl/target/
|-- extracted-sources1
|   `-- model
|       `-- models
|           |-- topiatest.objectmodel
|           `-- topiatest.properties
|-- extracted-sources2
|   `-- model
|       `-- models2
|           `-- topiatest2.objectmodel
|-- extracted-sources3
|   `-- model
|       `-- topiatest3.objectmodel
|-- generated-sources1
|   `-- java
|       `-- org
|           `-- nuiton
|               `-- topia
|                   `-- test
|                       `-- entities
|-- generated-sources2
|   `-- java
|       `-- org
|           `-- nuiton
|               `-- topia
|                   `-- test
|                       `-- entities
|-- generated-sources3
|   `-- java
|       `-- org
|           `-- nuiton
|               `-- topia
|                   `-- test
|                       `-- entities
`-- generated-sources4
    `-- java
        `-- org
            `-- nuiton
                `-- topia
                    `-- test
                        `-- entities
