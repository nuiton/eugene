/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
outputDir = new File(basedir, 'model-impl/target/generated-sources');
outputEDir = new File(basedir, 'model-impl/target/extracted-sources/model');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'models').exists();
assert new File(outputEDir, 'models/topiatest.objectmodel').exists();
assert new File(outputEDir, 'models/topiatest.properties').exists();

assert new File(outputDir, 'java').exists();

outputDir = new File(basedir, 'model-impl/target/generated-sources2');
outputEDir = new File(basedir, 'model-impl/target/extracted-sources2/model');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'models2').exists();
assert new File(outputEDir, 'models2/topiatest.objectmodel').exists();
assert !new File(outputEDir, 'models2/topiatest.properties').exists();

assert new File(outputDir, 'java').exists();

outputDir = new File(basedir, 'model-impl/target/generated-sources3');
outputEDir = new File(basedir, 'model-impl/target/extracted-sources3/model');

assert outputDir.exists();
assert outputEDir.exists();

assert new File(outputEDir, 'topiatest.objectmodel').exists();
assert !new File(outputEDir, 'topiatest.properties').exists();

assert new File(outputDir, 'java').exists();

outputDir = new File(basedir, 'model-impl/target/generated-sources4');
outputEDir = new File(basedir, 'model-impl/target/extracted-sources4/model');

assert outputDir.exists();
assert !outputEDir.exists();

assert !new File(outputDir, 'models').exists();
assert new File(outputDir, 'java').exists();
*/

return true;

