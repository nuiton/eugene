/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.plugin;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

public class GenerationTest {

    static File target;

    File tmp;

    String extractdDir, generateDir;

    public static File getBaseDir() {
        // Search basedir from maven environment
        String basedirPath = System.getenv("basedir");
        if (basedirPath == null) {
            basedirPath = new File("").getAbsolutePath();
        }

        File result = new File(basedirPath);
        return result;
    }

    @BeforeClass
    public static void init() {
        File basedir = getBaseDir();
        target = new File(basedir, "target");
    }

    public void setTestNumber(int i) {
        extractdDir = "extracted-sources" + i;
        generateDir = "generated-sources" + i;

    }

    protected void checkExist(File dir, String... files) {
        for (String file : files) {
            File f = new File(dir, file);
            Assert.assertTrue("File " + file + " should exists.", f.exists());
        }
    }

//        <id>Test 1 : from classpath:/xmi with resources</id>
//        <phase>generate-sources</phase>
//        <configuration>
//          <inputs>classpath:xmi:/xmi:topiatest.xmi</inputs>
//          <outputDirectory>target/generated-sources1</outputDirectory>
//          <extractDirectory>target/extracted-sources1</extractDirectory>
//        </configuration>

//    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    |-- extracted-sources1
//    |   `-- xmi
//    |       `-- xmi
//    |           |-- topiatest.properties
//    |           `-- topiatest.xmi
//    `-- generated-sources1
//            `-- models
//                |-- topiatest.objectmodel
//                `-- topiatest.properties

    @Test
    public void test1() {
        setTestNumber(1);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "xmi");
        checkExist(tmp = new File(tmp, "xmi"), "xmi");
        checkExist(new File(tmp, "xmi"), "topiatest.properties", "topiatest.properties");

        checkExist(tmp = new File(target, generateDir), "models");
        checkExist(new File(tmp, "models"), "topiatest.properties", "topiatest.objectmodel");
    }

//    <id>Test 2 : from classpath:/xmi with resources (with no protocol)</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:/xmi:topiatest.xmi</inputs>
//      <outputDirectory>target/generated-sources2</outputDirectory>
//      <extractDirectory>target/extracted-sources2</extractDirectory>
//    </configuration>

//    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    |-- extracted-sources2
//    |   `-- xmi
//    |       `-- xmi
//    |           |-- topiatest.properties
//    |           `-- topiatest.xmi
//    `-- generated-sources2
//        `-- models
//            |-- topiatest.objectmodel
//            `-- topiatest.properties

    @Test
    public void test2() {
        setTestNumber(2);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "xmi");
        checkExist(tmp = new File(tmp, "xmi"), "xmi");
        checkExist(new File(tmp, "xmi"), "topiatest.properties", "topiatest.properties");

        checkExist(tmp = new File(target, generateDir), "models");
        checkExist(new File(tmp, "models"), "topiatest.properties", "topiatest.objectmodel");
    }


//    <id>Test 3 : from classpath:/xmi2 without resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:xmi:/xmi2:topiatest2.xmi</inputs>
//      <outputDirectory>target/generated-sources3</outputDirectory>
//      <extractDirectory>target/extracted-sources3</extractDirectory>
//    </configuration>

//    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    |-- extracted-sources3
//    |   `-- xmi
//    |       `-- xmi2
//    |           `-- topiatest2.xmi
//    `-- generated-sources3
//        `-- models
//            `-- topiatest2.objectmodel

    @Test
    public void test3() {
        setTestNumber(3);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "xmi");
        checkExist(tmp = new File(tmp, "xmi"), "xmi2");
        checkExist(new File(tmp, "xmi2"), "topiatest2.xmi");

        checkExist(tmp = new File(target, generateDir), "models");
        checkExist(new File(tmp, "models"), "topiatest2.objectmodel");
    }


//    <id>Test 4 : from classpath:/xmi2 without resources (with no protocol)</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:/xmi2:topiatest2.xmi</inputs>
//      <outputDirectory>target/generated-sources4</outputDirectory>
//      <extractDirectory>target/extracted-sources4</extractDirectory>
//    </configuration>

//    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    |-- extracted-sources4
//    |   `-- xmi
//    |       `-- xmi2
//    |           `-- topiatest2.xmi
//    `-- generated-sources4
//        `-- models
//            `-- topiatest2.objectmodel

    @Test
    public void test4() {
        setTestNumber(4);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "xmi");
        checkExist(tmp = new File(tmp, "xmi"), "xmi2");
        checkExist(new File(tmp, "xmi2"), "topiatest2.xmi");

        checkExist(tmp = new File(target, generateDir), "models");
        checkExist(new File(tmp, "models"), "topiatest2.objectmodel");
    }

//    <id>Test 5 : from classpath:/ without resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:xmi:/:topiatest3.xmi</inputs>
//      <outputDirectory>target/generated-sources5</outputDirectory>
//      <extractDirectory>target/extracted-sources5</extractDirectory>
//    </configuration>

    //    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    |-- extracted-sources5
//    |   `-- xmi
//    |       `-- topiatest3.xmi
//    `-- generated-sources5
//        `-- models
//            `-- topiatest3.objectmodel
    @Test
    public void test5() {
        setTestNumber(5);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "xmi");
        checkExist(new File(tmp, "xmi"), "topiatest3.xmi");

        checkExist(tmp = new File(target, generateDir), "models");
        checkExist(new File(tmp, "models"), "topiatest3.objectmodel");
    }


//    <id>Test 6 : from classpath:/ without resources (with no protocol)</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:/:topiatest3.xmi</inputs>
//      <outputDirectory>target/generated-sources6</outputDirectory>
//      <extractDirectory>target/extracted-sources6</extractDirectory>
//    </configuration>

    //    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    |-- extracted-sources6
//    |   `-- xmi
//    |       `-- topiatest3.xmi
//    `-- generated-sources6
//        `-- models
//            `-- topiatest3.objectmodel
    @Test
    public void test6() {
        setTestNumber(6);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "xmi");
        checkExist(new File(tmp, "xmi"), "topiatest3.xmi");

        checkExist(tmp = new File(target, generateDir), "models");
        checkExist(new File(tmp, "models"), "topiatest3.objectmodel");
    }

//    <id>Test 7 : from src/main/xmi with resources</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>src/main/xmi:topiatest4.xmi</inputs>
//      <outputDirectory>target/generated-sources7</outputDirectory>
//      <extractDirectory>target/extracted-sources7</extractDirectory>
//    </configuration>

    //    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    `-- generated-sources7
//        `-- models
//            |-- topiatest4.objectmodel
//            `-- topiatest4.properties
    @Test
    public void test7() {
        setTestNumber(7);
        checkExist(target, generateDir);

        checkExist(tmp = new File(target, generateDir), "models");
        checkExist(new File(tmp, "models"), "topiatest4.objectmodel", "topiatest4.properties");
    }

//    <id>Test 8 : from classpath:/*.xmi</id>
//    <phase>generate-sources</phase>
//    <configuration>
//      <inputs>classpath:xmi:/:.*topiatest.*\.xmi</inputs>
//      <outputDirectory>target/generated-sources8</outputDirectory>
//      <extractDirectory>target/extracted-sources8</extractDirectory>
//    </configuration>

//    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    |-- extracted-sources8
//    |   `-- xmi
//    |       |-- topiatest3.xmi
//    |       |-- xmi
//    |       |   |-- topiatest.properties
//    |       |   `-- topiatest.xmi
//    |       `-- xmi2
//    |           `-- topiatest2.xmi
//    `-- generated-sources8
//        `-- models
//            |-- topiatest3.objectmodel
//            |-- xmi
//            |   |-- topiatest.objectmodel
//            |   `-- topiatest.properties
//            `-- xmi2
//                `-- topiatest2.objectmodel

    @Test
    public void test8() {
        setTestNumber(8);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "xmi");
        checkExist(tmp = new File(tmp, "xmi"), "xmi", "xmi2", "topiatest3.xmi");
        checkExist(new File(tmp, "xmi"), "topiatest.xmi", "topiatest.properties");
        checkExist(new File(tmp, "xmi2"), "topiatest2.xmi");

        checkExist(tmp = new File(target, generateDir), "models");

        checkExist(tmp = new File(tmp, "models"), "topiatest3.objectmodel", "xmi", "xmi2");
        checkExist(new File(tmp, "xmi"), "topiatest.objectmodel", "topiatest.properties");
        checkExist(new File(tmp, "xmi2"), "topiatest2.objectmodel");
    }

//        <id>Test 9 : from classpath:/*.xmi (with no protocol)</id>
//        <phase>generate-sources</phase>
//        <configuration>
//          <inputs>classpath:/:.*topiatest.*\.xmi</inputs>
//          <outputDirectory>target/generated-sources9</outputDirectory>
//          <extractDirectory>target/extracted-sources9</extractDirectory>
//        </configuration>

//    eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
//    |-- extracted-sources9
//    |   `-- zargo
//    |       |-- topiatest3.zargo
//    |       |-- xmi
//    |       |   |-- topiatest.properties
//    |       |   `-- topiatest.zargo
//    |       `-- xmi2
//    |           `-- topiatest2.zargo
//    `-- generated-sources9
//        `-- models
//            |-- topiatest3.objectmodel
//            |-- xmi
//            |   |-- topiatest.objectmodel
//            |   `-- topiatest.properties
//            `-- xmi2
//               `-- topiatest2.objectmodel

    @Test
    public void test9() {
        setTestNumber(9);
        checkExist(target, extractdDir, generateDir);

        checkExist(tmp = new File(target, extractdDir), "xmi");
        checkExist(tmp = new File(tmp, "xmi"), "xmi", "xmi2", "topiatest3.xmi");
        checkExist(new File(tmp, "xmi"), "topiatest.xmi", "topiatest.properties");
        checkExist(new File(tmp, "xmi2"), "topiatest2.xmi");

        checkExist(tmp = new File(target, generateDir), "models");

        checkExist(tmp = new File(tmp, "models"), "topiatest3.objectmodel", "xmi", "xmi2");
        checkExist(new File(tmp, "xmi"), "topiatest.objectmodel", "topiatest.properties");
        checkExist(new File(tmp, "xmi2"), "topiatest2.objectmodel");
    }

}
