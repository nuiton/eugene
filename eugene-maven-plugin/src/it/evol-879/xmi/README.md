On teste ici le goal generate avec un fichier xmi dans le class-path :

Le result désiré est :

eugene-maven-plugin/target/its/evol-879/xmi/xmi-impl/target/
|-- extracted-sources1
|   `-- xmi
|       `-- xmi
|           |-- topiatest.properties
|           `-- topiatest.xmi
|-- extracted-sources2
|   `-- xmi
|       `-- xmi
|           |-- topiatest.properties
|           `-- topiatest.xmi
|-- extracted-sources3
|   `-- xmi
|       `-- xmi2
|           `-- topiatest2.xmi
|-- extracted-sources4
|   `-- xmi
|       `-- xmi2
|           `-- topiatest2.xmi
|-- extracted-sources5
|   `-- xmi
|       `-- topiatest3.xmi
|-- extracted-sources6
|   `-- xmi
|       `-- topiatest3.xmi
|-- extracted-sources8
|   `-- xmi
|       |-- topiatest3.xmi
|       |-- xmi
|       |   |-- topiatest.properties
|       |   `-- topiatest.xmi
|       `-- xmi2
|           `-- topiatest2.xmi
|-- extracted-sources9
|   `-- xmi
|       |-- topiatest3.xmi
|       |-- xmi
|       |   |-- topiatest.properties
|       |   `-- topiatest.xmi
|       `-- xmi2
|           `-- topiatest2.xmi
|-- generated-sources1
|   `-- models
|       |-- topiatest.objectmodel
|       `-- topiatest.properties
|-- generated-sources2
|   `-- models
|       |-- topiatest.objectmodel
|       `-- topiatest.properties
|-- generated-sources3
|   `-- models
|       `-- topiatest2.objectmodel
|-- generated-sources4
|   `-- models
|       `-- topiatest2.objectmodel
|-- generated-sources5
|   `-- models
|       `-- topiatest3.objectmodel
|-- generated-sources6
|   `-- models
|       `-- topiatest3.objectmodel
|-- generated-sources7
|   `-- models
|       |-- topiatest4.objectmodel
|       `-- topiatest4.properties
|-- generated-sources8
|   `-- models
|       |-- topiatest3.objectmodel
|       |-- xmi
|       |   |-- topiatest.objectmodel
|       |   `-- topiatest.properties
|       `-- xmi2
|           `-- topiatest2.objectmodel
|-- generated-sources9
|   `-- models
|       |-- topiatest3.objectmodel
|       |-- xmi
|       |   |-- topiatest.objectmodel
|       |   `-- topiatest.properties
|       `-- xmi2
|           `-- topiatest2.objectmodel
