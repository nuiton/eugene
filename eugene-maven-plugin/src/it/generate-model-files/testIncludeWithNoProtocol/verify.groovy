/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


// Zargo to object model

assert new File(basedir, 'target/generated-sources-zargo2ObjectModel').exists();
assert new File(basedir, 'target/generated-sources-zargo2ObjectModel/xmi').exists();
assert new File(basedir, 'target/generated-sources-zargo2ObjectModel/xmi/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-zargo2ObjectModel/xmi/topiatest.xmi').exists();

assert new File(basedir, 'target/generated-sources-zargo2ObjectModel/models').exists();
assert new File(basedir, 'target/generated-sources-zargo2ObjectModel/models/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-zargo2ObjectModel/models/topiatest.objectmodel').exists();

// Zargo to state model

assert new File(basedir, 'target/generated-sources-zargo2StateModel').exists();

assert new File(basedir, 'target/generated-sources-zargo2StateModel/xmi').exists();
assert new File(basedir, 'target/generated-sources-zargo2StateModel/xmi/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-zargo2StateModel/xmi/topiatest.xmi').exists();

assert new File(basedir, 'target/generated-sources-zargo2StateModel/models').exists();
assert new File(basedir, 'target/generated-sources-zargo2StateModel/models/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-zargo2StateModel/models/topiatest.statemodel').exists();

// Xmi to object model

assert new File(basedir, 'target/generated-sources-xmi2ObjectModel').exists();
assert new File(basedir, 'target/generated-sources-xmi2ObjectModel/models').exists();
assert new File(basedir, 'target/generated-sources-xmi2ObjectModel/models/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-xmi2ObjectModel/models/topiatest.objectmodel').exists();


// Xmi to state model

assert new File(basedir, 'target/generated-sources-xmi2StateModel').exists();
assert new File(basedir, 'target/generated-sources-xmi2StateModel/models').exists();
assert new File(basedir, 'target/generated-sources-xmi2StateModel/models/topiatest.properties').exists();
assert new File(basedir, 'target/generated-sources-xmi2StateModel/models/topiatest.statemodel').exists();

return true;

