On teste ici le goal generate-model-files avec en include que des includes sans protocol
des writers connus :

<execution>
    <phase>generate-sources</phase>
    <configuration>
        <modelType>objectmodel</modelType>
        <includes>
            <include>src/main/zargo:**/*.zargo</include>
        </includes>
        <fullPackagePath>org.nuiton.topia.test.entities</fullPackagePath>
    </configuration>
    <goals>
        <goal>generate-model</goal>
    </goals>
</execution>