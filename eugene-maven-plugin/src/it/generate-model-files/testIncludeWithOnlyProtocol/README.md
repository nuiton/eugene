On teste ici le goal generate-model-files avec en include que des includes simplifiés n'utilisant que les noms
des writers connus :

<execution>
    <phase>generate-sources</phase>
    <configuration>
        <modelType>objectmodel</modelType>
        <includes>
            <include>zargo</include>
        </includes>
        <fullPackagePath>org.nuiton.topia.test.entities</fullPackagePath>
    </configuration>
    <goals>
        <goal>generate-model</goal>
    </goals>
</execution>