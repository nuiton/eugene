/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.plugin.writer;

import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.util.DirectoryScanner;
import org.codehaus.plexus.util.IOUtil;
import org.nuiton.eugene.writer.ChainedFileWriter;
import org.nuiton.eugene.writer.ChainedFileWriterConfiguration;
import org.nuiton.eugene.writer.WriterReport;
import org.nuiton.plugin.PluginHelper;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * To write model files from zargo files.
 *
 * @author tchemit
 * @since 2.0.0
 */
@Component(role = ChainedFileWriter.class, hint = "zargo2xmi")
public class ZargoChainedFileWriter extends BaseChainedFileWriter {

    public static final String[] XMI_FILE_FILTER = new String[]{"*.xmi",
                                                                "**/*.xmi"};

    @Override
    public String getInputProtocol() {
        return "zargo";
    }

    @Override
    public String getOutputProtocol(String modelType) {
        // next writer to use is a xmi to model one
        return "xmi";
    }

    @Override
    public boolean acceptModel(String modelType) {
        // accept all models
        return acceptObjectModelOrStateModel(modelType);
    }

    @Override
    public boolean acceptInclude(String include) {
        return include.startsWith("zargo:") ||
               include.endsWith(".zargo") || include.endsWith(".zuml");
    }

    @Override
    public String getDefaultIncludes() {
        return "**/*.zargo";
    }

    @Override
    public String getDefaultInputDirectory() {
        return "src/main/xmi";
    }

    @Override
    public String getDefaultOutputDirectory() {
        return "xmi";
    }

    @Override
    public String getDefaultTestInputDirectory() {
        return "src/test/xmi";
    }

    @Override
    public String getDefaultTestOutputDirectory() {
        return "test-xmi";
    }

    @Override
    public void generate(ChainedFileWriterConfiguration configuration,
                         File outputDirectory,
                         Map<File, List<File>> filesByRoot,
                         Map<File, List<File>> resourcesByFile) throws IOException {

        for (Map.Entry<File, List<File>> entry : filesByRoot.entrySet()) {
            File inputDirectory = entry.getKey();
            List<File> files = entry.getValue();

            if (configuration.isVerbose()) {
                getLog().info("Expanding " + files.size() + " xmi file(s) from " + inputDirectory);
            }

            for (File file : files) {

                File mirrorDirectory = FileUtil.getRelativeFile(
                        inputDirectory,
                        outputDirectory,
                        file.getParentFile()
                );

                expandFile(file, mirrorDirectory, XMI_FILE_FILTER, configuration);

                // copy resources associated with the file
                copyResources(configuration,
                              outputDirectory,
                              inputDirectory,
                              file,
                              resourcesByFile
                );
            }
        }
    }

    public void expandFile(File src,
                           File dst,
                           String[] includes,
                           ChainedFileWriterConfiguration configuration) throws IOException {
        ZipFile zipFile = new ZipFile(src);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry nextElement = entries.nextElement();
            String name = nextElement.getName();
            for (String include : includes) {
                if (DirectoryScanner.match(include, name)) {
                    if (configuration.isVerbose()) {
                        getLog().info("matching name : " + name +
                                      " with pattern " + include);
                    }
                    File dstFile = new File(dst, name);
                    if (configuration.isOverwrite() ||
                        !dstFile.exists() ||
                        nextElement.getTime() > dstFile.lastModified()) {

                        if (configuration.isVerbose()) {
                            getLog().info("will expand : " + name + " to " + dstFile);
                        }

                        WriterReport writerReport = getWriterReport();
                        if (writerReport != null) {
                            writerReport.addFile(getClass().getName(), dstFile, false);
                        }

                        PluginHelper.createDirectoryIfNecessary(dst);
                        InputStream inputStream =
                                zipFile.getInputStream(nextElement);
                        try (FileOutputStream outStream = new FileOutputStream(dstFile)) {
                            IOUtil.copy(inputStream, outStream, 2048);
                        }
                    }

                }
            }
        }
    }
}
