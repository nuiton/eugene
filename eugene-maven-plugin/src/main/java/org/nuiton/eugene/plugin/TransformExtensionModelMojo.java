package org.nuiton.eugene.plugin;

/*-
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.eugene.models.extension.io.ModelExtensionFormat;
import org.nuiton.eugene.models.extension.io.ModelExtensionWriter;
import org.nuiton.eugene.models.extension.model.ModelExtension;
import org.nuiton.eugene.models.extension.model.ModelExtensionBuilder;
import org.nuiton.plugin.AbstractPlugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FilenameFilter;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * To a transform some object model extension files from an input format to an output format.
 *
 * Available formats are
 * <ul>
 * <li>ini</li>
 * <li>properties</li>
 * </ul>
 * <h3>Example of ini format</h3>
 * <pre>
 * [model]
 * modeTagValue=value
 * modelStereotype=true
 *
 * [package fr.ird.observe.entities]
 * packageTagValue=value
 * packageStereotype=true
 *
 * [class fr.ird.observe.entities.CommentableEntity]
 * classTagValue=value
 * classStereotype=true
 * attribute.attributeTagValue=value
 * attribute.attributeStereotype=true
 * </pre>
 * <h3>Example of properties format</h3>
 * <pre>
 * model.tagValue.modeTagValue=value
 * model.stereotype.modelStereotype
 * package.fr.ird.observe.entities.tagValue.packageTagValue=value
 * package.fr.ird.observe.entities.stereotype=packageStereotype
 * fr.ird.observe.entities.CommentableEntity.class.tagValue.classTagValue=value
 * fr.ird.observe.entities.CommentableEntity.class.stereotype=classStereotype
 * fr.ird.observe.entities.CommentableEntity.attribute.attribute.tagValue.attributeTagValue=value
 * fr.ird.observe.entities.CommentableEntity.attribute.attribute.stereotype=attributeStereotype*
 * </pre>
 * Created on 09/09/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
@Mojo(name = "transform-extension-model")
public class TransformExtensionModelMojo extends AbstractPlugin {

    /**
     * Name of model (the generated file name is {@code modelName.properties}).
     */
    @Parameter(property = "modelName", required = true)
    protected String modelName;

    /**
     * The input format to read extension model.
     */
    @Parameter(property = "inputFormat", required = true)
    protected String inputFormat;

    /**
     * The output format to write extension model.
     */
    @Parameter(property = "outputFormat", required = true)
    protected String outputFormat;

    /**
     * Display transformation result, but do not generate file.
     */
    @Parameter(property = "dryRun")
    protected boolean dryRun;

    /**
     * Verbose mode.
     */
    @Parameter(defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * Where to find and generate files.
     */
    @Parameter(property = "eugene.directory", defaultValue = "${project.basedir}/src/main/xmi", required = true)
    protected File directory;

    /**
     * Maven project.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    protected MavenProject project;

    /**
     * Encoding to be used for generation of files.
     *
     * <b>Note:</b> If nothing is filled here, we will use the system property {@code file.encoding}.
     */
    @Parameter(property = "eugene.encoding", defaultValue = "${project.build.sourceEncoding}")
    protected String encoding;

    private File[] inputFiles;
    private File outputFile;

    @Override
    protected void init() throws Exception {

        try {
            ModelExtensionFormat.valueOf(inputFormat);
        } catch (IllegalArgumentException e) {
            throw new MojoExecutionException("Unknown input format: " + inputFormat + ", must be one of " + Arrays.toString(ModelExtensionFormat.values()));
        }

        try {
            ModelExtensionFormat.valueOf(outputFormat);
        } catch (IllegalArgumentException e) {
            throw new MojoExecutionException("Unknown output format: " + outputFormat + ", must be one of " + Arrays.toString(ModelExtensionFormat.values()));
        }

        inputFiles = directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith("." + inputFormat);
            }
        });

        getLog().info("Found " + inputFiles.length + " model extension file(s).");

        outputFile = new File(directory, modelName + "." + outputFormat);

    }

    @Override
    protected void doAction() throws Exception {

        ModelExtensionBuilder modelExtensionBuilder = new ModelExtensionBuilder(false, modelName);

        for (File inputFile : inputFiles) {

            modelExtensionBuilder.addFile(inputFile);
        }

        ModelExtension modelExtension = modelExtensionBuilder.build();

        getLog().info(modelExtensionBuilder.getStereotypeHits() + " stereotype(s) detected.");
        getLog().info(modelExtensionBuilder.getTagValueHits() + " tag value(s) detected.");

        ModelExtensionWriter modelExtensionWriter = ModelExtensionWriter.newWriter(outputFile);

        if (dryRun) {

            String content;

            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, Charset.forName(encoding)))) {
                    modelExtensionWriter.write(modelExtension, writer);
                }
                content = out.toString(encoding);
            }

            getLog().info("\n\nDryRun mode\ncontent:\n\n" + content + "\n\n");

        } else {

            try (BufferedWriter writer = Files.newWriter(outputFile, Charset.forName(encoding))) {
                modelExtensionWriter.write(modelExtension, writer);
            }

        }

    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

}
