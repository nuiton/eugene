package org.nuiton.eugene.plugin;

/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.doxia.siterenderer.Renderer;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.AbstractMavenReport;
import org.apache.maven.reporting.MavenReportException;
import org.codehaus.plexus.i18n.I18N;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.plugin.renderer.TagValuesReportRenderer;

import java.io.File;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

/**
 * To report a {@link TagValueMetadatasProvider}.
 *
 * Created on 5/31/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.10
 */
@Mojo(name = "tag-values-report", requiresReports = true, requiresDependencyResolution = ResolutionScope.COMPILE)
public class TagValuesReport extends AbstractMavenReport {

    /**
     * Name of the tagValue provider.
     *
     * This is the role-hint given to the tag value provider to describe.
     */
    @Parameter(property = "eugene.providerName", defaultValue = "${project.artifactId}", required = true)
    protected String providerName;

    /**
     * Report output directory. Note that this parameter is only relevant if the goal is run from the command line or
     * from the default build lifecycle. If the goal is run indirectly as part of a site generation, the output
     * directory configured in the Maven Site Plugin is used instead.
     */
    @Parameter(property = "config.outputDirectory",
            defaultValue = "${project.reporting.outputDirectory}",
            required = true)
    protected File outputDirectory;

    /**
     * Report output encoding. Note that this parameter is only relevant if the goal is run from the command line or
     * from the default build lifecycle. If the goal is run indirectly as part of a site generation, the output
     * encoding configured in the Maven Site Plugin is used instead.
     */
    @Parameter(property = "config.outputEncoding",
            defaultValue = "${project.reporting.outputEncoding}",
            required = true)
    protected String outputEncoding;

    /**
     * Skip to generate the report.
     */
    @Parameter(property = "config.skip")
    protected boolean skip;

    /**
     * Location of javadoc report.
     */
    @Parameter(property = "eugene.javaDocDestDir", defaultValue = "apidocs")
    protected String javaDocDestDir;

    /**
     * Location of xref report.
     */
    @Parameter(property = "eugene.jxrDestDir", defaultValue = "xref")
    protected String jxrDestDir;

    /**
     * The Maven Project.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    protected MavenProject project;

    /**
     * Doxia Site Renderer component.
     */
    @Component
    protected Renderer siteRenderer;

    /**
     * Internationalization component.
     */
    @Component
    protected I18N i18n;

    @Component(role = TagValueMetadatasProvider.class)
    protected Map<String, TagValueMetadatasProvider> providers;


    @Override
    public String getDescription(Locale locale) {
        return i18n.getString(getOutputName(), locale, "report.description");
    }


    @Override
    public String getName(Locale locale) {
        return i18n.getString(getOutputName(), locale, "report.title");
    }

    @Override
    public String getCategoryName() {
        return CATEGORY_PROJECT_REPORTS;
    }

    @Override
    public boolean canGenerateReport() {
        return !skip;
    }

    @Override
    protected Renderer getSiteRenderer() {
        return siteRenderer;
    }

    @Override
    protected String getOutputDirectory() {
        return outputDirectory.getAbsolutePath();
    }

    @Override
    protected MavenProject getProject() {
        return project;
    }

    @Override
    protected void executeReport(Locale locale) throws MavenReportException {

        Collection<TagValueMetadatasProvider> providers = this.providers.values();

        TagValuesReportRenderer reportRenderer = new TagValuesReportRenderer(
                getSink(), i18n, locale, getOutputName(), javaDocDestDir, jxrDestDir, providers);
        reportRenderer.render();

    }

    @Override
    public String getOutputName() {
        return "tag-values-report";
    }

}
