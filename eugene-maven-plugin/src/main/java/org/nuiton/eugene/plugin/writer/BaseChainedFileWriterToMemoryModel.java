package org.nuiton.eugene.plugin.writer;

/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.ModelReader;
import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.models.extension.tagvalue.InvalidStereotypeSyntaxException;
import org.nuiton.eugene.models.extension.tagvalue.InvalidTagValueSyntaxException;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.extension.io.ModelExtensionReader;
import org.nuiton.eugene.writer.ChainedFileWriterConfiguration;
import org.nuiton.eugene.writer.ChainedFileWriterToMemoryModel;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.3
 */
public abstract class BaseChainedFileWriterToMemoryModel extends BaseChainedFileWriter implements ChainedFileWriterToMemoryModel {

    public static final String PROP_MODEL_READER = "modelReader";

    public static final String PROP_FAIL_IF_UNSAFE = "failIfUnsafe";

    public static final String PROP_READER = "reader";

    protected Model model;

    protected File lastModifiedFile;

    protected File outputDirectory;

    protected BaseChainedFileWriterToMemoryModel() {
        super(
                PROP_READER, "reader",
                PROP_MODEL_READER, "xmlModelReader",
                PROP_FAIL_IF_UNSAFE, "failIfUnsafe"
        );
    }

    @Override
    public Model getModel() {
        return model;
    }

    @Override
    public long getLastModifiedSource() {
        return lastModifiedFile == null ? 0 : lastModifiedFile.lastModified();
    }

    @Override
    public File getOutputDirectory() {
        return outputDirectory;
    }

    @Override
    public String getOutputProtocol(String modelType) {
        // nothing after java files
        //TODO check in engine that a memory chained file has null output protocol
        return null;
    }

    @Override
    public boolean acceptModel(String modelType) {
        // accept all models
        return acceptObjectModelOrStateModel(modelType);
    }

    protected abstract String getInputType();

    protected ModelReader<?> getModelReader() {
        return getProperty(PROP_MODEL_READER, ModelReader.class);
    }

    protected String getReader() {
        return getProperty(PROP_READER, String.class);
    }

    protected boolean isFailIfUnsafe() {
        Boolean property = getProperty(PROP_FAIL_IF_UNSAFE, Boolean.class);
        return property != null && property;
    }

    @Override
    protected void initWriter(ChainedFileWriterConfiguration configuration) {
        super.initWriter(configuration);

        // obtain a reader
        ClassLoader classLoader = configuration.getClassLoader();
        ClassLoader loader = classLoader;
        if (getModelReader() == null) {

            String modelType = configuration.getModelType();

            ModelReader<?> modelReader;

            if (getReader() != null) {

                // use a specific reader
                String reader = getReader();
                try {
                    ClassLoader fixedClassLoader = loader;
                    modelReader = (ModelReader<?>) Class.forName(reader, true,
                            fixedClassLoader).newInstance();
                    String modelTypeFromReader = modelReader.getModelType();
                    if (!modelType.equals(modelTypeFromReader)) {
                        throw new IllegalStateException("Model reader [" + modelTypeFromReader + "] does not match with modelType: " + modelType);
                    }
                } catch (IllegalStateException eee) {
                    throw eee;
                } catch (Exception eee) {
                    throw new IllegalStateException("could not obtain reader "
                            + reader, eee);
                }
            } else {

                String inputType = getInputType();

                modelReader = configuration.getModelHelper().getModelReader(modelType, inputType);
                if (modelReader == null) {
                    throw new IllegalStateException(
                            "could not find a model reader for modelType: " +
                                    modelType + ", and input type: " + inputType + ", availables readers : " +
                                    configuration.getModelHelper().getModelReaders().values()
                    );
                }

            }
            modelReader.setStrictLoading(isFailIfUnsafe());
            properties.put(PROP_MODEL_READER, modelReader);
        }

        boolean verbose = configuration.isVerbose();

        // set the verbose level of the model reader
        getModelReader().setVerbose(verbose);
    }

    @Override
    public void generate(ChainedFileWriterConfiguration configuration,
                         File outputDir,
                         Map<File, List<File>> filesByRoot,
                         Map<File, List<File>> resourcesByFile) throws IOException {

        Set<File> modelFiles = new HashSet<File>();
        for (List<File> files : filesByRoot.values()) {
            modelFiles.addAll(files);
            setLastModifiedSource(files);
        }

        for (List<File> files : resourcesByFile.values()) {
            setLastModifiedSource(files);
        }

        File[] filesToRead = modelFiles.toArray(new File[modelFiles.size()]);

        if (configuration.isVerbose()) {
            getLog().info("Will read " + filesToRead.length + " model(s).");
        }

        // read memory model from all files models
        model = getModelReader().read(filesToRead);

        if (model instanceof ObjectModel && configuration.getModelExtensionFile() != null) {

            ModelExtensionReader<ObjectModel> modelExtensionReader = new ModelExtensionReader<>(configuration.isVerbose(), isFailIfUnsafe(), (ObjectModel) model);

            try {
                modelExtensionReader.read(configuration.getModelExtensionFile());
            } catch (InvalidTagValueSyntaxException | InvalidStereotypeSyntaxException e) {
                //FIXME
                throw new IllegalStateException(e);
            }
        }

        if (configuration.isVerbose()) {

            getLog().info("Last modified file: " + lastModifiedFile + " - " + new Date(getLastModifiedSource()));

        }

        // set the ouput directory
        outputDirectory = outputDir;
    }

    protected void setLastModifiedSource(Iterable<File> files) {

        for (File file : files) {

            if (file.lastModified() > getLastModifiedSource()) {

                lastModifiedFile = file;

                if (getLog().isDebugEnabled()) {
                    getLog().debug("New lastModifiedFile date: " + lastModifiedFile + " : " + getLastModifiedSource());
                }
            }

        }

    }

}
