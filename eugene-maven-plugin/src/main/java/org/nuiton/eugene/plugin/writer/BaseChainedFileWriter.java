/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.plugin.writer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.codehaus.plexus.util.FileUtils;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.state.StateModel;
import org.nuiton.eugene.writer.AbstractChainedFileWriter;
import org.nuiton.eugene.writer.ChainedFileWriterConfiguration;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Surcharge de l'implentation abstraite pour avoir le logger de la console
 * maven.
 *
 * @author tchemit
 * @since 2.0.0
 */
public abstract class BaseChainedFileWriter extends AbstractChainedFileWriter {

    /** Logger */
    private Log log;

    protected BaseChainedFileWriter(String... propertyNameAndDescriptions) {
        super(propertyNameAndDescriptions);
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public Log getLog() {
        if (log == null) {
            log = new SystemStreamLog();
        }

        return log;
    }

    @Override
    protected void initWriter(ChainedFileWriterConfiguration configuration) {
        super.initWriter(configuration);

        if (!configuration.isVerbose()) {

            // nothing else to do
            return;
        }

        // log writer config
        StringBuilder buffer = new StringBuilder();
        Set<Map.Entry<String, String>> set =
                getAuthorizedPropertyDescriptions().entrySet();
        if (set.isEmpty()) {
            buffer.append("Writer [");
            buffer.append(getInputProtocol());
            buffer.append("]");
            buffer.append(" does not use any specific properties.");
        } else {
            buffer.append("Writer [");
            buffer.append(getInputProtocol());
            buffer.append("]");
            buffer.append(" use ");
            buffer.append(properties.size());
            buffer.append(" properties :");
            if (getLog().isInfoEnabled()) {
                for (Map.Entry<String, String> e : set) {

                    String key = e.getKey();
                    Object value = properties.get(key);
                    if (value != null) {
                        buffer.append("\n");
                        buffer.append("  [");
                        buffer.append(key);
                        buffer.append("] (");
                        buffer.append(e.getValue());
                        buffer.append(") : ");
                        buffer.append(value);
                    }
                }
            }
        }
        getLog().info(buffer.toString());
    }

    protected boolean acceptObjectModelOrStateModel(String modelType) {
        modelType = modelType.trim().toLowerCase();
        return ObjectModel.NAME.equals(modelType) ||
               StateModel.NAME.equals(modelType);
    }

    protected void copyResources(ChainedFileWriterConfiguration configuration,
                                 File outputDirectory,
                                 File inputDirectory,
                                 File file,
                                 Map<File, List<File>> resourcesByFile) throws IOException {

        List<File> resources = resourcesByFile.get(file);

        if (CollectionUtils.isEmpty(resources)) {

            // no resource associated to the xmi file
            if (configuration.isVerbose()) {
                getLog().info("No resources to copy for file " + file);
            }
            return;
        }

        if (configuration.isVerbose()) {
            getLog().info("Copy " + resources.size() + " resource file(s).");
        }

        boolean overwrite = configuration.isOverwrite();

//        String parentPath = inputDirectory.getAbsolutePath();
        for (File in : resources) {

//            String relativePath = in.getAbsolutePath().substring(parentPath.length());

            File out = FileUtil.getRelativeFile(inputDirectory, outputDirectory, in);
//            File out = new File(outputDirectory, relativePath);
//            File out = new File(outputDirectory, in.getName());

            if (configuration.isVerbose()) {
                getLog().info("Copy file " + in + " to " + out);
            }

            if (overwrite) {

                getWriterReport().addResource(getClass().getName(), out, configuration.isVerbose());
                FileUtils.copyFile(in, out);

            } else {

                if (out.lastModified() < in.lastModified()) {

                    getWriterReport().addResource(getClass().getName(), out, configuration.isVerbose());
                    FileUtils.copyFile(in, out);

                }

            }
        }
    }

}
