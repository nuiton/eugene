/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.plugin.writer;

import org.codehaus.plexus.component.annotations.Component;
import org.nuiton.eugene.writer.ChainedFileWriter;

/**
 * To create a memory model from xml model files.
 *
 * @author tchemit
 * @since 2.6.3
 */
@Component(role = ChainedFileWriter.class, hint = "xmlModel2Memory")
public class XmlModelChainedFileWriter extends BaseChainedFileWriterToMemoryModel {


    public XmlModelChainedFileWriter() {
        super();
    }

    @Override
    protected String getInputType() {
        return "xml";
    }

    @Override
    public String getInputProtocol() {
        return "model";
    }

    @Override
    public boolean acceptInclude(String include) {
        return include.startsWith("model:") ||
               include.endsWith(".objectmodel") ||
               include.endsWith(".statemodel");
    }

    @Override
    public String getDefaultIncludes() {
        return "**/*.*model";
    }

    @Override
    public String getDefaultInputDirectory() {
        return "src/main/models";
    }

    @Override
    public String getDefaultOutputDirectory() {
        return "java";
    }

    @Override
    public String getDefaultTestInputDirectory() {
        return "src/test/models";
    }

    @Override
    public String getDefaultTestOutputDirectory() {
        return "test-java";
    }
}
