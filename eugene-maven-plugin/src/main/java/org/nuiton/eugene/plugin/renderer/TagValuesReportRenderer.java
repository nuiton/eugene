package org.nuiton.eugene.plugin.renderer;

/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.sink.SinkEventAttributes;
import org.apache.maven.reporting.AbstractMavenReportRenderer;
import org.codehaus.plexus.i18n.I18N;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.ObjectModelParameter;

import java.util.Collection;
import java.util.Locale;
import java.util.Set;

/**
 * Created on 5/31/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.10
 */
public class TagValuesReportRenderer extends AbstractMavenReportRenderer {

    /**
     * Internationalization component.
     */
    protected final I18N i18n;

    /**
     * The locale we are rendering for.
     */
    protected final Locale locale;

    /**
     * The name of the bundle containing our I18n resources.
     */
    protected final String bundleName;

    /**
     * Data to describe.
     */
    protected final Collection<TagValueMetadatasProvider> data;

    protected final String javaDocDestDir;

    @Override
    public String getTitle() {
        return getText("report.title");
    }

    @Override
    public void renderBody() {

        sink.section1();
        sink.sectionTitle1();
        sink.text(getText("report.overview.title"));
        sink.sectionTitle1_();

        sink.paragraph();
        sink.link(getEugeneDocLink());
        sink.text(getText("report.overview.more.information"));
        sink.link_();
        sink.paragraph_();

        for (TagValueMetadatasProvider provider : data) {

            String implementation = provider.getClass().getName();

            sink.section2();
            sink.sectionTitle2();
            sink.text(provider.getDescription());
            sink.sectionTitle2_();

            sink.lineBreak();

            sink.paragraph();
            sink.bold();
            sink.text(implementation);
            sink.bold_();
            sink.text(" ");
            sink.link("./" + javaDocDestDir + "/" + implementation.replace('.', '/') + ".html");
            sink.text("javadoc");
            sink.link_();
            sink.link("./" + jxrDestDir + "/" + implementation.replace('.', '/') + ".html");
            sink.text(" ");
            sink.text("xref");
            sink.link_();
            sink.text(".");
            sink.paragraph_();

            sink.table();

            sink.tableRow();
            sinkHeaderCellText(getText("report.tagValue.name"));
            sinkHeaderCellText(getText("report.tagValue.description"));
            sink.tableRow_();

            for (TagValueMetadata entry : provider.getTagValues()) {

                String tagValueName = entry.getName();

                sink.tableRow();
                sinkCellLink(tagValueName, "./" + bundleName + ".html#detail_" + tagValueName);
                sinkCellText(entry.getDescription());
                sink.tableRow_();

            }

            sink.table_();

            sink.section2_();

        }

        sink.section1();
        sink.sectionTitle1();
        sink.text(getText("report.overview.detail"));
        sink.sectionTitle1_();

        for (TagValueMetadatasProvider provider : data) {

            if (CollectionUtils.isNotEmpty(provider.getTagValues())) {

                sink.section2();

                sink.sectionTitle2();
                sink.text(getText("report.detail.title") + provider.getDescription());
                sink.sectionTitle2_();

                for (TagValueMetadata detail : provider.getTagValues()) {

                    sink.section2();
                    sink.sectionTitle2();
                    sink.anchor("detail_" + detail.getName());
                    sink.text(getText("report.detail.tagValue.title") + "   " + detail.getName());
                    sink.anchor_();
                    sink.sectionTitle2_();

                    sink.lineBreak();

                    sink.table();

                    sink.tableRow();
                    sinkHeaderCellText(getText("report.detail.tagValue.name"));
                    sinkCellText(detail.getName());
                    sink.tableRow_();

                    sink.tableRow();
                    sinkHeaderCellText(getText("report.detail.tagValue.description"));
                    sinkCellText(detail.getDescription());
                    sink.tableRow_();

                    sink.tableRow();
                    sinkHeaderCellText(getText("report.detail.tagValue.defaultValue"));
                    sinkCellText(detail.getDefaultValue());
                    sink.tableRow_();

                    sink.tableRow();
                    sinkHeaderCellText(getText("report.detail.tagValue.matcherClass"));
                    sinkCellText(detail.getMatcherClass().getName());
                    sink.tableRow_();

                    sink.tableRow();
                    sinkHeaderCellText(getText("report.detail.tagValue.target"));
                    sink.tableCell();

                    renderTargets(detail.getTargets());

                    sink.tableCell_();
                    sink.tableRow_();

                    sink.table_();

                    sink.section3();
                    sink.sectionTitle3();
                    sink.text(getText("report.detail.tagValue.usage.title"));
                    sink.sectionTitle3_();

                    sink.text("//TODO Example of usage of this tag value");

                    sink.section3_();

                    sink.section2_();

                }

                sink.section2_();

            }

        }

        sink.section1_();

    }

    protected final String jxrDestDir;

    public TagValuesReportRenderer(Sink sink,
                                   I18N i18n,
                                   Locale locale,
                                   String bundleName,
                                   String javaDocDestDir,
                                   String jxrDestDir,
                                   Collection<TagValueMetadatasProvider> data) {
        super(sink);
        this.i18n = i18n;
        this.locale = locale;
        this.bundleName = bundleName;
        this.javaDocDestDir = javaDocDestDir;
        this.jxrDestDir = jxrDestDir;
        this.data = data;
        this.sink = sink;
    }


    protected String getEugeneDocLink() {

        String url;

        if (locale.getCountry().equals(Locale.FRENCH.getCountry())) {
            url = "http://eugene.nuiton.org/v/latest/fr/eugene/tagValues.html";
        } else {
            url = "http://eugene.nuiton.org/v/latest/eugene/tagValues.html";
        }

        return url;

    }

    /**
     * Gets the localized message for this report.
     *
     * @param key the message key.
     * @return the message.
     */
    protected String getText(String key) {
        return i18n.getString(bundleName, locale, key);
    }

    protected void sinkHeaderCellText(String text) {
        sink.tableHeaderCell();
        sink.text(text);
        sink.tableHeaderCell_();
    }

    protected void sinkHeaderCellText(SinkEventAttributes width, String text) {
        sink.tableHeaderCell(width);
        sink.text(text);
        sink.tableHeaderCell_();
    }

    protected void sinkCellText(SinkEventAttributes width, String text) {
        sink.tableCell(width);
        sink.text(text);
        sink.tableCell_();
    }

    protected void sinkCellText(String text) {
        sink.tableCell();
        sink.text(text);
        sink.tableCell_();
    }

    protected void sinkCellLink(String text, String url) {
        sink.tableCell();
        sink.link(url);
        sink.text(text);
        sink.link_();
        sink.tableCell_();
    }

    protected void renderTargets(Set<Class<?>> targets) {

        boolean moreThanOne = targets.size() > 1;

        if (moreThanOne) {

            sink.list_();

        }

        for (Class<?> target : targets) {

            if (moreThanOne) {

                sink.listItem();

            }

            renderDetailTarget(target);

            if (moreThanOne) {

                sink.listItem_();

            }

        }

        if (moreThanOne) {

            sink.list_();

        }

    }

    protected void renderDetailTarget(Class<?> target) {

        if (ObjectModel.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.model"));

        } else if (ObjectModelPackage.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.package"));

        } else if (ObjectModelClassifier.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.classifier"));

        } else if (ObjectModelClass.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.class"));

        } else if (ObjectModelInterface.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.interface"));

        } else if (ObjectModelEnumeration.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.enumeration"));

        } else if (ObjectModelAttribute.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.attribute"));

        } else if (ObjectModelParameter.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.parameter"));

        } else if (ObjectModelOperation.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.operation"));

        } else if (ObjectModelElement.class.isAssignableFrom(target)) {

            sink.text(getText("report.detail.target.element"));

        }

    }

}
