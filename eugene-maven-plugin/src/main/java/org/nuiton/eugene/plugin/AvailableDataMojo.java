/*
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.plugin;

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.eugene.ModelReader;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.models.Model;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.writer.ChainedFileWriter;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Obtain the list of some known data informations.
 *
 * Use the {@code dataTypes} property to specify a specific data type to use
 * (otherwise will display all known data types).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0.0
 */
@Mojo(name = "available-data",
        requiresDirectInvocation = true,
        requiresDependencyResolution = ResolutionScope.TEST)
public class AvailableDataMojo extends AbstractMojo {

    /**
     * Data type to display (let empty to see all datas).
     * Can specify more than one separated by comma.
     *
     * Available types are :
     * <pre>
     * modeltype,
     * modelreader,
     * modeltemplate,
     * writer,
     * stereotype,
     * tagvalue
     * </pre>
     *
     * <b>Note:</b> Let empty to display all data types.
     *
     * @since 2.0.0
     */
    @Parameter(property = "dataTypes")
    protected String dataTypes;

    /**
     * All available models (obtain by plexus, keys are plexus roles, values
     * are a instance of corresponding model).
     */
    @Component(role = Model.class)
    protected Map<String, Model> modelTypes;

    /** All available writers introspects via plexus. */
    @Component(role = ModelReader.class)
    protected Map<String, ModelReader<?>> modelReaders;

    /** All available templates introspects via plexus. */
    @Component(role = Template.class)
    protected Map<String, Template<?>> modelTemplates;

    /** All available writers introspects via plexus. */
    @Component(role = ChainedFileWriter.class)
    protected Map<String, ChainedFileWriter> writers;

    /**
     * All available tag value providers introspects via plexus.
     *
     * @since 2.9
     */
    @Component(role = TagValueMetadatasProvider.class)
    protected Map<String, TagValueMetadatasProvider> tagValueMetadatasProviders;

    protected TagValueMetadatasProvider currentTagValueMetadatasProvider;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        StringBuilder buffer = new StringBuilder();
        dataTypes = dataTypes == null ? "" : dataTypes.trim();

        Set<AvailableData> safeDataTypes;

        if (StringUtils.isEmpty(dataTypes)) {
            // treate all data types
            safeDataTypes = EnumSet.allOf(AvailableData.class);

            if (getLog().isDebugEnabled()) {
                getLog().debug("will use all data types : " + safeDataTypes);
            }

        } else {

            safeDataTypes = EnumSet.noneOf(AvailableData.class);
            for (String s : dataTypes.split(",")) {
                s = s.trim().toLowerCase();
                try {
                    AvailableData data = AvailableData.valueOf(s);
                    if (getLog().isDebugEnabled()) {
                        getLog().debug("will use data type " + data);
                    }
                    safeDataTypes.add(data);
                } catch (IllegalArgumentException e) {
                    getLog().warn(
                            "does not know data type : " + s + " use one of " + Arrays.toString(AvailableData.values())
                    );
                }
            }
        }

        for (AvailableData data : safeDataTypes) {
            buffer.append("\n");
            appendData(data, buffer);
        }

        getLog().info("Get datas for data types : " + safeDataTypes + buffer.toString());
    }

    protected void appendData(AvailableData data, StringBuilder buffer) {

        String dataType = data.name();

        if (data == AvailableData.tagvalue) {

            int nbData = 0;
            for (TagValueMetadatasProvider provider : tagValueMetadatasProviders.values()) {
                currentTagValueMetadatasProvider = provider;
                nbData += data.getData(this).size();
            }
            currentTagValueMetadatasProvider = null;

            String format = "\nFound %s %ss in %s provider(s) : %s\n";

            buffer.append(String.format(format,
                                        nbData,
                                        dataType,
                                        tagValueMetadatasProviders.size(),
                                        tagValueMetadatasProviders.keySet())
            );

            for (Map.Entry<String, TagValueMetadatasProvider> e : tagValueMetadatasProviders.entrySet()) {
                String providerName = e.getKey();
                currentTagValueMetadatasProvider = e.getValue();
                Map<String, ?> map = data.getData(this);

                int size = map.size();
                buffer.append("\nProvider [").append(providerName).append("] - ");
                if (size == 0) {
                    buffer.append("No available ").append(dataType).append(".");
                } else if (size == 1) {
                    buffer.append("Found one ").append(dataType).append(" : ");
                } else {
                    buffer.append("Found ");
                    buffer.append(size);
                    buffer.append(" ");
                    buffer.append(dataType);
                    buffer.append("s : ");
                }
                for (Map.Entry<String, ?> e2 : map.entrySet()) {
                    data.toString(buffer, e2);
                }
            }
        } else {
            Map<String, ?> map = data.getData(this);

            int size = map == null ? 0 : map.size();
            if (size == 0) {
                buffer.append("\nNo available ").append(dataType).append(".");
            } else {
                if (size == 1) {
                    buffer.append("\nFound one ").append(dataType).append(" : ");
                } else {
                    buffer.append("\nFound ");
                    buffer.append(size);
                    buffer.append(" ");
                    buffer.append(dataType);
                    buffer.append("s : ");
                }
                for (Map.Entry<String, ?> e : map.entrySet()) {
                    data.toString(buffer, e);
                }
            }
        }
    }

    enum AvailableData {
        modeltype {
            @Override
            public Map<String, ?> getData(AvailableDataMojo mojo) {
                return mojo.modelTypes;
            }
        },
        writer {
            @Override
            public Map<String, ?> getData(AvailableDataMojo mojo) {
                return mojo.writers;
            }

            @Override
            String toString(Object data) {
                ChainedFileWriter w = (ChainedFileWriter) data;
                String b = super.toString(data) + "\n" + "  inputProtocol             : " +
                        w.getInputProtocol() +
                        "\n" + "  outputProtocol            : " +
                        w.getOutputProtocol(ObjectModel.NAME) +
                        "\n" + "  defaultIncludes           : " +
                        w.getDefaultIncludes() +
                        "\n" + "  defaultInputDirectory     : " +
                        w.getDefaultInputDirectory() +
                        "\n" + "  defaultTestInputDirectory : " +
                        w.getDefaultTestInputDirectory();
                return b;
            }
        },
        modelreader {
            @Override
            public Map<String, ?> getData(AvailableDataMojo mojo) {
                return mojo.modelReaders;
            }
        },
        modeltemplate {
            @Override
            public Map<String, ?> getData(AvailableDataMojo mojo) {
                return mojo.modelTemplates;
            }
        },
        tagvalue {
            @Override
            public Map<String, ?> getData(AvailableDataMojo mojo) {
                Set<TagValueMetadata> tagValues = mojo.currentTagValueMetadatasProvider.getTagValues();
                return Maps.uniqueIndex(tagValues, new Function<TagValueMetadata, String>() {
                    @Override
                    public String apply(TagValueMetadata input) {
                        return input.getName();
                    }
                });
            }

            @Override
            String toString(Object data) {
                TagValueMetadata d = (TagValueMetadata) data;
                StringBuilder sb = new StringBuilder();
                Set<Class<?>> targets = d.getTargets();
                for (Class<?> aClass : targets) {
                    sb.append(", ").append(aClass.getSimpleName());
                }
                String result = sb.toString();
                if (targets.size() > 0) {
                    result = result.substring(2);
                }
                return result;
            }

            @Override
            void toString(StringBuilder buffer, Map.Entry<String, ?> e) {
                String name = e.getKey();
                Object value = e.getValue();
                buffer.append("\n [");
                buffer.append(name);
                buffer.append("] targets : '");
                buffer.append(toString(value));
                buffer.append("\' : ");
                buffer.append(t(((TagValueMetadata) value).getDescription()));
            }
        };

        abstract Map<String, ?> getData(AvailableDataMojo mojo);

        String toString(Object data) {
            return data.getClass().getName();
        }

        void toString(StringBuilder buffer, Map.Entry<String, ?> e) {
            String name = e.getKey();
            Object value = e.getValue();
            buffer.append("\n [");
            buffer.append(name);
            buffer.append("] with implementation '");
            buffer.append(toString(value));
            buffer.append('\'');
        }
    }

}
