<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  EUGene :: Maven plugin

  $HeadURL$
  %%
  Copyright (C) 2006 - 2010 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.

  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.nuiton</groupId>
    <artifactId>eugene</artifactId>
    <version>3.1-SNAPSHOT</version>
  </parent>

  <groupId>org.nuiton.eugene</groupId>
  <artifactId>eugene-maven-plugin</artifactId>
  <packaging>maven-plugin</packaging>

  <name>EUGene :: Maven plugin</name>
  <description>Maven plugin to use the eugene library</description>
  <inceptionYear>2006</inceptionYear>

  <properties>

    <!-- Post Release configuration -->
    <skipPostRelease>false</skipPostRelease>

  </properties>

  <dependencies>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>eugene</artifactId>
      <version>${project.version}</version>
      <scope>compile</scope>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>eugene-java-templates</artifactId>
      <version>${project.version}</version>
      <scope>runtime</scope>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>eugene-plantuml-templates</artifactId>
      <version>${project.version}</version>
      <scope>runtime</scope>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>eugene-yaml-templates</artifactId>
      <version>${project.version}</version>
      <scope>runtime</scope>
    </dependency>

    <dependency>
      <groupId>com.tunnelvisionlabs</groupId>
      <artifactId>antlr4-runtime</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-utils</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton.i18n</groupId>
      <artifactId>nuiton-i18n</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-collections4</artifactId>
    </dependency>

    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
    </dependency>

    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-plugin-api</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-core</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-model</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-artifact</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven</groupId>
      <artifactId>maven-settings</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.plugin-tools</groupId>
      <artifactId>maven-plugin-annotations</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.reporting</groupId>
      <artifactId>maven-reporting-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.maven.reporting</groupId>
      <artifactId>maven-reporting-impl</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.doxia</groupId>
      <artifactId>doxia-sink-api</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.doxia</groupId>
      <artifactId>doxia-site-renderer</artifactId>
    </dependency>

    <dependency>
      <groupId>xml-apis</groupId>
      <artifactId>xml-apis</artifactId>
    </dependency>

    <dependency>
      <groupId>org.codehaus.plexus</groupId>
      <artifactId>plexus-i18n</artifactId>
    </dependency>

    <dependency>
      <groupId>org.codehaus.plexus</groupId>
      <artifactId>plexus-utils</artifactId>
    </dependency>

    <dependency>
      <groupId>org.codehaus.plexus</groupId>
      <artifactId>plexus-component-annotations</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>helper-maven-plugin-api</artifactId>
    </dependency>

    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <scope>runtime</scope>
    </dependency>

  </dependencies>

  <build>
    <pluginManagement>
      <plugins>

        <plugin>
          <artifactId>maven-plugin-plugin</artifactId>
          <configuration>
            <skipErrorNoDescriptorsFound>true</skipErrorNoDescriptorsFound>
          </configuration>
          <executions>
            <execution>
              <goals>
                <goal>helpmojo</goal>
                <goal>descriptor</goal>
              </goals>
            </execution>
          </executions>
        </plugin>

      </plugins>
    </pluginManagement>
    <plugins>

      <!-- expose new plexus components -->
      <plugin>
        <groupId>org.codehaus.plexus</groupId>
        <artifactId>plexus-component-metadata</artifactId>
        <executions>
          <execution>
            <goals>
              <goal>generate-metadata</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>

  <profiles>

    <!-- perform only on a release stage when using the maven-release-plugin -->
    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <reporting>
        <plugins>
          <!--Site report's plugin-->
          <plugin>
            <artifactId>maven-plugin-plugin</artifactId>
            <version>${pluginPluginVersion}</version>
          </plugin>

          <plugin>
            <artifactId>maven-invoker-plugin</artifactId>
            <version>${invokerPluginVersion}</version>
          </plugin>

          <plugin>
            <groupId>org.codehaus.plexus</groupId>
            <artifactId>plexus-maven-plugin</artifactId>
            <version>${plexusPluginVersion}</version>
          </plugin>
        </plugins>
      </reporting>

    </profile>

    <profile>
      <id>run-its</id>
      <activation>
        <property>
          <name>maven.test.skip</name>
          <value>!true</value>
        </property>
      </activation>
      <build>
        <defaultGoal>integration-test</defaultGoal>
        <plugins>
          <plugin>
            <artifactId>maven-invoker-plugin</artifactId>
            <configuration>
              <pomIncludes>
                <pomInclude>evol-308/pom.xml</pomInclude>
                <pomInclude>evol-879/zargo/pom.xml</pomInclude>
                <pomInclude>evol-879/xmi/pom.xml</pomInclude>
                <pomInclude>generate/only-zargo/pom.xml</pomInclude>
                <pomInclude>generate/only-zargo-xmi/pom.xml</pomInclude>
                <pomInclude>generate/all/pom.xml</pomInclude>
                <!-- This test should be removed : too much complex to test,... nothing :( -->
                <!--<pomInclude>generate/generators/pom.xml</pomInclude>-->
                <!--this test used topia and must be move in topia not here-->
                <!--<pomInclude>evol-879/model/pom.xml</pomInclude>-->
              </pomIncludes>
              <postBuildHookScript>verify</postBuildHookScript>
              <localRepositoryPath>${basedir}/target/local-repo
              </localRepositoryPath>
              <settingsFile>src/it/settings.xml</settingsFile>
              <cloneProjectsTo>${project.build.directory}/its</cloneProjectsTo>
              <debug>${maven.verbose}</debug>
            </configuration>
            <executions>
              <execution>
                <id>integration-test</id>
                <goals>
                  <goal>install</goal>
                  <goal>run</goal>
                </goals>
                <phase>integration-test</phase>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>
