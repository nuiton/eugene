To deploy new version of pom: mvn clean deploy -DperformRelease
To install localy: mvn clean install

Release
-------

# start release
mvn jgitflow:release-start

# do checks before ending release
mvn clean verify -DperformRelease

# do a git commit if some files were modified
git commit -am"Release ..."

# finish release
mvn jgitflow:release-finish

# update eugene-plugin version (for tag values report generation)

change on **master** the eugene maven plugin version to the just released (in **eugene-java-templates/pom.xml** profile **reporting**).

```git push```

cherry pick this push to **develop**.


For more information see 
https://forge.nuiton.org/projects/pom/repository/revisions/master/entry/nuitonpom/README.txt

