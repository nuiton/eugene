.. -
.. * #%L
.. * EUGene :: PlantUML templates
.. * %%
.. * Copyright (C) 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========================
EUGene PlantUML templates
=========================

.. contents::

Présentation
------------
Le plugin *eugene-plantuml-templates* est un template permettant de générer un diagramme de classes `PlantUML`_ à partir d'un `ObjectModel`_ au moment de l'installation d'un projet.
`PlantUML`_ utilise Graphviz lors de la génération d'images, si vous ne pouvez pas installer `Graphviz`_, le `PlantUML Server`_ permet de générer des diagrammes en ligne.

Démonstration
-----------------------------
Voici un exemple d'utilisation du plugin *eugene-plantuml-templates* avec le projet `Pollen`_.


Ajouter le template dans le pom.xml
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Après avoir récupéré les sources du projet `Pollen`_, on ajoute le template PlantUML au fichier pollen-persistence/pom.xml (sans oublier la virgule pour séparer les templates)::

  <templates>
    org.nuiton.topia.generator.TopiaMetaTransformer,
    org.nuiton.topia.generator.BinderHelperTransformer,
    org.nuiton.eugene.plantuml.PlantumlTemplatesGenerator
  </templates>

Installation du projet
~~~~~~~~~~~~~~~~~~~~~~
On lance ensuite l'installation avec la commande::

  pollen$ mvn install

La ligne suivante devrait apparaitre lors de l'installation::

  [INFO] Apply generator PlantumlTemplatesGenerator"

Résultat
~~~~~~~~
Le diagramme de classe se trouve dans le répertoire pollen-persistence/target/classes (ou pollen-persistence/target/generated-sources/java):
Pollen.plantuml est la représentation textuelle du modèle en `PlantUML`_
Pollen.png est l'image générée par `PlantUML`_ à partir du fichier Pollen.plantuml
Pour générer le diagramme à partir du `PlantUML Server`_, il suffit de copier le contenu du fichier Pollen.plantuml dans le formulaire du site et d'envoyer.

Voici à quoi devrait ressembler le diagramme:

.. image:: ../images/Pollen.png

Légende
~~~~~~~
Visibilité:

.. image:: ../images/legend1.png

Abstract & Static:

.. image:: ../images/legend2.png

Relations:

.. image:: ../images/legend3.png

.. _PlantUML : http://plantuml.sourceforge.net/index.html
.. _ObjectModel : http://doc.nuiton.org/eugene/02-objectmodel.html
.. _Graphviz : http://www.graphviz.org/
.. _PlantUML Server : http://plantuml.com/plantuml/
.. _Pollen : http://doc.chorem.org/pollen/
